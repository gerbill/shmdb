cmake_minimum_required(VERSION 3.5)
project(shm C)

set(CMAKE_C_STANDARD 99)

include_directories(src)
include_directories(ext/shm_cli)
include_directories(ext/shm_cli/adapters)
include_directories(ext/liblzf)

add_definitions(-D__STDC_WANT_LIB_EXT2__=1)

add_executable(shm-server
        src/list.c
        src/el.c
        src/snet.c
        src/appendonly.c
        src/config.c
        src/db.c
        src/debug.c
        src/hash.c
        src/is.c
        ext/liblzf/lzf_c.c
        ext/liblzf/lzf_d.c
        src/begin.c
        src/netutils.c
        src/object.c
        src/psort.c
        src/msg.c
        src/release.c
        src/repl.c
        src/sdb.c
        src/dstr.c
        src/sha1.c
        src/shm.c
        src/simplerl.c
        src/slog.c
        src/sort.c
        src/syncio.c
        src/hashtype.c
        src/listtype.c
        src/settype.c
        src/stringtype.c
        src/sortsettype.c
        src/util.c
        src/vm.c
        src/sortlist.c
        src/zipmap.c
        src/dbmalloc.c
)

add_executable(shm-cli
        ext/shm_cli/net.c
        ext/shm_cli/shm_cli.c
#        ext/shm_cli/dstr.c
        ext/shm_cli/async.c
        src/snet.c
        src/dstr.c
        src/list.c
        src/shm-cli.c
        src/dbmalloc.c
        src/release.c
        src/simplerl.c
        )

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -ggdb -rdynamic -std=c99 -pedantic -W -g")

target_link_libraries(shm-server Threads::Threads)
target_link_libraries(shm-server m)

target_link_libraries(shm-cli Threads::Threads)
target_link_libraries(shm-cli m)

install(TARGETS shm-server shm-cli RUNTIME DESTINATION bin)
