#include "fmacros.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <netdb.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include "net.h"
#include "dstr.h"

void __shmSetError(shmContext *c, int type, dstr err);

static int shmCreateSocket(shmContext *c, int type) {
				int s, on = 1;
				if ((s = socket(type, SOCK_STREAM, 0)) == -1) {
								__shmSetError(c, SHM_ERR_IO, NULL);
								return SHM_ERR;
				}
				if (type == AF_INET) {
								if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) == -1) {
												__shmSetError(c, SHM_ERR_IO, NULL);
												close(s);
												return SHM_ERR;
								}
				}
				return s;
}

static int shmSetNonBlock(shmContext *c, int fd) {
				int flags;
				if ((flags = fcntl(fd, F_GETFL)) == -1) {
								__shmSetError(
												c, SHM_ERR_IO,
												dstrcatprintf(dstrempty(), "fcntl(F_GETFL): %s", strerror(errno)));
								close(fd);
								return SHM_ERR;
				}
				if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1) {
								__shmSetError(c, SHM_ERR_IO,
								              dstrcatprintf(dstrempty(), "fcntl(F_SETFL,O_NONBLOCK): %s",
								                           strerror(errno)));
								close(fd);
								return SHM_ERR;
				}
				return SHM_OK;
}

static int shmSetTcpNoDelay(shmContext *c, int fd) {
				int yes = 1;
				if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &yes, sizeof(yes)) == -1) {
								__shmSetError(c, SHM_ERR_IO,
								              dstrcatprintf(dstrempty(), "setsockopt(TCP_NODELAY): %s",
								                           strerror(errno)));
								return SHM_ERR;
				}
				return SHM_OK;
}

int shmContextConnectTcp(shmContext *c, const char *addr, int port) {
				int s;
				int blocking = (c->flags & SHM_BLOCK);
				struct sockaddr_in sa;
				if ((s = shmCreateSocket(c, AF_INET)) == SHM_ERR) return SHM_ERR;
				if (!blocking && shmSetNonBlock(c, s) == SHM_ERR) return SHM_ERR;
				sa.sin_family = AF_INET;
				sa.sin_port = htons(port);
				if (inet_aton(addr, &sa.sin_addr) == 0) {
								struct hostent *he;
								he = gethostbyname(addr);
								if (he == NULL) {
												__shmSetError(c, SHM_ERR_OTHER,
												              dstrcatprintf(dstrempty(), "Can't resolve: %s", addr));
												close(s);
												return SHM_ERR;
								}
								memcpy(&sa.sin_addr, he->h_addr, sizeof(struct in_addr));
				}
				if (connect(s, (struct sockaddr *)&sa, sizeof(sa)) == -1) {
								if (errno == EINPROGRESS && !blocking) {
								} else {
												__shmSetError(c, SHM_ERR_IO, NULL);
												close(s);
												return SHM_ERR;
								}
				}
				if (shmSetTcpNoDelay(c, s) != SHM_OK) {
								close(s);
								return SHM_ERR;
				}
				c->fd = s;
				c->flags |= SHM_CONNECTED;
				return SHM_OK;
}

int shmContextConnectUnix(shmContext *c, const char *path) {
				int s;
				int blocking = (c->flags & SHM_BLOCK);
				struct sockaddr_un sa;
				if ((s = shmCreateSocket(c, AF_LOCAL)) == SHM_ERR) return SHM_ERR;
				if (!blocking && shmSetNonBlock(c, s) != SHM_OK) return SHM_ERR;
				sa.sun_family = AF_LOCAL;
				strncpy(sa.sun_path, path, sizeof(sa.sun_path) - 1);
				if (connect(s, (struct sockaddr *)&sa, sizeof(sa)) == -1) {
								if (errno == EINPROGRESS && !blocking) {
								} else {
												__shmSetError(c, SHM_ERR_IO, NULL);
												close(s);
												return SHM_ERR;
								}
				}
				c->fd = s;
				c->flags |= SHM_CONNECTED;
				return SHM_OK;
}
