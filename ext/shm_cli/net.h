#ifndef __NET_H
#define __NET_H
#include "shm_cli.h"
#if defined(__sun)
#define AF_LOCAL AF_UNIX
#endif

int shmContextConnectTcp(shmContext *c, const char *addr, int port);

int shmContextConnectUnix(shmContext *c, const char *path);

#endif
