#ifndef __DSTR_H
#define __DSTR_H
#include <sys/types.h>
#include <stdarg.h>
typedef char *dstr;
struct dstrhdr {
				int len;
				int free;
				char buf[];
};

dstr dstrnewlen(const void *init, size_t initlen);

dstr dstrnew(const char *init);

dstr dstrempty();

size_t dstrlen(const dstr s);

dstr dstrdup(const dstr s);

void dstrfree(dstr s);

size_t dstravail(dstr s);

dstr dstrcatlen(dstr s, const void *t, size_t len);

dstr dstrcat(dstr s, const char *t);

dstr dstrcpylen(dstr s, char *t, size_t len);

dstr dstrcpy(dstr s, char *t);

dstr dstrcatvprintf(dstr s, const char *fmt, va_list ap);

#ifdef __GNUC__

dstr dstrcatprintf(dstr s, const char *fmt, ...)
__attribute__((format(printf, 2, 3)));

#else

dstr dstrcatprintf(dstr s, const char *fmt, ...);

#endif

dstr dstrtrim(dstr s, const char *cset);

dstr dstrrange(dstr s, int start, int end);

void dstrupdatelen(dstr s);

int dstrcmp(dstr s1, dstr s2);

dstr *dstrsplitlen(char *s, int len, char *sep, int seplen, int *count);

void dstrfreesplitres(dstr *tokens, int count);

void dstrtolower(dstr s);

void dstrtoupper(dstr s);

dstr dstrfromlonglong(long long value);

dstr dstrcatrepr(dstr s, char *p, size_t len);

dstr *dstrsplitargs(char *line, int *argc);

#endif
