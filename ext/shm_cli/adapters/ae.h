#include <sys/types.h>
#include <el.h>
#include "../shm_cli.h"
#include "../async.h"
typedef struct shmAeEvents {
				shmAsyncContext *context;
				elLoop *loop;
				int fd;
				int reading, writing;
} shmAeEvents;

void shmAeReadEvent(elLoop *el, int fd, void *privdata, int mask) {
				((void)el);
				((void)fd);
				((void)mask);
				shmAeEvents *e = (shmAeEvents *)privdata;
				shmAsyncHandleRead(e->context);
}

void shmAeWriteEvent(elLoop *el, int fd, void *privdata, int mask) {
				((void)el);
				((void)fd);
				((void)mask);
				shmAeEvents *e = (shmAeEvents *)privdata;
				shmAsyncHandleWrite(e->context);
}

void shmAeAddRead(void *privdata) {
				shmAeEvents *e = (shmAeEvents *)privdata;
				elLoop *loop = e->loop;
				if (!e->reading) {
								e->reading = 1;
								elCreateFileEvent(loop, e->fd, EL_READABLE, shmAeReadEvent, e);
				}
}

void shmAeDelRead(void *privdata) {
				shmAeEvents *e = (shmAeEvents *)privdata;
				elLoop *loop = e->loop;
				if (e->reading) {
								e->reading = 0;
								elDeleteFileEvent(loop, e->fd, EL_READABLE);
				}
}

void shmAeAddWrite(void *privdata) {
				shmAeEvents *e = (shmAeEvents *)privdata;
				elLoop *loop = e->loop;
				if (!e->writing) {
								e->writing = 1;
								elCreateFileEvent(loop, e->fd, EL_WRITABLE, shmAeWriteEvent, e);
				}
}

void shmAeDelWrite(void *privdata) {
				shmAeEvents *e = (shmAeEvents *)privdata;
				elLoop *loop = e->loop;
				if (e->writing) {
								e->writing = 0;
								elDeleteFileEvent(loop, e->fd, EL_WRITABLE);
				}
}

void shmAeCleanup(void *privdata) {
				shmAeEvents *e = (shmAeEvents *)privdata;
				shmAeDelRead(privdata);
				shmAeDelWrite(privdata);
				free(e);
}

int shmAeAttach(elLoop *loop, shmAsyncContext *ac) {
				shmContext *c = &(ac->c);
				shmAeEvents *e;
				if (ac->_adapter_data != NULL) return SHM_ERR;
				e = (shmAeEvents *)malloc(sizeof(*e));
				e->context = ac;
				e->loop = loop;
				e->fd = c->fd;
				e->reading = e->writing = 0;
				ac->evAddRead = shmAeAddRead;
				ac->evDelRead = shmAeDelRead;
				ac->evAddWrite = shmAeAddWrite;
				ac->evDelWrite = shmAeDelWrite;
				ac->evCleanup = shmAeCleanup;
				ac->_adapter_data = e;
				return SHM_OK;
}
