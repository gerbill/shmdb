#include <sys/types.h>
#include <event.h>
#include "../shm_cli.h"
#include "../async.h"
typedef struct shmLibeventEvents {
				shmAsyncContext *context;
				struct event rev, wev;
} shmLibeventEvents;

void shmLibeventReadEvent(int fd, short event, void *arg) {
				((void)fd);
				((void)event);
				shmLibeventEvents *e = (shmLibeventEvents *)arg;
				shmAsyncHandleRead(e->context);
}

void shmLibeventWriteEvent(int fd, short event, void *arg) {
				((void)fd);
				((void)event);
				shmLibeventEvents *e = (shmLibeventEvents *)arg;
				shmAsyncHandleWrite(e->context);
}

void shmLibeventAddRead(void *privdata) {
				shmLibeventEvents *e = (shmLibeventEvents *)privdata;
				event_add(&e->rev, NULL);
}

void shmLibeventDelRead(void *privdata) {
				shmLibeventEvents *e = (shmLibeventEvents *)privdata;
				event_del(&e->rev);
}

void shmLibeventAddWrite(void *privdata) {
				shmLibeventEvents *e = (shmLibeventEvents *)privdata;
				event_add(&e->wev, NULL);
}

void shmLibeventDelWrite(void *privdata) {
				shmLibeventEvents *e = (shmLibeventEvents *)privdata;
				event_del(&e->wev);
}

void shmLibeventCleanup(void *privdata) {
				shmLibeventEvents *e = (shmLibeventEvents *)privdata;
				event_del(&e->rev);
				event_del(&e->wev);
				free(e);
}

int shmLibeventAttach(shmAsyncContext *ac, struct event_base *base) {
				shmContext *c = &(ac->c);
				shmLibeventEvents *e;
				if (ac->_adapter_data != NULL) return SHM_ERR;
				e = (shmLibeventEvents *)malloc(sizeof(*e));
				e->context = ac;
				ac->evAddRead = shmLibeventAddRead;
				ac->evDelRead = shmLibeventDelRead;
				ac->evAddWrite = shmLibeventAddWrite;
				ac->evDelWrite = shmLibeventDelWrite;
				ac->evCleanup = shmLibeventCleanup;
				ac->_adapter_data = e;
				evensettype(&e->rev, c->fd, EV_READ, shmLibeventReadEvent, e);
				evensettype(&e->wev, c->fd, EV_WRITE, shmLibeventWriteEvent, e);
				event_base_set(base, &e->rev);
				event_base_set(base, &e->wev);
				return SHM_OK;
}
