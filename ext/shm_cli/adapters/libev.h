#include <sys/types.h>
#include <ev.h>
#include "../shm_cli.h"
#include "../async.h"
typedef struct shmLibevEvents {
				shmAsyncContext *context;
				struct ev_loop *loop;
				int reading, writing;
				ev_io rev, wev;
} shmLibevEvents;

void shmLibevReadEvent(EV_P_ ev_io *watcher, int revents) {
#if EV_MULTIPLICITY
				((void)loop);
#endif
				((void)revents);
				shmLibevEvents *e = (shmLibevEvents *)watcher->data;
				shmAsyncHandleRead(e->context);
}

void shmLibevWriteEvent(EV_P_ ev_io *watcher, int revents) {
#if EV_MULTIPLICITY
				((void)loop);
#endif
				((void)revents);
				shmLibevEvents *e = (shmLibevEvents *)watcher->data;
				shmAsyncHandleWrite(e->context);
}

void shmLibevAddRead(void *privdata) {
				shmLibevEvents *e = (shmLibevEvents *)privdata;
				struct ev_loop *loop = e->loop;
				((void)loop);
				if (!e->reading) {
								e->reading = 1;
								ev_io_start(EV_A_ & e->rev);
				}
}

void shmLibevDelRead(void *privdata) {
				shmLibevEvents *e = (shmLibevEvents *)privdata;
				struct ev_loop *loop = e->loop;
				((void)loop);
				if (e->reading) {
								e->reading = 0;
								ev_io_stop(EV_A_ & e->rev);
				}
}

void shmLibevAddWrite(void *privdata) {
				shmLibevEvents *e = (shmLibevEvents *)privdata;
				struct ev_loop *loop = e->loop;
				((void)loop);
				if (!e->writing) {
								e->writing = 1;
								ev_io_start(EV_A_ & e->wev);
				}
}

void shmLibevDelWrite(void *privdata) {
				shmLibevEvents *e = (shmLibevEvents *)privdata;
				struct ev_loop *loop = e->loop;
				((void)loop);
				if (e->writing) {
								e->writing = 0;
								ev_io_stop(EV_A_ & e->wev);
				}
}

void shmLibevCleanup(void *privdata) {
				shmLibevEvents *e = (shmLibevEvents *)privdata;
				shmLibevDelRead(privdata);
				shmLibevDelWrite(privdata);
				free(e);
}

int shmLibevAttach(EV_P_ shmAsyncContext *ac) {
				shmContext *c = &(ac->c);
				shmLibevEvents *e;
				if (ac->_adapter_data != NULL) return SHM_ERR;
				e = (shmLibevEvents *)malloc(sizeof(*e));
				e->context = ac;
#if EV_MULTIPLICITY
				e->loop = loop;
#else
				e->loop = NULL;
#endif
				e->reading = e->writing = 0;
				e->rev.data = e;
				e->wev.data = e;
				ac->evAddRead = shmLibevAddRead;
				ac->evDelRead = shmLibevDelRead;
				ac->evAddWrite = shmLibevAddWrite;
				ac->evDelWrite = shmLibevDelWrite;
				ac->evCleanup = shmLibevCleanup;
				ac->_adapter_data = e;
				ev_io_init(&e->rev, shmLibevReadEvent, c->fd, EV_READ);
				ev_io_init(&e->wev, shmLibevWriteEvent, c->fd, EV_WRITE);
				return SHM_OK;
}
