#define DSTR_ABORT_ON_OOM
#include "dstr.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

static void dstrOomAbort(void) {
				fprintf(stderr, "DSTR: Out Of Memory (DSTR_ABORT_ON_OOM defined)\n");
				abort();
}

dstr dstrnewlen(const void *init, size_t initlen) {
				struct dstrhdr *sh;
				sh = malloc(sizeof(struct dstrhdr) + initlen + 1);
#ifdef DSTR_ABORT_ON_OOM
				if (sh == NULL) dstrOomAbort();
#else
				if (sh == NULL) return NULL;
#endif
				sh->len = initlen;
				sh->free = 0;
				if (initlen) {
								if (init)
												memcpy(sh->buf, init, initlen);
								else
												memset(sh->buf, 0, initlen);
				}
				sh->buf[initlen] = '\0';
				return (char *)sh->buf;
}

dstr dstrempty(void) {
				return dstrnewlen("", 0);
}

dstr dstrnew(const char *init) {
				size_t initlen = (init == NULL) ? 0 : strlen(init);
				return dstrnewlen(init, initlen);
}

size_t dstrlen(const dstr s) {
				struct dstrhdr *sh = (void *)(s - (sizeof(struct dstrhdr)));
				return sh->len;
}

dstr dstrdup(const dstr s) {
				return dstrnewlen(s, dstrlen(s));
}

void dstrfree(dstr s) {
				if (s == NULL) return;
				free(s - sizeof(struct dstrhdr));
}

size_t dstravail(dstr s) {
				struct dstrhdr *sh = (void *)(s - (sizeof(struct dstrhdr)));
				return sh->free;
}

void dstrupdatelen(dstr s) {
				struct dstrhdr *sh = (void *)(s - (sizeof(struct dstrhdr)));
				int reallen = strlen(s);
				sh->free += (sh->len - reallen);
				sh->len = reallen;
}

static dstr dstrMakeRoomFor(dstr s, size_t addlen) {
				struct dstrhdr *sh, *newsh;
				size_t free = dstravail(s);
				size_t len, newlen;
				if (free >= addlen) return s;
				len = dstrlen(s);
				sh = (void *)(s - (sizeof(struct dstrhdr)));
				newlen = (len + addlen) * 2;
				newsh = realloc(sh, sizeof(struct dstrhdr) + newlen + 1);
#ifdef DSTR_ABORT_ON_OOM
				if (newsh == NULL) dstrOomAbort();
#else
				if (newsh == NULL) return NULL;
#endif
				newsh->free = newlen - len;
				return newsh->buf;
}

dstr dstrcatlen(dstr s, const void *t, size_t len) {
				struct dstrhdr *sh;
				size_t curlen = dstrlen(s);
				s = dstrMakeRoomFor(s, len);
				if (s == NULL) return NULL;
				sh = (void *)(s - (sizeof(struct dstrhdr)));
				memcpy(s + curlen, t, len);
				sh->len = curlen + len;
				sh->free = sh->free - len;
				s[curlen + len] = '\0';
				return s;
}

dstr dstrcat(dstr s, const char *t) {
				return dstrcatlen(s, t, strlen(t));
}

dstr dstrcpylen(dstr s, char *t, size_t len) {
				struct dstrhdr *sh = (void *)(s - (sizeof(struct dstrhdr)));
				size_t totlen = sh->free + sh->len;
				if (totlen < len) {
								s = dstrMakeRoomFor(s, len - sh->len);
								if (s == NULL) return NULL;
								sh = (void *)(s - (sizeof(struct dstrhdr)));
								totlen = sh->free + sh->len;
				}
				memcpy(s, t, len);
				s[len] = '\0';
				sh->len = len;
				sh->free = totlen - len;
				return s;
}

dstr dstrcpy(dstr s, char *t) {
				return dstrcpylen(s, t, strlen(t));
}

dstr dstrcatvprintf(dstr s, const char *fmt, va_list ap) {
				va_list cpy;
				char *buf, *t;
				size_t buflen = 16;
				while (1) {
								buf = malloc(buflen);
#ifdef DSTR_ABORT_ON_OOM
								if (buf == NULL) dstrOomAbort();
#else
								if (buf == NULL) return NULL;
#endif
								buf[buflen - 2] = '\0';
								va_copy(cpy, ap);
								vsnprintf(buf, buflen, fmt, cpy);
								if (buf[buflen - 2] != '\0') {
												free(buf);
												buflen *= 2;
												continue;
								}
								break;
				}
				t = dstrcat(s, buf);
				free(buf);
				return t;
}

dstr dstrcatprintf(dstr s, const char *fmt, ...) {
				va_list ap;
				char *t;
				va_start(ap, fmt);
				t = dstrcatvprintf(s, fmt, ap);
				va_end(ap);
				return t;
}

dstr dstrtrim(dstr s, const char *cset) {
				struct dstrhdr *sh = (void *)(s - (sizeof(struct dstrhdr)));
				char *start, *end, *sp, *ep;
				size_t len;
				sp = start = s;
				ep = end = s + dstrlen(s) - 1;
				while (sp <= end && strchr(cset, *sp)) sp++;
				while (ep > start && strchr(cset, *ep)) ep--;
				len = (sp > ep) ? 0 : ((ep - sp) + 1);
				if (sh->buf != sp) memmove(sh->buf, sp, len);
				sh->buf[len] = '\0';
				sh->free = sh->free + (sh->len - len);
				sh->len = len;
				return s;
}

dstr dstrrange(dstr s, int start, int end) {
				struct dstrhdr *sh = (void *)(s - (sizeof(struct dstrhdr)));
				size_t newlen, len = dstrlen(s);
				if (len == 0) return s;
				if (start < 0) {
								start = len + start;
								if (start < 0) start = 0;
				}
				if (end < 0) {
								end = len + end;
								if (end < 0) end = 0;
				}
				newlen = (start > end) ? 0 : (end - start) + 1;
				if (newlen != 0) {
								if (start >= (signed)len) start = len - 1;
								if (end >= (signed)len) end = len - 1;
								newlen = (start > end) ? 0 : (end - start) + 1;
				} else {
								start = 0;
				}
				if (start != 0) memmove(sh->buf, sh->buf + start, newlen);
				sh->buf[newlen] = 0;
				sh->free = sh->free + (sh->len - newlen);
				sh->len = newlen;
				return s;
}

void dstrtolower(dstr s) {
				int len = dstrlen(s), j;
				for (j = 0; j < len; j++) s[j] = tolower(s[j]);
}

void dstrtoupper(dstr s) {
				int len = dstrlen(s), j;
				for (j = 0; j < len; j++) s[j] = toupper(s[j]);
}

int dstrcmp(dstr s1, dstr s2) {
				size_t l1, l2, minlen;
				int cmp;
				l1 = dstrlen(s1);
				l2 = dstrlen(s2);
				minlen = (l1 < l2) ? l1 : l2;
				cmp = memcmp(s1, s2, minlen);
				if (cmp == 0) return l1 - l2;
				return cmp;
}

dstr *dstrsplitlen(char *s, int len, char *sep, int seplen, int *count) {
				int elements = 0, slots = 5, start = 0, j;
				dstr *tokens = malloc(sizeof(dstr) * slots);
#ifdef DSTR_ABORT_ON_OOM
				if (tokens == NULL) dstrOomAbort();
#endif
				if (seplen < 1 || len < 0 || tokens == NULL) return NULL;
				if (len == 0) {
								*count = 0;
								return tokens;
				}
				for (j = 0; j < (len - (seplen - 1)); j++) {
								if (slots < elements + 2) {
												dstr *newtokens;
												slots *= 2;
												newtokens = realloc(tokens, sizeof(dstr) * slots);
												if (newtokens == NULL) {
#ifdef DSTR_ABORT_ON_OOM
																dstrOomAbort();
#else
																goto cleanup;
#endif
												}
												tokens = newtokens;
								}
								if ((seplen == 1 && *(s + j) == sep[0]) ||
								    (memcmp(s + j, sep, seplen) == 0)) {
												tokens[elements] = dstrnewlen(s + start, j - start);
												if (tokens[elements] == NULL) {
#ifdef DSTR_ABORT_ON_OOM
																dstrOomAbort();
#else
																goto cleanup;
#endif
												}
												elements++;
												start = j + seplen;
												j = j + seplen - 1;
								}
				}
				tokens[elements] = dstrnewlen(s + start, len - start);
				if (tokens[elements] == NULL) {
#ifdef DSTR_ABORT_ON_OOM
								dstrOomAbort();
#else
								goto cleanup;
#endif
				}
				elements++;
				*count = elements;
				return tokens;
#ifndef DSTR_ABORT_ON_OOM
cleanup: {
								int i;
								for (i = 0; i < elements; i++) dstrfree(tokens[i]);
								free(tokens);
								return NULL;
				}
#endif
}

void dstrfreesplitres(dstr *tokens, int count) {
				if (!tokens) return;
				while (count--) dstrfree(tokens[count]);
				free(tokens);
}

dstr dstrfromlonglong(long long value) {
				char buf[32], *p;
				unsigned long long v;
				v = (value < 0) ? -value : value;
				p = buf + 31;
				do {
								*p-- = '0' + (v % 10);
								v /= 10;
				} while (v);
				if (value < 0) *p-- = '-';
				p++;
				return dstrnewlen(p, 32 - (p - buf));
}

dstr dstrcatrepr(dstr s, char *p, size_t len) {
				s = dstrcatlen(s, "\"", 1);
				while (len--) {
								switch (*p) {
								case '\\':
								case '"':
												s = dstrcatprintf(s, "\\%c", *p);
												break;
								case '\n':
												s = dstrcatlen(s, "\\n", 1);
												break;
								case '\r':
												s = dstrcatlen(s, "\\r", 1);
												break;
								case '\t':
												s = dstrcatlen(s, "\\t", 1);
												break;
								case '\a':
												s = dstrcatlen(s, "\\a", 1);
												break;
								case '\b':
												s = dstrcatlen(s, "\\b", 1);
												break;
								default:
												if (isprint(*p))
																s = dstrcatprintf(s, "%c", *p);
												else
																s = dstrcatprintf(s, "\\x%02x", (unsigned char)*p);
												break;
								}
								p++;
				}
				return dstrcatlen(s, "\"", 1);
}

dstr *dstrsplitargs(char *line, int *argc) {
				char *p = line;
				char *current = NULL;
				char **vector = NULL;
				*argc = 0;
				while (1) {
								while (*p && isspace(*p)) p++;
								if (*p) {
												int inq = 0;
												int done = 0;
												if (current == NULL) current = dstrempty();
												while (!done) {
																if (inq) {
																				if (*p == '\\' && *(p + 1)) {
																								char c;
																								p++;
																								switch (*p) {
																								case 'n':
																												c = '\n';
																												break;
																								case 'r':
																												c = '\r';
																												break;
																								case 't':
																												c = '\t';
																												break;
																								case 'b':
																												c = '\b';
																												break;
																								case 'a':
																												c = '\a';
																												break;
																								default:
																												c = *p;
																												break;
																								}
																								current = dstrcatlen(current, &c, 1);
																				} else if (*p == '"') {
																								if (*(p + 1) && !isspace(*(p + 1))) goto err;
																								done = 1;
																				} else if (!*p) {
																								goto err;
																				} else {
																								current = dstrcatlen(current, p, 1);
																				}
																} else {
																				switch (*p) {
																				case ' ':
																				case '\n':
																				case '\r':
																				case '\t':
																				case '\0':
																								done = 1;
																								break;
																				case '"':
																								inq = 1;
																								break;
																				default:
																								current = dstrcatlen(current, p, 1);
																								break;
																				}
																}
																if (*p) p++;
												}
												vector = realloc(vector, ((*argc) + 1) * sizeof(char *));
												vector[*argc] = current;
												(*argc)++;
												current = NULL;
								} else {
												return vector;
								}
				}
err:
				while ((*argc)--) dstrfree(vector[*argc]);
				free(vector);
				if (current) dstrfree(current);
				return NULL;
}
