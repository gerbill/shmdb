#include <string.h>
#include <assert.h>
#include "async.h"
#include "dstr.h"
#include "util.h"

void __shmAppendCmd(shmContext *c, char *cmd, size_t len);

static shmAsyncContext *shmAsyncInitialize(shmContext *c) {
				shmAsyncContext *ac = realloc(c, sizeof(shmAsyncContext));
				c = &(ac->c);
				c->flags &= ~SHM_CONNECTED;
				ac->err = 0;
				ac->errstr = NULL;
				ac->data = NULL;
				ac->_adapter_data = NULL;
				ac->evAddRead = NULL;
				ac->evDelRead = NULL;
				ac->evAddWrite = NULL;
				ac->evDelWrite = NULL;
				ac->evCleanup = NULL;
				ac->onConnect = NULL;
				ac->onDisconnect = NULL;
				ac->replies.head = NULL;
				ac->replies.tail = NULL;
				return ac;
}

static void __shmAsyncCopyError(shmAsyncContext *ac) {
				shmContext *c = &(ac->c);
				ac->err = c->err;
				ac->errstr = c->errstr;
}

shmAsyncContext *shmAsyncConnect(const char *ip, int port) {
				shmContext *c = shmConnectNonBlock(ip, port);
				shmAsyncContext *ac = shmAsyncInitialize(c);
				__shmAsyncCopyError(ac);
				return ac;
}

shmAsyncContext *shmAsyncConnectUnix(const char *path) {
				shmContext *c = shmConnectUnixNonBlock(path);
				shmAsyncContext *ac = shmAsyncInitialize(c);
				__shmAsyncCopyError(ac);
				return ac;
}

int shmAsyncSetReplyObjectFunctions(shmAsyncContext *ac,
                                    shmReplyObjectFunctions *fn) {
				shmContext *c = &(ac->c);
				return shmSetReplyObjectFunctions(c, fn);
}

int shmAsyncSetConnectCallback(shmAsyncContext *ac, shmConnectCallback *fn) {
				if (ac->onConnect == NULL) {
								ac->onConnect = fn;
								return SHM_OK;
				}
				return SHM_ERR;
}

int shmAsyncSetDisconnectCallback(shmAsyncContext *ac,
                                  shmDisconnectCallback *fn) {
				if (ac->onDisconnect == NULL) {
								ac->onDisconnect = fn;
								return SHM_OK;
				}
				return SHM_ERR;
}

static int __shmPushCallback(shmCallbackList *list, shmCallback *source) {
				shmCallback *cb;
				cb = calloc(1, sizeof(*cb));
				if (!cb) shmOOM();
				if (source != NULL) {
								cb->fn = source->fn;
								cb->privdata = source->privdata;
				}
				if (list->head == NULL) list->head = cb;
				if (list->tail != NULL) list->tail->next = cb;
				list->tail = cb;
				return SHM_OK;
}

static int __shmShiftCallback(shmCallbackList *list, shmCallback *target) {
				shmCallback *cb = list->head;
				if (cb != NULL) {
								list->head = cb->next;
								if (cb == list->tail) list->tail = NULL;
								if (target != NULL) memcpy(target, cb, sizeof(*cb));
								free(cb);
								return SHM_OK;
				}
				return SHM_ERR;
}

void shmAsyncDisconnect(shmAsyncContext *ac) {
				shmContext *c = &(ac->c);
				c->flags |= SHM_DISCONNECTING;
}

static void __shmAsyncDisconnect(shmAsyncContext *ac) {
				shmContext *c = &(ac->c);
				shmCallback cb;
				int status;
				__shmAsyncCopyError(ac);
				status = (ac->err == 0) ? SHM_OK : SHM_ERR;
				if (status == SHM_OK) {
								assert(__shmShiftCallback(&ac->replies, NULL) == SHM_ERR);
				} else {
								c->flags |= SHM_DISCONNECTING;
								while (__shmShiftCallback(&ac->replies, &cb) == SHM_OK) {
												if (cb.fn != NULL) cb.fn(ac, NULL, cb.privdata);
								}
				}
				if (ac->evCleanup) ac->evCleanup(ac->_adapter_data);
				if (ac->onDisconnect) ac->onDisconnect(ac, status);
				shmFree(c);
}

void shmProcessCallbacks(shmAsyncContext *ac) {
				shmContext *c = &(ac->c);
				shmCallback cb;
				void *reply = NULL;
				int status;
				while ((status = shmGetReply(c, &reply)) == SHM_OK) {
								if (reply == NULL) {
												if (c->flags & SHM_DISCONNECTING && dstrlen(c->obuf) == 0) {
																__shmAsyncDisconnect(ac);
																return;
												}
												break;
								}
								assert(__shmShiftCallback(&ac->replies, &cb) == SHM_OK);
								if (cb.fn != NULL) {
												cb.fn(ac, reply, cb.privdata);
								} else {
												c->fn->freeObject(reply);
								}
				}
				if (status != SHM_OK) __shmAsyncDisconnect(ac);
}

void shmAsyncHandleRead(shmAsyncContext *ac) {
				shmContext *c = &(ac->c);
				if (shmBufferRead(c) == SHM_ERR) {
								__shmAsyncDisconnect(ac);
				} else {
								if (ac->evAddRead) ac->evAddRead(ac->_adapter_data);
								shmProcessCallbacks(ac);
				}
}

void shmAsyncHandleWrite(shmAsyncContext *ac) {
				shmContext *c = &(ac->c);
				int done = 0;
				if (shmBufferWrite(c, &done) == SHM_ERR) {
								__shmAsyncDisconnect(ac);
				} else {
								if (!done) {
												if (ac->evAddWrite) ac->evAddWrite(ac->_adapter_data);
								} else {
												if (ac->evDelWrite) ac->evDelWrite(ac->_adapter_data);
								}
								if (ac->evAddRead) ac->evAddRead(ac->_adapter_data);
								if (!(c->flags & SHM_CONNECTED)) {
												c->flags |= SHM_CONNECTED;
												if (ac->onConnect) ac->onConnect(ac);
								}
				}
}

static int __shmAsyncCmd(shmAsyncContext *ac, shmCallbackFn *fn,
                             void *privdata, char *cmd, size_t len) {
				shmContext *c = &(ac->c);
				shmCallback cb;
				if (c->flags & SHM_DISCONNECTING) return SHM_ERR;
				__shmAppendCmd(c, cmd, len);
				cb.fn = fn;
				cb.privdata = privdata;
				__shmPushCallback(&ac->replies, &cb);
				if (ac->evAddWrite) ac->evAddWrite(ac->_adapter_data);
				return SHM_OK;
}

int shmvAsyncCmd(shmAsyncContext *ac, shmCallbackFn *fn, void *privdata,
                     const char *format, va_list ap) {
				char *cmd;
				int len;
				int status;
				len = shmvFormatCmd(&cmd, format, ap);
				status = __shmAsyncCmd(ac, fn, privdata, cmd, len);
				free(cmd);
				return status;
}

int shmAsyncCmd(shmAsyncContext *ac, shmCallbackFn *fn, void *privdata,
                    const char *format, ...) {
				va_list ap;
				int status;
				va_start(ap, format);
				status = shmvAsyncCmd(ac, fn, privdata, format, ap);
				va_end(ap);
				return status;
}

int shmAsyncCommandArgv(shmAsyncContext *ac, shmCallbackFn *fn, void *privdata,
                        int argc, const char **argv, const size_t *argvlen) {
				char *cmd;
				int len;
				int status;
				len = shmFormatCommandArgv(&cmd, argc, argv, argvlen);
				status = __shmAsyncCmd(ac, fn, privdata, cmd, len);
				free(cmd);
				return status;
}
