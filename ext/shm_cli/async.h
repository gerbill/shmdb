#ifndef __SHM_CLI_ASYNC_H
#define __SHM_CLI_ASYNC_H
#include "shm_cli.h"
#ifdef __cplusplus
extern "C" {
#endif
struct shmAsyncContext;
typedef void (shmCallbackFn)(struct shmAsyncContext *, void *, void *);
typedef struct shmCallback {
				struct shmCallback *next;
				shmCallbackFn *fn;
				void *privdata;
} shmCallback;
typedef struct shmCallbackList {
				shmCallback *head, *tail;
} shmCallbackList;
typedef void (shmDisconnectCallback)(const struct shmAsyncContext *, int status);
typedef void (shmConnectCallback)(const struct shmAsyncContext *);
typedef struct shmAsyncContext {
				shmContext c;
				int err;
				char *errstr;
				void *data;
				void *_adapter_data;
				void (*evAddRead)(void *privdata);
				void (*evDelRead)(void *privdata);
				void (*evAddWrite)(void *privdata);
				void (*evDelWrite)(void *privdata);
				void (*evCleanup)(void *privdata);
				shmDisconnectCallback *onDisconnect;
				shmConnectCallback *onConnect;
				shmCallbackList replies;
} shmAsyncContext;

shmAsyncContext *shmAsyncConnect(const char *ip, int port);

int shmAsyncSetReplyObjectFunctions(shmAsyncContext *ac,
                                    shmReplyObjectFunctions *fn);

int shmAsyncSetConnectCallback(shmAsyncContext *ac, shmConnectCallback *fn);

int shmAsyncSetDisconnectCallback(shmAsyncContext *ac,
                                  shmDisconnectCallback *fn);

void shmAsyncDisconnect(shmAsyncContext *ac);

void shmAsyncHandleRead(shmAsyncContext *ac);

void shmAsyncHandleWrite(shmAsyncContext *ac);

int shmvAsyncCmd(shmAsyncContext *ac, shmCallbackFn *fn, void *privdata,
                     const char *format, va_list ap);

int shmAsyncCmd(shmAsyncContext *ac, shmCallbackFn *fn, void *privdata,
                    const char *format, ...);

int shmAsyncCommandArgv(shmAsyncContext *ac, shmCallbackFn *fn, void *privdata,
                        int argc, const char **argv, const size_t *argvlen);

#ifdef __cplusplus
}
#endif
#endif
