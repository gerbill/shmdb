#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>
#include <assert.h>
#include <unistd.h>
#include <signal.h>
#include "shm_cli.h"
static int tests = 0, fails = 0;
#define test(_s)               \
				{                            \
								printf("#%02d ", ++tests); \
								printf(_s);                \
				}
#define test_cond(_c)   \
				if (_c)               \
								printf("PASSED\n"); \
				else {                \
								printf("FAILED\n"); \
								fails++;            \
				}

static long long usec(void) {
				struct timeval tv;
				gettimeofday(&tv, NULL);
				return (((long long)tv.tv_sec) * 1000000) + tv.tv_usec;
}
static int use_unix = 0;
static shmContext *blocking_context = NULL;

static void __connect(shmContext **target) {
				*target = blocking_context =
								(use_unix ? shmConnectUnix("/tmp/shm.sock")
								 : shmConnect((char *)"127.0.0.1", 20971));
				if (blocking_context->err) {
								printf("Connection error: %s\n", blocking_context->errstr);
								exit(1);
				}
}

static void test_format_commands() {
				char *cmd;
				int len;
				test("Format command without interpolation: ");
				len = shmFormatCmd(&cmd, "SET foo bar");
				test_cond(
								strncmp(cmd, "*3\r\n$3\r\nSET\r\n$3\r\nfoo\r\n$3\r\nbar\r\n", len) == 0 &&
								len == 4 + 4 + (3 + 2) + 4 + (3 + 2) + 4 + (3 + 2));
				free(cmd);
				test("Format command with %%s string interpolation: ");
				len = shmFormatCmd(&cmd, "SET %s %s", "foo", "bar");
				test_cond(
								strncmp(cmd, "*3\r\n$3\r\nSET\r\n$3\r\nfoo\r\n$3\r\nbar\r\n", len) == 0 &&
								len == 4 + 4 + (3 + 2) + 4 + (3 + 2) + 4 + (3 + 2));
				free(cmd);
				test("Format command with %%s and an empty string: ");
				len = shmFormatCmd(&cmd, "SET %s %s", "foo", "");
				test_cond(strncmp(cmd, "*3\r\n$3\r\nSET\r\n$3\r\nfoo\r\n$0\r\n\r\n", len) ==
				          0 &&
				          len == 4 + 4 + (3 + 2) + 4 + (3 + 2) + 4 + (0 + 2));
				free(cmd);
				test("Format command with %%b string interpolation: ");
				len = shmFormatCmd(&cmd, "SET %b %b", "foo", 3, "b\0r", 3);
				test_cond(strncmp(cmd, "*3\r\n$3\r\nSET\r\n$3\r\nfoo\r\n$3\r\nb\0r\r\n",
				                  len) == 0 &&
				          len == 4 + 4 + (3 + 2) + 4 + (3 + 2) + 4 + (3 + 2));
				free(cmd);
				test("Format command with %%b and an empty string: ");
				len = shmFormatCmd(&cmd, "SET %b %b", "foo", 3, "", 0);
				test_cond(strncmp(cmd, "*3\r\n$3\r\nSET\r\n$3\r\nfoo\r\n$0\r\n\r\n", len) ==
				          0 &&
				          len == 4 + 4 + (3 + 2) + 4 + (3 + 2) + 4 + (0 + 2));
				free(cmd);
				test("Format command with literal %%: ");
				len = shmFormatCmd(&cmd, "SET %% %%");
				test_cond(strncmp(cmd, "*3\r\n$3\r\nSET\r\n$1\r\n%\r\n$1\r\n%\r\n", len) ==
				          0 &&
				          len == 4 + 4 + (3 + 2) + 4 + (1 + 2) + 4 + (1 + 2));
				free(cmd);
				test("Format command with printf-delegation (long long): ");
				len = shmFormatCmd(&cmd, "key:%08lld", 1234ll);
				test_cond(strncmp(cmd, "*1\r\n$12\r\nkey:00001234\r\n", len) == 0 &&
				          len == 4 + 5 + (12 + 2));
				free(cmd);
				test("Format command with printf-delegation (float): ");
				len = shmFormatCmd(&cmd, "v:%06.1f", 12.34f);
				test_cond(strncmp(cmd, "*1\r\n$8\r\nv:0012.3\r\n", len) == 0 &&
				          len == 4 + 4 + (8 + 2));
				free(cmd);
				test("Format command with printf-delegation and extra interpolation: ");
				len = shmFormatCmd(&cmd, "key:%d %b", 1234, "foo", 3);
				test_cond(strncmp(cmd, "*2\r\n$8\r\nkey:1234\r\n$3\r\nfoo\r\n", len) == 0 &&
				          len == 4 + 4 + (8 + 2) + 4 + (3 + 2));
				free(cmd);
				test("Format command with wrong printf format and extra interpolation: ");
				len = shmFormatCmd(&cmd, "key:%08p %b", 1234, "foo", 3);
				test_cond(strncmp(cmd, "*2\r\n$6\r\nkey:8p\r\n$3\r\nfoo\r\n", len) == 0 &&
				          len == 4 + 4 + (6 + 2) + 4 + (3 + 2));
				free(cmd);
				const char *argv[3];
				argv[0] = "SET";
				argv[1] = "foo\0xxx";
				argv[2] = "bar";
				size_t lens[3] = {3, 7, 3};
				int argc = 3;
				test("Format command by passing argc/argv without lengths: ");
				len = shmFormatCommandArgv(&cmd, argc, argv, NULL);
				test_cond(
								strncmp(cmd, "*3\r\n$3\r\nSET\r\n$3\r\nfoo\r\n$3\r\nbar\r\n", len) == 0 &&
								len == 4 + 4 + (3 + 2) + 4 + (3 + 2) + 4 + (3 + 2));
				free(cmd);
				test("Format command by passing argc/argv with lengths: ");
				len = shmFormatCommandArgv(&cmd, argc, argv, lens);
				test_cond(strncmp(cmd, "*3\r\n$3\r\nSET\r\n$7\r\nfoo\0xxx\r\n$3\r\nbar\r\n",
				                  len) == 0 &&
				          len == 4 + 4 + (3 + 2) + 4 + (7 + 2) + 4 + (3 + 2));
				free(cmd);
}

static void test_blocking_connection() {
				shmContext *c;
				shmReply *reply;
				int major, minor;
				test("Returns error when host cannot be resolved: ");
				c = shmConnect((char *)"idontexist.local", 20971);
				test_cond(c->err == SHM_ERR_OTHER &&
				          strcmp(c->errstr, "Can't resolve: idontexist.local") == 0);
				shmFree(c);
				test("Returns error when the port is not open: ");
				c = shmConnect((char *)"localhost", 56380);
				test_cond(c->err == SHM_ERR_IO &&
				          strcmp(c->errstr, "Connection refused") == 0);
				shmFree(c);
				__connect(&c);
				test("Is able to deliver commands: ");
				reply = cmdShm(c, "PING");
				test_cond(reply->type == SHM_REPLY_STATUS &&
				          strcasecmp(reply->str, "world") == 0) freeReplyObject(reply);
				reply = cmdShm(c, "SELECT 9");
				freeReplyObject(reply);
				reply = cmdShm(c, "DBSIZE");
				if (reply->type != SHM_REPLY_INTEGER || reply->integer != 0) {
								printf("Database #9 is not empty, test can not continue\n");
								exit(1);
				}
				freeReplyObject(reply);
				test("Is a able to send commands verbatim: ");
				reply = cmdShm(c, "SET foo bar");
				test_cond(reply->type == SHM_REPLY_STATUS &&
				          strcasecmp(reply->str, "ok") == 0) freeReplyObject(reply);
				test("%%s String interpolation works: ");
				reply = cmdShm(c, "SET %s %s", "foo", "hello world");
				freeReplyObject(reply);
				reply = cmdShm(c, "GET foo");
				test_cond(reply->type == SHM_REPLY_STRING &&
				          strcmp(reply->str, "hello world") == 0);
				freeReplyObject(reply);
				test("%%b String interpolation works: ");
				reply = cmdShm(c, "SET %b %b", "foo", 3, "hello\x00world", 11);
				freeReplyObject(reply);
				reply = cmdShm(c, "GET foo");
				test_cond(reply->type == SHM_REPLY_STRING &&
				          memcmp(reply->str, "hello\x00world", 11) == 0)
				test("Binary reply length is correct: ");
				test_cond(reply->len == 11) freeReplyObject(reply);
				test("Can parse nil replies: ");
				reply = cmdShm(c, "GET nokey");
				test_cond(reply->type == SHM_REPLY_NIL) freeReplyObject(reply);
				test("Can parse integer replies: ");
				reply = cmdShm(c, "INCR mycounter");
				test_cond(reply->type == SHM_REPLY_INTEGER && reply->integer == 1)
				freeReplyObject(reply);
				test("Can parse multi bulk replies: ");
				freeReplyObject(cmdShm(c, "LPUSH mylist foo"));
				freeReplyObject(cmdShm(c, "LPUSH mylist bar"));
				reply = cmdShm(c, "LRANGE mylist 0 -1");
				test_cond(reply->type == SHM_REPLY_ARRAY && reply->elements == 2 &&
				          !memcmp(reply->element[0]->str, "bar", 3) &&
				          !memcmp(reply->element[1]->str, "foo", 3)) freeReplyObject(reply);
				test("Can handle nested multi bulk replies: ");
				freeReplyObject(cmdShm(c, "MULTI"));
				freeReplyObject(cmdShm(c, "LRANGE mylist 0 -1"));
				freeReplyObject(cmdShm(c, "PING"));
				reply = (cmdShm(c, "EXEC"));
				test_cond(reply->type == SHM_REPLY_ARRAY && reply->elements == 2 &&
				          reply->element[0]->type == SHM_REPLY_ARRAY &&
				          reply->element[0]->elements == 2 &&
				          !memcmp(reply->element[0]->element[0]->str, "bar", 3) &&
				          !memcmp(reply->element[0]->element[1]->str, "foo", 3) &&
				          reply->element[1]->type == SHM_REPLY_STATUS &&
				          strcasecmp(reply->element[1]->str, "world") == 0);
				freeReplyObject(reply);
				{
								const char *field = "shm_version:";
								char *p, *eptr;
								reply = cmdShm(c, "INFO");
								p = strstr(reply->str, field);
								major = strtol(p + strlen(field), &eptr, 10);
								p = eptr + 1;
								minor = strtol(p, &eptr, 10);
								freeReplyObject(reply);
				}
				test("Returns I/O error when the connection is lost: ");
				reply = cmdShm(c, "QUIT");
				if (major >= 2 && minor > 0) {
								test_cond(strcasecmp(reply->str, "OK") == 0 &&
								          shmGetReply(c, (void **)&reply) == SHM_ERR);
								freeReplyObject(reply);
				} else {
								test_cond(reply == NULL);
				}
				assert(c->err == SHM_ERR_EOF &&
				       strcmp(c->errstr, "Server closed the connection") == 0);
				shmFree(c);
				__connect(&c);
}

static void test_reply_reader() {
				void *reader;
				void *reply;
				char *err;
				int ret;
				test("Error handling in reply parser: ");
				reader = shmReplyReaderCreate();
				shmReplyReaderFeed(reader, (char *)"@foo\r\n", 6);
				ret = shmReplyReaderGetReply(reader, NULL);
				err = shmReplyReaderGetError(reader);
				test_cond(ret == SHM_ERR &&
				          strcasecmp(err, "Protocol error, got \"@\" as reply type byte") ==
				          0);
				shmReplyReaderFree(reader);
				test("Memory cleanup in reply parser: ");
				reader = shmReplyReaderCreate();
				shmReplyReaderFeed(reader, (char *)"*2\r\n", 4);
				shmReplyReaderFeed(reader, (char *)"$5\r\nhello\r\n", 11);
				shmReplyReaderFeed(reader, (char *)"@foo\r\n", 6);
				ret = shmReplyReaderGetReply(reader, NULL);
				err = shmReplyReaderGetError(reader);
				test_cond(ret == SHM_ERR &&
				          strcasecmp(err, "Protocol error, got \"@\" as reply type byte") ==
				          0);
				shmReplyReaderFree(reader);
				test("Set error on nested multi bulks with depth > 1: ");
				reader = shmReplyReaderCreate();
				shmReplyReaderFeed(reader, (char *)"*1\r\n", 4);
				shmReplyReaderFeed(reader, (char *)"*1\r\n", 4);
				shmReplyReaderFeed(reader, (char *)"*1\r\n", 4);
				ret = shmReplyReaderGetReply(reader, NULL);
				err = shmReplyReaderGetError(reader);
				test_cond(ret == SHM_ERR && strncasecmp(err, "No support for", 14) == 0);
				shmReplyReaderFree(reader);
				test("Works with NULL functions for reply: ");
				reader = shmReplyReaderCreate();
				shmReplyReaderSetReplyObjectFunctions(reader, NULL);
				shmReplyReaderFeed(reader, (char *)"+OK\r\n", 5);
				ret = shmReplyReaderGetReply(reader, &reply);
				test_cond(ret == SHM_OK && reply == (void *)SHM_REPLY_STATUS);
				shmReplyReaderFree(reader);
				test("Works when a single newline (\\r\\n) covers two calls to feed: ");
				reader = shmReplyReaderCreate();
				shmReplyReaderSetReplyObjectFunctions(reader, NULL);
				shmReplyReaderFeed(reader, (char *)"+OK\r", 4);
				ret = shmReplyReaderGetReply(reader, &reply);
				assert(ret == SHM_OK && reply == NULL);
				shmReplyReaderFeed(reader, (char *)"\n", 1);
				ret = shmReplyReaderGetReply(reader, &reply);
				test_cond(ret == SHM_OK && reply == (void *)SHM_REPLY_STATUS);
				shmReplyReaderFree(reader);
}

static void test_throughput() {
				int i;
				long long t1, t2;
				shmContext *c = blocking_context;
				shmReply **replies;
				test("Throughput:\n");
				for (i = 0; i < 500; i++) freeReplyObject(cmdShm(c, "LPUSH mylist foo"));
				replies = malloc(sizeof(shmReply *) * 1000);
				t1 = usec();
				for (i = 0; i < 1000; i++) {
								replies[i] = cmdShm(c, "PING");
								assert(replies[i] != NULL && replies[i]->type == SHM_REPLY_STATUS);
				}
				t2 = usec();
				for (i = 0; i < 1000; i++) freeReplyObject(replies[i]);
				free(replies);
				printf("\t(1000x PING: %.2fs)\n", (t2 - t1) / 1000000.0);
				replies = malloc(sizeof(shmReply *) * 1000);
				t1 = usec();
				for (i = 0; i < 1000; i++) {
								replies[i] = cmdShm(c, "LRANGE mylist 0 499");
								assert(replies[i] != NULL && replies[i]->type == SHM_REPLY_ARRAY);
								assert(replies[i] != NULL && replies[i]->elements == 500);
				}
				t2 = usec();
				for (i = 0; i < 1000; i++) freeReplyObject(replies[i]);
				free(replies);
				printf("\t(1000x LRANGE with 500 elements: %.2fs)\n", (t2 - t1) / 1000000.0);
}

static void cleanup() {
				shmContext *c = blocking_context;
				shmReply *reply;
				reply = cmdShm(c, "SELECT 9");
				assert(reply != NULL);
				freeReplyObject(reply);
				reply = cmdShm(c, "FLUSHDB");
				assert(reply != NULL);
				freeReplyObject(reply);
				shmFree(c);
}

int main(int argc, char **argv) {
				if (argc > 1) {
								if (strcmp(argv[1], "-s") == 0) use_unix = 1;
				}
				signal(SIGPIPE, SIG_IGN);
				test_format_commands();
				test_blocking_connection();
				test_reply_reader();
				test_throughput();
				cleanup();
				if (fails == 0) {
								printf("ALL TESTS PASSED\n");
				} else {
								printf("*** %d TESTS FAILED ***\n", fails);
				}
				return 0;
}
