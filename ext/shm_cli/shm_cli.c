#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <ctype.h>
#include "shm_cli.h"
#include "net.h"
#include "dstr.h"
#include "util.h"
typedef struct shmReader {
				struct shmReplyObjectFunctions *fn;
				dstr error;
				void *reply;
				dstr buf;
				size_t pos;
				size_t len;
				shmReadTask rstack[9];
				int ridx;
				void *privdata;
} shmReader;

static shmReply *createReplyObject(int type);

static void *objCreateString(const shmReadTask *task, char *str, size_t len);

static void *createArrayObject(const shmReadTask *task, int elements);

static void *createIntegerObject(const shmReadTask *task, long long value);

static void *createNilObject(const shmReadTask *task);

static void shmSetReplyReaderError(shmReader *r, dstr err);

static shmReplyObjectFunctions defaultFunctions = {
				objCreateString, createArrayObject, createIntegerObject, createNilObject,
				freeReplyObject
};

static shmReply *createReplyObject(int type) {
				shmReply *r = malloc(sizeof(*r));
				if (!r) shmOOM();
				r->type = type;
				return r;
}

void freeReplyObject(void *reply) {
				shmReply *r = reply;
				size_t j;
				switch (r->type) {
				case SHM_REPLY_INTEGER:
								break;
				case SHM_REPLY_ARRAY:
								for (j = 0; j < r->elements; j++)
												if (r->element[j]) freeReplyObject(r->element[j]);
								free(r->element);
								break;
				case SHM_REPLY_ERROR:
				case SHM_REPLY_STATUS:
				case SHM_REPLY_STRING:
								free(r->str);
								break;
				}
				free(r);
}

static void *objCreateString(const shmReadTask *task, char *str,
                                size_t len) {
				shmReply *r = createReplyObject(task->type);
				char *value = malloc(len + 1);
				if (!value) shmOOM();
				assert(task->type == SHM_REPLY_ERROR || task->type == SHM_REPLY_STATUS ||
				       task->type == SHM_REPLY_STRING);
				memcpy(value, str, len);
				value[len] = '\0';
				r->str = value;
				r->len = len;
				if (task->parent) {
								shmReply *parent = task->parent->obj;
								assert(parent->type == SHM_REPLY_ARRAY);
								parent->element[task->idx] = r;
				}
				return r;
}

static void *createArrayObject(const shmReadTask *task, int elements) {
				shmReply *r = createReplyObject(SHM_REPLY_ARRAY);
				r->elements = elements;
				if ((r->element = calloc(sizeof(shmReply *), elements)) == NULL) shmOOM();
				if (task->parent) {
								shmReply *parent = task->parent->obj;
								assert(parent->type == SHM_REPLY_ARRAY);
								parent->element[task->idx] = r;
				}
				return r;
}

static void *createIntegerObject(const shmReadTask *task, long long value) {
				shmReply *r = createReplyObject(SHM_REPLY_INTEGER);
				r->integer = value;
				if (task->parent) {
								shmReply *parent = task->parent->obj;
								assert(parent->type == SHM_REPLY_ARRAY);
								parent->element[task->idx] = r;
				}
				return r;
}

static void *createNilObject(const shmReadTask *task) {
				shmReply *r = createReplyObject(SHM_REPLY_NIL);
				if (task->parent) {
								shmReply *parent = task->parent->obj;
								assert(parent->type == SHM_REPLY_ARRAY);
								parent->element[task->idx] = r;
				}
				return r;
}

static char *readBytes(shmReader *r, unsigned int bytes) {
				char *p;
				if (r->len - r->pos >= bytes) {
								p = r->buf + r->pos;
								r->pos += bytes;
								return p;
				}
				return NULL;
}

static char *seekNewline(char *s, size_t len) {
				int pos = 0;
				int _len = len - 1;
				while (pos < _len) {
								while (pos < _len && s[pos] != '\r') pos++;
								if (s[pos] != '\r') {
												return NULL;
								} else {
												if (s[pos + 1] == '\n') {
																return s + pos;
												} else {
																pos++;
												}
								}
				}
				return NULL;
}

static long long readLongLong(char *s) {
				long long v = 0;
				int dec, mult = 1;
				char c;
				if (*s == '-') {
								mult = -1;
								s++;
				} else if (*s == '+') {
								mult = 1;
								s++;
				}
				while ((c = *(s++)) != '\r') {
								dec = c - '0';
								if (dec >= 0 && dec < 10) {
												v *= 10;
												v += dec;
								} else {
												return -1;
								}
				}
				return mult * v;
}

static char *readLine(shmReader *r, int *_len) {
				char *p, *s;
				int len;
				p = r->buf + r->pos;
				s = seekNewline(p, (r->len - r->pos));
				if (s != NULL) {
								len = s - (r->buf + r->pos);
								r->pos += len + 2;
								if (_len) *_len = len;
								return p;
				}
				return NULL;
}

static void moveToNextTask(shmReader *r) {
				shmReadTask *cur, *prv;
				while (r->ridx >= 0) {
								if (r->ridx == 0) {
												r->ridx--;
												return;
								}
								cur = &(r->rstack[r->ridx]);
								prv = &(r->rstack[r->ridx - 1]);
								assert(prv->type == SHM_REPLY_ARRAY);
								if (cur->idx == prv->elements - 1) {
												r->ridx--;
								} else {
												assert(cur->idx < prv->elements);
												cur->type = -1;
												cur->elements = -1;
												cur->idx++;
												return;
								}
				}
}

static int processLineItem(shmReader *r) {
				shmReadTask *cur = &(r->rstack[r->ridx]);
				void *obj;
				char *p;
				int len;
				if ((p = readLine(r, &len)) != NULL) {
								if (r->fn) {
												if (cur->type == SHM_REPLY_INTEGER) {
																obj = r->fn->createInteger(cur, readLongLong(p));
												} else {
																obj = r->fn->createString(cur, p, len);
												}
								} else {
												obj = (void *)(size_t)(cur->type);
								}
								if (r->ridx == 0) r->reply = obj;
								moveToNextTask(r);
								return 0;
				}
				return -1;
}

static int processBulkItem(shmReader *r) {
				shmReadTask *cur = &(r->rstack[r->ridx]);
				void *obj = NULL;
				char *p, *s;
				long len;
				unsigned long bytelen;
				int success = 0;
				p = r->buf + r->pos;
				s = seekNewline(p, r->len - r->pos);
				if (s != NULL) {
								p = r->buf + r->pos;
								bytelen = s - (r->buf + r->pos) + 2;
								len = readLongLong(p);
								if (len < 0) {
												obj = r->fn ? r->fn->createNil(cur) : (void *)SHM_REPLY_NIL;
												success = 1;
								} else {
												bytelen += len + 2;
												if (r->pos + bytelen <= r->len) {
																obj = r->fn ? r->fn->createString(cur, s + 2, len)
																      : (void *)SHM_REPLY_STRING;
																success = 1;
												}
								}
								if (success) {
												r->pos += bytelen;
												if (r->ridx == 0) r->reply = obj;
												moveToNextTask(r);
												return 0;
								}
				}
				return -1;
}

static int processBeginBulkItem(shmReader *r) {
				shmReadTask *cur = &(r->rstack[r->ridx]);
				void *obj;
				char *p;
				long elements;
				int root = 0;
				if (r->ridx == 8) {
								shmSetReplyReaderError(
												r, dstrcatprintf(
																dstrempty(),
																"No support for nested multi bulk replies with depth > 7"));
								return -1;
				}
				if ((p = readLine(r, NULL)) != NULL) {
								elements = readLongLong(p);
								root = (r->ridx == 0);
								if (elements == -1) {
												obj = r->fn ? r->fn->createNil(cur) : (void *)SHM_REPLY_NIL;
												moveToNextTask(r);
								} else {
												obj = r->fn ? r->fn->createArray(cur, elements) : (void *)SHM_REPLY_ARRAY;
												if (elements > 0) {
																cur->elements = elements;
																cur->obj = obj;
																r->ridx++;
																r->rstack[r->ridx].type = -1;
																r->rstack[r->ridx].elements = -1;
																r->rstack[r->ridx].idx = 0;
																r->rstack[r->ridx].obj = NULL;
																r->rstack[r->ridx].parent = cur;
																r->rstack[r->ridx].privdata = r->privdata;
												} else {
																moveToNextTask(r);
												}
								}
								if (root) r->reply = obj;
								return 0;
				}
				return -1;
}

static int processItem(shmReader *r) {
				shmReadTask *cur = &(r->rstack[r->ridx]);
				char *p;
				dstr byte;
				if (cur->type < 0) {
								if ((p = readBytes(r, 1)) != NULL) {
												switch (p[0]) {
												case '-':
																cur->type = SHM_REPLY_ERROR;
																break;
												case '+':
																cur->type = SHM_REPLY_STATUS;
																break;
												case ':':
																cur->type = SHM_REPLY_INTEGER;
																break;
												case '$':
																cur->type = SHM_REPLY_STRING;
																break;
												case '*':
																cur->type = SHM_REPLY_ARRAY;
																break;
												default:
																byte = dstrcatrepr(dstrempty(), p, 1);
																shmSetReplyReaderError(
																				r,
																				dstrcatprintf(dstrempty(),
																				             "Protocol error, got %s as reply type byte", byte));
																dstrfree(byte);
																return -1;
												}
								} else {
												return -1;
								}
				}
				switch (cur->type) {
				case SHM_REPLY_ERROR:
				case SHM_REPLY_STATUS:
				case SHM_REPLY_INTEGER:
								return processLineItem(r);
				case SHM_REPLY_STRING:
								return processBulkItem(r);
				case SHM_REPLY_ARRAY:
								return processBeginBulkItem(r);
				default:
								assert(NULL);
								return -1;
				}
}

void *shmReplyReaderCreate() {
				shmReader *r = calloc(sizeof(shmReader), 1);
				r->error = NULL;
				r->fn = &defaultFunctions;
				r->buf = dstrempty();
				r->ridx = -1;
				return r;
}

int shmReplyReaderSetReplyObjectFunctions(void *reader,
                                          shmReplyObjectFunctions *fn) {
				shmReader *r = reader;
				if (r->reply == NULL) {
								r->fn = fn;
								return SHM_OK;
				}
				return SHM_ERR;
}

int shmReplyReaderSetPrivdata(void *reader, void *privdata) {
				shmReader *r = reader;
				if (r->reply == NULL) {
								r->privdata = privdata;
								return SHM_OK;
				}
				return SHM_ERR;
}

void *shmReplyReaderGetObject(void *reader) {
				shmReader *r = reader;
				return r->reply;
}

void shmReplyReaderFree(void *reader) {
				shmReader *r = reader;
				if (r->error != NULL) dstrfree(r->error);
				if (r->reply != NULL && r->fn) r->fn->freeObject(r->reply);
				if (r->buf != NULL) dstrfree(r->buf);
				free(r);
}

static void shmSetReplyReaderError(shmReader *r, dstr err) {
				if (r->reply != NULL) r->fn->freeObject(r->reply);
				if (r->buf != NULL) {
								dstrfree(r->buf);
								r->buf = dstrempty();
								r->pos = 0;
				}
				r->ridx = -1;
				r->error = err;
}

char *shmReplyReaderGetError(void *reader) {
				shmReader *r = reader;
				return r->error;
}

void shmReplyReaderFeed(void *reader, char *buf, size_t len) {
				shmReader *r = reader;
				if (buf != NULL && len >= 1) {
								r->buf = dstrcatlen(r->buf, buf, len);
								r->len = dstrlen(r->buf);
				}
}

int shmReplyReaderGetReply(void *reader, void **reply) {
				shmReader *r = reader;
				if (reply != NULL) *reply = NULL;
				if (r->len == 0) return SHM_OK;
				if (r->ridx == -1) {
								r->rstack[0].type = -1;
								r->rstack[0].elements = -1;
								r->rstack[0].idx = -1;
								r->rstack[0].obj = NULL;
								r->rstack[0].parent = NULL;
								r->rstack[0].privdata = r->privdata;
								r->ridx = 0;
				}
				while (r->ridx >= 0)
								if (processItem(r) < 0) break;
				if (r->pos > 0) {
								if (r->pos == r->len) {
												dstrfree(r->buf);
												r->buf = dstrempty();
								} else {
												r->buf = dstrrange(r->buf, r->pos, r->len);
								}
								r->pos = 0;
								r->len = dstrlen(r->buf);
				}
				if (r->ridx == -1) {
								void *aux = r->reply;
								r->reply = NULL;
								if (r->len == 0 && dstravail(r->buf) > 16 * 1024) {
												dstrfree(r->buf);
												r->buf = dstrempty();
												r->pos = 0;
								}
								if (r->error != NULL) {
												return SHM_ERR;
								} else {
												if (reply != NULL) *reply = aux;
								}
				}
				return SHM_OK;
}

static int intlen(int i) {
				int len = 0;
				if (i < 0) {
								len++;
								i = -i;
				}
				do {
								len++;
								i /= 10;
				} while (i);
				return len;
}

static void addArgument(dstr a, char ***argv, int *argc, int *totlen) {
				(*argc)++;
				if ((*argv = realloc(*argv, sizeof(char *) * (*argc))) == NULL) shmOOM();
				if (totlen) *totlen = *totlen + 1 + intlen(dstrlen(a)) + 2 + dstrlen(a) + 2;
				(*argv)[(*argc) - 1] = a;
}

int shmvFormatCmd(char **target, const char *format, va_list ap) {
				size_t size;
				const char *arg, *c = format;
				char *cmd = NULL;
				int pos;
				dstr current;
				int interpolated = 0;
				char **argv = NULL;
				int argc = 0, j;
				int totlen = 0;
				if (target == NULL) return -1;
				current = dstrempty();
				while (*c != '\0') {
								if (*c != '%' || c[1] == '\0') {
												if (*c == ' ') {
																if (dstrlen(current) != 0) {
																				addArgument(current, &argv, &argc, &totlen);
																				current = dstrempty();
																				interpolated = 0;
																}
												} else {
																current = dstrcatlen(current, c, 1);
												}
								} else {
												switch (c[1]) {
												case 's':
																arg = va_arg(ap, char *);
																size = strlen(arg);
																if (size > 0) current = dstrcatlen(current, arg, size);
																interpolated = 1;
																break;
												case 'b':
																arg = va_arg(ap, char *);
																size = va_arg(ap, size_t);
																if (size > 0) current = dstrcatlen(current, arg, size);
																interpolated = 1;
																break;
												case '%':
																current = dstrcat(current, "%");
																break;
												default: {
																char _format[16];
																const char *_p = c + 1;
																size_t _l = 0;
																va_list _cpy;
																if (*_p != '\0' && *_p == '#') _p++;
																if (*_p != '\0' && *_p == '0') _p++;
																if (*_p != '\0' && *_p == '-') _p++;
																if (*_p != '\0' && *_p == ' ') _p++;
																if (*_p != '\0' && *_p == '+') _p++;
																while (*_p != '\0' && isdigit(*_p)) _p++;
																if (*_p == '.') {
																				_p++;
																				while (*_p != '\0' && isdigit(*_p)) _p++;
																}
																if (*_p != '\0') {
																				if (*_p == 'h' || *_p == 'l') {
																								if (_p[0] == _p[1]) _p++;
																								_p++;
																				}
																}
																if (*_p != '\0' && strchr("diouxXeEfFgGaA", *_p) != NULL) {
																				_l = (_p + 1) - c;
																				if (_l < sizeof(_format) - 2) {
																								memcpy(_format, c, _l);
																								_format[_l] = '\0';
																								va_copy(_cpy, ap);
																								current = dstrcatvprintf(current, _format, _cpy);
																								interpolated = 1;
																								va_end(_cpy);
																								c = _p - 1;
																				}
																}
																va_arg(ap, char*);
												}
												}
												c++;
								}
								c++;
				}
				if (interpolated || dstrlen(current) != 0) {
								addArgument(current, &argv, &argc, &totlen);
				} else {
								dstrfree(current);
				}
				totlen += 1 + intlen(argc) + 2;
				cmd = malloc(totlen + 1);
				if (!cmd) shmOOM();
				pos = sprintf(cmd, "*%d\r\n", argc);
				for (j = 0; j < argc; j++) {
								pos += sprintf(cmd + pos, "$%zu\r\n", dstrlen(argv[j]));
								memcpy(cmd + pos, argv[j], dstrlen(argv[j]));
								pos += dstrlen(argv[j]);
								dstrfree(argv[j]);
								cmd[pos++] = '\r';
								cmd[pos++] = '\n';
				}
				assert(pos == totlen);
				free(argv);
				cmd[totlen] = '\0';
				*target = cmd;
				return totlen;
}

int shmFormatCmd(char **target, const char *format, ...) {
				va_list ap;
				int len;
				va_start(ap, format);
				len = shmvFormatCmd(target, format, ap);
				va_end(ap);
				return len;
}

int shmFormatCommandArgv(char **target, int argc, const char **argv,
                         const size_t *argvlen) {
				char *cmd = NULL;
				int pos;
				size_t len;
				int totlen, j;
				totlen = 1 + intlen(argc) + 2;
				for (j = 0; j < argc; j++) {
								len = argvlen ? argvlen[j] : strlen(argv[j]);
								totlen += 1 + intlen(len) + 2 + len + 2;
				}
				cmd = malloc(totlen + 1);
				if (!cmd) shmOOM();
				pos = sprintf(cmd, "*%d\r\n", argc);
				for (j = 0; j < argc; j++) {
								len = argvlen ? argvlen[j] : strlen(argv[j]);
								pos += sprintf(cmd + pos, "$%zu\r\n", len);
								memcpy(cmd + pos, argv[j], len);
								pos += len;
								cmd[pos++] = '\r';
								cmd[pos++] = '\n';
				}
				assert(pos == totlen);
				cmd[totlen] = '\0';
				*target = cmd;
				return totlen;
}

void __shmSetError(shmContext *c, int type, const dstr errstr) {
				c->err = type;
				if (errstr != NULL) {
								c->errstr = errstr;
				} else {
								assert(type == SHM_ERR_IO);
								c->errstr = dstrnew(strerror(errno));
				}
}

static shmContext *shmContextInit() {
				shmContext *c = calloc(sizeof(shmContext), 1);
				c->err = 0;
				c->errstr = NULL;
				c->obuf = dstrempty();
				c->fn = &defaultFunctions;
				c->reader = NULL;
				return c;
}

void shmFree(shmContext *c) {
				if (c->flags & SHM_CONNECTED) close(c->fd);
				if (c->errstr != NULL) dstrfree(c->errstr);
				if (c->obuf != NULL) dstrfree(c->obuf);
				if (c->reader != NULL) shmReplyReaderFree(c->reader);
				free(c);
}

shmContext *shmConnect(const char *ip, int port) {
				shmContext *c = shmContextInit();
				c->flags |= SHM_BLOCK;
				shmContextConnectTcp(c, ip, port);
				return c;
}

shmContext *shmConnectNonBlock(const char *ip, int port) {
				shmContext *c = shmContextInit();
				c->flags &= ~SHM_BLOCK;
				shmContextConnectTcp(c, ip, port);
				return c;
}

shmContext *shmConnectUnix(const char *path) {
				shmContext *c = shmContextInit();
				c->flags |= SHM_BLOCK;
				shmContextConnectUnix(c, path);
				return c;
}

shmContext *shmConnectUnixNonBlock(const char *path) {
				shmContext *c = shmContextInit();
				c->flags &= ~SHM_BLOCK;
				shmContextConnectUnix(c, path);
				return c;
}

int shmSetReplyObjectFunctions(shmContext *c, shmReplyObjectFunctions *fn) {
				if (c->reader != NULL) return SHM_ERR;
				c->fn = fn;
				return SHM_OK;
}

static void __shmCreateReplyReader(shmContext *c) {
				if (c->reader == NULL) {
								c->reader = shmReplyReaderCreate();
								assert(shmReplyReaderSetReplyObjectFunctions(c->reader, c->fn) == SHM_OK);
				}
}

int shmBufferRead(shmContext *c) {
				char buf[2048];
				int nread = read(c->fd, buf, sizeof(buf));
				if (nread == -1) {
								if (errno == EAGAIN) {
								} else {
												__shmSetError(c, SHM_ERR_IO, NULL);
												return SHM_ERR;
								}
				} else if (nread == 0) {
								__shmSetError(c, SHM_ERR_EOF, dstrnew("Server closed the connection"));
								return SHM_ERR;
				} else {
								__shmCreateReplyReader(c);
								shmReplyReaderFeed(c->reader, buf, nread);
				}
				return SHM_OK;
}

int shmBufferWrite(shmContext *c, int *done) {
				int nwritten;
				if (dstrlen(c->obuf) > 0) {
								nwritten = write(c->fd, c->obuf, dstrlen(c->obuf));
								if (nwritten == -1) {
												if (errno == EAGAIN) {
												} else {
																__shmSetError(c, SHM_ERR_IO, NULL);
																return SHM_ERR;
												}
								} else if (nwritten > 0) {
												if (nwritten == (signed)dstrlen(c->obuf)) {
																dstrfree(c->obuf);
																c->obuf = dstrempty();
												} else {
																c->obuf = dstrrange(c->obuf, nwritten, -1);
												}
								}
				}
				if (done != NULL) *done = (dstrlen(c->obuf) == 0);
				return SHM_OK;
}

int shmGetReplyFromReader(shmContext *c, void **reply) {
				__shmCreateReplyReader(c);
				if (shmReplyReaderGetReply(c->reader, reply) == SHM_ERR) {
								__shmSetError(c, SHM_ERR_PROTOCOL, dstrnew(((shmReader *)c->reader)->error));
								return SHM_ERR;
				}
				return SHM_OK;
}

int shmGetReply(shmContext *c, void **reply) {
				int wdone = 0;
				void *aux = NULL;
				if (shmGetReplyFromReader(c, &aux) == SHM_ERR) return SHM_ERR;
				if (aux == NULL && c->flags & SHM_BLOCK) {
								do {
												if (shmBufferWrite(c, &wdone) == SHM_ERR) return SHM_ERR;
								} while (!wdone);
								do {
												if (shmBufferRead(c) == SHM_ERR) return SHM_ERR;
												if (shmGetReplyFromReader(c, &aux) == SHM_ERR) return SHM_ERR;
								} while (aux == NULL);
				}
				if (reply != NULL) *reply = aux;
				return SHM_OK;
}

void __shmAppendCmd(shmContext *c, char *cmd, size_t len) {
				c->obuf = dstrcatlen(c->obuf, cmd, len);
}

void shmvAppendCmd(shmContext *c, const char *format, va_list ap) {
				char *cmd;
				int len;
				len = shmvFormatCmd(&cmd, format, ap);
				__shmAppendCmd(c, cmd, len);
				free(cmd);
}

void shmAppendCmd(shmContext *c, const char *format, ...) {
				va_list ap;
				va_start(ap, format);
				shmvAppendCmd(c, format, ap);
				va_end(ap);
}

void shmAppendCommandArgv(shmContext *c, int argc, const char **argv,
                          const size_t *argvlen) {
				char *cmd;
				int len;
				len = shmFormatCommandArgv(&cmd, argc, argv, argvlen);
				__shmAppendCmd(c, cmd, len);
				free(cmd);
}

static void *__cmdShm(shmContext *c, char *cmd, size_t len) {
				void *aux = NULL;
				__shmAppendCmd(c, cmd, len);
				if (c->flags & SHM_BLOCK) {
								if (shmGetReply(c, &aux) == SHM_OK) return aux;
								return NULL;
				}
				return NULL;
}

void *shmvCmd(shmContext *c, const char *format, va_list ap) {
				char *cmd;
				int len;
				void *reply = NULL;
				len = shmvFormatCmd(&cmd, format, ap);
				reply = __cmdShm(c, cmd, len);
				free(cmd);
				return reply;
}

void *cmdShm(shmContext *c, const char *format, ...) {
				va_list ap;
				void *reply = NULL;
				va_start(ap, format);
				reply = shmvCmd(c, format, ap);
				va_end(ap);
				return reply;
}

void *cmdShmArgv(shmContext *c, int argc, const char **argv,
                     const size_t *argvlen) {
				char *cmd;
				int len;
				void *reply = NULL;
				len = shmFormatCommandArgv(&cmd, argc, argv, argvlen);
				reply = __cmdShm(c, cmd, len);
				free(cmd);
				return reply;
}
