#ifndef __SHM_CLI_H
#define __SHM_CLI_H
#include <stdio.h>
#include <stdarg.h>
#define SHM_CLI_MAJOR 0
#define SHM_CLI_MINOR 9
#define SHM_CLI_PATCH 2
#define SHM_ERR -1
#define SHM_OK 0
#define SHM_ERR_IO 1
#define SHM_ERR_EOF 3
#define SHM_ERR_PROTOCOL 4
#define SHM_ERR_OTHER 2
#define SHM_BLOCK 0x1
#define SHM_CONNECTED 0x2
#define SHM_DISCONNECTING 0x4
#define SHM_REPLY_STRING 1
#define SHM_REPLY_ARRAY 2
#define SHM_REPLY_INTEGER 3
#define SHM_REPLY_NIL 4
#define SHM_REPLY_STATUS 5
#define SHM_REPLY_ERROR 6
#ifdef __cplusplus
extern "C" {
#endif
typedef struct shmReply {
				int type;
				long long integer;
				int len;
				char *str;
				size_t elements;
				struct shmReply **element;
} shmReply;
typedef struct shmReadTask {
				int type;
				int elements;
				int idx;
				void *obj;
				struct shmReadTask *parent;
				void *privdata;
} shmReadTask;
typedef struct shmReplyObjectFunctions {
				void *(*createString)(const shmReadTask *, char *, size_t);
				void *(*createArray)(const shmReadTask *, int);
				void *(*createInteger)(const shmReadTask *, long long);
				void *(*createNil)(const shmReadTask *);
				void (*freeObject)(void *);
} shmReplyObjectFunctions;
struct shmContext;
typedef struct shmContext {
				int fd;
				int flags;
				char *obuf;
				int err;
				char *errstr;
				shmReplyObjectFunctions *fn;
				void *reader;
} shmContext;

void freeReplyObject(void *reply);

void *shmReplyReaderCreate();

int shmReplyReaderSetReplyObjectFunctions(void *reader,
                                          shmReplyObjectFunctions *fn);

int shmReplyReaderSetPrivdata(void *reader, void *privdata);

void *shmReplyReaderGetObject(void *reader);

char *shmReplyReaderGetError(void *reader);

void shmReplyReaderFree(void *ptr);

void shmReplyReaderFeed(void *reader, char *buf, size_t len);

int shmReplyReaderGetReply(void *reader, void **reply);

int shmvFormatCmd(char **target, const char *format, va_list ap);

int shmFormatCmd(char **target, const char *format, ...);

int shmFormatCommandArgv(char **target, int argc, const char **argv,
                         const size_t *argvlen);

shmContext *shmConnect(const char *ip, int port);

shmContext *shmConnectNonBlock(const char *ip, int port);

shmContext *shmConnectUnix(const char *path);

shmContext *shmConnectUnixNonBlock(const char *path);

int shmSetReplyObjectFunctions(shmContext *c, shmReplyObjectFunctions *fn);

void shmFree(shmContext *c);

int shmBufferRead(shmContext *c);

int shmBufferWrite(shmContext *c, int *done);

int shmGetReply(shmContext *c, void **reply);

int shmGetReplyFromReader(shmContext *c, void **reply);

void shmvAppendCmd(shmContext *c, const char *format, va_list ap);

void shmAppendCmd(shmContext *c, const char *format, ...);

void shmAppendCommandArgv(shmContext *c, int argc, const char **argv,
                          const size_t *argvlen);

void *shmvCmd(shmContext *c, const char *format, va_list ap);

void *cmdShm(shmContext *c, const char *format, ...);

void *cmdShmArgv(shmContext *c, int argc, const char **argv,
                     const size_t *argvlen);

#ifdef __cplusplus
}
#endif
#endif
