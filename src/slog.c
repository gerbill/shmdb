#include "shm.h"
#include "slog.h"

slogEntry *slogCreateEntry(robj **argv, int argc, long long duration) {
				slogEntry *se = dbmalloc(sizeof(*se));
				int j;
				se->argc = argc;
				se->argv = dbmalloc(sizeof(robj *) * argc);
				for (j = 0; j < argc; j++) {
								se->argv[j] = argv[j];
								objIncRefCount(argv[j]);
				}
				se->time = time(NULL);
				se->duration = duration;
				se->id = server.slog_entry_id++;
				return se;
}

void slogFreeEntry(void *septr) {
				slogEntry *se = septr;
				int j;
				for (j = 0; j < se->argc; j++) objDecRefCount(se->argv[j]);
				dbfree(se->argv);
				dbfree(se);
}

void slogInit(void) {
				server.slog = lCreate();
				server.slog_entry_id = 0;
				lSetFreeMethod(server.slog, slogFreeEntry);
}

void slogPushEntryIfNeeded(robj **argv, int argc, long long duration) {
				if (server.slog_log_slower_than < 0) return;
				if (duration >= server.slog_log_slower_than)
					lAddNodeHead(server.slog, slogCreateEntry(argv, argc, duration));
				while (lLength(server.slog) > server.slog_max_len)
					lDelNode(server.slog, lLast(server.slog));
}

void slogReset(void) {
				while (lLength(server.slog) > 0)
					lDelNode(server.slog, lLast(server.slog));
}

void slogCmd(shmClient *c) {
				if (c->argc == 2 && !strcasecmp(c->argv[1]->ptr, "reset")) {
								slogReset();
								netAdd(c, shared.ok);
				} else if (c->argc == 2 && !strcasecmp(c->argv[1]->ptr, "len")) {
								netAddLongLong(c, lLength(server.slog));
				} else if ((c->argc == 2 || c->argc == 3) &&
				           !strcasecmp(c->argv[1]->ptr, "get")) {
								long count = 10, sent = 0;
								lIter li;
								void *totentries;
								lNode *ln;
								slogEntry *se;
								if (c->argc == 3 &&
								    objGetLongFromOrReply(c, c->argv[2], &count, NULL) != SHM_OK)
												return;
					lRewind(server.slog, &li);
								totentries = addDeferredBeginBulkLength(c);
								while (count-- && (ln = lNext(&li))) {
												int j;
												se = ln->value;
												netAddBeginBulkLen(c, 4);
												netAddLongLong(c, se->id);
												netAddLongLong(c, se->time);
												netAddLongLong(c, se->duration);
												netAddBeginBulkLen(c, se->argc);
												for (j = 0; j < se->argc; j++) netAddBulk(c, se->argv[j]);
												sent++;
								}
								setDeferredBeginBulkLength(c, totentries, sent);
				} else {
								netAddError(
												c,
												"Unknown SLOG subcommand or wrong # of args. Try GET, RESET, LEN.");
				}
}
