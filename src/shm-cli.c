#include "fmacros.h"
#include "version.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <assert.h>
#include "shm_cli.h"
#include "dstr.h"
#include "dbmalloc.h"
#include "simplerl.h"
#include "help.h"
#define SHM_NOTUSED(V) ((void)V)
static shmContext *context;
static struct config {
				char *hostip;
				int hostport;
				char *hostsocket;
				long repeat;
				long interval;
				int dbnum;
				int interactive;
				int shutdown;
				int monitor_mode;
				int msg_mode;
				int latency_mode;
				int stdinarg;
				char *auth;
				int raw_output;
				dstr mb_delim;
				char prompt[32];
} config;

static void usage();

char *shmGitSHA1(void);

char *shmGitDirty(void);

static long long mstime(void) {
				struct timeval tv;
				long long mst;
				gettimeofday(&tv, NULL);
				mst = ((long)tv.tv_sec) * 1000;
				mst += tv.tv_usec / 1000;
				return mst;
}

static void cliRefreshPrompt(void) {
				if (config.dbnum == 0)
								snprintf(config.prompt, sizeof(config.prompt), "shm %s:%d> ", config.hostip,
								         config.hostport);
				else
								snprintf(config.prompt, sizeof(config.prompt), "shm %s:%d[%d]> ",
								         config.hostip, config.hostport, config.dbnum);
}
#define CLI_HELP_COMMAND 1
#define CLI_HELP_GROUP 2
typedef struct {
				int type;
				int argc;
				dstr *argv;
				dstr full;
				struct commandHelp *org;
} helpEntry;
static helpEntry *helpEntries;
static int helpEntriesLen;

static dstr cliVersion() {
				dstr version;
				version = dstrcatprintf(dstrempty(), "%s", SHM_VERSION);
				if (strtoll(shmGitSHA1(), NULL, 16)) {
								version = dstrcatprintf(version, " (git:%s", shmGitSHA1());
								if (strtoll(shmGitDirty(), NULL, 10))
												version = dstrcatprintf(version, "-dirty");
								version = dstrcat(version, ")");
				}
				return version;
}

static void cliInitHelp() {
				int commandslen = sizeof(commandHelp) / sizeof(struct commandHelp);
				int groupslen = sizeof(commandGroups) / sizeof(char *);
				int i, len, pos = 0;
				helpEntry tmp;
				helpEntriesLen = len = commandslen + groupslen;
				helpEntries = malloc(sizeof(helpEntry) * len);
				for (i = 0; i < groupslen; i++) {
								tmp.argc = 1;
								tmp.argv = malloc(sizeof(dstr));
								tmp.argv[0] = dstrcatprintf(dstrempty(), "@%s", commandGroups[i]);
								tmp.full = tmp.argv[0];
								tmp.type = CLI_HELP_GROUP;
								tmp.org = NULL;
								helpEntries[pos++] = tmp;
				}
				for (i = 0; i < commandslen; i++) {
								tmp.argv = dstrsplitargs(commandHelp[i].name, &tmp.argc);
								tmp.full = dstrnew(commandHelp[i].name);
								tmp.type = CLI_HELP_COMMAND;
								tmp.org = &commandHelp[i];
								helpEntries[pos++] = tmp;
				}
}

static void cliOutputCommandHelp(struct commandHelp *help, int group) {
				printf("\r\n  %s %s\r\n", help->name,
				       help->params);
				printf("  %s\r\n", help->summary);
				if (group) {
								printf("  group: %s\r\n", commandGroups[help->group]);
				}
}

static void cliOutputGenericHelp() {
				dstr version = cliVersion();
				printf(
								"shm-cli %s\r\n"
								"Type: \"help @<group>\" to get a list of commands in <group>\r\n"
								"      \"help <command>\" for help on <command>\r\n"
								"      \"help <tab>\" to get a list of possible help topics\r\n"
								"      \"quit\" to exit\r\n",
								version);
				dstrfree(version);
}

static void cliOutputHelp(int argc, char **argv) {
				int i, j, len;
				int group = -1;
				helpEntry *entry;
				struct commandHelp *help;
				if (argc == 0) {
								cliOutputGenericHelp();
								return;
				} else if (argc > 0 && argv[0][0] == '@') {
								len = sizeof(commandGroups) / sizeof(char *);
								for (i = 0; i < len; i++) {
												if (strcasecmp(argv[0] + 1, commandGroups[i]) == 0) {
																group = i;
																break;
												}
								}
				}
				assert(argc > 0);
				for (i = 0; i < helpEntriesLen; i++) {
								entry = &helpEntries[i];
								if (entry->type != CLI_HELP_COMMAND) continue;
								help = entry->org;
								if (group == -1) {
												if (argc == entry->argc) {
																for (j = 0; j < argc; j++) {
																				if (strcasecmp(argv[j], entry->argv[j]) != 0) break;
																}
																if (j == argc) {
																				cliOutputCommandHelp(help, 1);
																}
												}
								} else {
												if (group == help->group) {
																cliOutputCommandHelp(help, 0);
												}
								}
				}
				printf("\r\n");
}

static void completionCallback(const char *buf, simplerlCompletions *lc) {
				size_t startpos = 0;
				int mask;
				int i;
				size_t matchlen;
				dstr tmp;
				if (strncasecmp(buf, "help ", 5) == 0) {
								startpos = 5;
								while (isspace(buf[startpos])) startpos++;
								mask = CLI_HELP_COMMAND | CLI_HELP_GROUP;
				} else {
								mask = CLI_HELP_COMMAND;
				}
				for (i = 0; i < helpEntriesLen; i++) {
								if (!(helpEntries[i].type & mask)) continue;
								matchlen = strlen(buf + startpos);
								if (strncasecmp(buf + startpos, helpEntries[i].full, matchlen) == 0) {
												tmp = dstrnewlen(buf, startpos);
												tmp = dstrcat(tmp, helpEntries[i].full);
												simplerlAddCompletion(lc, tmp);
												dstrfree(tmp);
								}
				}
}

static int cliAuth() {
				shmReply *reply;
				if (config.auth == NULL) return SHM_OK;
				reply = cmdShm(context, "AUTH %s", config.auth);
				if (reply != NULL) {
								freeReplyObject(reply);
								return SHM_OK;
				}
				return SHM_ERR;
}

static int cliSelect() {
				shmReply *reply;
				if (config.dbnum == 0) return SHM_OK;
				reply = cmdShm(context, "SELECT %d", config.dbnum);
				if (reply != NULL) {
								freeReplyObject(reply);
								return SHM_OK;
				}
				return SHM_ERR;
}

static int cliConnect(int force) {
				if (context == NULL || force) {
								if (context != NULL) shmFree(context);
								if (config.hostsocket == NULL) {
												context = shmConnect(config.hostip, config.hostport);
								} else {
												context = shmConnectUnix(config.hostsocket);
								}
								if (context->err) {
												fprintf(stderr, "Could not connect to Shm at ");
												if (config.hostsocket == NULL)
																fprintf(stderr, "%s:%d: %s\n", config.hostip, config.hostport,
																        context->errstr);
												else
																fprintf(stderr, "%s: %s\n", config.hostsocket, context->errstr);
												shmFree(context);
												context = NULL;
												return SHM_ERR;
								}
								if (cliAuth() != SHM_OK) return SHM_ERR;
								if (cliSelect() != SHM_OK) return SHM_ERR;
				}
				return SHM_OK;
}

static void cliPrintContextError() {
				if (context == NULL) return;
				fprintf(stderr, "Error: %s\n", context->errstr);
}

static dstr cliFormatReplyTTY(shmReply *r, char *prefix) {
				dstr out = dstrempty();
				switch (r->type) {
				case SHM_REPLY_ERROR:
								out = dstrcatprintf(out, "(error) %s\n", r->str);
								break;
				case SHM_REPLY_STATUS:
								out = dstrcat(out, r->str);
								out = dstrcat(out, "\n");
								break;
				case SHM_REPLY_INTEGER:
								out = dstrcatprintf(out, "(integer) %lld\n", r->integer);
								break;
				case SHM_REPLY_STRING:
								out = dstrcatrepr(out, r->str, r->len);
								out = dstrcat(out, "\n");
								break;
				case SHM_REPLY_NIL:
								out = dstrcat(out, "(nil)\n");
								break;
				case SHM_REPLY_ARRAY:
								if (r->elements == 0) {
												out = dstrcat(out, "(empty list or set)\n");
								} else {
												unsigned int i, idxlen = 0;
												char _prefixlen[16];
												char _prefixfmt[16];
												dstr _prefix;
												dstr tmp;
												i = r->elements;
												do {
																idxlen++;
																i /= 10;
												} while (i);
												memset(_prefixlen, ' ', idxlen + 2);
												_prefixlen[idxlen + 2] = '\0';
												_prefix = dstrcat(dstrnew(prefix), _prefixlen);
												snprintf(_prefixfmt, sizeof(_prefixfmt), "%%s%%%dd) ", idxlen);
												for (i = 0; i < r->elements; i++) {
																out = dstrcatprintf(out, _prefixfmt, i == 0 ? "" : prefix, i + 1);
																tmp = cliFormatReplyTTY(r->element[i], _prefix);
																out = dstrcatlen(out, tmp, dstrlen(tmp));
																dstrfree(tmp);
												}
												dstrfree(_prefix);
								}
								break;
				default:
								fprintf(stderr, "Unknown reply type: %d\n", r->type);
								exit(1);
				}
				return out;
}

static dstr cliFormatReplyRaw(shmReply *r) {
				dstr out = dstrempty(), tmp;
				size_t i;
				switch (r->type) {
				case SHM_REPLY_NIL:
								break;
				case SHM_REPLY_ERROR:
								out = dstrcatlen(out, r->str, r->len);
								out = dstrcatlen(out, "\n", 1);
								break;
				case SHM_REPLY_STATUS:
				case SHM_REPLY_STRING:
								out = dstrcatlen(out, r->str, r->len);
								break;
				case SHM_REPLY_INTEGER:
								out = dstrcatprintf(out, "%lld", r->integer);
								break;
				case SHM_REPLY_ARRAY:
								for (i = 0; i < r->elements; i++) {
												if (i > 0) out = dstrcat(out, config.mb_delim);
												tmp = cliFormatReplyRaw(r->element[i]);
												out = dstrcatlen(out, tmp, dstrlen(tmp));
												dstrfree(tmp);
								}
								break;
				default:
								fprintf(stderr, "Unknown reply type: %d\n", r->type);
								exit(1);
				}
				return out;
}

static int cliReadReply(int output_raw_strings) {
				void *_reply;
				shmReply *reply;
				dstr out;
				if (shmGetReply(context, &_reply) != SHM_OK) {
								if (config.shutdown) return SHM_OK;
								if (config.interactive) {
												if (context->err == SHM_ERR_IO && errno == ECONNRESET) return SHM_ERR;
												if (context->err == SHM_ERR_EOF) return SHM_ERR;
								}
								cliPrintContextError();
								exit(1);
								return SHM_ERR;
				}
				reply = (shmReply *)_reply;
				if (output_raw_strings) {
								out = cliFormatReplyRaw(reply);
				} else {
								if (config.raw_output) {
												out = cliFormatReplyRaw(reply);
												out = dstrcat(out, "\n");
								} else {
												out = cliFormatReplyTTY(reply, "");
								}
				}
				fwrite(out, dstrlen(out), 1, stdout);
				dstrfree(out);
				freeReplyObject(reply);
				return SHM_OK;
}

static int cliSendCmd(int argc, char **argv, int repeat) {
				char *command = argv[0];
				size_t *argvlen;
				int j, output_raw;
				if (context == NULL) return SHM_ERR;
				output_raw = 0;
				if (!strcasecmp(command, "info") ||
				    (argc == 2 && !strcasecmp(command, "client") &&
				     !strcasecmp(argv[1], "list"))) {
								output_raw = 1;
				}
				if (!strcasecmp(command, "help") || !strcasecmp(command, "?")) {
								cliOutputHelp(--argc, ++argv);
								return SHM_OK;
				}
				if (!strcasecmp(command, "shutdown")) config.shutdown = 1;
				if (!strcasecmp(command, "monitor")) config.monitor_mode = 1;
				if (!strcasecmp(command, "subscribe") || !strcasecmp(command, "psubscribe"))
								config.msg_mode = 1;
				argvlen = malloc(argc * sizeof(size_t));
				for (j = 0; j < argc; j++) argvlen[j] = dstrlen(argv[j]);
				while (repeat--) {
								shmAppendCommandArgv(context, argc, (const char **)argv, argvlen);
								while (config.monitor_mode) {
												if (cliReadReply(output_raw) != SHM_OK) exit(1);
												fflush(stdout);
								}
								if (config.msg_mode) {
												if (!config.raw_output)
																printf("Reading messages... (press Ctrl-C to quit)\n");
												while (1) {
																if (cliReadReply(output_raw) != SHM_OK) exit(1);
												}
								}
								if (cliReadReply(output_raw) != SHM_OK) {
												free(argvlen);
												return SHM_ERR;
								} else {
												if (!strcasecmp(command, "select") && argc == 2) {
																config.dbnum = atoi(argv[1]);
																cliRefreshPrompt();
												}
								}
								if (config.interval) usleep(config.interval);
								fflush(stdout);
				}
				free(argvlen);
				return SHM_OK;
}

static int parseOptions(int argc, char **argv) {
				int i;
				for (i = 1; i < argc; i++) {
								int lastarg = i == argc - 1;
								if (!strcmp(argv[i], "-h") && !lastarg) {
												dstrfree(config.hostip);
												config.hostip = dstrnew(argv[i + 1]);
												i++;
								} else if (!strcmp(argv[i], "-h") && lastarg) {
												usage();
								} else if (!strcmp(argv[i], "--help")) {
												usage();
								} else if (!strcmp(argv[i], "-x")) {
												config.stdinarg = 1;
								} else if (!strcmp(argv[i], "-p") && !lastarg) {
												config.hostport = atoi(argv[i + 1]);
												i++;
								} else if (!strcmp(argv[i], "-s") && !lastarg) {
												config.hostsocket = argv[i + 1];
												i++;
								} else if (!strcmp(argv[i], "-r") && !lastarg) {
												config.repeat = strtoll(argv[i + 1], NULL, 10);
												i++;
								} else if (!strcmp(argv[i], "-i") && !lastarg) {
												double seconds = atof(argv[i + 1]);
												config.interval = seconds * 1000000;
												i++;
								} else if (!strcmp(argv[i], "-n") && !lastarg) {
												config.dbnum = atoi(argv[i + 1]);
												i++;
								} else if (!strcmp(argv[i], "-a") && !lastarg) {
												config.auth = argv[i + 1];
												i++;
								} else if (!strcmp(argv[i], "--raw")) {
												config.raw_output = 1;
								} else if (!strcmp(argv[i], "--latency")) {
												config.latency_mode = 1;
								} else if (!strcmp(argv[i], "-d") && !lastarg) {
												dstrfree(config.mb_delim);
												config.mb_delim = dstrnew(argv[i + 1]);
												i++;
								} else if (!strcmp(argv[i], "-v") || !strcmp(argv[i], "--version")) {
												dstr version = cliVersion();
												printf("shm-cli %s\n", version);
												dstrfree(version);
												exit(0);
								} else {
												break;
								}
				}
				return i;
}

static dstr readArgFromStdin(void) {
				char buf[1024];
				dstr arg = dstrempty();
				while (1) {
								int nread = read(fileno(stdin), buf, 1024);
								if (nread == 0)
												break;
								else if (nread == -1) {
												perror("Reading from standard input");
												exit(1);
								}
								arg = dstrcatlen(arg, buf, nread);
				}
				return arg;
}

static void usage() {
				dstr version = cliVersion();
				fprintf(
								stderr,
								"shm-cli %s\n"
								"\n"
								"Usage: shm-cli [OPTIONS] [cmd [arg [arg ...]]]\n"
								"  -h <hostname>    Server hostname (default: 127.0.0.1)\n"
								"  -p <port>        Server port (default: 20971)\n"
								"  -s <socket>      Server socket (overrides hostname and port)\n"
								"  -a <password>    Password to use when connecting to the server\n"
								"  -r <repeat>      Execute specified command N times\n"
								"  -i <interval>    When -r is used, waits <interval> seconds per "
								"command.\n"
								"                   It is possible to specify sub-second times like -i "
								"0.1.\n"
								"  -n <db>          Database number\n"
								"  -x               Read last argument from STDIN\n"
								"  -d <delimiter>   Multi-bulk delimiter in for raw formatting (default: "
								"\\n)\n"
								"  --raw            Use raw formatting for replies (default when STDOUT "
								"is not a tty)\n"
								"  --latency        Enter a special mode continuously sampling latency.\n"
								"  --help           Output this help and exit\n"
								"  --version        Output version and exit\n"
								"\n"
								"Examples:\n"
								"  cat /etc/passwd | shm-cli -x set mypasswd\n"
								"  shm-cli get mypasswd\n"
								"  shm-cli -r 100 lpush mylist x\n"
								"  shm-cli -r 100 -i 1 info | grep used_memory_human:\n"
								"\n"
								"When no command is given, shm-cli starts in interactive mode.\n"
								"Type \"help\" in interactive mode for information on available "
								"commands.\n"
								"\n",
								version);
				dstrfree(version);
				exit(1);
}

static char **convertToDstR(int count, char **args) {
				int j;
				char **dstr = dbmalloc(sizeof(char *) * count);
				for (j = 0; j < count; j++) dstr[j] = dstrnew(args[j]);
				return dstr;
}
#define LINE_BUFLEN 4096

static void repl() {
				dstr historyfile = NULL;
				int history = 0;
				char *line;
				int argc;
				dstr *argv;
				config.interactive = 1;
				simplerlSetCompletionCallback(completionCallback);
				if (isatty(fileno(stdin))) {
								history = 1;
								if (getenv("HOME") != NULL) {
												historyfile =
																dstrcatprintf(dstrempty(), "%s/.shmcli_history", getenv("HOME"));
												simplerlHistoryLoad(historyfile);
								}
				}
				cliRefreshPrompt();
				while ((line = simplerl(context ? config.prompt : "not connected> ")) !=
				       NULL) {
								if (line[0] != '\0') {
												argv = dstrsplitargs(line, &argc);
												if (history) simplerlHistoryAdd(line);
												if (historyfile) simplerlHistorySave(historyfile);
												if (argv == NULL) {
																printf("Invalid argument(s)\n");
																continue;
												} else if (argc > 0) {
																if (strcasecmp(argv[0], "quit") == 0 ||
																    strcasecmp(argv[0], "exit") == 0) {
																				exit(0);
																} else if (argc == 3 && !strcasecmp(argv[0], "connect")) {
																				dstrfree(config.hostip);
																				config.hostip = dstrnew(argv[1]);
																				config.hostport = atoi(argv[2]);
																				cliConnect(1);
																} else if (argc == 1 && !strcasecmp(argv[0], "clear")) {
																				simplerlClearScreen();
																} else {
																				long long start_time = mstime(), elapsed;
																				int repeat, skipargs = 0;
																				repeat = atoi(argv[0]);
																				if (argc > 1 && repeat) {
																								skipargs = 1;
																				} else {
																								repeat = 1;
																				}
																				if (cliSendCmd(argc - skipargs, argv + skipargs, repeat) !=
																				    SHM_OK) {
																								cliConnect(1);
																								if (cliSendCmd(argc - skipargs, argv + skipargs, repeat) !=
																								    SHM_OK)
																												cliPrintContextError();
																				}
																				elapsed = mstime() - start_time;
																				if (elapsed >= 500) {
																								printf("(%.2fs)\n", (double)elapsed / 1000);
																				}
																}
												}
												while (argc--) dstrfree(argv[argc]);
												dbfree(argv);
								}
								free(line);
				}
				exit(0);
}

static int noninteractive(int argc, char **argv) {
				int retval = 0;
				if (config.stdinarg) {
								argv = dbrealloc(argv, (argc + 1) * sizeof(char *));
								argv[argc] = readArgFromStdin();
								retval = cliSendCmd(argc + 1, argv, config.repeat);
				} else {
								retval = cliSendCmd(argc, argv, config.repeat);
				}
				return retval;
}

static void latencyMode(void) {
				shmReply *reply;
				long long start, latency, min, max, tot, count = 0;
				double avg;
				if (!context) exit(1);
				while (1) {
								start = mstime();
								reply = cmdShm(context, "PING");
								if (reply == NULL) {
												fprintf(stderr, "\nI/O error\n");
												exit(1);
								}
								latency = mstime() - start;
								freeReplyObject(reply);
								count++;
								if (count == 1) {
												min = max = tot = latency;
												avg = (double)latency;
								} else {
												if (latency < min) min = latency;
												if (latency > max) max = latency;
												tot += latency;
												avg = (double)tot / count;
								}
								printf("\x1b[0G\x1b[2Kmin: %lld, max: %lld, avg: %.2f (%lld samples)", min,
								       max, avg, count);
								fflush(stdout);
								usleep(10000);
				}
}

int main(int argc, char **argv) {
				int firstarg;
				config.hostip = dstrnew("127.0.0.1");
				config.hostport = 20971;
				config.hostsocket = NULL;
				config.repeat = 1;
				config.interval = 0;
				config.dbnum = 0;
				config.interactive = 0;
				config.shutdown = 0;
				config.monitor_mode = 0;
				config.msg_mode = 0;
				config.latency_mode = 0;
				config.stdinarg = 0;
				config.auth = NULL;
				config.raw_output = !isatty(fileno(stdout)) && (getenv("FAKETTY") == NULL);
				config.mb_delim = dstrnew("\n");
				cliInitHelp();
				firstarg = parseOptions(argc, argv);
				argc -= firstarg;
				argv += firstarg;
				if (config.latency_mode) {
								cliConnect(0);
								latencyMode();
				}
				if (argc == 0) {
								cliConnect(0);
								repl();
				}
				if (cliConnect(0) != SHM_OK) exit(1);
				return noninteractive(argc, convertToDstR(argc, argv));
}
