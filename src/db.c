#include "shm.h"
#include <signal.h>

robj *lookupKey(shmDb *db, robj *key) {
				hashEntry *de = hashFind(db->hash, key->ptr);
				if (de) {
								robj *val = hashGetEntryVal(de);
								if (server.asavechildpid == -1 && server.bgrewritechildpid == -1)
												val->lru = server.lruclock;
								if (server.vm_enabled) {
												if (val->storage == SHM_VM_MEMORY || val->storage == SHM_VM_SWAPPING) {
																if (val->storage == SHM_VM_SWAPPING) vmStopThreadedIOJob(val);
												} else {
																int notify = (val->storage == SHM_VM_LOADING);
																shmAssert(val->type == SHM_VMPTR);
																val = vmReadObj(val);
																hashGetEntryVal(de) = val;
																if (notify) vmHandleClientsBlockedOnSwappedKey(db, key);
												}
								}
								server.stat_keyspace_hits++;
								return val;
				} else {
								server.stat_keyspace_misses++;
								return NULL;
				}
}

robj *lookupKeyRead(shmDb *db, robj *key) {
				expireIfNeeded(db, key);
				return lookupKey(db, key);
}

robj *lookupKeyWrite(shmDb *db, robj *key) {
				expireIfNeeded(db, key);
				return lookupKey(db, key);
}

robj *lookupKeyReadOrReply(shmClient *c, robj *key, robj *reply) {
				robj *o = lookupKeyRead(c->db, key);
				if (!o) netAdd(c, reply);
				return o;
}

robj *lookupKeyWriteOrReply(shmClient *c, robj *key, robj *reply) {
				robj *o = lookupKeyWrite(c->db, key);
				if (!o) netAdd(c, reply);
				return o;
}

int dbAdd(shmDb *db, robj *key, robj *val) {
				if (hashFind(db->hash, key->ptr) != NULL) {
								return SHM_ERR;
				} else {
								dstr copy = dstrdup(key->ptr);
								hashAdd(db->hash, copy, val);
								return SHM_OK;
				}
}

int dbReplace(shmDb *db, robj *key, robj *val) {
				if (hashFind(db->hash, key->ptr) == NULL) {
								dstr copy = dstrdup(key->ptr);
								hashAdd(db->hash, copy, val);
								return 1;
				} else {
								hashReplace(db->hash, key->ptr, val);
								return 0;
				}
}

int dbExists(shmDb *db, robj *key) {
				return hashFind(db->hash, key->ptr) != NULL;
}

robj *dbRandomKey(shmDb *db) {
				struct hashEntry *de;
				while (1) {
								dstr key;
								robj *keyobj;
								de = hashGetRandomKey(db->hash);
								if (de == NULL) return NULL;
								key = hashGetEntryKey(de);
								keyobj = objCreateString(key, dstrlen(key));
								if (hashFind(db->expires, key)) {
												if (expireIfNeeded(db, keyobj)) {
																objDecRefCount(keyobj);
																continue;
												}
								}
								return keyobj;
				}
}

int dbDelete(shmDb *db, robj *key) {
				if (server.vm_enabled) vmHandleClientsBlockedOnSwappedKey(db, key);
				if (hashSize(db->expires) > 0) hashDelete(db->expires, key->ptr);
				return hashDelete(db->hash, key->ptr) == HASH_OK;
}

long long emptyDb() {
				int j;
				long long removed = 0;
				for (j = 0; j < server.dbnum; j++) {
								removed += hashSize(server.db[j].hash);
								hashEmpty(server.db[j].hash);
								hashEmpty(server.db[j].expires);
				}
				return removed;
}

int selectDb(shmClient *c, int id) {
				if (id < 0 || id >= server.dbnum) return SHM_ERR;
				c->db = &server.db[id];
				return SHM_OK;
}

void dropdbCmd(shmClient *c) {
				server.dirty += hashSize(c->db->hash);
				touchWatchedKeysOnFlush(c->db->id);
				hashEmpty(c->db->hash);
				hashEmpty(c->db->expires);
				netAdd(c, shared.ok);
}

void dropallCmd(shmClient *c) {
				touchWatchedKeysOnFlush(-1);
				server.dirty += emptyDb();
				netAdd(c, shared.ok);
				if (server.asavechildpid != -1) {
								kill(server.asavechildpid, SIGKILL);
								sdbRemoveTempFile(server.asavechildpid);
				}
				if (server.saveparamslen > 0) {
								int saved_dirty = server.dirty;
								sdbSave(server.dbfilename);
								server.dirty = saved_dirty;
				}
				server.dirty++;
}

void delCmd(shmClient *c) {
				int deleted = 0, j;
				for (j = 1; j < c->argc; j++) {
								if (dbDelete(c->db, c->argv[j])) {
												touchWatchedKey(c->db, c->argv[j]);
												server.dirty++;
												deleted++;
								}
				}
				netAddLongLong(c, deleted);
}

void existsCmd(shmClient *c) {
				expireIfNeeded(c->db, c->argv[1]);
				if (dbExists(c->db, c->argv[1])) {
								netAdd(c, shared.cone);
				} else {
								netAdd(c, shared.czero);
				}
}

void changedbCmd(shmClient *c) {
				int id = atoi(c->argv[1]->ptr);
				if (selectDb(c, id) == SHM_ERR) {
								netAddError(c, "invalid DB index");
				} else {
								netAdd(c, shared.ok);
				}
}

void randkeyCmd(shmClient *c) {
				robj *key;
				if ((key = dbRandomKey(c->db)) == NULL) {
								netAdd(c, shared.nullbulk);
								return;
				}
				netAddBulk(c, key);
				objDecRefCount(key);
}

void allkeysCmd(shmClient *c) {
				hashIterator *di;
				hashEntry *de;
				dstr pattern = c->argv[1]->ptr;
				int plen = dstrlen(pattern), allkeys;
				unsigned long numkeys = 0;
				void *replylen = addDeferredBeginBulkLength(c);
				di = hashGetIterator(c->db->hash);
				allkeys = (pattern[0] == '*' && pattern[1] == '\0');
				while ((de = hashNext(di)) != NULL) {
								dstr key = hashGetEntryKey(de);
								robj *keyobj;
								if (allkeys || strCmpLen(pattern, plen, key, dstrlen(key), 0)) {
												keyobj = objCreateString(key, dstrlen(key));
												if (expireIfNeeded(c->db, keyobj) == 0) {
																netAddBulk(c, keyobj);
																numkeys++;
												}
												objDecRefCount(keyobj);
								}
				}
				hashReleaseIterator(di);
				setDeferredBeginBulkLength(c, replylen, numkeys);
}

void dbcountCmd(shmClient *c) {
				netAddLongLong(c, hashSize(c->db->hash));
}

void lsavetimeCmd(shmClient *c) {
				netAddLongLong(c, server.lastsave);
}

void tpCmd(shmClient *c) {
				robj *o;
				char *type;
				o = lookupKeyRead(c->db, c->argv[1]);
				if (o == NULL) {
								type = "none";
				} else {
								switch (o->type) {
								case SHM_STRING:
												type = "string";
												break;
								case SHM_LIST:
												type = "list";
												break;
								case SHM_SET:
												type = "set";
												break;
								case SHM_SORTSET:
												type = "sortset";
												break;
								case SHM_HASH:
												type = "hash";
												break;
								default:
												type = "unknown";
												break;
								}
				}
				netAddStatus(c, type);
}

void saveCmd(shmClient *c) {
				if (server.asavechildpid != -1) {
								netAddError(c, "Background save already in progress");
								return;
				}
				if (sdbSave(server.dbfilename) == SHM_OK) {
								netAdd(c, shared.ok);
				} else {
								netAdd(c, shared.err);
				}
}

void asaveCmd(shmClient *c) {
				if (server.asavechildpid != -1) {
								netAddError(c, "Background save already in progress");
								return;
				}
				if (sdbSaveBackground(server.dbfilename) == SHM_OK) {
								netAddStatus(c, "Background saving started");
				} else {
								netAdd(c, shared.err);
				}
}

void shutoffCmd(shmClient *c) {
				if (prepareForShutdown() == SHM_OK) exit(0);
				netAddError(c, "Errors trying to SHUTDOWN. Check logs.");
}

void renameGenericCmd(shmClient *c, int nx) {
				robj *o;
				if (dstrcmp(c->argv[1]->ptr, c->argv[2]->ptr) == 0) {
								netAdd(c, shared.sameobjecterr);
								return;
				}
				if ((o = lookupKeyWriteOrReply(c, c->argv[1], shared.nokeyerr)) == NULL)
								return;
				objIncRefCount(o);
				if (dbAdd(c->db, c->argv[2], o) == SHM_ERR) {
								if (nx) {
												objDecRefCount(o);
												netAdd(c, shared.czero);
												return;
								}
								dbReplace(c->db, c->argv[2], o);
				}
				dbDelete(c->db, c->argv[1]);
				touchWatchedKey(c->db, c->argv[1]);
				touchWatchedKey(c->db, c->argv[2]);
				server.dirty++;
				netAdd(c, nx ? shared.cone : shared.ok);
}

void renameCmd(shmClient *c) {
				renameGenericCmd(c, 0);
}

void renameneCmd(shmClient *c) {
				renameGenericCmd(c, 1);
}

void mvkeyCmd(shmClient *c) {
				robj *o;
				shmDb *src, *dst;
				int srcid;
				src = c->db;
				srcid = c->db->id;
				if (selectDb(c, atoi(c->argv[2]->ptr)) == SHM_ERR) {
								netAdd(c, shared.outofrangeerr);
								return;
				}
				dst = c->db;
				selectDb(c, srcid);
				if (src == dst) {
								netAdd(c, shared.sameobjecterr);
								return;
				}
				o = lookupKeyWrite(c->db, c->argv[1]);
				if (!o) {
								netAdd(c, shared.czero);
								return;
				}
				if (dbAdd(dst, c->argv[1], o) == SHM_ERR) {
								netAdd(c, shared.czero);
								return;
				}
				objIncRefCount(o);
				dbDelete(src, c->argv[1]);
				server.dirty++;
				netAdd(c, shared.cone);
}

int removeExpire(shmDb *db, robj *key) {
				shmAssert(hashFind(db->hash, key->ptr) != NULL);
				return hashDelete(db->expires, key->ptr) == HASH_OK;
}

void setExpire(shmDb *db, robj *key, time_t when) {
				hashEntry *de;
				de = hashFind(db->hash, key->ptr);
				shmAssert(de != NULL);
				hashReplace(db->expires, hashGetEntryKey(de), (void *)when);
}

time_t getExpire(shmDb *db, robj *key) {
				hashEntry *de;
				if (hashSize(db->expires) == 0 ||
				    (de = hashFind(db->expires, key->ptr)) == NULL)
								return -1;
				shmAssert(hashFind(db->hash, key->ptr) != NULL);
				return (time_t)hashGetEntryVal(de);
}

void propagateExpire(shmDb *db, robj *key) {
				robj *argv[2];
				argv[0] = objCreateString("DEL", 3);
				argv[1] = key;
				objIncRefCount(key);
				if (server.appendonly) appendOnlyFeed(server.delCmd, db->id, argv, 2);
				if (lLength(server.slaves))
								replFeedSlaves(server.slaves, db->id, argv, 2);
				objDecRefCount(argv[0]);
				objDecRefCount(argv[1]);
}

int expireIfNeeded(shmDb *db, robj *key) {
				time_t when = getExpire(db, key);
				if (when < 0) return 0;
				if (server.loading) return 0;
				if (server.masterhost != NULL) {
								return time(NULL) > when;
				}
				if (time(NULL) <= when) return 0;
				server.stat_expiredkeys++;
				propagateExpire(db, key);
				return dbDelete(db, key);
}

void expireGenericCmd(shmClient *c, robj *key, robj *param, long offset) {
				hashEntry *de;
				long seconds;
				if (objGetLongFromOrReply(c, param, &seconds, NULL) != SHM_OK) return;
				seconds -= offset;
				de = hashFind(c->db->hash, key->ptr);
				if (de == NULL) {
								netAdd(c, shared.czero);
								return;
				}
				if (seconds <= 0 && !server.loading && !server.masterhost) {
								robj *aux;
								shmAssert(dbDelete(c->db, key));
								server.dirty++;
								aux = objCreateString("DEL", 3);
								netRewriteClientCommandVector(c, 2, aux, key);
								objDecRefCount(aux);
								touchWatchedKey(c->db, key);
								netAdd(c, shared.cone);
								return;
				} else {
								time_t when = time(NULL) + seconds;
								setExpire(c->db, key, when);
								netAdd(c, shared.cone);
								touchWatchedKey(c->db, key);
								server.dirty++;
								return;
				}
}

void expireCmd(shmClient *c) {
				expireGenericCmd(c, c->argv[1], c->argv[2], 0);
}

void exatCmd(shmClient *c) {
				expireGenericCmd(c, c->argv[1], c->argv[2], time(NULL));
}

void timetoliveCmd(shmClient *c) {
				time_t expire, ttl = -1;
				expire = getExpire(c->db, c->argv[1]);
				if (expire != -1) {
								ttl = (expire - time(NULL));
								if (ttl < 0) ttl = -1;
				}
				netAddLongLong(c, (long long)ttl);
}

void remexpirationCmd(shmClient *c) {
				hashEntry *de;
				de = hashFind(c->db->hash, c->argv[1]->ptr);
				if (de == NULL) {
								netAdd(c, shared.czero);
				} else {
								if (removeExpire(c->db, c->argv[1])) {
												netAdd(c, shared.cone);
												server.dirty++;
								} else {
												netAdd(c, shared.czero);
								}
				}
}
