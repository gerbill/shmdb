#include <string.h>
typedef struct elApiState {
				fd_set rfds, wfds;
				fd_set _rfds, _wfds;
} elApiState;

static int elApiCreate(elLoop *eventLoop) {
				elApiState *state = dbmalloc(sizeof(elApiState));
				if (!state) return -1;
				FD_ZERO(&state->rfds);
				FD_ZERO(&state->wfds);
				eventLoop->apidata = state;
				return 0;
}

static void elApiFree(elLoop *eventLoop) {
				dbfree(eventLoop->apidata);
}

static int elApiAddEvent(elLoop *eventLoop, int fd, int mask) {
				elApiState *state = eventLoop->apidata;
				if (mask & EL_READABLE) FD_SET(fd, &state->rfds);
				if (mask & EL_WRITABLE) FD_SET(fd, &state->wfds);
				return 0;
}

static void elApiDelEvent(elLoop *eventLoop, int fd, int mask) {
				elApiState *state = eventLoop->apidata;
				if (mask & EL_READABLE) FD_CLR(fd, &state->rfds);
				if (mask & EL_WRITABLE) FD_CLR(fd, &state->wfds);
}

static int elApiPoll(elLoop *eventLoop, struct timeval *tvp) {
				elApiState *state = eventLoop->apidata;
				int retval, j, numevents = 0;
				memcpy(&state->_rfds, &state->rfds, sizeof(fd_set));
				memcpy(&state->_wfds, &state->wfds, sizeof(fd_set));
				retval =
								select(eventLoop->maxfd + 1, &state->_rfds, &state->_wfds, NULL, tvp);
				if (retval > 0) {
								for (j = 0; j <= eventLoop->maxfd; j++) {
												int mask = 0;
												elFileEvent *fe = &eventLoop->events[j];
												if (fe->mask == EL_NONE) continue;
												if (fe->mask & EL_READABLE && FD_ISSET(j, &state->_rfds))
																mask |= EL_READABLE;
												if (fe->mask & EL_WRITABLE && FD_ISSET(j, &state->_wfds))
																mask |= EL_WRITABLE;
												eventLoop->fired[numevents].fd = j;
												eventLoop->fired[numevents].mask = mask;
												numevents++;
								}
				}
				return numevents;
}

static char *elApiName(void) {
				return "select";
}
