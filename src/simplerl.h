#ifndef __SIMPLERL_H
#define __SIMPLERL_H
typedef struct simplerlCompletions {
				size_t len;
				char **cvec;
} simplerlCompletions;
typedef void (simplerlCompletionCallback)(const char *, simplerlCompletions *);

void simplerlSetCompletionCallback(simplerlCompletionCallback *);

void simplerlAddCompletion(simplerlCompletions *, char *);

char *simplerl(const char *prompt);

int simplerlHistoryAdd(const char *line);

int simplerlHistorySetMaxLen(int len);

int simplerlHistorySave(char *filename);

int simplerlHistoryLoad(char *filename);

void simplerlClearScreen(void);

#endif
