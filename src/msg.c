#include "shm.h"

void freeMsgPattern(void *p) {
				msgPattern *pat = p;
				objDecRefCount(pat->pattern);
				dbfree(pat);
}

int listMatchMsgPattern(void *a, void *b) {
				msgPattern *pa = a, *pb = b;
				return (pa->client == pb->client) &&
				       (objEqualString(pa->pattern, pb->pattern));
}

int msgSubscribeChannel(shmClient *c, robj *channel) {
				struct hashEntry *de;
				list *clients = NULL;
				int retval = 0;
				if (hashAdd(c->msg_channels, channel, NULL) == HASH_OK) {
								retval = 1;
								objIncRefCount(channel);
								de = hashFind(server.msg_channels, channel);
								if (de == NULL) {
												clients = lCreate();
												hashAdd(server.msg_channels, channel, clients);
												objIncRefCount(channel);
								} else {
												clients = hashGetEntryVal(de);
								}
					lAddNodeTail(clients, c);
				}
				netAdd(c, shared.mbulk3);
				netAdd(c, shared.subscribebulk);
				netAddBulk(c, channel);
				netAddLongLong(
								c, hashSize(c->msg_channels) + lLength(c->msg_patterns));
				return retval;
}

int msgUnsubscribeChannel(shmClient *c, robj *channel, int notify) {
				struct hashEntry *de;
				list *clients;
				lNode *ln;
				int retval = 0;
				objIncRefCount(channel);
				if (hashDelete(c->msg_channels, channel) == HASH_OK) {
								retval = 1;
								de = hashFind(server.msg_channels, channel);
								shmAssert(de != NULL);
								clients = hashGetEntryVal(de);
								ln = lSearchKey(clients, c);
								shmAssert(ln != NULL);
					lDelNode(clients, ln);
								if (lLength(clients) == 0) {
												hashDelete(server.msg_channels, channel);
								}
				}
				if (notify) {
								netAdd(c, shared.mbulk3);
								netAdd(c, shared.unsubscribebulk);
								netAddBulk(c, channel);
								netAddLongLong(
												c, hashSize(c->msg_channels) + lLength(c->msg_patterns));
				}
				objDecRefCount(channel);
				return retval;
}

int msgSubscribePattern(shmClient *c, robj *pattern) {
				int retval = 0;
				if (lSearchKey(c->msg_patterns, pattern) == NULL) {
								retval = 1;
								msgPattern *pat;
					lAddNodeTail(c->msg_patterns, pattern);
								objIncRefCount(pattern);
								pat = dbmalloc(sizeof(*pat));
								pat->pattern = objGetDecoded(pattern);
								pat->client = c;
					lAddNodeTail(server.msg_patterns, pat);
				}
				netAdd(c, shared.mbulk3);
				netAdd(c, shared.psubscribebulk);
				netAddBulk(c, pattern);
				netAddLongLong(
								c, hashSize(c->msg_channels) + lLength(c->msg_patterns));
				return retval;
}

int msgUnsubscribePattern(shmClient *c, robj *pattern, int notify) {
				lNode *ln;
				msgPattern pat;
				int retval = 0;
				objIncRefCount(pattern);
				if ((ln = lSearchKey(c->msg_patterns, pattern)) != NULL) {
								retval = 1;
					lDelNode(c->msg_patterns, ln);
								pat.client = c;
								pat.pattern = pattern;
								ln = lSearchKey(server.msg_patterns, &pat);
					lDelNode(server.msg_patterns, ln);
				}
				if (notify) {
								netAdd(c, shared.mbulk3);
								netAdd(c, shared.punsubscribebulk);
								netAddBulk(c, pattern);
								netAddLongLong(
												c, hashSize(c->msg_channels) + lLength(c->msg_patterns));
				}
				objDecRefCount(pattern);
				return retval;
}

int msgUnsubscribeAllChannels(shmClient *c, int notify) {
				hashIterator *di = hashGetSafeIterator(c->msg_channels);
				hashEntry *de;
				int count = 0;
				while ((de = hashNext(di)) != NULL) {
								robj *channel = hashGetEntryKey(de);
								count += msgUnsubscribeChannel(c, channel, notify);
				}
				hashReleaseIterator(di);
				return count;
}

int msgUnsubscribeAllPatterns(shmClient *c, int notify) {
				lNode *ln;
				lIter li;
				int count = 0;
	lRewind(c->msg_patterns, &li);
				while ((ln = lNext(&li)) != NULL) {
								robj *pattern = ln->value;
								count += msgUnsubscribePattern(c, pattern, notify);
				}
				return count;
}

int msgPublishMessage(robj *channel, robj *message) {
				int receivers = 0;
				struct hashEntry *de;
				lNode *ln;
				lIter li;
				de = hashFind(server.msg_channels, channel);
				if (de) {
								list *list = hashGetEntryVal(de);
								lNode *ln;
								lIter li;
					lRewind(list, &li);
								while ((ln = lNext(&li)) != NULL) {
												shmClient *c = ln->value;
												netAdd(c, shared.mbulk3);
												netAdd(c, shared.messagebulk);
												netAddBulk(c, channel);
												netAddBulk(c, message);
												receivers++;
								}
				}
				if (lLength(server.msg_patterns)) {
					lRewind(server.msg_patterns, &li);
								channel = objGetDecoded(channel);
								while ((ln = lNext(&li)) != NULL) {
												msgPattern *pat = ln->value;
												if (strCmpLen((char *)pat->pattern->ptr, dstrlen(pat->pattern->ptr),
												                   (char *)channel->ptr, dstrlen(channel->ptr), 0)) {
																netAdd(pat->client, shared.mbulk4);
																netAdd(pat->client, shared.pmessagebulk);
																netAddBulk(pat->client, pat->pattern);
																netAddBulk(pat->client, channel);
																netAddBulk(pat->client, message);
																receivers++;
												}
								}
								objDecRefCount(channel);
				}
				return receivers;
}

void listenCmd(shmClient *c) {
				int j;
				for (j = 1; j < c->argc; j++) msgSubscribeChannel(c, c->argv[j]);
}

void unlistenCmd(shmClient *c) {
				if (c->argc == 1) {
								msgUnsubscribeAllChannels(c, 1);
								return;
				} else {
								int j;
								for (j = 1; j < c->argc; j++) msgUnsubscribeChannel(c, c->argv[j], 1);
				}
}

void plistenCmd(shmClient *c) {
				int j;
				for (j = 1; j < c->argc; j++) msgSubscribePattern(c, c->argv[j]);
}

void punlistenCmd(shmClient *c) {
				if (c->argc == 1) {
								msgUnsubscribeAllPatterns(c, 1);
								return;
				} else {
								int j;
								for (j = 1; j < c->argc; j++) msgUnsubscribePattern(c, c->argv[j], 1);
				}
}

void postmesCmd(shmClient *c) {
				int receivers = msgPublishMessage(c->argv[1], c->argv[2]);
				netAddLongLong(c, receivers);
}
