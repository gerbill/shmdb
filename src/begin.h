#pragma once

#include "robj.h"

typedef struct cmdBegin {
  robj **argv;
  int argc;
  struct cmdShm *cmd;
} cmdBegin;

typedef struct beginState {
  cmdBegin *commands;
  int count;
} beginState;
