#include "shm.h"
#include "lzf.h"
#include <math.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <sys/stat.h>

static int sdbWriteRaw(FILE *fp, void *p, size_t len) {
				if (fp != NULL && fwrite(p, len, 1, fp) == 0) return -1;
				return len;
}

int sdbSaveType(FILE *fp, unsigned char type) {
				return sdbWriteRaw(fp, &type, 1);
}

int sdbSaveTime(FILE *fp, time_t t) {
				int32_t t32 = (int32_t)t;
				return sdbWriteRaw(fp, &t32, 4);
}

int sdbSaveLen(FILE *fp, uint32_t len) {
				unsigned char buf[2];
				int nwritten;
				if (len < (1 << 6)) {
								buf[0] = (len & 0xFF) | (SHM_SDB_6BITLEN << 6);
								if (sdbWriteRaw(fp, buf, 1) == -1) return -1;
								nwritten = 1;
				} else if (len < (1 << 14)) {
								buf[0] = ((len >> 8) & 0xFF) | (SHM_SDB_14BITLEN << 6);
								buf[1] = len & 0xFF;
								if (sdbWriteRaw(fp, buf, 2) == -1) return -1;
								nwritten = 2;
				} else {
								buf[0] = (SHM_SDB_32BITLEN << 6);
								if (sdbWriteRaw(fp, buf, 1) == -1) return -1;
								len = htonl(len);
								if (sdbWriteRaw(fp, &len, 4) == -1) return -1;
								nwritten = 1 + 4;
				}
				return nwritten;
}

int sdbEncodeInteger(long long value, unsigned char *enc) {
				if (value >= -(1 << 7) && value <= (1 << 7) - 1) {
								enc[0] = (SHM_SDB_ENCVAL << 6) | SHM_SDB_ENC_INT8;
								enc[1] = value & 0xFF;
								return 2;
				} else if (value >= -(1 << 15) && value <= (1 << 15) - 1) {
								enc[0] = (SHM_SDB_ENCVAL << 6) | SHM_SDB_ENC_INT16;
								enc[1] = value & 0xFF;
								enc[2] = (value >> 8) & 0xFF;
								return 3;
				} else if (value >= -((long long)1 << 31) &&
				           value <= ((long long)1 << 31) - 1) {
								enc[0] = (SHM_SDB_ENCVAL << 6) | SHM_SDB_ENC_INT32;
								enc[1] = value & 0xFF;
								enc[2] = (value >> 8) & 0xFF;
								enc[3] = (value >> 16) & 0xFF;
								enc[4] = (value >> 24) & 0xFF;
								return 5;
				} else {
								return 0;
				}
}

int sdbTryIntegerEncoding(char *s, size_t len, unsigned char *enc) {
				long long value;
				char *endptr, buf[32];
				value = strtoll(s, &endptr, 10);
				if (endptr[0] != '\0') return 0;
				longLongToStr(buf, 32, value);
				if (strlen(buf) != len || memcmp(buf, s, len)) return 0;
				return sdbEncodeInteger(value, enc);
}

int sdbSaveLzfStringObject(FILE *fp, unsigned char *s, size_t len) {
				size_t comprlen, outlen;
				unsigned char byte;
				int n, nwritten = 0;
				void *out;
				if (len <= 4) return 0;
				outlen = len - 4;
				if ((out = dbmalloc(outlen + 1)) == NULL) return 0;
				comprlen = lzf_compress(s, len, out, outlen);
				if (comprlen == 0) {
								dbfree(out);
								return 0;
				}
				byte = (SHM_SDB_ENCVAL << 6) | SHM_SDB_ENC_LZF;
				if ((n = sdbWriteRaw(fp, &byte, 1)) == -1) goto writeerr;
				nwritten += n;
				if ((n = sdbSaveLen(fp, comprlen)) == -1) goto writeerr;
				nwritten += n;
				if ((n = sdbSaveLen(fp, len)) == -1) goto writeerr;
				nwritten += n;
				if ((n = sdbWriteRaw(fp, out, comprlen)) == -1) goto writeerr;
				nwritten += n;
				dbfree(out);
				return nwritten;
writeerr:
				dbfree(out);
				return -1;
}

int sdbSaveRawString(FILE *fp, unsigned char *s, size_t len) {
				int enclen;
				int n, nwritten = 0;
				if (len <= 11) {
								unsigned char buf[5];
								if ((enclen = sdbTryIntegerEncoding((char *)s, len, buf)) > 0) {
												if (sdbWriteRaw(fp, buf, enclen) == -1) return -1;
												return enclen;
								}
				}
				if (server.sdbcompression && len > 20) {
								n = sdbSaveLzfStringObject(fp, s, len);
								if (n == -1) return -1;
								if (n > 0) return n;
				}
				if ((n = sdbSaveLen(fp, len)) == -1) return -1;
				nwritten += n;
				if (len > 0) {
								if (sdbWriteRaw(fp, s, len) == -1) return -1;
								nwritten += len;
				}
				return nwritten;
}

int sdbSaveLongLongAsStringObject(FILE *fp, long long value) {
				unsigned char buf[32];
				int n, nwritten = 0;
				int enclen = sdbEncodeInteger(value, buf);
				if (enclen > 0) {
								return sdbWriteRaw(fp, buf, enclen);
				} else {
								enclen = longLongToStr((char *)buf, 32, value);
								shmAssert(enclen < 32);
								if ((n = sdbSaveLen(fp, enclen)) == -1) return -1;
								nwritten += n;
								if ((n = sdbWriteRaw(fp, buf, enclen)) == -1) return -1;
								nwritten += n;
				}
				return nwritten;
}

int sdbSaveStringObject(FILE *fp, robj *obj) {
				if (obj->encoding == SHM_ENCODING_INT) {
								return sdbSaveLongLongAsStringObject(fp, (long)obj->ptr);
				} else {
								shmAssert(obj->encoding == SHM_ENCODING_RAW);
								return sdbSaveRawString(fp, obj->ptr, dstrlen(obj->ptr));
				}
}

int sdbSaveDoubleValue(FILE *fp, double val) {
				unsigned char buf[128];
				int len;
				if (isnan(val)) {
								buf[0] = 253;
								len = 1;
				} else if (!isfinite(val)) {
								len = 1;
								buf[0] = (val < 0) ? 255 : 254;
				} else {
#if (DBL_MANT_DIG >= 52) && (LLONG_MAX == 0x7fffffffffffffffLL)
								double min = -4503599627370495;
								double max = 4503599627370496;
								if (val > min && val < max && val == ((double)((long long)val)))
												longLongToStr((char *)buf + 1, sizeof(buf), (long long)val);
								else
#endif
								snprintf((char *)buf + 1, sizeof(buf) - 1, "%.17g", val);
								buf[0] = strlen((char *)buf + 1);
								len = buf[0] + 1;
				}
				return sdbWriteRaw(fp, buf, len);
}

int sdbSaveObject(FILE *fp, robj *o) {
				int n, nwritten = 0;
				if (o->type == SHM_STRING) {
								if ((n = sdbSaveStringObject(fp, o)) == -1) return -1;
								nwritten += n;
				} else if (o->type == SHM_LIST) {
								if (o->encoding == SHM_ENCODING_SORTLIST) {
												unsigned char *p;
												unsigned char *vstr;
												unsigned int vlen;
												long long vlong;
												if ((n = sdbSaveLen(fp, sortlistLen(o->ptr))) == -1) return -1;
												nwritten += n;
												p = sortlistIndex(o->ptr, 0);
												while (sortlistGet(p, &vstr, &vlen, &vlong)) {
																if (vstr) {
																				if ((n = sdbSaveRawString(fp, vstr, vlen)) == -1) return -1;
																				nwritten += n;
																} else {
																				if ((n = sdbSaveLongLongAsStringObject(fp, vlong)) == -1) return -1;
																				nwritten += n;
																}
																p = sortlistNext(o->ptr, p);
												}
								} else if (o->encoding == SHM_ENCODING_LINKEDLIST) {
												list *list = o->ptr;
												lIter li;
												lNode *ln;
												if ((n = sdbSaveLen(fp, lLength(list))) == -1) return -1;
												nwritten += n;
									lRewind(list, &li);
												while ((ln = lNext(&li))) {
																robj *eleobj = lNodeValue(ln);
																if ((n = sdbSaveStringObject(fp, eleobj)) == -1) return -1;
																nwritten += n;
												}
								} else {
												shmPanic("Unknown list encoding");
								}
				} else if (o->type == SHM_SET) {
								if (o->encoding == SHM_ENCODING_HT) {
												hash *set = o->ptr;
												hashIterator *di = hashGetIterator(set);
												hashEntry *de;
												if ((n = sdbSaveLen(fp, hashSize(set))) == -1) return -1;
												nwritten += n;
												while ((de = hashNext(di)) != NULL) {
																robj *eleobj = hashGetEntryKey(de);
																if ((n = sdbSaveStringObject(fp, eleobj)) == -1) return -1;
																nwritten += n;
												}
												hashReleaseIterator(di);
								} else if (o->encoding == SHM_ENCODING_IS) {
												is *is = o->ptr;
												int64_t llval;
												int i = 0;
												if ((n = sdbSaveLen(fp, isLen(is))) == -1) return -1;
												nwritten += n;
												while (isGet(is, i++, &llval)) {
																if ((n = sdbSaveLongLongAsStringObject(fp, llval)) == -1) return -1;
																nwritten += n;
												}
								} else {
												shmPanic("Unknown set encoding");
								}
				} else if (o->type == SHM_SORTSET) {
								sortset *zs = o->ptr;
								hashIterator *di = hashGetIterator(zs->hash);
								hashEntry *de;
								if ((n = sdbSaveLen(fp, hashSize(zs->hash))) == -1) return -1;
								nwritten += n;
								while ((de = hashNext(di)) != NULL) {
												robj *eleobj = hashGetEntryKey(de);
												double *score = hashGetEntryVal(de);
												if ((n = sdbSaveStringObject(fp, eleobj)) == -1) return -1;
												nwritten += n;
												if ((n = sdbSaveDoubleValue(fp, *score)) == -1) return -1;
												nwritten += n;
								}
								hashReleaseIterator(di);
				} else if (o->type == SHM_HASH) {
								if (o->encoding == SHM_ENCODING_ZIPMAP) {
												unsigned char *p = zipmapRewind(o->ptr);
												unsigned int count = zipmapLen(o->ptr);
												unsigned char *key, *val;
												unsigned int klen, vlen;
												if ((n = sdbSaveLen(fp, count)) == -1) return -1;
												nwritten += n;
												while ((p = zipmapNext(p, &key, &klen, &val, &vlen)) != NULL) {
																if ((n = sdbSaveRawString(fp, key, klen)) == -1) return -1;
																nwritten += n;
																if ((n = sdbSaveRawString(fp, val, vlen)) == -1) return -1;
																nwritten += n;
												}
								} else {
												hashIterator *di = hashGetIterator(o->ptr);
												hashEntry *de;
												if ((n = sdbSaveLen(fp, hashSize((hash *)o->ptr))) == -1) return -1;
												nwritten += n;
												while ((de = hashNext(di)) != NULL) {
																robj *key = hashGetEntryKey(de);
																robj *val = hashGetEntryVal(de);
																if ((n = sdbSaveStringObject(fp, key)) == -1) return -1;
																nwritten += n;
																if ((n = sdbSaveStringObject(fp, val)) == -1) return -1;
																nwritten += n;
												}
												hashReleaseIterator(di);
								}
				} else {
								shmPanic("Unknown object type");
				}
				return nwritten;
}

off_t sdbSavedObjectLen(robj *o) {
				int len = sdbSaveObject(NULL, o);
				shmAssert(len != -1);
				return len;
}

off_t sdbSavedObjectPages(robj *o) {
				off_t bytes = sdbSavedObjectLen(o);
				return (bytes + (server.vm_page_size - 1)) / server.vm_page_size;
}

int sdbSave(char *filename) {
				hashIterator *di = NULL;
				hashEntry *de;
				FILE *fp;
				char tmpfile[256];
				int j;
				time_t now = time(NULL);
				if (server.vm_enabled) vmWaitIOJobQueueEmpty();
				snprintf(tmpfile, 256, "temp-%d.sdb", (int)getpid());
				fp = fopen(tmpfile, "w");
				if (!fp) {
								shmLog(SHM_WARNING, "Failed saving the DB: %s", strerror(errno));
								return SHM_ERR;
				}
				if (fwrite("SHMDB0001", 9, 1, fp) == 0) goto werr;
				for (j = 0; j < server.dbnum; j++) {
								shmDb *db = server.db + j;
								hash *d = db->hash;
								if (hashSize(d) == 0) continue;
								di = hashGetSafeIterator(d);
								if (!di) {
												fclose(fp);
												return SHM_ERR;
								}
								if (sdbSaveType(fp, SHM_SELECTDB) == -1) goto werr;
								if (sdbSaveLen(fp, j) == -1) goto werr;
								while ((de = hashNext(di)) != NULL) {
												dstr keystr = hashGetEntryKey(de);
												robj key, *o = hashGetEntryVal(de);
												time_t expiretime;
												initStaticStringObject(key, keystr);
												expiretime = getExpire(db, &key);
												if (expiretime != -1) {
																if (expiretime < now) continue;
																if (sdbSaveType(fp, SHM_EXPIRETIME) == -1) goto werr;
																if (sdbSaveTime(fp, expiretime) == -1) goto werr;
												}
												if (!server.vm_enabled || o->storage == SHM_VM_MEMORY ||
												    o->storage == SHM_VM_SWAPPING) {
																if (sdbSaveType(fp, o->type) == -1) goto werr;
																if (sdbSaveStringObject(fp, &key) == -1) goto werr;
																if (sdbSaveObject(fp, o) == -1) goto werr;
												} else {
																robj *po;
																po = vmPreviewObj(o);
																if (sdbSaveType(fp, po->type) == -1) goto werr;
																if (sdbSaveStringObject(fp, &key) == -1) goto werr;
																if (sdbSaveObject(fp, po) == -1) goto werr;
																objDecRefCount(po);
												}
								}
								hashReleaseIterator(di);
				}
				if (sdbSaveType(fp, SHM_EOF) == -1) goto werr;
				fflush(fp);
				fsync(fileno(fp));
				fclose(fp);
				if (rename(tmpfile, filename) == -1) {
								shmLog(SHM_WARNING,
								       "Error moving temp DB file on the final destination: %s",
								       strerror(errno));
								unlink(tmpfile);
								return SHM_ERR;
				}
				shmLog(SHM_NOTICE, "DB saved on disk");
				server.dirty = 0;
				server.lastsave = time(NULL);
				return SHM_OK;
werr:
				fclose(fp);
				unlink(tmpfile);
				shmLog(SHM_WARNING, "Write error saving DB on disk: %s", strerror(errno));
				if (di) hashReleaseIterator(di);
				return SHM_ERR;
}

int sdbSaveBackground(char *filename) {
				pid_t childpid;
				if (server.asavechildpid != -1) return SHM_ERR;
				if (server.vm_enabled) vmWaitIOJobQueueEmpty();
				server.dirty_before_asave = server.dirty;
				if ((childpid = fork()) == 0) {
								if (server.vm_enabled) vmReopenSwap();
								if (server.ipfd > 0) close(server.ipfd);
								if (server.sofd > 0) close(server.sofd);
								if (sdbSave(filename) == SHM_OK) {
												_exit(0);
								} else {
												_exit(1);
								}
				} else {
								if (childpid == -1) {
												shmLog(SHM_WARNING, "Can't save in background: fork: %s",
												       strerror(errno));
												return SHM_ERR;
								}
								shmLog(SHM_NOTICE, "Background saving started by pid %d", childpid);
								server.asavechildpid = childpid;
								updateHashResizePolicy();
								return SHM_OK;
				}
				return SHM_OK;
}

void sdbRemoveTempFile(pid_t childpid) {
				char tmpfile[256];
				snprintf(tmpfile, 256, "temp-%d.sdb", (int)childpid);
				unlink(tmpfile);
}

int sdbLoadType(FILE *fp) {
				unsigned char type;
				if (fread(&type, 1, 1, fp) == 0) return -1;
				return type;
}

time_t sdbLoadTime(FILE *fp) {
				int32_t t32;
				if (fread(&t32, 4, 1, fp) == 0) return -1;
				return (time_t)t32;
}

uint32_t sdbLoadLen(FILE *fp, int *isencoded) {
				unsigned char buf[2];
				uint32_t len;
				int type;
				if (isencoded) *isencoded = 0;
				if (fread(buf, 1, 1, fp) == 0) return SHM_SDB_LENERR;
				type = (buf[0] & 0xC0) >> 6;
				if (type == SHM_SDB_6BITLEN) {
								return buf[0] & 0x3F;
				} else if (type == SHM_SDB_ENCVAL) {
								if (isencoded) *isencoded = 1;
								return buf[0] & 0x3F;
				} else if (type == SHM_SDB_14BITLEN) {
								if (fread(buf + 1, 1, 1, fp) == 0) return SHM_SDB_LENERR;
								return ((buf[0] & 0x3F) << 8) | buf[1];
				} else {
								if (fread(&len, 4, 1, fp) == 0) return SHM_SDB_LENERR;
								return ntohl(len);
				}
}

robj *sdbLoadIntegerObject(FILE *fp, int enctype, int encode) {
				unsigned char enc[4];
				long long val;
				if (enctype == SHM_SDB_ENC_INT8) {
								if (fread(enc, 1, 1, fp) == 0) return NULL;
								val = (signed char)enc[0];
				} else if (enctype == SHM_SDB_ENC_INT16) {
								uint16_t v;
								if (fread(enc, 2, 1, fp) == 0) return NULL;
								v = enc[0] | (enc[1] << 8);
								val = (int16_t)v;
				} else if (enctype == SHM_SDB_ENC_INT32) {
								uint32_t v;
								if (fread(enc, 4, 1, fp) == 0) return NULL;
								v = enc[0] | (enc[1] << 8) | (enc[2] << 16) | (enc[3] << 24);
								val = (int32_t)v;
				} else {
								val = 0;
								shmPanic("Unknown SDB integer encoding type");
				}
				if (encode)
								return objCreateStringFromLongLong(val);
				else
								return objCreate(SHM_STRING, dstrfromlonglong(val));
}

robj *sdbLoadLzfStringObject(FILE *fp) {
				unsigned int len, clen;
				unsigned char *c = NULL;
				dstr val = NULL;
				if ((clen = sdbLoadLen(fp, NULL)) == SHM_SDB_LENERR) return NULL;
				if ((len = sdbLoadLen(fp, NULL)) == SHM_SDB_LENERR) return NULL;
				if ((c = dbmalloc(clen)) == NULL) goto err;
				if ((val = dstrnewlen(NULL, len)) == NULL) goto err;
				if (fread(c, clen, 1, fp) == 0) goto err;
				if (lzf_decompress(c, clen, val, len) == 0) goto err;
				dbfree(c);
				return objCreate(SHM_STRING, val);
err:
				dbfree(c);
				dstrfree(val);
				return NULL;
}

robj *sdbGenericLoadStringObject(FILE *fp, int encode) {
				int isencoded;
				uint32_t len;
				dstr val;
				len = sdbLoadLen(fp, &isencoded);
				if (isencoded) {
								switch (len) {
								case SHM_SDB_ENC_INT8:
								case SHM_SDB_ENC_INT16:
								case SHM_SDB_ENC_INT32:
												return sdbLoadIntegerObject(fp, len, encode);
								case SHM_SDB_ENC_LZF:
												return sdbLoadLzfStringObject(fp);
								default:
												shmPanic("Unknown SDB encoding type");
								}
				}
				if (len == SHM_SDB_LENERR) return NULL;
				val = dstrnewlen(NULL, len);
				if (len && fread(val, len, 1, fp) == 0) {
								dstrfree(val);
								return NULL;
				}
				return objCreate(SHM_STRING, val);
}

robj *sdbLoadStringObject(FILE *fp) {
				return sdbGenericLoadStringObject(fp, 0);
}

robj *sdbLoadEncodedStringObject(FILE *fp) {
				return sdbGenericLoadStringObject(fp, 1);
}

int sdbLoadDoubleValue(FILE *fp, double *val) {
				char buf[128];
				unsigned char len;
				if (fread(&len, 1, 1, fp) == 0) return -1;
				switch (len) {
				case 255:
								*val = R_NegInf;
								return 0;
				case 254:
								*val = R_PosInf;
								return 0;
				case 253:
								*val = R_Nan;
								return 0;
				default:
								if (fread(buf, len, 1, fp) == 0) return -1;
								buf[len] = '\0';
								sscanf(buf, "%lg", val);
								return 0;
				}
}

robj *sdbLoadObject(int type, FILE *fp) {
				robj *o, *ele, *dec;
				size_t len;
				unsigned int i;
				shmLog(SHM_DEBUG, "LOADING OBJECT %d (at %d)\n", type, ftell(fp));
				if (type == SHM_STRING) {
								if ((o = sdbLoadEncodedStringObject(fp)) == NULL) return NULL;
								o = objTryEncoding(o);
				} else if (type == SHM_LIST) {
								if ((len = sdbLoadLen(fp, NULL)) == SHM_SDB_LENERR) return NULL;
								if (len > server.list_max_sortlist_entries) {
												o = objCreateList();
								} else {
												o = objCreateSortlIst();
								}
								while (len--) {
												if ((ele = sdbLoadEncodedStringObject(fp)) == NULL) return NULL;
												if (o->encoding == SHM_ENCODING_SORTLIST &&
												    ele->encoding == SHM_ENCODING_RAW &&
												    dstrlen(ele->ptr) > server.list_max_sortlist_value)
																listTypeConvert(o, SHM_ENCODING_LINKEDLIST);
												if (o->encoding == SHM_ENCODING_SORTLIST) {
																dec = objGetDecoded(ele);
																o->ptr = sortlistPush(o->ptr, dec->ptr, dstrlen(dec->ptr), SHM_TAIL);
																objDecRefCount(dec);
																objDecRefCount(ele);
												} else {
																ele = objTryEncoding(ele);
													lAddNodeTail(o->ptr, ele);
												}
								}
				} else if (type == SHM_SET) {
								if ((len = sdbLoadLen(fp, NULL)) == SHM_SDB_LENERR) return NULL;
								if (len > server.set_max_is_entries) {
												o = objCreateSet();
												if (len > HASH_HT_INITIAL_SIZE) hashExpand(o->ptr, len);
								} else {
												o = objCreateIs();
								}
								for (i = 0; i < len; i++) {
												long long llval;
												if ((ele = sdbLoadEncodedStringObject(fp)) == NULL) return NULL;
												ele = objTryEncoding(ele);
												if (o->encoding == SHM_ENCODING_IS) {
																if (objToLongLong(ele, &llval) == SHM_OK) {
																				o->ptr = isAdd(o->ptr, llval, NULL);
																} else {
																				setTypeConvert(o, SHM_ENCODING_HT);
																				hashExpand(o->ptr, len);
																}
												}
												if (o->encoding == SHM_ENCODING_HT) {
																hashAdd((hash *)o->ptr, ele, NULL);
												} else {
																objDecRefCount(ele);
												}
								}
				} else if (type == SHM_SORTSET) {
								size_t sortsetlen;
								sortset *zs;
								if ((sortsetlen = sdbLoadLen(fp, NULL)) == SHM_SDB_LENERR) return NULL;
								o = objCreateSortSet();
								zs = o->ptr;
								while (sortsetlen--) {
												robj *ele;
												double score;
												sortskiplistNode *znode;
												if ((ele = sdbLoadEncodedStringObject(fp)) == NULL) return NULL;
												ele = objTryEncoding(ele);
												if (sdbLoadDoubleValue(fp, &score) == -1) return NULL;
												znode = zslInsert(zs->zsl, score, ele);
												hashAdd(zs->hash, ele, &znode->score);
												objIncRefCount(ele);
								}
				} else if (type == SHM_HASH) {
								size_t hashlen;
								if ((hashlen = sdbLoadLen(fp, NULL)) == SHM_SDB_LENERR) return NULL;
								o = objCreateHash();
								if (hashlen > server.hash_max_zipmap_entries) convertToRealHash(o);
								while (hashlen--) {
												robj *key, *val;
												if ((key = sdbLoadEncodedStringObject(fp)) == NULL) return NULL;
												if ((val = sdbLoadEncodedStringObject(fp)) == NULL) return NULL;
												if (o->encoding != SHM_ENCODING_HT &&
												    ((key->encoding == SHM_ENCODING_RAW &&
												      dstrlen(key->ptr) > server.hash_max_zipmap_value) ||
												     (val->encoding == SHM_ENCODING_RAW &&
												      dstrlen(val->ptr) > server.hash_max_zipmap_value))) {
																convertToRealHash(o);
												}
												if (o->encoding == SHM_ENCODING_ZIPMAP) {
																unsigned char *zm = o->ptr;
																robj *deckey, *decval;
																deckey = objGetDecoded(key);
																decval = objGetDecoded(val);
																zm = zipmapSet(zm, deckey->ptr, dstrlen(deckey->ptr), decval->ptr,
																               dstrlen(decval->ptr), NULL);
																o->ptr = zm;
																objDecRefCount(deckey);
																objDecRefCount(decval);
																objDecRefCount(key);
																objDecRefCount(val);
												} else {
																key = objTryEncoding(key);
																val = objTryEncoding(val);
																hashAdd((hash *)o->ptr, key, val);
												}
								}
				} else {
								shmPanic("Unknown object type");
				}
				return o;
}

void startLoading(FILE *fp) {
				struct stat sb;
				server.loading = 1;
				server.loading_start_time = time(NULL);
				if (fstat(fileno(fp), &sb) == -1) {
								server.loading_total_bytes = 1;
				} else {
								server.loading_total_bytes = sb.st_size;
				}
}

void loadingProgress(off_t pos) {
				server.loading_loaded_bytes = pos;
}

void stopLoading(void) {
				server.loading = 0;
}

int sdbLoad(char *filename) {
				FILE *fp;
				uint32_t dbid;
				int type, retval, sdbver;
				int swap_all_values = 0;
				shmDb *db = server.db + 0;
				char buf[1024];
				time_t expiretime, now = time(NULL);
				long loops = 0;
				fp = fopen(filename, "r");
				if (!fp) return SHM_ERR;
				if (fread(buf, 9, 1, fp) == 0) goto eoferr;
				buf[9] = '\0';
				if (memcmp(buf, "SHMDB", 5) != 0) {
								fclose(fp);
								shmLog(SHM_WARNING, "Wrong signature trying to load DB from file");
								return SHM_ERR;
				}
				sdbver = atoi(buf + 5);
				if (sdbver != 1) {
								fclose(fp);
								shmLog(SHM_WARNING, "Can't handle SDB format version %d", sdbver);
								return SHM_ERR;
				}
				startLoading(fp);
				while (1) {
								robj *key, *val;
								int force_swapout;
								expiretime = -1;
								if (!(loops++ % 1000)) {
												loadingProgress(ftello(fp));
												elProcessEvents(server.el, EL_FILE_EVENTS | EL_DONT_WAIT);
								}
								if ((type = sdbLoadType(fp)) == -1) goto eoferr;
								if (type == SHM_EXPIRETIME) {
												if ((expiretime = sdbLoadTime(fp)) == -1) goto eoferr;
												if ((type = sdbLoadType(fp)) == -1) goto eoferr;
								}
								if (type == SHM_EOF) break;
								if (type == SHM_SELECTDB) {
												if ((dbid = sdbLoadLen(fp, NULL)) == SHM_SDB_LENERR) goto eoferr;
												if (dbid >= (unsigned)server.dbnum) {
																shmLog(SHM_WARNING,
																       "FATAL: Data file was created with a Shm server configured "
																       "to handle more than %d databases. Exiting\n",
																       server.dbnum);
																exit(1);
												}
												db = server.db + dbid;
												continue;
								}
								if ((key = sdbLoadStringObject(fp)) == NULL) goto eoferr;
								if ((val = sdbLoadObject(type, fp)) == NULL) goto eoferr;
								if (expiretime != -1 && expiretime < now) {
												objDecRefCount(key);
												objDecRefCount(val);
												continue;
								}
								retval = dbAdd(db, key, val);
								if (retval == SHM_ERR) {
												shmLog(SHM_WARNING,
												       "Loading DB, duplicated key (%s) found! Unrecoverable error, "
												       "exiting now.",
												       key->ptr);
												exit(1);
								}
								if (expiretime != -1) setExpire(db, key, expiretime);
								if (swap_all_values) {
												hashEntry *de = hashFind(db->hash, key->ptr);
												if (de) {
																vmptr *vp;
																val = hashGetEntryVal(de);
																if (val->refcount == 1 && (vp = vmSwapOutObjBlocking(val)) != NULL)
																				hashGetEntryVal(de) = vp;
												}
												objDecRefCount(key);
												continue;
								}
								objDecRefCount(key);
								force_swapout = 0;
								if ((dbmalloc_used_memory() - server.vm_max_memory) > 1024 * 1024 * 32)
												force_swapout = 1;
								if (!swap_all_values && server.vm_enabled && force_swapout) {
												while (dbmalloc_used_memory() > server.vm_max_memory) {
																if (vmSwapOutOneObjBlocking() == SHM_ERR) break;
												}
												if (dbmalloc_used_memory() > server.vm_max_memory) swap_all_values = 1;
								}
				}
				fclose(fp);
				stopLoading();
				return SHM_OK;
eoferr:
				shmLog(SHM_WARNING,
				       "Short read or OOM loading DB. Unrecoverable error, aborting now.");
				exit(1);
				return SHM_ERR;
}

void backgroundSaveDoneHandler(int statloc) {
				int exitcode = WEXITSTATUS(statloc);
				int bysignal = WIFSIGNALED(statloc);
				if (!bysignal && exitcode == 0) {
								shmLog(SHM_NOTICE, "Background saving terminated with success");
								server.dirty = server.dirty - server.dirty_before_asave;
								server.lastsave = time(NULL);
				} else if (!bysignal && exitcode != 0) {
								shmLog(SHM_WARNING, "Background saving error");
				} else {
								shmLog(SHM_WARNING, "Background saving terminated by signal %d",
								       WTERMSIG(statloc));
								sdbRemoveTempFile(server.asavechildpid);
				}
				server.asavechildpid = -1;
				updateSlavesWaitingAsave(exitcode == 0 ? SHM_OK : SHM_ERR);
}
