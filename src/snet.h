#ifndef SNET_H
#define SNET_H
#define SNET_OK 0
#define SNET_ERR -1
#define SNET_ERR_LEN 256
#if defined(__sun)
#define AF_LOCAL AF_UNIX
#endif

int snetTcpConnect(char *err, char *addr, int port);

int snetTcpNonBlockConnect(char *err, char *addr, int port);

int snetUnixConnect(char *err, char *path);

int snetUnixNonBlockConnect(char *err, char *path);

int snetRead(int fd, char *buf, int count);

int snetResolve(char *err, char *host, char *ipbuf);

int snetTcpServer(char *err, int port, char *bindaddr);

int snetUnixServer(char *err, char *path);

int snetTcpAccept(char *err, int serversock, char *ip, int *port);

int snetUnixAccept(char *err, int serversock);

int snetWrite(int fd, char *buf, int count);

int snetNonBlock(char *err, int fd);

int snetTcpNoDelay(char *err, int fd);

int snetTcpKeepAlive(char *err, int fd);

#endif
