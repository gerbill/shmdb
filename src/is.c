#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "is.h"
#include "dbmalloc.h"
#define IS_INT16 (sizeof(int16_t))
#define IS_ENC_INT32 (sizeof(int32_t))
#define IS_ENC_INT64 (sizeof(int64_t))

static uint8_t _isValueEncoding(int64_t v) {
				if (v < INT32_MIN || v > INT32_MAX)
								return IS_ENC_INT64;
				else if (v < INT16_MIN || v > INT16_MAX)
								return IS_ENC_INT32;
				return IS_INT16;
}

static int64_t _isGetEncoded(is *is, int pos, uint8_t enc) {
				if (enc == IS_ENC_INT64)
								return ((int64_t *)is->contents)[pos];
				else if (enc == IS_ENC_INT32)
								return ((int32_t *)is->contents)[pos];
				return ((int16_t *)is->contents)[pos];
}

static int64_t _isGet(is *is, int pos) {
				return _isGetEncoded(is, pos, is->encoding);
}

static void _isSet(is *is, int pos, int64_t value) {
				if (is->encoding == IS_ENC_INT64)
								((int64_t *)is->contents)[pos] = value;
				else if (is->encoding == IS_ENC_INT32)
								((int32_t *)is->contents)[pos] = value;
				else
								((int16_t *)is->contents)[pos] = value;
}

is *isNew(void) {
				is *is = dbmalloc(sizeof(is));
				is->encoding = IS_INT16;
				is->length = 0;
				return is;
}

static is *isResize(is *is, uint32_t len) {
				uint32_t size = len * is->encoding;
				is = dbrealloc(is, sizeof(is) + size);
				return is;
}

static uint8_t isSearch(is *is, int64_t value, uint32_t *pos) {
				int min = 0, max = is->length - 1, mid = -1;
				int64_t cur = -1;
				if (is->length == 0) {
								if (pos) *pos = 0;
								return 0;
				} else {
								if (value > _isGet(is, is->length - 1)) {
												if (pos) *pos = is->length;
												return 0;
								} else if (value < _isGet(is, 0)) {
												if (pos) *pos = 0;
												return 0;
								}
				}
				while (max >= min) {
								mid = (min + max) / 2;
								cur = _isGet(is, mid);
								if (value > cur) {
												min = mid + 1;
								} else if (value < cur) {
												max = mid - 1;
								} else {
												break;
								}
				}
				if (value == cur) {
								if (pos) *pos = mid;
								return 1;
				} else {
								if (pos) *pos = min;
								return 0;
				}
}

static is *isUpgradeAndAdd(is *is, int64_t value) {
				uint8_t curenc = is->encoding;
				uint8_t newenc = _isValueEncoding(value);
				int length = is->length;
				int prepend = value < 0 ? 1 : 0;
				is->encoding = newenc;
				is = isResize(is, is->length + 1);
				while (length--)
								_isSet(is, length + prepend, _isGetEncoded(is, length, curenc));
				if (prepend)
								_isSet(is, 0, value);
				else
								_isSet(is, is->length, value);
				is->length++;
				return is;
}

static void isMoveTail(is *is, uint32_t from, uint32_t to) {
				void *src, *dst;
				uint32_t bytes = is->length - from;
				if (is->encoding == IS_ENC_INT64) {
								src = (int64_t *)is->contents + from;
								dst = (int64_t *)is->contents + to;
								bytes *= sizeof(int64_t);
				} else if (is->encoding == IS_ENC_INT32) {
								src = (int32_t *)is->contents + from;
								dst = (int32_t *)is->contents + to;
								bytes *= sizeof(int32_t);
				} else {
								src = (int16_t *)is->contents + from;
								dst = (int16_t *)is->contents + to;
								bytes *= sizeof(int16_t);
				}
				memmove(dst, src, bytes);
}

is *isAdd(is *is, int64_t value, uint8_t *success) {
				uint8_t valenc = _isValueEncoding(value);
				uint32_t pos;
				if (success) *success = 1;
				if (valenc > is->encoding) {
								return isUpgradeAndAdd(is, value);
				} else {
								if (isSearch(is, value, &pos)) {
												if (success) *success = 0;
												return is;
								}
								is = isResize(is, is->length + 1);
								if (pos < is->length) isMoveTail(is, pos, pos + 1);
				}
				_isSet(is, pos, value);
				is->length++;
				return is;
}

is *isRemove(is *is, int64_t value, int *success) {
				uint8_t valenc = _isValueEncoding(value);
				uint32_t pos;
				if (success) *success = 0;
				if (valenc <= is->encoding && isSearch(is, value, &pos)) {
								if (success) *success = 1;
								if (pos < (is->length - 1)) isMoveTail(is, pos + 1, pos);
								is = isResize(is, is->length - 1);
								is->length--;
				}
				return is;
}

uint8_t isFind(is *is, int64_t value) {
				uint8_t valenc = _isValueEncoding(value);
				return valenc <= is->encoding && isSearch(is, value, NULL);
}

int64_t isRandom(is *is) {
				return _isGet(is, rand() % is->length);
}

uint8_t isGet(is *is, uint32_t pos, int64_t *value) {
				if (pos < is->length) {
								*value = _isGet(is, pos);
								return 1;
				}
				return 0;
}

uint32_t isLen(is *is) {
				return is->length;
}
#ifdef IS_TEST_MAIN
#include <sys/time.h>

void isRepr(is *is) {
				int i;
				for (i = 0; i < is->length; i++) {
								printf("%lld\n", (uint64_t)_isGet(is, i));
				}
				printf("\n");
}

void error(char *err) {
				printf("%s\n", err);
				exit(1);
}

void ok(void) {
				printf("OK\n");
}

long long usec(void) {
				struct timeval tv;
				gettimeofday(&tv, NULL);
				return (((long long)tv.tv_sec) * 1000000) + tv.tv_usec;
}
#define assert(_e) \
				((_e) ? (void)0 : (_assert(#_e, __FILE__, __LINE__), exit(1)))

void _assert(char *estr, char *file, int line) {
				printf("\n\n=== ASSERTION FAILED ===\n");
				printf("==> %s:%d '%s' is not true\n", file, line, estr);
}

is *createSet(int bits, int size) {
				uint64_t mask = (1 << bits) - 1;
				uint64_t i, value;
				is *is = isNew();
				for (i = 0; i < size; i++) {
								if (bits > 32) {
												value = (rand() * rand()) & mask;
								} else {
												value = rand() & mask;
								}
								is = isAdd(is, value, NULL);
				}
				return is;
}

void checkConsistency(is *is) {
				int i;
				for (i = 0; i < (is->length - 1); i++) {
								if (is->encoding == IS_INT16) {
												int16_t *i16 = (int16_t *)is->contents;
												assert(i16[i] < i16[i + 1]);
								} else if (is->encoding == IS_ENC_INT32) {
												int32_t *i32 = (int32_t *)is->contents;
												assert(i32[i] < i32[i + 1]);
								} else {
												int64_t *i64 = (int64_t *)is->contents;
												assert(i64[i] < i64[i + 1]);
								}
				}
}

int main(int argc, char **argv) {
				uint8_t success;
				int i;
				is *is;
				sranddev();
				printf("Value encodings: ");
				{
								assert(_isValueEncoding(-32768) == IS_INT16);
								assert(_isValueEncoding(+32767) == IS_INT16);
								assert(_isValueEncoding(-32769) == IS_ENC_INT32);
								assert(_isValueEncoding(+32768) == IS_ENC_INT32);
								assert(_isValueEncoding(-2147483648) == IS_ENC_INT32);
								assert(_isValueEncoding(+2147483647) == IS_ENC_INT32);
								assert(_isValueEncoding(-2147483649) == IS_ENC_INT64);
								assert(_isValueEncoding(+2147483648) == IS_ENC_INT64);
								assert(_isValueEncoding(-9223372036854775808ull) == IS_ENC_INT64);
								assert(_isValueEncoding(+9223372036854775807ull) == IS_ENC_INT64);
								ok();
				}
				printf("Basic adding: ");
				{
								is = isNew();
								is = isAdd(is, 5, &success);
								assert(success);
								is = isAdd(is, 6, &success);
								assert(success);
								is = isAdd(is, 4, &success);
								assert(success);
								is = isAdd(is, 4, &success);
								assert(!success);
								ok();
				}
				printf("Large number of random adds: ");
				{
								int inserts = 0;
								is = isNew();
								for (i = 0; i < 1024; i++) {
												is = isAdd(is, rand() % 0x800, &success);
												if (success) inserts++;
								}
								assert(is->length == inserts);
								checkConsistency(is);
								ok();
				}
				printf("Upgrade from int16 to int32: ");
				{
								is = isNew();
								is = isAdd(is, 32, NULL);
								assert(is->encoding == IS_INT16);
								is = isAdd(is, 65535, NULL);
								assert(is->encoding == IS_ENC_INT32);
								assert(isFind(is, 32));
								assert(isFind(is, 65535));
								checkConsistency(is);
								is = isNew();
								is = isAdd(is, 32, NULL);
								assert(is->encoding == IS_INT16);
								is = isAdd(is, -65535, NULL);
								assert(is->encoding == IS_ENC_INT32);
								assert(isFind(is, 32));
								assert(isFind(is, -65535));
								checkConsistency(is);
								ok();
				}
				printf("Upgrade from int16 to int64: ");
				{
								is = isNew();
								is = isAdd(is, 32, NULL);
								assert(is->encoding == IS_INT16);
								is = isAdd(is, 4294967295, NULL);
								assert(is->encoding == IS_ENC_INT64);
								assert(isFind(is, 32));
								assert(isFind(is, 4294967295));
								checkConsistency(is);
								is = isNew();
								is = isAdd(is, 32, NULL);
								assert(is->encoding == IS_INT16);
								is = isAdd(is, -4294967295, NULL);
								assert(is->encoding == IS_ENC_INT64);
								assert(isFind(is, 32));
								assert(isFind(is, -4294967295));
								checkConsistency(is);
								ok();
				}
				printf("Upgrade from int32 to int64: ");
				{
								is = isNew();
								is = isAdd(is, 65535, NULL);
								assert(is->encoding == IS_ENC_INT32);
								is = isAdd(is, 4294967295, NULL);
								assert(is->encoding == IS_ENC_INT64);
								assert(isFind(is, 65535));
								assert(isFind(is, 4294967295));
								checkConsistency(is);
								is = isNew();
								is = isAdd(is, 65535, NULL);
								assert(is->encoding == IS_ENC_INT32);
								is = isAdd(is, -4294967295, NULL);
								assert(is->encoding == IS_ENC_INT64);
								assert(isFind(is, 65535));
								assert(isFind(is, -4294967295));
								checkConsistency(is);
								ok();
				}
				printf("Stress lookups: ");
				{
								long num = 100000, size = 10000;
								int i, bits = 20;
								long long start;
								is = createSet(bits, size);
								checkConsistency(is);
								start = usec();
								for (i = 0; i < num; i++)
												isSearch(is, rand() % ((1 << bits) - 1), NULL);
								printf("%ld lookups, %ld element set, %lldusec\n", num, size,
								       usec() - start);
				}
				printf("Stress add+delete: ");
				{
								int i, v1, v2;
								is = isNew();
								for (i = 0; i < 0xffff; i++) {
												v1 = rand() % 0xfff;
												is = isAdd(is, v1, NULL);
												assert(isFind(is, v1));
												v2 = rand() % 0xfff;
												is = isRemove(is, v2, NULL);
												assert(!isFind(is, v2));
								}
								checkConsistency(is);
								ok();
				}
}
#endif
