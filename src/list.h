#pragma once

typedef struct lNode {
  struct lNode *prev;
  struct lNode *next;
  void *value;
} lNode;

typedef struct lIter {
  lNode *next;
  int direction;
} lIter;

typedef struct list {
  lNode *head;
  lNode *tail;

  void *(*dup)(void *ptr);

  void (*free)(void *ptr);

  int (*match)(void *ptr, void *key);

  unsigned int len;
} list;

#define lLength(l) ((l)->len)
#define lFirst(l) ((l)->head)
#define lLast(l) ((l)->tail)
#define lNodeValue(n) ((n)->value)
#define lSetDupMethod(l, m) ((l)->dup = (m))
#define lSetFreeMethod(l, m) ((l)->free = (m))
#define lSetMatchMethod(l, m) ((l)->match = (m))

list *lCreate(void);

void lRelease(list *l);

list *lAddNodeHead(list *l, void *v);

list *lAddNodeTail(list *l, void *v);

list *lInsertNode(list *l, lNode *old, void *v, int after);

void lDelNode(list *l, lNode *node);

lIter *lGetIterator(list *l, int dir);

lNode *lNext(lIter *i);

void lReleaseIterator(lIter *i);

list *lDup(list *src);

lNode *lSearchKey(list *l, void *key);

lNode *lIndex(list *l, int idx);

void lRewind(list *list, lIter *i);

void listRewindTail(list *l, lIter *i);

#define L_START_HEAD 0
#define L_START_TAIL 1
