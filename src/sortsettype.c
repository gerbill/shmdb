#include "shm.h"
#include <math.h>

sortskiplistNode *zslCreateNode(int level, double score, robj *obj) {
				sortskiplistNode *zn =
								dbmalloc(sizeof(*zn) + level * sizeof(struct sortskiplistLevel));
				zn->score = score;
				zn->obj = obj;
				return zn;
}

sortskiplist *zslCreate(void) {
				int j;
				sortskiplist *zsl;
				zsl = dbmalloc(sizeof(*zsl));
				zsl->level = 1;
				zsl->length = 0;
				zsl->header = zslCreateNode(SORTSKIPLIST_MAXLEVEL, 0, NULL);
				for (j = 0; j < SORTSKIPLIST_MAXLEVEL; j++) {
								zsl->header->level[j].forward = NULL;
								zsl->header->level[j].span = 0;
				}
				zsl->header->backward = NULL;
				zsl->tail = NULL;
				return zsl;
}

void zslFreeNode(sortskiplistNode *node) {
				objDecRefCount(node->obj);
				dbfree(node);
}

void zslFree(sortskiplist *zsl) {
				sortskiplistNode *node = zsl->header->level[0].forward, *next;
				dbfree(zsl->header);
				while (node) {
								next = node->level[0].forward;
								zslFreeNode(node);
								node = next;
				}
				dbfree(zsl);
}

int zslRandomLevel(void) {
				int level = 1;
				while ((random() & 0xFFFF) < (SORTSKIPLIST_P * 0xFFFF)) level += 1;
				return (level < SORTSKIPLIST_MAXLEVEL) ? level : SORTSKIPLIST_MAXLEVEL;
}

sortskiplistNode *zslInsert(sortskiplist *zsl, double score, robj *obj) {
				sortskiplistNode *update[SORTSKIPLIST_MAXLEVEL], *x;
				unsigned int rank[SORTSKIPLIST_MAXLEVEL];
				int i, level;
				x = zsl->header;
				for (i = zsl->level - 1; i >= 0; i--) {
								rank[i] = i == (zsl->level - 1) ? 0 : rank[i + 1];
								while (x->level[i].forward &&
								       (x->level[i].forward->score < score ||
								        (x->level[i].forward->score == score &&
								         objCompareString(x->level[i].forward->obj, obj) < 0))) {
												rank[i] += x->level[i].span;
												x = x->level[i].forward;
								}
								update[i] = x;
				}
				level = zslRandomLevel();
				if (level > zsl->level) {
								for (i = zsl->level; i < level; i++) {
												rank[i] = 0;
												update[i] = zsl->header;
												update[i]->level[i].span = zsl->length;
								}
								zsl->level = level;
				}
				x = zslCreateNode(level, score, obj);
				for (i = 0; i < level; i++) {
								x->level[i].forward = update[i]->level[i].forward;
								update[i]->level[i].forward = x;
								x->level[i].span = update[i]->level[i].span - (rank[0] - rank[i]);
								update[i]->level[i].span = (rank[0] - rank[i]) + 1;
				}
				for (i = level; i < zsl->level; i++) {
								update[i]->level[i].span++;
				}
				x->backward = (update[0] == zsl->header) ? NULL : update[0];
				if (x->level[0].forward)
								x->level[0].forward->backward = x;
				else
								zsl->tail = x;
				zsl->length++;
				return x;
}

void zslDeleteNode(sortskiplist *zsl, sortskiplistNode *x, sortskiplistNode **update) {
				int i;
				for (i = 0; i < zsl->level; i++) {
								if (update[i]->level[i].forward == x) {
												update[i]->level[i].span += x->level[i].span - 1;
												update[i]->level[i].forward = x->level[i].forward;
								} else {
												update[i]->level[i].span -= 1;
								}
				}
				if (x->level[0].forward) {
								x->level[0].forward->backward = x->backward;
				} else {
								zsl->tail = x->backward;
				}
				while (zsl->level > 1 && zsl->header->level[zsl->level - 1].forward == NULL)
								zsl->level--;
				zsl->length--;
}

int zslDelete(sortskiplist *zsl, double score, robj *obj) {
				sortskiplistNode *update[SORTSKIPLIST_MAXLEVEL], *x;
				int i;
				x = zsl->header;
				for (i = zsl->level - 1; i >= 0; i--) {
								while (x->level[i].forward &&
								       (x->level[i].forward->score < score ||
								        (x->level[i].forward->score == score &&
								         objCompareString(x->level[i].forward->obj, obj) < 0)))
												x = x->level[i].forward;
								update[i] = x;
				}
				x = x->level[0].forward;
				if (x && score == x->score && objEqualString(x->obj, obj)) {
								zslDeleteNode(zsl, x, update);
								zslFreeNode(x);
								return 1;
				} else {
								return 0;
				}
				return 0;
}
typedef struct {
				double min, max;
				int minex, maxex;
} zrangespec;

unsigned long zslDeleteRangeByScore(sortskiplist *zsl, zrangespec range,
                                    hash *hash) {
				sortskiplistNode *update[SORTSKIPLIST_MAXLEVEL], *x;
				unsigned long removed = 0;
				int i;
				x = zsl->header;
				for (i = zsl->level - 1; i >= 0; i--) {
								while (x->level[i].forward &&
								       (range.minex ? x->level[i].forward->score <= range.min
								        : x->level[i].forward->score < range.min))
												x = x->level[i].forward;
								update[i] = x;
				}
				x = x->level[0].forward;
				while (x && (range.maxex ? x->score < range.max : x->score <= range.max)) {
								sortskiplistNode *next = x->level[0].forward;
								zslDeleteNode(zsl, x, update);
								hashDelete(hash, x->obj);
								zslFreeNode(x);
								removed++;
								x = next;
				}
				return removed;
}

unsigned long zslDeleteRangeByRank(sortskiplist *zsl, unsigned int start,
                                   unsigned int end, hash *hash) {
				sortskiplistNode *update[SORTSKIPLIST_MAXLEVEL], *x;
				unsigned long traversed = 0, removed = 0;
				int i;
				x = zsl->header;
				for (i = zsl->level - 1; i >= 0; i--) {
								while (x->level[i].forward && (traversed + x->level[i].span) < start) {
												traversed += x->level[i].span;
												x = x->level[i].forward;
								}
								update[i] = x;
				}
				traversed++;
				x = x->level[0].forward;
				while (x && traversed <= end) {
								sortskiplistNode *next = x->level[0].forward;
								zslDeleteNode(zsl, x, update);
								hashDelete(hash, x->obj);
								zslFreeNode(x);
								removed++;
								traversed++;
								x = next;
				}
				return removed;
}

sortskiplistNode *zslFirstWithScore(sortskiplist *zsl, double score) {
				sortskiplistNode *x;
				int i;
				x = zsl->header;
				for (i = zsl->level - 1; i >= 0; i--) {
								while (x->level[i].forward && x->level[i].forward->score < score)
												x = x->level[i].forward;
				}
				return x->level[0].forward;
}

unsigned long zslGetRank(sortskiplist *zsl, double score, robj *o) {
				sortskiplistNode *x;
				unsigned long rank = 0;
				int i;
				x = zsl->header;
				for (i = zsl->level - 1; i >= 0; i--) {
								while (x->level[i].forward &&
								       (x->level[i].forward->score < score ||
								        (x->level[i].forward->score == score &&
								         objCompareString(x->level[i].forward->obj, o) <= 0))) {
												rank += x->level[i].span;
												x = x->level[i].forward;
								}
								if (x->obj && objEqualString(x->obj, o)) {
												return rank;
								}
				}
				return 0;
}

sortskiplistNode *zslGetElementByRank(sortskiplist *zsl, unsigned long rank) {
				sortskiplistNode *x;
				unsigned long traversed = 0;
				int i;
				x = zsl->header;
				for (i = zsl->level - 1; i >= 0; i--) {
								while (x->level[i].forward && (traversed + x->level[i].span) <= rank) {
												traversed += x->level[i].span;
												x = x->level[i].forward;
								}
								if (traversed == rank) {
												return x;
								}
				}
				return NULL;
}

static int zslParseRange(robj *min, robj *max, zrangespec *spec) {
				char *eptr;
				spec->minex = spec->maxex = 0;
				if (min->encoding == SHM_ENCODING_INT) {
								spec->min = (long)min->ptr;
				} else {
								if (((char *)min->ptr)[0] == '(') {
												spec->min = strtod((char *)min->ptr + 1, &eptr);
												if (eptr[0] != '\0' || isnan(spec->min)) return SHM_ERR;
												spec->minex = 1;
								} else {
												spec->min = strtod((char *)min->ptr, &eptr);
												if (eptr[0] != '\0' || isnan(spec->min)) return SHM_ERR;
								}
				}
				if (max->encoding == SHM_ENCODING_INT) {
								spec->max = (long)max->ptr;
				} else {
								if (((char *)max->ptr)[0] == '(') {
												spec->max = strtod((char *)max->ptr + 1, &eptr);
												if (eptr[0] != '\0' || isnan(spec->max)) return SHM_ERR;
												spec->maxex = 1;
								} else {
												spec->max = strtod((char *)max->ptr, &eptr);
												if (eptr[0] != '\0' || isnan(spec->max)) return SHM_ERR;
								}
				}
				return SHM_OK;
}

void zaddGenericCmd(shmClient *c, robj *key, robj *ele, double score,
                        int incr) {
				robj *sortsetobj;
				sortset *zs;
				sortskiplistNode *znode;
				sortsetobj = lookupKeyWrite(c->db, key);
				if (sortsetobj == NULL) {
								sortsetobj = objCreateSortSet();
								dbAdd(c->db, key, sortsetobj);
				} else {
								if (sortsetobj->type != SHM_SORTSET) {
												netAdd(c, shared.wrongtypeerr);
												return;
								}
				}
				zs = sortsetobj->ptr;
				if (incr) {
								hashEntry *de = hashFind(zs->hash, ele);
								if (de != NULL) score += *(double *)hashGetEntryVal(de);
								if (isnan(score)) {
												netAddError(c, "resulting score is not a number (NaN)");
												return;
								}
				}
				if (hashAdd(zs->hash, ele, NULL) == HASH_OK) {
								hashEntry *de;
								objIncRefCount(ele);
								znode = zslInsert(zs->zsl, score, ele);
								objIncRefCount(ele);
								de = hashFind(zs->hash, ele);
								shmAssert(de != NULL);
								hashGetEntryVal(de) = &znode->score;
								touchWatchedKey(c->db, c->argv[1]);
								server.dirty++;
								if (incr)
												netAddDouble(c, score);
								else
												netAdd(c, shared.cone);
				} else {
								hashEntry *de;
								robj *curobj;
								double *curscore;
								int deleted;
								de = hashFind(zs->hash, ele);
								shmAssert(de != NULL);
								curobj = hashGetEntryKey(de);
								curscore = hashGetEntryVal(de);
								if (score != *curscore) {
												deleted = zslDelete(zs->zsl, *curscore, curobj);
												shmAssert(deleted != 0);
												znode = zslInsert(zs->zsl, score, curobj);
												objIncRefCount(curobj);
												hashGetEntryVal(de) = &znode->score;
												touchWatchedKey(c->db, c->argv[1]);
												server.dirty++;
								}
								if (incr)
												netAddDouble(c, score);
								else
												netAdd(c, shared.czero);
				}
}

void sortaddCmd(shmClient *c) {
				double scoreval;
				if (objGetDoubleOrReply(c, c->argv[2], &scoreval, NULL) != SHM_OK)
								return;
				c->argv[3] = objTryEncoding(c->argv[3]);
				zaddGenericCmd(c, c->argv[1], c->argv[3], scoreval, 0);
}

void sortraisebyCmd(shmClient *c) {
				double scoreval;
				if (objGetDoubleOrReply(c, c->argv[2], &scoreval, NULL) != SHM_OK)
								return;
				c->argv[3] = objTryEncoding(c->argv[3]);
				zaddGenericCmd(c, c->argv[1], c->argv[3], scoreval, 1);
}

void sortremCmd(shmClient *c) {
				robj *sortsetobj;
				sortset *zs;
				hashEntry *de;
				double curscore;
				int deleted;
				if ((sortsetobj = lookupKeyWriteOrReply(c, c->argv[1], shared.czero)) == NULL ||
				    objCheckType(c, sortsetobj, SHM_SORTSET))
								return;
				zs = sortsetobj->ptr;
				c->argv[2] = objTryEncoding(c->argv[2]);
				de = hashFind(zs->hash, c->argv[2]);
				if (de == NULL) {
								netAdd(c, shared.czero);
								return;
				}
				curscore = *(double *)hashGetEntryVal(de);
				deleted = zslDelete(zs->zsl, curscore, c->argv[2]);
				shmAssert(deleted != 0);
				hashDelete(zs->hash, c->argv[2]);
				if (htNeedsResize(zs->hash)) hashResize(zs->hash);
				if (hashSize(zs->hash) == 0) dbDelete(c->db, c->argv[1]);
				touchWatchedKey(c->db, c->argv[1]);
				server.dirty++;
				netAdd(c, shared.cone);
}

void sortremrangebyscoreCmd(shmClient *c) {
				zrangespec range;
				long deleted;
				robj *o;
				sortset *zs;
				if (zslParseRange(c->argv[2], c->argv[3], &range) != SHM_OK) {
								netAddError(c, "min or max is not a double");
								return;
				}
				if ((o = lookupKeyWriteOrReply(c, c->argv[1], shared.czero)) == NULL ||
				    objCheckType(c, o, SHM_SORTSET))
								return;
				zs = o->ptr;
				deleted = zslDeleteRangeByScore(zs->zsl, range, zs->hash);
				if (htNeedsResize(zs->hash)) hashResize(zs->hash);
				if (hashSize(zs->hash) == 0) dbDelete(c->db, c->argv[1]);
				if (deleted) touchWatchedKey(c->db, c->argv[1]);
				server.dirty += deleted;
				netAddLongLong(c, deleted);
}

void sortremrangebyrankCmd(shmClient *c) {
				long start;
				long end;
				int llen;
				long deleted;
				robj *sortsetobj;
				sortset *zs;
				if ((objGetLongFromOrReply(c, c->argv[2], &start, NULL) != SHM_OK) ||
				    (objGetLongFromOrReply(c, c->argv[3], &end, NULL) != SHM_OK))
								return;
				if ((sortsetobj = lookupKeyWriteOrReply(c, c->argv[1], shared.czero)) == NULL ||
				    objCheckType(c, sortsetobj, SHM_SORTSET))
								return;
				zs = sortsetobj->ptr;
				llen = zs->zsl->length;
				if (start < 0) start = llen + start;
				if (end < 0) end = llen + end;
				if (start < 0) start = 0;
				if (start > end || start >= llen) {
								netAdd(c, shared.czero);
								return;
				}
				if (end >= llen) end = llen - 1;
				deleted = zslDeleteRangeByRank(zs->zsl, start + 1, end + 1, zs->hash);
				if (htNeedsResize(zs->hash)) hashResize(zs->hash);
				if (hashSize(zs->hash) == 0) dbDelete(c->db, c->argv[1]);
				if (deleted) touchWatchedKey(c->db, c->argv[1]);
				server.dirty += deleted;
				netAddLongLong(c, deleted);
}
typedef struct {
				hash *hash;
				double weight;
} sortsetopsrc;

int qsortCompareSortSetopsrcByCardinality(const void *s1, const void *s2) {
				sortsetopsrc *d1 = (void *)s1, *d2 = (void *)s2;
				unsigned long size1, size2;
				size1 = d1->hash ? hashSize(d1->hash) : 0;
				size2 = d2->hash ? hashSize(d2->hash) : 0;
				return size1 - size2;
}
#define SHM_AGGR_SUM 1
#define SHM_AGGR_MIN 2
#define SHM_AGGR_MAX 3
#define sortunionInterHashValue(_e) \
				(hashGetEntryVal(_e) == NULL ? 1.0 : *(double *)hashGetEntryVal(_e))

inline static void sortunionInterAggregate(double *target, double val,
                                        int aggregate) {
				if (aggregate == SHM_AGGR_SUM) {
								*target = *target + val;
								if (isnan(*target)) *target = 0.0;
				} else if (aggregate == SHM_AGGR_MIN) {
								*target = val < *target ? val : *target;
				} else if (aggregate == SHM_AGGR_MAX) {
								*target = val > *target ? val : *target;
				} else {
								shmPanic("Unknown ZUNION/INTER aggregate type");
				}
}

void sortunionInterGenericCmd(shmClient *c, robj *dstkey, int op) {
				int i, j, setnum;
				int aggregate = SHM_AGGR_SUM;
				sortsetopsrc *src;
				robj *dstobj;
				sortset *dstsortset;
				sortskiplistNode *znode;
				hashIterator *di;
				hashEntry *de;
				int touched = 0;
				setnum = atoi(c->argv[2]->ptr);
				if (setnum < 1) {
								netAddError(c,
								              "at least 1 input key is needed for ZUNIONSTORE/ZINTERSTORE");
								return;
				}
				if (3 + setnum > c->argc) {
								netAdd(c, shared.syntaxerr);
								return;
				}
				src = dbmalloc(sizeof(sortsetopsrc) * setnum);
				for (i = 0, j = 3; i < setnum; i++, j++) {
								robj *obj = lookupKeyWrite(c->db, c->argv[j]);
								if (!obj) {
												src[i].hash = NULL;
								} else {
												if (obj->type == SHM_SORTSET) {
																src[i].hash = ((sortset *)obj->ptr)->hash;
												} else if (obj->type == SHM_SET) {
																if (obj->encoding == SHM_ENCODING_IS)
																				setTypeConvert(obj, SHM_ENCODING_HT);
																shmAssert(obj->encoding == SHM_ENCODING_HT);
																src[i].hash = (obj->ptr);
												} else {
																dbfree(src);
																netAdd(c, shared.wrongtypeerr);
																return;
												}
								}
								src[i].weight = 1.0;
				}
				if (j < c->argc) {
								int remaining = c->argc - j;
								while (remaining) {
												if (remaining >= (setnum + 1) &&
												    !strcasecmp(c->argv[j]->ptr, "weights")) {
																j++;
																remaining--;
																for (i = 0; i < setnum; i++, j++, remaining--) {
																				if (objGetDoubleOrReply(c, c->argv[j], &src[i].weight,
																				                               "weight value is not a double") !=
																				    SHM_OK) {
																								dbfree(src);
																								return;
																				}
																}
												} else if (remaining >= 2 && !strcasecmp(c->argv[j]->ptr, "aggregate")) {
																j++;
																remaining--;
																if (!strcasecmp(c->argv[j]->ptr, "sum")) {
																				aggregate = SHM_AGGR_SUM;
																} else if (!strcasecmp(c->argv[j]->ptr, "min")) {
																				aggregate = SHM_AGGR_MIN;
																} else if (!strcasecmp(c->argv[j]->ptr, "max")) {
																				aggregate = SHM_AGGR_MAX;
																} else {
																				dbfree(src);
																				netAdd(c, shared.syntaxerr);
																				return;
																}
																j++;
																remaining--;
												} else {
																dbfree(src);
																netAdd(c, shared.syntaxerr);
																return;
												}
								}
				}
				qsort(src, setnum, sizeof(sortsetopsrc), qsortCompareSortSetopsrcByCardinality);
				dstobj = objCreateSortSet();
				dstsortset = dstobj->ptr;
				if (op == SHM_OP_INTER) {
								if (src[0].hash && hashSize(src[0].hash) > 0) {
												di = hashGetIterator(src[0].hash);
												while ((de = hashNext(di)) != NULL) {
																double score, value;
																score = src[0].weight * sortunionInterHashValue(de);
																for (j = 1; j < setnum; j++) {
																				hashEntry *other;
																				if (src[j].hash == src[0].hash) {
																								other = de;
																				} else {
																								other = hashFind(src[j].hash, hashGetEntryKey(de));
																				}
																				if (other) {
																								value = src[j].weight * sortunionInterHashValue(other);
																								sortunionInterAggregate(&score, value, aggregate);
																				} else {
																								break;
																				}
																}
																if (j == setnum) {
																				robj *o = hashGetEntryKey(de);
																				znode = zslInsert(dstsortset->zsl, score, o);
																				objIncRefCount(o);
																				hashAdd(dstsortset->hash, o, &znode->score);
																				objIncRefCount(o);
																}
												}
												hashReleaseIterator(di);
								}
				} else if (op == SHM_OP_UNION) {
								for (i = 0; i < setnum; i++) {
												if (!src[i].hash) continue;
												di = hashGetIterator(src[i].hash);
												while ((de = hashNext(di)) != NULL) {
																double score, value;
																if (hashFind(dstsortset->hash, hashGetEntryKey(de)) != NULL) continue;
																score = src[i].weight * sortunionInterHashValue(de);
																for (j = (i + 1); j < setnum; j++) {
																				if (src[j].hash == src[i].hash) {
																								value = src[i].weight * sortunionInterHashValue(de);
																								sortunionInterAggregate(&score, value, aggregate);
																				} else {
																								hashEntry *other;
																								other = hashFind(src[j].hash, hashGetEntryKey(de));
																								if (other) {
																												value = src[j].weight * sortunionInterHashValue(other);
																												sortunionInterAggregate(&score, value, aggregate);
																								}
																				}
																}
																robj *o = hashGetEntryKey(de);
																znode = zslInsert(dstsortset->zsl, score, o);
																objIncRefCount(o);
																hashAdd(dstsortset->hash, o, &znode->score);
																objIncRefCount(o);
												}
												hashReleaseIterator(di);
								}
				} else {
								shmAssert(op == SHM_OP_INTER || op == SHM_OP_UNION);
				}
				if (dbDelete(c->db, dstkey)) {
								touchWatchedKey(c->db, dstkey);
								touched = 1;
								server.dirty++;
				}
				if (dstsortset->zsl->length) {
								dbAdd(c->db, dstkey, dstobj);
								netAddLongLong(c, dstsortset->zsl->length);
								if (!touched) touchWatchedKey(c->db, dstkey);
								server.dirty++;
				} else {
								objDecRefCount(dstobj);
								netAdd(c, shared.czero);
				}
				dbfree(src);
}

void sortuniontokeyCmd(shmClient *c) {
				sortunionInterGenericCmd(c, c->argv[1], SHM_OP_UNION);
}

void sortintertokeyCmd(shmClient *c) {
				sortunionInterGenericCmd(c, c->argv[1], SHM_OP_INTER);
}

void zrangeGenericCmd(shmClient *c, int reverse) {
				robj *o;
				long start;
				long end;
				int withscores = 0;
				int llen;
				int rangelen, j;
				sortset *sortsetobj;
				sortskiplist *zsl;
				sortskiplistNode *ln;
				robj *ele;
				if ((objGetLongFromOrReply(c, c->argv[2], &start, NULL) != SHM_OK) ||
				    (objGetLongFromOrReply(c, c->argv[3], &end, NULL) != SHM_OK))
								return;
				if (c->argc == 5 && !strcasecmp(c->argv[4]->ptr, "withscores")) {
								withscores = 1;
				} else if (c->argc >= 5) {
								netAdd(c, shared.syntaxerr);
								return;
				}
				if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.emptybeginbulk)) ==
				    NULL ||
				    objCheckType(c, o, SHM_SORTSET))
								return;
				sortsetobj = o->ptr;
				zsl = sortsetobj->zsl;
				llen = zsl->length;
				if (start < 0) start = llen + start;
				if (end < 0) end = llen + end;
				if (start < 0) start = 0;
				if (start > end || start >= llen) {
								netAdd(c, shared.emptybeginbulk);
								return;
				}
				if (end >= llen) end = llen - 1;
				rangelen = (end - start) + 1;
				if (reverse) {
								ln = start == 0 ? zsl->tail : zslGetElementByRank(zsl, llen - start);
				} else {
								ln = start == 0 ? zsl->header->level[0].forward
								     : zslGetElementByRank(zsl, start + 1);
				}
				netAddBeginBulkLen(c, withscores ? (rangelen * 2) : rangelen);
				for (j = 0; j < rangelen; j++) {
								ele = ln->obj;
								netAddBulk(c, ele);
								if (withscores) netAddDouble(c, ln->score);
								ln = reverse ? ln->backward : ln->level[0].forward;
				}
}

void sortrangeCmd(shmClient *c) {
				zrangeGenericCmd(c, 0);
}

void sortrevrangeCmd(shmClient *c) {
				zrangeGenericCmd(c, 1);
}

void genericZrangebyscoreCmd(shmClient *c, int reverse, int justcount) {
				zrangespec range;
				robj *o, *emptyreply;
				sortset *sortsetobj;
				sortskiplist *zsl;
				sortskiplistNode *ln;
				int offset = 0, limit = -1;
				int withscores = 0;
				unsigned long rangelen = 0;
				void *replylen = NULL;
				if (zslParseRange(c->argv[2], c->argv[3], &range) != SHM_OK) {
								netAddError(c, "min or max is not a double");
								return;
				}
				if (c->argc > 4) {
								int remaining = c->argc - 4;
								int pos = 4;
								while (remaining) {
												if (remaining >= 1 && !strcasecmp(c->argv[pos]->ptr, "withscores")) {
																pos++;
																remaining--;
																withscores = 1;
												} else if (remaining >= 3 && !strcasecmp(c->argv[pos]->ptr, "limit")) {
																offset = atoi(c->argv[pos + 1]->ptr);
																limit = atoi(c->argv[pos + 2]->ptr);
																pos += 3;
																remaining -= 3;
												} else {
																netAdd(c, shared.syntaxerr);
																return;
												}
								}
				}
				emptyreply = justcount ? shared.czero : shared.emptybeginbulk;
				if ((o = lookupKeyReadOrReply(c, c->argv[1], emptyreply)) == NULL ||
				    objCheckType(c, o, SHM_SORTSET))
								return;
				sortsetobj = o->ptr;
				zsl = sortsetobj->zsl;
				ln = zslFirstWithScore(zsl, range.min);
				if (reverse) {
								if (ln == NULL) ln = zsl->tail;
								while (ln && ln->score > range.min) ln = ln->backward;
								if (range.minex) {
												while (ln && ln->score == range.min) ln = ln->backward;
								} else {
												while (ln && ln->level[0].forward &&
												       ln->level[0].forward->score == range.min)
																ln = ln->level[0].forward;
								}
				} else {
								if (range.minex) {
												while (ln && ln->score == range.min) ln = ln->level[0].forward;
								}
				}
				if (ln == NULL) {
								netAdd(c, emptyreply);
								return;
				}
				if (!justcount) replylen = addDeferredBeginBulkLength(c);
				while (ln && offset--) {
								if (reverse)
												ln = ln->backward;
								else
												ln = ln->level[0].forward;
				}
				while (ln && limit--) {
								if (reverse) {
												if (range.maxex) {
																if (ln->score <= range.max) break;
												} else {
																if (ln->score < range.max) break;
												}
								} else {
												if (range.maxex) {
																if (ln->score >= range.max) break;
												} else {
																if (ln->score > range.max) break;
												}
								}
								rangelen++;
								if (!justcount) {
												netAddBulk(c, ln->obj);
												if (withscores) netAddDouble(c, ln->score);
								}
								if (reverse)
												ln = ln->backward;
								else
												ln = ln->level[0].forward;
				}
				if (justcount) {
								netAddLongLong(c, (long)rangelen);
				} else {
								setDeferredBeginBulkLength(c, replylen,
								                           withscores ? (rangelen * 2) : rangelen);
				}
}

void sortrangebyscoreCmd(shmClient *c) {
				genericZrangebyscoreCmd(c, 0, 0);
}

void sortrevrangebyscoreCmd(shmClient *c) {
				genericZrangebyscoreCmd(c, 1, 0);
}

void sortcountCmd(shmClient *c) {
				genericZrangebyscoreCmd(c, 0, 1);
}

void sortcardCmd(shmClient *c) {
				robj *o;
				sortset *zs;
				if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.czero)) == NULL ||
				    objCheckType(c, o, SHM_SORTSET))
								return;
				zs = o->ptr;
				netAddLongLong(c, zs->zsl->length);
}

void sortscoreCmd(shmClient *c) {
				robj *o;
				sortset *zs;
				hashEntry *de;
				if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.nullbulk)) == NULL ||
				    objCheckType(c, o, SHM_SORTSET))
								return;
				zs = o->ptr;
				c->argv[2] = objTryEncoding(c->argv[2]);
				de = hashFind(zs->hash, c->argv[2]);
				if (!de) {
								netAdd(c, shared.nullbulk);
				} else {
								double *score = hashGetEntryVal(de);
								netAddDouble(c, *score);
				}
}

void zrankGenericCmd(shmClient *c, int reverse) {
				robj *o;
				sortset *zs;
				sortskiplist *zsl;
				hashEntry *de;
				unsigned long rank;
				double *score;
				if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.nullbulk)) == NULL ||
				    objCheckType(c, o, SHM_SORTSET))
								return;
				zs = o->ptr;
				zsl = zs->zsl;
				c->argv[2] = objTryEncoding(c->argv[2]);
				de = hashFind(zs->hash, c->argv[2]);
				if (!de) {
								netAdd(c, shared.nullbulk);
								return;
				}
				score = hashGetEntryVal(de);
				rank = zslGetRank(zsl, *score, c->argv[2]);
				if (rank) {
								if (reverse) {
												netAddLongLong(c, zsl->length - rank);
								} else {
												netAddLongLong(c, rank - 1);
								}
				} else {
								netAdd(c, shared.nullbulk);
				}
}

void sortrankCmd(shmClient *c) {
				zrankGenericCmd(c, 0);
}

void sortrevrankCmd(shmClient *c) {
				zrankGenericCmd(c, 1);
}
