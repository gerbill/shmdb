#include "shm.h"
#include <ctype.h>
#include <limits.h>
#include <sys/time.h>

int strCmpLen(const char *pattern, int patternLen, const char *string,
                   int stringLen, int nocase) {
				while (patternLen) {
								switch (pattern[0]) {
								case '*':
												while (pattern[1] == '*') {
																pattern++;
																patternLen--;
												}
												if (patternLen == 1) return 1;
												while (stringLen) {
																if (strCmpLen(pattern + 1, patternLen - 1, string, stringLen,
																                   nocase))
																				return 1;
																string++;
																stringLen--;
												}
												return 0;
												break;
								case '?':
												if (stringLen == 0) return 0;
												string++;
												stringLen--;
												break;
								case '[': {
												int not, match;
												pattern++;
												patternLen--;
												not = pattern[0] == '^';
												if (not) {
																pattern++;
																patternLen--;
												}
												match = 0;
												while (1) {
																if (pattern[0] == '\\') {
																				pattern++;
																				patternLen--;
																				if (pattern[0] == string[0]) match = 1;
																} else if (pattern[0] == ']') {
																				break;
																} else if (patternLen == 0) {
																				pattern--;
																				patternLen++;
																				break;
																} else if (pattern[1] == '-' && patternLen >= 3) {
																				int start = pattern[0];
																				int end = pattern[2];
																				int c = string[0];
																				if (start > end) {
																								int t = start;
																								start = end;
																								end = t;
																				}
																				if (nocase) {
																								start = tolower(start);
																								end = tolower(end);
																								c = tolower(c);
																				}
																				pattern += 2;
																				patternLen -= 2;
																				if (c >= start && c <= end) match = 1;
																} else {
																				if (!nocase) {
																								if (pattern[0] == string[0]) match = 1;
																				} else {
																								if (tolower((int)pattern[0]) == tolower((int)string[0]))
																												match = 1;
																				}
																}
																pattern++;
																patternLen--;
												}
												if (not) match = !match;
												if (!match) return 0;
												string++;
												stringLen--;
												break;
								}
								case '\\':
												if (patternLen >= 2) {
																pattern++;
																patternLen--;
												}
								default:
												if (!nocase) {
																if (pattern[0] != string[0]) return 0;
												} else {
																if (tolower((int)pattern[0]) != tolower((int)string[0])) return 0;
												}
												string++;
												stringLen--;
												break;
								}
								pattern++;
								patternLen--;
								if (stringLen == 0) {
												while (*pattern == '*') {
																pattern++;
																patternLen--;
												}
												break;
								}
				}
				if (patternLen == 0 && stringLen == 0) return 1;
				return 0;
}

int strCmp(const char *pattern, const char *string, int nocase) {
				return strCmpLen(pattern, strlen(pattern), string, strlen(string),
				                      nocase);
}

long long memToLongLong(const char *p, int *err) {
				const char *u;
				char buf[128];
				long mul;
				long long val;
				unsigned int digits;
				if (err) *err = 0;
				u = p;
				if (*u == '-') u++;
				while (*u && isdigit(*u)) u++;
				if (*u == '\0' || !strcasecmp(u, "b")) {
								mul = 1;
				} else if (!strcasecmp(u, "k")) {
								mul = 1000;
				} else if (!strcasecmp(u, "kb")) {
								mul = 1024;
				} else if (!strcasecmp(u, "m")) {
								mul = 1000 * 1000;
				} else if (!strcasecmp(u, "mb")) {
								mul = 1024 * 1024;
				} else if (!strcasecmp(u, "g")) {
								mul = 1000L * 1000 * 1000;
				} else if (!strcasecmp(u, "gb")) {
								mul = 1024L * 1024 * 1024;
				} else {
								if (err) *err = 1;
								mul = 1;
				}
				digits = u - p;
				if (digits >= sizeof(buf)) {
								if (err) *err = 1;
								return LLONG_MAX;
				}
				memcpy(buf, p, digits);
				buf[digits] = '\0';
				val = strtoll(buf, NULL, 10);
				return val * mul;
}

int longLongToStr(char *s, size_t len, long long value) {
				char buf[32], *p;
				unsigned long long v;
				size_t l;
				if (len == 0) return 0;
				v = (value < 0) ? -value : value;
				p = buf + 31;
				do {
								*p-- = '0' + (v % 10);
								v /= 10;
				} while (v);
				if (value < 0) *p-- = '-';
				p++;
				l = 32 - (p - buf);
				if (l + 1 > len) l = len - 1;
				memcpy(s, p, l);
				s[l] = '\0';
				return l;
}

int dstrToLongLong(dstr s, long long *llongval) {
				char buf[32], *endptr;
				long long value;
				int slen;
				value = strtoll(s, &endptr, 10);
				if (endptr[0] != '\0') return SHM_ERR;
				slen = longLongToStr(buf, 32, value);
				if (dstrlen(s) != (unsigned)slen || memcmp(buf, s, slen)) return SHM_ERR;
				if (llongval) *llongval = value;
				return SHM_OK;
}

int dstrToLong(dstr s, long *longval) {
				long long ll;
				if (dstrToLongLong(s, &ll) == SHM_ERR) return SHM_ERR;
				if (ll < LONG_MIN || ll > LONG_MAX) return SHM_ERR;
				*longval = (long)ll;
				return SHM_OK;
}

int objToLongLong(robj *o, long long *llongval) {
				shmAssert(o->type == SHM_STRING);
				if (o->encoding == SHM_ENCODING_INT) {
								if (llongval) *llongval = (long)o->ptr;
								return SHM_OK;
				} else {
								return dstrToLongLong(o->ptr, llongval);
				}
}

long long timeUs(void) {
				struct timeval tv;
				long long ust;
				gettimeofday(&tv, NULL);
				ust = ((long long)tv.tv_sec) * 1000000;
				ust += tv.tv_usec;
				return ust;
}
