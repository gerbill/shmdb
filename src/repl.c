#include "shm.h"
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

void replFeedSlaves(list *slaves, int hashid, robj **argv, int argc) {
				lNode *ln;
				lIter li;
				int outc = 0, j;
				robj **outv;
				robj *static_outv[SHM_STATIC_ARGS * 3 + 1];
				robj *lenobj;
				if (argc <= SHM_STATIC_ARGS) {
								outv = static_outv;
				} else {
								outv = dbmalloc(sizeof(robj *) * (argc * 3 + 1));
				}
				lenobj = objCreate(SHM_STRING, dstrcatprintf(dstrempty(), "*%d\r\n", argc));
				lenobj->refcount = 0;
				outv[outc++] = lenobj;
				for (j = 0; j < argc; j++) {
								lenobj = objCreate(
												SHM_STRING, dstrcatprintf(dstrempty(), "$%lu\r\n",
												                         (unsigned long)objStringLen(argv[j])));
								lenobj->refcount = 0;
								outv[outc++] = lenobj;
								outv[outc++] = argv[j];
								outv[outc++] = shared.crlf;
				}
				for (j = 0; j < outc; j++) objIncRefCount(outv[j]);
	lRewind(slaves, &li);
				while ((ln = lNext(&li))) {
								shmClient *slave = ln->value;
								if (slave->replstate == SHM_REPL_WAIT_ASAVE_START) continue;
								if (slave->slaveseldb != hashid) {
												robj *selectcmd;
												switch (hashid) {
												case 0:
																selectcmd = shared.select0;
																break;
												case 1:
																selectcmd = shared.select1;
																break;
												case 2:
																selectcmd = shared.select2;
																break;
												case 3:
																selectcmd = shared.select3;
																break;
												case 4:
																selectcmd = shared.select4;
																break;
												case 5:
																selectcmd = shared.select5;
																break;
												case 6:
																selectcmd = shared.select6;
																break;
												case 7:
																selectcmd = shared.select7;
																break;
												case 8:
																selectcmd = shared.select8;
																break;
												case 9:
																selectcmd = shared.select9;
																break;
												default:
																selectcmd = objCreate(
																				SHM_STRING, dstrcatprintf(dstrempty(), "select %d\r\n", hashid));
																selectcmd->refcount = 0;
																break;
												}
												netAdd(slave, selectcmd);
												slave->slaveseldb = hashid;
								}
								for (j = 0; j < outc; j++) netAdd(slave, outv[j]);
				}
				for (j = 0; j < outc; j++) objDecRefCount(outv[j]);
				if (outv != static_outv) dbfree(outv);
}

void replFeedMonitors(list *monitors, int hashid, robj **argv,
                             int argc) {
				lNode *ln;
				lIter li;
				int j;
				dstr cmdrepr = dstrnew("+");
				robj *cmdobj;
				struct timeval tv;
				gettimeofday(&tv, NULL);
				cmdrepr =
								dstrcatprintf(cmdrepr, "%ld.%06ld ", (long)tv.tv_sec, (long)tv.tv_usec);
				if (hashid != 0) cmdrepr = dstrcatprintf(cmdrepr, "(db %d) ", hashid);
				for (j = 0; j < argc; j++) {
								if (argv[j]->encoding == SHM_ENCODING_INT) {
												cmdrepr = dstrcatprintf(cmdrepr, "\"%ld\"", (long)argv[j]->ptr);
								} else {
												cmdrepr = dstrcatrepr(cmdrepr, (char *)argv[j]->ptr, dstrlen(argv[j]->ptr));
								}
								if (j != argc - 1) cmdrepr = dstrcatlen(cmdrepr, " ", 1);
				}
				cmdrepr = dstrcatlen(cmdrepr, "\r\n", 2);
				cmdobj = objCreate(SHM_STRING, cmdrepr);
	lRewind(monitors, &li);
				while ((ln = lNext(&li))) {
								shmClient *monitor = ln->value;
								netAdd(monitor, cmdobj);
				}
				objDecRefCount(cmdobj);
}

void syncCmd(shmClient *c) {
				if (c->flags & SHM_SLAVE) return;
				if (server.masterhost && server.replstate != SHM_REPL_CONNECTED) {
								netAddError(c, "Can't SYNC while not connected with my master");
								return;
				}
				if (lLength(c->reply) != 0) {
								netAddError(c, "SYNC is invalid with pending input");
								return;
				}
				shmLog(SHM_NOTICE, "Slave ask for synchronization");
				if (server.asavechildpid != -1) {
								shmClient *slave;
								lNode *ln;
								lIter li;
					lRewind(server.slaves, &li);
								while ((ln = lNext(&li))) {
												slave = ln->value;
												if (slave->replstate == SHM_REPL_WAIT_ASAVE_END) break;
								}
								if (ln) {
									lRelease(c->reply);
												c->reply = lDup(slave->reply);
												c->replstate = SHM_REPL_WAIT_ASAVE_END;
												shmLog(SHM_NOTICE, "Waiting for end of ASAVE for SYNC");
								} else {
												c->replstate = SHM_REPL_WAIT_ASAVE_START;
												shmLog(SHM_NOTICE, "Waiting for next ASAVE for SYNC");
								}
				} else {
								shmLog(SHM_NOTICE, "Starting ASAVE for SYNC");
								if (sdbSaveBackground(server.dbfilename) != SHM_OK) {
												shmLog(SHM_NOTICE, "Repl failed, can't ASAVE");
												netAddError(c, "Unable to perform background save");
												return;
								}
								c->replstate = SHM_REPL_WAIT_ASAVE_END;
				}
				c->repldbfd = -1;
				c->flags |= SHM_SLAVE;
				c->slaveseldb = 0;
	lAddNodeTail(server.slaves, c);
				return;
}

void sendBulkToSlave(elLoop *el, int fd, void *privdata, int mask) {
				shmClient *slave = privdata;
				SHM_NOTUSED(el);
				SHM_NOTUSED(mask);
				char buf[SHM_IOBUF_LEN];
				ssize_t nwritten, buflen;
				if (slave->repldboff == 0) {
								dstr bulkcount;
								bulkcount = dstrcatprintf(dstrempty(), "$%lld\r\n",
								                         (unsigned long long)slave->repldbsize);
								if (write(fd, bulkcount, dstrlen(bulkcount)) != (signed)dstrlen(bulkcount)) {
												dstrfree(bulkcount);
												netFreeClient(slave);
												return;
								}
								dstrfree(bulkcount);
				}
				lseek(slave->repldbfd, slave->repldboff, SEEK_SET);
				buflen = read(slave->repldbfd, buf, SHM_IOBUF_LEN);
				if (buflen <= 0) {
								shmLog(SHM_WARNING, "Read error sending DB to slave: %s",
								       (buflen == 0) ? "premature EOF" : strerror(errno));
								netFreeClient(slave);
								return;
				}
				if ((nwritten = write(fd, buf, buflen)) == -1) {
								shmLog(SHM_VERBOSE, "Write error sending DB to slave: %s", strerror(errno));
								netFreeClient(slave);
								return;
				}
				slave->repldboff += nwritten;
				if (slave->repldboff == slave->repldbsize) {
								close(slave->repldbfd);
								slave->repldbfd = -1;
								elDeleteFileEvent(server.el, slave->fd, EL_WRITABLE);
								slave->replstate = SHM_REPL_ONLINE;
								if (elCreateFileEvent(server.el, slave->fd, EL_WRITABLE, netSendReply,
								                      slave) == EL_ERR) {
												netFreeClient(slave);
												return;
								}
								netAddDstR(slave, dstrempty());
								shmLog(SHM_NOTICE, "Synchronization with slave succeeded");
				}
}

void updateSlavesWaitingAsave(int asaveerr) {
				lNode *ln;
				int startasave = 0;
				lIter li;
	lRewind(server.slaves, &li);
				while ((ln = lNext(&li))) {
								shmClient *slave = ln->value;
								if (slave->replstate == SHM_REPL_WAIT_ASAVE_START) {
												startasave = 1;
												slave->replstate = SHM_REPL_WAIT_ASAVE_END;
								} else if (slave->replstate == SHM_REPL_WAIT_ASAVE_END) {
												struct shm_stat buf;
												if (asaveerr != SHM_OK) {
																netFreeClient(slave);
																shmLog(SHM_WARNING, "SYNC failed. ASAVE child returned an error");
																continue;
												}
												if ((slave->repldbfd = open(server.dbfilename, O_RDONLY)) == -1 ||
												    shm_fstat(slave->repldbfd, &buf) == -1) {
																netFreeClient(slave);
																shmLog(SHM_WARNING, "SYNC failed. Can't open/stat DB after ASAVE: %s",
																       strerror(errno));
																continue;
												}
												slave->repldboff = 0;
												slave->repldbsize = buf.st_size;
												slave->replstate = SHM_REPL_SEND_BULK;
												elDeleteFileEvent(server.el, slave->fd, EL_WRITABLE);
												if (elCreateFileEvent(server.el, slave->fd, EL_WRITABLE, sendBulkToSlave,
												                      slave) == EL_ERR) {
																netFreeClient(slave);
																continue;
												}
								}
				}
				if (startasave) {
								if (sdbSaveBackground(server.dbfilename) != SHM_OK) {
												lIter li;
									lRewind(server.slaves, &li);
												shmLog(SHM_WARNING, "SYNC failed. ASAVE failed");
												while ((ln = lNext(&li))) {
																shmClient *slave = ln->value;
																if (slave->replstate == SHM_REPL_WAIT_ASAVE_START) netFreeClient(slave);
												}
								}
				}
}

void replAbortSyncTransfer(void) {
				shmAssert(server.replstate == SHM_REPL_TRANSFER);
				elDeleteFileEvent(server.el, server.repl_transfer_s, EL_READABLE);
				close(server.repl_transfer_s);
				close(server.repl_transfer_fd);
				unlink(server.repl_transfer_tmpfile);
				dbfree(server.repl_transfer_tmpfile);
				server.replstate = SHM_REPL_CONNECT;
}

void readSyncBulkPayload(elLoop *el, int fd, void *privdata, int mask) {
				char buf[4096];
				ssize_t nread, readlen;
				SHM_NOTUSED(el);
				SHM_NOTUSED(privdata);
				SHM_NOTUSED(mask);
				if (server.repl_transfer_left == -1) {
								if (syncReadLine(fd, buf, 1024, 3600) == -1) {
												shmLog(SHM_WARNING, "I/O error reading bulk count from MASTER: %s",
												       strerror(errno));
												replAbortSyncTransfer();
												return;
								}
								if (buf[0] == '-') {
												shmLog(SHM_WARNING, "MASTER aborted repl with an error: %s",
												       buf + 1);
												replAbortSyncTransfer();
												return;
								} else if (buf[0] == '\0') {
												server.repl_transfer_lastio = time(NULL);
												return;
								} else if (buf[0] != '$') {
												shmLog(SHM_WARNING,
												       "Bad protocol from MASTER, the first byte is not '$', are you "
												       "sure the host and port are right?");
												replAbortSyncTransfer();
												return;
								}
								server.repl_transfer_left = strtol(buf + 1, NULL, 10);
								shmLog(SHM_NOTICE, "MASTER <-> SLAVE sync: receiving %ld bytes from master",
								       server.repl_transfer_left);
								return;
				}
				readlen = (server.repl_transfer_left < (signed)sizeof(buf))
				          ? server.repl_transfer_left
				          : (signed)sizeof(buf);
				nread = read(fd, buf, readlen);
				if (nread <= 0) {
								shmLog(SHM_WARNING, "I/O error trying to sync with MASTER: %s",
								       (nread == -1) ? strerror(errno) : "connection lost");
								replAbortSyncTransfer();
								return;
				}
				server.repl_transfer_lastio = time(NULL);
				if (write(server.repl_transfer_fd, buf, nread) != nread) {
								shmLog(SHM_WARNING,
								       "Write error or short write writing to the DB dump file needed "
								       "for MASTER <-> SLAVE synchrnonization: %s",
								       strerror(errno));
								replAbortSyncTransfer();
								return;
				}
				server.repl_transfer_left -= nread;
				if (server.repl_transfer_left == 0) {
								if (rename(server.repl_transfer_tmpfile, server.dbfilename) == -1) {
												shmLog(SHM_WARNING,
												       "Failed trying to rename the temp DB into dump.sdb in MASTER "
												       "<-> SLAVE synchronization: %s",
												       strerror(errno));
												replAbortSyncTransfer();
												return;
								}
								shmLog(SHM_NOTICE, "MASTER <-> SLAVE sync: Loading DB in memory");
								emptyDb();
								elDeleteFileEvent(server.el, server.repl_transfer_s, EL_READABLE);
								if (sdbLoad(server.dbfilename) != SHM_OK) {
												shmLog(SHM_WARNING,
												       "Failed trying to load the MASTER synchronization DB from disk");
												replAbortSyncTransfer();
												return;
								}
								dbfree(server.repl_transfer_tmpfile);
								close(server.repl_transfer_fd);
								server.master = netCreateClient(server.repl_transfer_s);
								server.master->flags |= SHM_MASTER;
								server.master->authenticated = 1;
								server.replstate = SHM_REPL_CONNECTED;
								shmLog(SHM_NOTICE, "MASTER <-> SLAVE sync: Finished with success");
								if (server.appendonly) appendOnlyRewriteAsync();
				}
}

int syncWithMaster(void) {
				char buf[1024], tmpfile[256], authcmd[1024];
				int fd = snetTcpConnect(NULL, server.masterhost, server.masterport);
				int dfd, maxtries = 5;
				if (fd == -1) {
								shmLog(SHM_WARNING, "Unable to connect to MASTER: %s", strerror(errno));
								return SHM_ERR;
				}
				if (server.masterauth) {
								snprintf(authcmd, 1024, "AUTH %s\r\n", server.masterauth);
								if (syncWrite(fd, authcmd, strlen(server.masterauth) + 7, 5) == -1) {
												close(fd);
												shmLog(SHM_WARNING, "Unable to AUTH to MASTER: %s", strerror(errno));
												return SHM_ERR;
								}
								if (syncReadLine(fd, buf, 1024, 3600) == -1) {
												close(fd);
												shmLog(SHM_WARNING, "I/O error reading auth result from MASTER: %s",
												       strerror(errno));
												return SHM_ERR;
								}
								if (buf[0] != '+') {
												close(fd);
												shmLog(SHM_WARNING,
												       "Cannot AUTH to MASTER, is the masterauth password correct?");
												return SHM_ERR;
								}
				}
				if (syncWrite(fd, "SYNC \r\n", 7, 5) == -1) {
								close(fd);
								shmLog(SHM_WARNING, "I/O error writing to MASTER: %s", strerror(errno));
								return SHM_ERR;
				}
				while (maxtries--) {
								snprintf(tmpfile, 256, "temp-%d.%ld.sdb", (int)time(NULL),
								         (long int)getpid());
								dfd = open(tmpfile, O_CREAT | O_WRONLY | O_EXCL, 0644);
								if (dfd != -1) break;
								sleep(1);
				}
				if (dfd == -1) {
								close(fd);
								shmLog(
												SHM_WARNING,
												"Opening the temp file needed for MASTER <-> SLAVE synchronization: %s",
												strerror(errno));
								return SHM_ERR;
				}
				if (elCreateFileEvent(server.el, fd, EL_READABLE, readSyncBulkPayload,
				                      NULL) == EL_ERR) {
								close(fd);
								shmLog(SHM_WARNING, "Can't create readable event for SYNC");
								return SHM_ERR;
				}
				server.replstate = SHM_REPL_TRANSFER;
				server.repl_transfer_left = -1;
				server.repl_transfer_s = fd;
				server.repl_transfer_fd = dfd;
				server.repl_transfer_lastio = time(NULL);
				server.repl_transfer_tmpfile = dbstrdup(tmpfile);
				return SHM_OK;
}

void setslaveCmd(shmClient *c) {
				if (!strcasecmp(c->argv[1]->ptr, "no") &&
				    !strcasecmp(c->argv[2]->ptr, "one")) {
								if (server.masterhost) {
												dstrfree(server.masterhost);
												server.masterhost = NULL;
												if (server.master) netFreeClient(server.master);
												if (server.replstate == SHM_REPL_TRANSFER) replAbortSyncTransfer();
												server.replstate = SHM_REPL_NONE;
												shmLog(SHM_NOTICE, "MASTER MODE enabled (user request)");
								}
				} else {
								dstrfree(server.masterhost);
								server.masterhost = dstrdup(c->argv[1]->ptr);
								server.masterport = atoi(c->argv[2]->ptr);
								if (server.master) netFreeClient(server.master);
								if (server.replstate == SHM_REPL_TRANSFER) replAbortSyncTransfer();
								server.replstate = SHM_REPL_CONNECT;
								shmLog(SHM_NOTICE, "SLAVE OF %s:%d enabled (user request)",
								       server.masterhost, server.masterport);
				}
				netAdd(c, shared.ok);
}
#define SHM_REPL_TIMEOUT 60
#define SHM_REPL_PING_SLAVE_PERIOD 10

void replCron(void) {
				if (server.masterhost && server.replstate == SHM_REPL_TRANSFER &&
				    (time(NULL) - server.repl_transfer_lastio) > SHM_REPL_TIMEOUT) {
								shmLog(SHM_WARNING, "Timeout receiving bulk data from MASTER...");
								replAbortSyncTransfer();
				}
				if (server.masterhost && server.replstate == SHM_REPL_CONNECTED &&
				    (time(NULL) - server.master->lastinteraction) > SHM_REPL_TIMEOUT) {
								shmLog(SHM_WARNING, "MASTER time out: no data nor PING received...");
								netFreeClient(server.master);
				}
				if (server.replstate == SHM_REPL_CONNECT) {
								shmLog(SHM_NOTICE, "Connecting to MASTER...");
								if (syncWithMaster() == SHM_OK) {
												shmLog(SHM_NOTICE, "MASTER <-> SLAVE sync started: SYNC sent");
								}
				}
				if (!(server.cronloops % (SHM_REPL_PING_SLAVE_PERIOD * 10))) {
								lIter li;
								lNode *ln;
					lRewind(server.slaves, &li);
								while ((ln = lNext(&li))) {
												shmClient *slave = ln->value;
												if (slave->replstate == SHM_REPL_SEND_BULK) continue;
												if (slave->replstate == SHM_REPL_ONLINE) {
																netAddDstR(slave, dstrnew("PING\r\n"));
												} else {
																if (write(slave->fd, "\n", 1) == -1) {
																}
												}
								}
				}
}
