#include "fmacros.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <limits.h>
#include <sys/time.h>
#include <ctype.h>
#include "hash.h"
#include "dbmalloc.h"
static int hash_can_resize = 1;
static unsigned int hash_force_resize_ratio = 5;

static int _hashExpandIfNeeded(hash *ht);

static unsigned long _hashNextPower(unsigned long size);

static int _hashKeyIndex(hash *ht, const void *key);

static int _hashInit(hash *ht, hashType *type, void *privDataPtr);

unsigned int hashIntHashFunction(unsigned int key) {
				key += ~(key << 15);
				key ^= (key >> 10);
				key += (key << 3);
				key ^= (key >> 6);
				key += ~(key << 11);
				key ^= (key >> 16);
				return key;
}

unsigned int hashIdentityHashFunction(unsigned int key) {
				return key;
}

unsigned int hashGenHashFunction(const unsigned char *buf, int len) {
				unsigned int hash = 5381;
				while (len--) hash = ((hash << 5) + hash) + (*buf++);
				return hash;
}

unsigned int hashGenCaseHashFunction(const unsigned char *buf, int len) {
				unsigned int hash = 5381;
				while (len--) hash = ((hash << 5) + hash) + (tolower(*buf++));
				return hash;
}

static void _hashReset(hashht *ht) {
				ht->table = NULL;
				ht->size = 0;
				ht->sizemask = 0;
				ht->used = 0;
}

hash *hashCreate(hashType *type, void *privDataPtr) {
				hash *d = dbmalloc(sizeof(*d));
				_hashInit(d, type, privDataPtr);
				return d;
}

int _hashInit(hash *d, hashType *type, void *privDataPtr) {
				_hashReset(&d->ht[0]);
				_hashReset(&d->ht[1]);
				d->type = type;
				d->privdata = privDataPtr;
				d->rehashidx = -1;
				d->iterators = 0;
				return HASH_OK;
}

int hashResize(hash *d) {
				int minimal;
				if (!hash_can_resize || hashIsRehashing(d)) return HASH_ERR;
				minimal = d->ht[0].used;
				if (minimal < HASH_HT_INITIAL_SIZE) minimal = HASH_HT_INITIAL_SIZE;
				return hashExpand(d, minimal);
}

int hashExpand(hash *d, unsigned long size) {
				hashht n;
				unsigned long realsize = _hashNextPower(size);
				if (hashIsRehashing(d) || d->ht[0].used > size) return HASH_ERR;
				n.size = realsize;
				n.sizemask = realsize - 1;
				n.table = dbcalloc(realsize * sizeof(hashEntry *));
				n.used = 0;
				if (d->ht[0].table == NULL) {
								d->ht[0] = n;
								return HASH_OK;
				}
				d->ht[1] = n;
				d->rehashidx = 0;
				return HASH_OK;
}

int hashRehash(hash *d, int n) {
				if (!hashIsRehashing(d)) return 0;
				while (n--) {
								hashEntry *de, *nextde;
								if (d->ht[0].used == 0) {
												dbfree(d->ht[0].table);
												d->ht[0] = d->ht[1];
												_hashReset(&d->ht[1]);
												d->rehashidx = -1;
												return 0;
								}
								while (d->ht[0].table[d->rehashidx] == NULL) d->rehashidx++;
								de = d->ht[0].table[d->rehashidx];
								while (de) {
												unsigned int h;
												nextde = de->next;
												h = hashHashKey(d, de->key) & d->ht[1].sizemask;
												de->next = d->ht[1].table[h];
												d->ht[1].table[h] = de;
												d->ht[0].used--;
												d->ht[1].used++;
												de = nextde;
								}
								d->ht[0].table[d->rehashidx] = NULL;
								d->rehashidx++;
				}
				return 1;
}

long long timeInMilliseconds(void) {
				struct timeval tv;
				gettimeofday(&tv, NULL);
				return (((long long)tv.tv_sec) * 1000) + (tv.tv_usec / 1000);
}

int hashRehashMilliseconds(hash *d, int ms) {
				long long start = timeInMilliseconds();
				int rehashes = 0;
				while (hashRehash(d, 100)) {
								rehashes += 100;
								if (timeInMilliseconds() - start > ms) break;
				}
				return rehashes;
}

static void _hashRehashStep(hash *d) {
				if (d->iterators == 0) hashRehash(d, 1);
}

int hashAdd(hash *d, void *key, void *val) {
				int index;
				hashEntry *entry;
				hashht *ht;
				if (hashIsRehashing(d)) _hashRehashStep(d);
				if ((index = _hashKeyIndex(d, key)) == -1) return HASH_ERR;
				ht = hashIsRehashing(d) ? &d->ht[1] : &d->ht[0];
				entry = dbmalloc(sizeof(*entry));
				entry->next = ht->table[index];
				ht->table[index] = entry;
				ht->used++;
				hashSetHashKey(d, entry, key);
				hashSetHashVal(d, entry, val);
				return HASH_OK;
}

int hashReplace(hash *d, void *key, void *val) {
				hashEntry *entry, auxentry;
				if (hashAdd(d, key, val) == HASH_OK) return 1;
				entry = hashFind(d, key);
				auxentry = *entry;
				hashSetHashVal(d, entry, val);
				hashFreeEntryVal(d, &auxentry);
				return 0;
}

static int hashGenericDelete(hash *d, const void *key, int nofree) {
				unsigned int h, idx;
				hashEntry *he, *prevHe;
				int table;
				if (d->ht[0].size == 0) return HASH_ERR;
				if (hashIsRehashing(d)) _hashRehashStep(d);
				h = hashHashKey(d, key);
				for (table = 0; table <= 1; table++) {
								idx = h & d->ht[table].sizemask;
								he = d->ht[table].table[idx];
								prevHe = NULL;
								while (he) {
												if (hashCompareHashKeys(d, key, he->key)) {
																if (prevHe)
																				prevHe->next = he->next;
																else
																				d->ht[table].table[idx] = he->next;
																if (!nofree) {
																				hashFreeEntryKey(d, he);
																				hashFreeEntryVal(d, he);
																}
																dbfree(he);
																d->ht[table].used--;
																return HASH_OK;
												}
												prevHe = he;
												he = he->next;
								}
								if (!hashIsRehashing(d)) break;
				}
				return HASH_ERR;
}

int hashDelete(hash *ht, const void *key) {
				return hashGenericDelete(ht, key, 0);
}

int hashDeleteNoFree(hash *ht, const void *key) {
				return hashGenericDelete(ht, key, 1);
}

int _hashClear(hash *d, hashht *ht) {
				unsigned long i;
				for (i = 0; i < ht->size && ht->used > 0; i++) {
								hashEntry *he, *nextHe;
								if ((he = ht->table[i]) == NULL) continue;
								while (he) {
												nextHe = he->next;
												hashFreeEntryKey(d, he);
												hashFreeEntryVal(d, he);
												dbfree(he);
												ht->used--;
												he = nextHe;
								}
				}
				dbfree(ht->table);
				_hashReset(ht);
				return HASH_OK;
}

void hashRelease(hash *d) {
				_hashClear(d, &d->ht[0]);
				_hashClear(d, &d->ht[1]);
				dbfree(d);
}

hashEntry *hashFind(hash *d, const void *key) {
				hashEntry *he;
				unsigned int h, idx, table;
				if (d->ht[0].size == 0) return NULL;
				if (hashIsRehashing(d)) _hashRehashStep(d);
				h = hashHashKey(d, key);
				for (table = 0; table <= 1; table++) {
								idx = h & d->ht[table].sizemask;
								he = d->ht[table].table[idx];
								while (he) {
												if (hashCompareHashKeys(d, key, he->key)) return he;
												he = he->next;
								}
								if (!hashIsRehashing(d)) return NULL;
				}
				return NULL;
}

void *hashFetchValue(hash *d, const void *key) {
				hashEntry *he;
				he = hashFind(d, key);
				return he ? hashGetEntryVal(he) : NULL;
}

hashIterator *hashGetIterator(hash *d) {
				hashIterator *iter = dbmalloc(sizeof(*iter));
				iter->d = d;
				iter->table = 0;
				iter->index = -1;
				iter->safe = 0;
				iter->entry = NULL;
				iter->nextEntry = NULL;
				return iter;
}

hashIterator *hashGetSafeIterator(hash *d) {
				hashIterator *i = hashGetIterator(d);
				i->safe = 1;
				return i;
}

hashEntry *hashNext(hashIterator *iter) {
				while (1) {
								if (iter->entry == NULL) {
												hashht *ht = &iter->d->ht[iter->table];
												if (iter->safe && iter->index == -1 && iter->table == 0)
																iter->d->iterators++;
												iter->index++;
												if (iter->index >= (signed)ht->size) {
																if (hashIsRehashing(iter->d) && iter->table == 0) {
																				iter->table++;
																				iter->index = 0;
																				ht = &iter->d->ht[1];
																} else {
																				break;
																}
												}
												iter->entry = ht->table[iter->index];
								} else {
												iter->entry = iter->nextEntry;
								}
								if (iter->entry) {
												iter->nextEntry = iter->entry->next;
												return iter->entry;
								}
				}
				return NULL;
}

void hashReleaseIterator(hashIterator *iter) {
				if (iter->safe && !(iter->index == -1 && iter->table == 0))
								iter->d->iterators--;
				dbfree(iter);
}

hashEntry *hashGetRandomKey(hash *d) {
				hashEntry *he, *orighe;
				unsigned int h;
				int listlen, listele;
				if (hashSize(d) == 0) return NULL;
				if (hashIsRehashing(d)) _hashRehashStep(d);
				if (hashIsRehashing(d)) {
								do {
												h = random() % (d->ht[0].size + d->ht[1].size);
												he = (h >= d->ht[0].size) ? d->ht[1].table[h - d->ht[0].size]
												     : d->ht[0].table[h];
								} while (he == NULL);
				} else {
								do {
												h = random() & d->ht[0].sizemask;
												he = d->ht[0].table[h];
								} while (he == NULL);
				}
				listlen = 0;
				orighe = he;
				while (he) {
								he = he->next;
								listlen++;
				}
				listele = random() % listlen;
				he = orighe;
				while (listele--) he = he->next;
				return he;
}

static int _hashExpandIfNeeded(hash *d) {
				if (hashIsRehashing(d)) return HASH_OK;
				if (d->ht[0].size == 0) return hashExpand(d, HASH_HT_INITIAL_SIZE);
				if (d->ht[0].used >= d->ht[0].size &&
				    (hash_can_resize ||
				     d->ht[0].used / d->ht[0].size > hash_force_resize_ratio)) {
								return hashExpand(
												d,
												((d->ht[0].size > d->ht[0].used) ? d->ht[0].size : d->ht[0].used) * 2);
				}
				return HASH_OK;
}

static unsigned long _hashNextPower(unsigned long size) {
				unsigned long i = HASH_HT_INITIAL_SIZE;
				if (size >= LONG_MAX) return LONG_MAX;
				while (1) {
								if (i >= size) return i;
								i *= 2;
				}
}

static int _hashKeyIndex(hash *d, const void *key) {
				unsigned int h, idx, table;
				hashEntry *he;
				if (_hashExpandIfNeeded(d) == HASH_ERR) return -1;
				h = hashHashKey(d, key);
				for (table = 0; table <= 1; table++) {
								idx = h & d->ht[table].sizemask;
								he = d->ht[table].table[idx];
								while (he) {
												if (hashCompareHashKeys(d, key, he->key)) return -1;
												he = he->next;
								}
								if (!hashIsRehashing(d)) break;
				}
				return idx;
}

void hashEmpty(hash *d) {
				_hashClear(d, &d->ht[0]);
				_hashClear(d, &d->ht[1]);
				d->rehashidx = -1;
				d->iterators = 0;
}
#define HASH_STATS_VECTLEN 50

static void _hashPrintStatsHt(hashht *ht) {
				unsigned long i, slots = 0, chainlen, maxchainlen = 0;
				unsigned long totchainlen = 0;
				unsigned long clvector[HASH_STATS_VECTLEN];
				if (ht->used == 0) {
								printf("No stats available for empty hashionaries\n");
								return;
				}
				for (i = 0; i < HASH_STATS_VECTLEN; i++) clvector[i] = 0;
				for (i = 0; i < ht->size; i++) {
								hashEntry *he;
								if (ht->table[i] == NULL) {
												clvector[0]++;
												continue;
								}
								slots++;
								chainlen = 0;
								he = ht->table[i];
								while (he) {
												chainlen++;
												he = he->next;
								}
								clvector[(chainlen < HASH_STATS_VECTLEN) ? chainlen
								         : (HASH_STATS_VECTLEN - 1)]++;
								if (chainlen > maxchainlen) maxchainlen = chainlen;
								totchainlen += chainlen;
				}
				printf("Hash table stats:\n");
				printf(" table size: %ld\n", ht->size);
				printf(" number of elements: %ld\n", ht->used);
				printf(" different slots: %ld\n", slots);
				printf(" max chain length: %ld\n", maxchainlen);
				printf(" avg chain length (counted): %.02f\n", (float)totchainlen / slots);
				printf(" avg chain length (computed): %.02f\n", (float)ht->used / slots);
				printf(" Chain length distribution:\n");
				for (i = 0; i < HASH_STATS_VECTLEN - 1; i++) {
								if (clvector[i] == 0) continue;
								printf("   %s%ld: %ld (%.02f%%)\n",
								       (i == HASH_STATS_VECTLEN - 1) ? ">= " : "", i, clvector[i],
								       ((float)clvector[i] / ht->size) * 100);
				}
}

void hashPrintStats(hash *d) {
				_hashPrintStatsHt(&d->ht[0]);
				if (hashIsRehashing(d)) {
								printf("-- Rehashing into ht[1]:\n");
								_hashPrintStatsHt(&d->ht[1]);
				}
}

void hashEnableResize(void) {
				hash_can_resize = 1;
}

void hashDisableResize(void) {
				hash_can_resize = 0;
}
#if 0

static unsigned int _hashStringCopyHTHashFunction(const void *key)
{
				return hashGenHashFunction(key, strlen(key));
}

static void *_hashStringDup(void *privdata, const void *key)
{
				int len = strlen(key);
				char *copy = dbmalloc(len+1);
				HASH_NOTUSED(privdata);
				memcpy(copy, key, len);
				copy[len] = '\0';
				return copy;
}

static int _hashStringCopyHTKeyCompare(void *privdata, const void *key1,
                                       const void *key2)
{
				HASH_NOTUSED(privdata);
				return strcmp(key1, key2) == 0;
}

static void _hashStringDestructor(void *privdata, void *key)
{
				HASH_NOTUSED(privdata);
				dbfree(key);
}
hashType hashTypeHeapStringCopyKey = {
				_hashStringCopyHTHashFunction,
				_hashStringDup,
				NULL,
				_hashStringCopyHTKeyCompare,
				_hashStringDestructor,
				NULL
};
hashType hashTypeHeapStrings = {
				_hashStringCopyHTHashFunction,
				NULL,
				NULL,
				_hashStringCopyHTKeyCompare,
				_hashStringDestructor,
				NULL
};
hashType hashTypeHeapStringCopyKeyValue = {
				_hashStringCopyHTHashFunction,
				_hashStringDup,
				_hashStringDup,
				_hashStringCopyHTKeyCompare,
				_hashStringDestructor,
				_hashStringDestructor,
};
#endif
