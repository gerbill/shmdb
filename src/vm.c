#include "shm.h"
#include <fcntl.h>
#include <pthread.h>
#include <math.h>
#include <signal.h>

vmptr *vmCreatePtr(int vtype) {
				vmptr *vp = dbmalloc(sizeof(vmptr));
				vp->type = SHM_VMPTR;
				vp->storage = SHM_VM_SWAPPED;
				vp->vtype = vtype;
				return vp;
}

void vmInitialize(void) {
				off_t totsize;
				int pipefds[2];
				size_t stacksize;
				struct flock fl;
				if (server.vm_max_threads != 0) dbmalloc_enable_thread_safeness();
				shmLog(SHM_NOTICE, "Using '%s' as swap file", server.vm_swap_file);
				if ((server.vm_fp = fopen(server.vm_swap_file, "r+b")) == NULL) {
								server.vm_fp = fopen(server.vm_swap_file, "w+b");
				}
				if (server.vm_fp == NULL) {
								shmLog(SHM_WARNING, "Can't open the swap file: %s. Exiting.",
								       strerror(errno));
								exit(1);
				}
				server.vm_fd = fileno(server.vm_fp);
				fl.l_type = F_WRLCK;
				fl.l_whence = SEEK_SET;
				fl.l_start = fl.l_len = 0;
				if (fcntl(server.vm_fd, F_SETLK, &fl) == -1) {
								shmLog(SHM_WARNING,
								       "Can't lock the swap file at '%s': %s. Make sure it is not used "
								       "by another Shm instance.",
								       server.vm_swap_file, strerror(errno));
								exit(1);
				}
				server.vm_next_page = 0;
				server.vm_near_pages = 0;
				server.vm_stats_used_pages = 0;
				server.vm_stats_swapped_objects = 0;
				server.vm_stats_swapouts = 0;
				server.vm_stats_swapins = 0;
				totsize = server.vm_pages * server.vm_page_size;
				shmLog(SHM_NOTICE, "Allocating %lld bytes of swap file", totsize);
				if (ftruncate(server.vm_fd, totsize) == -1) {
								shmLog(SHM_WARNING, "Can't ftruncate swap file: %s. Exiting.",
								       strerror(errno));
								exit(1);
				} else {
								shmLog(SHM_NOTICE, "Swap file allocated with success");
				}
				server.vm_bitmap = dbcalloc((server.vm_pages + 7) / 8);
				shmLog(SHM_VERBOSE, "Allocated %lld bytes page table for %lld pages",
				       (long long)(server.vm_pages + 7) / 8, server.vm_pages);
				server.io_newjobs = lCreate();
				server.io_processing = lCreate();
				server.io_processed = lCreate();
				server.io_ready_clients = lCreate();
				pthread_mutex_init(&server.io_mutex, NULL);
				pthread_mutex_init(&server.io_swapfile_mutex, NULL);
				server.io_active_threads = 0;
				if (pipe(pipefds) == -1) {
								shmLog(SHM_WARNING, "Unable to intialized VM: pipe(2): %s. Exiting.",
								       strerror(errno));
								exit(1);
				}
				server.io_ready_pipe_read = pipefds[0];
				server.io_ready_pipe_write = pipefds[1];
				shmAssert(snetNonBlock(NULL, server.io_ready_pipe_read) != SNET_ERR);
				pthread_attr_init(&server.io_threads_attr);
				pthread_attr_getstacksize(&server.io_threads_attr, &stacksize);
				if (!stacksize) stacksize = 1;
				while (stacksize < SHM_THREAD_STACK_SIZE) stacksize *= 2;
				pthread_attr_setstacksize(&server.io_threads_attr, stacksize);
				if (elCreateFileEvent(server.el, server.io_ready_pipe_read, EL_READABLE,
				                      vmThreadedIODoneJob, NULL) == EL_ERR)
								oom("creating file event");
}

void vmSetPageUsedPageUsed(off_t page) {
				off_t byte = page / 8;
				int bit = page & 7;
				shmAssert(vmFree(page) == 1);
				server.vm_bitmap[byte] |= 1 << bit;
}

void vmSetPageUsedPagesUsed(off_t page, off_t count) {
				off_t j;
				for (j = 0; j < count; j++) vmSetPageUsedPageUsed(page + j);
				server.vm_stats_used_pages += count;
				shmLog(SHM_DEBUG, "Mark USED pages: %lld pages at %lld\n", (long long)count,
				       (long long)page);
}

void vmSetPageUsedPageFree(off_t page) {
				off_t byte = page / 8;
				int bit = page & 7;
				shmAssert(vmFree(page) == 0);
				server.vm_bitmap[byte] &= ~(1 << bit);
}

void vmSetPageUsedPagesFree(off_t page, off_t count) {
				off_t j;
				for (j = 0; j < count; j++) vmSetPageUsedPageFree(page + j);
				server.vm_stats_used_pages -= count;
				shmLog(SHM_DEBUG, "Mark FREE pages: %lld pages at %lld\n", (long long)count,
				       (long long)page);
}

int vmFree(off_t page) {
				off_t byte = page / 8;
				int bit = page & 7;
				return (server.vm_bitmap[byte] & (1 << bit)) == 0;
}

int vmFindAdjacentPages(off_t *first, off_t n) {
				off_t base, offset = 0, since_jump = 0, numfree = 0;
				if (server.vm_near_pages == SHM_VM_MAX_NEAR_PAGES) {
								server.vm_near_pages = 0;
								server.vm_next_page = 0;
				}
				server.vm_near_pages++;
				base = server.vm_next_page;
				while (offset < server.vm_pages) {
								off_t this = base + offset;
								if (this >= server.vm_pages) {
												this -= server.vm_pages;
												if (this == 0) {
																numfree = 0;
												}
								}
								if (vmFree(this)) {
												numfree++;
												if (numfree == n) {
																*first = this - (n - 1);
																server.vm_next_page = this + 1;
																shmLog(SHM_DEBUG, "FOUND CONTIGUOUS PAGES: %lld pages at %lld\n",
																       (long long)n, (long long)*first);
																return SHM_OK;
												}
								} else {
												numfree = 0;
								}
								since_jump++;
								if (!numfree && since_jump >= SHM_VM_MAX_RANDOM_JUMP / 4) {
												offset += random() % SHM_VM_MAX_RANDOM_JUMP;
												since_jump = 0;
								} else {
												offset++;
								}
				}
				return SHM_ERR;
}

int vmSwapOutObj(robj *o, off_t page) {
				if (server.vm_enabled) pthread_mutex_lock(&server.io_swapfile_mutex);
				if (fseeko(server.vm_fp, page * server.vm_page_size, SEEK_SET) == -1) {
								if (server.vm_enabled) pthread_mutex_unlock(&server.io_swapfile_mutex);
								shmLog(SHM_WARNING,
								       "Critical VM problem in vmSwapOutObj(): can't seek: %s",
								       strerror(errno));
								return SHM_ERR;
				}
				sdbSaveObject(server.vm_fp, o);
				fflush(server.vm_fp);
				if (server.vm_enabled) pthread_mutex_unlock(&server.io_swapfile_mutex);
				return SHM_OK;
}

vmptr *vmSwapOutObjBlocking(robj *val) {
				off_t pages = sdbSavedObjectPages(val);
				off_t page;
				vmptr *vp;
				shmAssert(val->storage == SHM_VM_MEMORY);
				shmAssert(val->refcount == 1);
				if (vmFindAdjacentPages(&page, pages) == SHM_ERR) return NULL;
				if (vmSwapOutObj(val, page) == SHM_ERR) return NULL;
				vp = vmCreatePtr(val->type);
				vp->page = page;
				vp->usedpages = pages;
				objDecRefCount(val);
				vmSetPageUsedPagesUsed(page, pages);
				shmLog(SHM_DEBUG, "VM: object %p swapped out at %lld (%lld pages)",
				       (void *)val, (unsigned long long)page, (unsigned long long)pages);
				server.vm_stats_swapped_objects++;
				server.vm_stats_swapouts++;
				return vp;
}

robj *vmSwapInObj(off_t page, int type) {
				robj *o;
				if (server.vm_enabled) pthread_mutex_lock(&server.io_swapfile_mutex);
				if (fseeko(server.vm_fp, page * server.vm_page_size, SEEK_SET) == -1) {
								shmLog(SHM_WARNING,
								       "Unrecoverable VM problem in vmSwapInObj(): can't seek: %s",
								       strerror(errno));
								_exit(1);
				}
				o = sdbLoadObject(type, server.vm_fp);
				if (o == NULL) {
								shmLog(SHM_WARNING,
								       "Unrecoverable VM problem in vmSwapInObj(): can't load "
								       "object from swap file: %s",
								       strerror(errno));
								_exit(1);
				}
				if (server.vm_enabled) pthread_mutex_unlock(&server.io_swapfile_mutex);
				return o;
}

robj *vmBasicReadObj(vmptr *vp, int preview) {
				robj *val;
				shmAssert(vp->type == SHM_VMPTR &&
				          (vp->storage == SHM_VM_SWAPPED || vp->storage == SHM_VM_LOADING));
				val = vmSwapInObj(vp->page, vp->vtype);
				if (!preview) {
								shmLog(SHM_DEBUG, "VM: object %p loaded from disk", (void *)vp);
								vmSetPageUsedPagesFree(vp->page, vp->usedpages);
								dbfree(vp);
								server.vm_stats_swapped_objects--;
				} else {
								shmLog(SHM_DEBUG, "VM: object %p previewed from disk", (void *)vp);
				}
				server.vm_stats_swapins++;
				return val;
}

robj *vmReadObj(robj *o) {
				if (o->storage == SHM_VM_LOADING) vmStopThreadedIOJob(o);
				return vmBasicReadObj((vmptr *)o, 0);
}

robj *vmPreviewObj(robj *o) {
				return vmBasicReadObj((vmptr *)o, 1);
}

double vmGetObjSwappability(robj *o) {
				time_t minage = objEstimateIdleTime(o);
				long asize = 0, elesize;
				robj *ele;
				list *l;
				lNode *ln;
				hash *d;
				struct hashEntry *de;
				int z;
				if (minage <= 0) return 0;
				switch (o->type) {
				case SHM_STRING:
								if (o->encoding != SHM_ENCODING_RAW) {
												asize = sizeof(*o);
								} else {
												asize = dstrlen(o->ptr) + sizeof(*o) + sizeof(long) * 2;
								}
								break;
				case SHM_LIST:
								if (o->encoding == SHM_ENCODING_SORTLIST) {
												asize = sizeof(*o) + sortlistSize(o->ptr);
								} else {
												l = o->ptr;
												ln = lFirst(l);
												asize = sizeof(list);
												if (ln) {
																ele = ln->value;
																elesize = (ele->encoding == SHM_ENCODING_RAW)
																          ? (sizeof(*o) + dstrlen(ele->ptr))
																          : sizeof(*o);
																asize += (sizeof(lNode) + elesize) * lLength(l);
												}
								}
								break;
				case SHM_SET:
				case SHM_SORTSET:
								z = (o->type == SHM_SORTSET);
								d = z ? ((sortset *)o->ptr)->hash : o->ptr;
								if (!z && o->encoding == SHM_ENCODING_IS) {
												is *is = o->ptr;
												asize = sizeof(*is) + is->encoding * is->length;
								} else {
												asize = sizeof(hash) + (sizeof(struct hashEntry *) * hashSlots(d));
												if (z) asize += sizeof(sortset) - sizeof(hash);
												if (hashSize(d)) {
																de = hashGetRandomKey(d);
																ele = hashGetEntryKey(de);
																elesize = (ele->encoding == SHM_ENCODING_RAW)
																          ? (sizeof(*o) + dstrlen(ele->ptr))
																          : sizeof(*o);
																asize += (sizeof(struct hashEntry) + elesize) * hashSize(d);
																if (z) asize += sizeof(sortskiplistNode) * hashSize(d);
												}
								}
								break;
				case SHM_HASH:
								if (o->encoding == SHM_ENCODING_ZIPMAP) {
												unsigned char *p = zipmapRewind((unsigned char *)o->ptr);
												unsigned int len = zipmapLen((unsigned char *)o->ptr);
												unsigned int klen, vlen;
												unsigned char *key, *val;
												if ((p = zipmapNext(p, &key, &klen, &val, &vlen)) == NULL) {
																klen = 0;
																vlen = 0;
												}
												asize = len * (klen + vlen + 3);
								} else if (o->encoding == SHM_ENCODING_HT) {
												d = o->ptr;
												asize = sizeof(hash) + (sizeof(struct hashEntry *) * hashSlots(d));
												if (hashSize(d)) {
																de = hashGetRandomKey(d);
																ele = hashGetEntryKey(de);
																elesize = (ele->encoding == SHM_ENCODING_RAW)
																          ? (sizeof(*o) + dstrlen(ele->ptr))
																          : sizeof(*o);
																ele = hashGetEntryVal(de);
																elesize = (ele->encoding == SHM_ENCODING_RAW)
																          ? (sizeof(*o) + dstrlen(ele->ptr))
																          : sizeof(*o);
																asize += (sizeof(struct hashEntry) + elesize) * hashSize(d);
												}
								}
								break;
				}
				return (double)minage * log(1 + asize);
}

int vmSwapOutOneObj(int usethreads) {
				int j, i;
				struct hashEntry *best = NULL;
				double best_swappability = 0;
				shmDb *best_db = NULL;
				robj *val;
				dstr key;
				for (j = 0; j < server.dbnum; j++) {
								shmDb *db = server.db + j;
								int maxtries = 100;
								if (hashSize(db->hash) == 0) continue;
								for (i = 0; i < 5; i++) {
												hashEntry *de;
												double swappability;
												if (maxtries) maxtries--;
												de = hashGetRandomKey(db->hash);
												val = hashGetEntryVal(de);
												if (val->storage != SHM_VM_MEMORY || val->refcount != 1) {
																if (maxtries) i--;
																continue;
												}
												swappability = vmGetObjSwappability(val);
												if (!best || swappability > best_swappability) {
																best = de;
																best_swappability = swappability;
																best_db = db;
												}
								}
				}
				if (best == NULL) return SHM_ERR;
				key = hashGetEntryKey(best);
				val = hashGetEntryVal(best);
				shmLog(SHM_DEBUG, "Key with best swappability: %s, %f", key,
				       best_swappability);
				if (usethreads) {
								robj *keyobj = objCreateString(key, dstrlen(key));
								vmSwapOutObjThreaded(keyobj, val, best_db);
								objDecRefCount(keyobj);
								return SHM_OK;
				} else {
								vmptr *vp;
								if ((vp = vmSwapOutObjBlocking(val)) != NULL) {
												hashGetEntryVal(best) = vp;
												return SHM_OK;
								} else {
												return SHM_ERR;
								}
				}
}

int vmSwapOutOneObjBlocking() {
				return vmSwapOutOneObj(0);
}

int vmSwapOutOneObjThreaded() {
				return vmSwapOutOneObj(1);
}

int vmCanSwapOutObj(void) {
				return (server.asavechildpid == -1 && server.bgrewritechildpid == -1);
}

void vmFreeIOJob(iojob *j) {
				if ((j->type == SHM_IOJOB_PREPARE_SWAP || j->type == SHM_IOJOB_DO_SWAP ||
				     j->type == SHM_IOJOB_LOAD) &&
				    j->val != NULL) {
								if (j->val->storage == SHM_VM_SWAPPING) j->val->storage = SHM_VM_MEMORY;
								objDecRefCount(j->val);
				}
				objDecRefCount(j->key);
				dbfree(j);
}

void vmThreadedIODoneJob(elLoop *el, int fd, void *privdata,
                              int mask) {
				char buf[1];
				int retval, processed = 0, toprocess = -1, trytoswap = 1;
				SHM_NOTUSED(el);
				SHM_NOTUSED(mask);
				SHM_NOTUSED(privdata);
				if (privdata != NULL) trytoswap = 0;
				while ((retval = read(fd, buf, 1)) == 1) {
								iojob *j;
								lNode *ln;
								struct hashEntry *de;
								shmLog(SHM_DEBUG, "Processing I/O completed job");
								vmLockThreadedIO();
								shmAssert(lLength(server.io_processed) != 0);
								if (toprocess == -1) {
												toprocess =
																(lLength(server.io_processed) * SHM_MAX_COMPLETED_JOBS_PROCESSED) /
																100;
												if (toprocess <= 0) toprocess = 1;
								}
								ln = lFirst(server.io_processed);
								j = ln->value;
					lDelNode(server.io_processed, ln);
								unvmLockThreadedIO();
								if (j->canceled) {
												vmFreeIOJob(j);
												continue;
								}
								shmLog(SHM_DEBUG, "COMPLETED Job type: %d, ID %p, key: %s", j->type,
								       (void *)j->id, (unsigned char *)j->key->ptr);
								de = hashFind(j->db->hash, j->key->ptr);
								shmAssert(de != NULL);
								if (j->type == SHM_IOJOB_LOAD) {
												shmDb *db;
												vmptr *vp = hashGetEntryVal(de);
												vmSetPageUsedPagesFree(vp->page, vp->usedpages);
												shmLog(SHM_DEBUG, "VM: object %s loaded from disk (threaded)",
												       (unsigned char *)j->key->ptr);
												server.vm_stats_swapped_objects--;
												server.vm_stats_swapins++;
												hashGetEntryVal(de) = j->val;
												objIncRefCount(j->val);
												db = j->db;
												vmHandleClientsBlockedOnSwappedKey(db, j->key);
												vmFreeIOJob(j);
												dbfree(vp);
								} else if (j->type == SHM_IOJOB_PREPARE_SWAP) {
												if (!vmCanSwapOutObj() ||
												    vmFindAdjacentPages(&j->page, j->pages) == SHM_ERR) {
																j->val->storage = SHM_VM_MEMORY;
																vmFreeIOJob(j);
												} else {
																vmSetPageUsedPagesUsed(j->page, j->pages);
																j->type = SHM_IOJOB_DO_SWAP;
																vmLockThreadedIO();
																vmQueueIOJob(j);
																unvmLockThreadedIO();
												}
								} else if (j->type == SHM_IOJOB_DO_SWAP) {
												vmptr *vp;
												if (j->val->storage != SHM_VM_SWAPPING) {
																vmptr *vp = (vmptr *)j->id;
																printf("storage: %d\n", vp->storage);
																printf("key->name: %s\n", (char *)j->key->ptr);
																printf("val: %p\n", (void *)j->val);
																printf("val->type: %d\n", j->val->type);
																printf("val->ptr: %s\n", (char *)j->val->ptr);
												}
												shmAssert(j->val->storage == SHM_VM_SWAPPING);
												vp = vmCreatePtr(j->val->type);
												vp->page = j->page;
												vp->usedpages = j->pages;
												hashGetEntryVal(de) = vp;
												j->val->storage = SHM_VM_MEMORY;
												objDecRefCount(j->val);
												shmLog(SHM_DEBUG,
												       "VM: object %s swapped out at %lld (%lld pages) (threaded)",
												       (unsigned char *)j->key->ptr, (unsigned long long)j->page,
												       (unsigned long long)j->pages);
												server.vm_stats_swapped_objects++;
												server.vm_stats_swapouts++;
												vmFreeIOJob(j);
												if (trytoswap && vmCanSwapOutObj() &&
												    dbmalloc_used_memory() > server.vm_max_memory) {
																int more = 1;
																while (more) {
																				vmLockThreadedIO();
																				more =
																								lLength(server.io_newjobs) < (unsigned)server.vm_max_threads;
																				unvmLockThreadedIO();
																				if (vmSwapOutOneObjThreaded() == SHM_ERR) {
																								trytoswap = 0;
																								break;
																				}
																}
												}
								}
								processed++;
								if (processed == toprocess) return;
				}
				if (retval < 0 && errno != EAGAIN) {
								shmLog(SHM_WARNING,
								       "WARNING: read(2) error in vmThreadedIODoneJob() %s",
								       strerror(errno));
				}
}

void vmLockThreadedIO(void) {
				pthread_mutex_lock(&server.io_mutex);
}

void unvmLockThreadedIO(void) {
				pthread_mutex_unlock(&server.io_mutex);
}

void vmStopThreadedIOJob(robj *o) {
				list *lists[3] = {server.io_newjobs, server.io_processing,
								          server.io_processed};
				int i;
				shmAssert(o->storage == SHM_VM_LOADING || o->storage == SHM_VM_SWAPPING);
again:
				vmLockThreadedIO();
				for (i = 0; i < 3; i++) {
								lNode *ln;
								lIter li;
					lRewind(lists[i], &li);
								while ((ln = lNext(&li)) != NULL) {
												iojob *job = ln->value;
												if (job->canceled) continue;
												if (job->id == o) {
																shmLog(SHM_DEBUG, "*** CANCELED %p (key %s) (type %d) (LIST ID %d)\n",
																       (void *)job, (char *)job->key->ptr, job->type, i);
																if (i != 1 && job->type == SHM_IOJOB_DO_SWAP)
																				vmSetPageUsedPagesFree(job->page, job->pages);
																switch (i) {
																case 0:
																				vmFreeIOJob(job);
																		lDelNode(lists[i], ln);
																				break;
																case 1:
																				unvmLockThreadedIO();
																				usleep(1);
																				goto again;
																case 2:
																				job->canceled = 1;
																				break;
																}
																if (o->storage == SHM_VM_LOADING)
																				o->storage = SHM_VM_SWAPPED;
																else if (o->storage == SHM_VM_SWAPPING)
																				o->storage = SHM_VM_MEMORY;
																unvmLockThreadedIO();
																shmLog(SHM_DEBUG, "*** DONE");
																return;
												}
								}
				}
				unvmLockThreadedIO();
				printf("Not found: %p\n", (void *)o);
				shmAssert(1 != 1);
}

void *vmIOThreadProc(void *arg) {
				iojob *j;
				lNode *ln;
				SHM_NOTUSED(arg);
				pthread_detach(pthread_self());
				while (1) {
								vmLockThreadedIO();
								if (lLength(server.io_newjobs) == 0) {
												shmLog(SHM_DEBUG, "Thread %ld exiting, nothing to do",
												       (long)pthread_self());
												server.io_active_threads--;
												unvmLockThreadedIO();
												return NULL;
								}
								ln = lFirst(server.io_newjobs);
								j = ln->value;
					lDelNode(server.io_newjobs, ln);
								j->thread = pthread_self();
					lAddNodeTail(server.io_processing, j);
								ln = lLast(server.io_processing);
								unvmLockThreadedIO();
								shmLog(SHM_DEBUG, "Thread %ld got a new job (type %d): %p about key '%s'",
								       (long)pthread_self(), j->type, (void *)j, (char *)j->key->ptr);
								if (j->type == SHM_IOJOB_LOAD) {
												vmptr *vp = (vmptr *)j->id;
												j->val = vmSwapInObj(j->page, vp->vtype);
								} else if (j->type == SHM_IOJOB_PREPARE_SWAP) {
												j->pages = sdbSavedObjectPages(j->val);
								} else if (j->type == SHM_IOJOB_DO_SWAP) {
												if (vmSwapOutObj(j->val, j->page) == SHM_ERR) j->canceled = 1;
								}
								shmLog(SHM_DEBUG, "Thread %ld completed the job: %p (key %s)",
								       (long)pthread_self(), (void *)j, (char *)j->key->ptr);
								vmLockThreadedIO();
					lDelNode(server.io_processing, ln);
					lAddNodeTail(server.io_processed, j);
								unvmLockThreadedIO();
								shmAssert(write(server.io_ready_pipe_write, "x", 1) == 1);
				}
				return NULL;
}

void vmSpawnIOThread(void) {
				pthread_t thread;
				sigset_t mask, omask;
				int err;
				sigemptyset(&mask);
				sigaddset(&mask, SIGCHLD);
				sigaddset(&mask, SIGHUP);
				sigaddset(&mask, SIGPIPE);
				pthread_sigmask(SIG_SETMASK, &mask, &omask);
				while ((err = pthread_create(&thread, &server.io_threads_attr,
				                             vmIOThreadProc, NULL)) != 0) {
								shmLog(SHM_WARNING, "Unable to spawn an I/O thread: %s", strerror(err));
								usleep(1000000);
				}
				pthread_sigmask(SIG_SETMASK, &omask, NULL);
				server.io_active_threads++;
}

void vmWaitIOJobQueueEmpty(void) {
				while (1) {
								int io_processed_len;
								vmLockThreadedIO();
								if (lLength(server.io_newjobs) == 0 &&
								    lLength(server.io_processing) == 0 &&
								    server.io_active_threads == 0) {
												unvmLockThreadedIO();
												return;
								}
								io_processed_len = lLength(server.io_processed);
								unvmLockThreadedIO();
								if (io_processed_len) {
												vmThreadedIODoneJob(NULL, server.io_ready_pipe_read,
												                         (void *)0xdeadbeef, 0);
												usleep(1000);
								} else {
												usleep(10000);
								}
				}
}

void vmReopenSwap(void) {
				server.vm_fp = fopen(server.vm_swap_file, "r+b");
				if (server.vm_fp == NULL) {
								shmLog(SHM_WARNING, "Can't re-open the VM swap file: %s. Exiting.",
								       server.vm_swap_file);
								_exit(1);
				}
				server.vm_fd = fileno(server.vm_fp);
}

void vmQueueIOJob(iojob *j) {
				shmLog(SHM_DEBUG, "Queued IO Job %p type %d about key '%s'\n", (void *)j,
				       j->type, (char *)j->key->ptr);
	lAddNodeTail(server.io_newjobs, j);
				if (server.io_active_threads < server.vm_max_threads) vmSpawnIOThread();
}

int vmSwapOutObjThreaded(robj *key, robj *val, shmDb *db) {
				iojob *j;
				j = dbmalloc(sizeof(*j));
				j->type = SHM_IOJOB_PREPARE_SWAP;
				j->db = db;
				j->key = key;
				objIncRefCount(key);
				j->id = j->val = val;
				objIncRefCount(val);
				j->canceled = 0;
				j->thread = (pthread_t)-1;
				val->storage = SHM_VM_SWAPPING;
				vmLockThreadedIO();
				vmQueueIOJob(j);
				unvmLockThreadedIO();
				return SHM_OK;
}

int vmWaitSwappedKey(shmClient *c, robj *key) {
				struct hashEntry *de;
				robj *o;
				list *l;
				de = hashFind(c->db->hash, key->ptr);
				if (de == NULL) return 0;
				o = hashGetEntryVal(de);
				if (o->storage == SHM_VM_MEMORY) {
								return 0;
				} else if (o->storage == SHM_VM_SWAPPING) {
								vmStopThreadedIOJob(o);
								return 0;
				}
	lAddNodeTail(c->io_keys, key);
				objIncRefCount(key);
				de = hashFind(c->db->io_keys, key);
				if (de == NULL) {
								int retval;
								l = lCreate();
								retval = hashAdd(c->db->io_keys, key, l);
								objIncRefCount(key);
								shmAssert(retval == HASH_OK);
				} else {
								l = hashGetEntryVal(de);
				}
	lAddNodeTail(l, c);
				if (o->storage == SHM_VM_SWAPPED) {
								iojob *j;
								vmptr *vp = (vmptr *)o;
								o->storage = SHM_VM_LOADING;
								j = dbmalloc(sizeof(*j));
								j->type = SHM_IOJOB_LOAD;
								j->db = c->db;
								j->id = (robj *)vp;
								j->key = key;
								objIncRefCount(key);
								j->page = vp->page;
								j->val = NULL;
								j->canceled = 0;
								j->thread = (pthread_t)-1;
								vmLockThreadedIO();
								vmQueueIOJob(j);
								unvmLockThreadedIO();
				}
				return 1;
}

void vmWaitMultipleSwappedKeys(shmClient *c, struct cmdShm *cmd, int argc,
                                robj **argv) {
				int j, last;
				if (cmd->vm_firstkey == 0) return;
				last = cmd->vm_lastkey;
				if (last < 0) last = argc + last;
				for (j = cmd->vm_firstkey; j <= last; j += cmd->vm_keystep) {
								shmAssert(j < argc);
								vmWaitSwappedKey(c, argv[j]);
				}
}

void vmSortunionPreloadKeys(shmClient *c, struct cmdShm *cmd,
                                         int argc, robj **argv) {
				int i, num;
				SHM_NOTUSED(cmd);
				num = atoi(argv[2]->ptr);
				if (num > (argc - 3)) return;
				for (i = 0; i < num; i++) {
								vmWaitSwappedKey(c, argv[3 + i]);
				}
}

void vmBeginPreloadKeys(shmClient *c, struct cmdShm *cmd,
                                  int argc, robj **argv) {
				int i, margc;
				struct cmdShm *mcmd;
				robj **margv;
				SHM_NOTUSED(cmd);
				SHM_NOTUSED(argc);
				SHM_NOTUSED(argv);
				if (!(c->flags & SHM_BEGIN)) return;
				for (i = 0; i < c->beginstate.count; i++) {
								mcmd = c->beginstate.commands[i].cmd;
								margc = c->beginstate.commands[i].argc;
								margv = c->beginstate.commands[i].argv;
								if (mcmd->vm_preload_proc != NULL) {
												mcmd->vm_preload_proc(c, mcmd, margc, margv);
								} else {
												vmWaitMultipleSwappedKeys(c, mcmd, margc, margv);
								}
				}
}

int vmBlockClientOnSwappedKeys(shmClient *c) {
				if (c->cmd->vm_preload_proc != NULL) {
								c->cmd->vm_preload_proc(c, c->cmd, c->argc, c->argv);
				} else {
								vmWaitMultipleSwappedKeys(c, c->cmd, c->argc, c->argv);
				}
				if (lLength(c->io_keys)) {
								c->flags |= SHM_IO_WAIT;
								elDeleteFileEvent(server.el, c->fd, EL_READABLE);
								server.vm_blocked_clients++;
								return 1;
				} else {
								return 0;
				}
}

int vmDontWaitForSwappedKey(shmClient *c, robj *key) {
				list *l;
				lNode *ln;
				lIter li;
				struct hashEntry *de;
				objIncRefCount(key);
	lRewind(c->io_keys, &li);
				while ((ln = lNext(&li)) != NULL) {
								if (objEqualString(ln->value, key)) {
									lDelNode(c->io_keys, ln);
												break;
								}
				}
				shmAssert(ln != NULL);
				de = hashFind(c->db->io_keys, key);
				shmAssert(de != NULL);
				l = hashGetEntryVal(de);
				ln = lSearchKey(l, c);
				shmAssert(ln != NULL);
	lDelNode(l, ln);
				if (lLength(l) == 0) hashDelete(c->db->io_keys, key);
				objDecRefCount(key);
				return lLength(c->io_keys) == 0;
}

void vmHandleClientsBlockedOnSwappedKey(shmDb *db, robj *key) {
				struct hashEntry *de;
				list *l;
				lNode *ln;
				int len;
				de = hashFind(db->io_keys, key);
				if (!de) return;
				l = hashGetEntryVal(de);
				len = lLength(l);
				while (len--) {
								ln = lFirst(l);
								shmClient *c = ln->value;
								if (vmDontWaitForSwappedKey(c, key)) {
									lAddNodeTail(server.io_ready_clients, c);
								}
				}
}
