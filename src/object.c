#include "shm.h"
#include <pthread.h>
#include <math.h>

robj *objCreate(int type, void *ptr) {
  robj *o = dbmalloc(sizeof(*o));
  o->type = type;
  o->encoding = SHM_ENCODING_RAW;
  o->ptr = ptr;
  o->refcount = 1;
  o->lru = server.lruclock;
  o->storage = SHM_VM_MEMORY;
  return o;
}

robj *objCreateString(char *ptr, size_t len) {
  return objCreate(SHM_STRING, dstrnewlen(ptr, len));
}

robj *objCreateStringFromLongLong(long long value) {
  robj *o;
  if (value >= 0 && value < SHM_SHARED_INTEGERS &&
      pthread_equal(pthread_self(), server.mainthread)) {
    objIncRefCount(shared.integers[value]);
    o = shared.integers[value];
  } else {
    if (value >= LONG_MIN && value <= LONG_MAX) {
      o = objCreate(SHM_STRING, NULL);
      o->encoding = SHM_ENCODING_INT;
      o->ptr = (void *)((long)value);
    } else {
      o = objCreate(SHM_STRING, dstrfromlonglong(value));
    }
  }
  return o;
}

robj *objStringDup(robj *o) {
  shmAssert(o->encoding == SHM_ENCODING_RAW);
  return objCreateString(o->ptr, dstrlen(o->ptr));
}

robj *objCreateList(void) {
  list *l = lCreate();
  robj *o = objCreate(SHM_LIST, l);
  lSetFreeMethod(l, objDecRefCount);
  o->encoding = SHM_ENCODING_LINKEDLIST;
  return o;
}

robj *objCreateSortlIst(void) {
  unsigned char *zl = sortlistNew();
  robj *o = objCreate(SHM_LIST, zl);
  o->encoding = SHM_ENCODING_SORTLIST;
  return o;
}

robj *objCreateSet(void) {
  hash *d = hashCreate(&setHashType, NULL);
  robj *o = objCreate(SHM_SET, d);
  o->encoding = SHM_ENCODING_HT;
  return o;
}

robj *objCreateIs(void) {
  is *is = isNew();
  robj *o = objCreate(SHM_SET, is);
  o->encoding = SHM_ENCODING_IS;
  return o;
}

robj *objCreateHash(void) {
  unsigned char *zm = zipmapNew();
  robj *o = objCreate(SHM_HASH, zm);
  o->encoding = SHM_ENCODING_ZIPMAP;
  return o;
}

robj *objCreateSortSet(void) {
  sortset *zs = dbmalloc(sizeof(*zs));
  robj *o;
  zs->hash = hashCreate(&sortsetHashType, NULL);
  zs->zsl = zslCreate();
  o = objCreate(SHM_SORTSET, zs);
  o->encoding = SHM_ENCODING_SKIPLIST;
  return o;
}

void objFreeString(robj *o) {
  if (o->encoding == SHM_ENCODING_RAW) {
    dstrfree(o->ptr);
  }
}

void objFreeList(robj *o) {
  switch (o->encoding) {
    case SHM_ENCODING_LINKEDLIST:
      lRelease((list *) o->ptr);
      break;
    case SHM_ENCODING_SORTLIST:
      dbfree(o->ptr);
      break;
    default:
      shmPanic("Unknown list encoding type");
  }
}

void objFreeSet(robj *o) {
  switch (o->encoding) {
    case SHM_ENCODING_HT:
      hashRelease((hash *)o->ptr);
      break;
    case SHM_ENCODING_IS:
      dbfree(o->ptr);
      break;
    default:
      shmPanic("Unknown set encoding type");
  }
}

void objFreeSortSet(robj *o) {
  sortset *zs = o->ptr;
  hashRelease(zs->hash);
  zslFree(zs->zsl);
  dbfree(zs);
}

void objFreeHash(robj *o) {
  switch (o->encoding) {
    case SHM_ENCODING_HT:
      hashRelease((hash *)o->ptr);
      break;
    case SHM_ENCODING_ZIPMAP:
      dbfree(o->ptr);
      break;
    default:
      shmPanic("Unknown hash encoding type");
      break;
  }
}

void objIncRefCount(robj *o) {
  o->refcount++;
}

void objDecRefCount(void *obj) {
  robj *o = obj;
  if (server.vm_enabled &&
      (o->storage == SHM_VM_SWAPPED || o->storage == SHM_VM_LOADING)) {
    vmptr *vp = obj;
    if (o->storage == SHM_VM_LOADING) vmStopThreadedIOJob(o);
    vmSetPageUsedPagesFree(vp->page, vp->usedpages);
    server.vm_stats_swapped_objects--;
    dbfree(vp);
    return;
  }
  if (o->refcount <= 0) shmPanic("objDecRefCount against refcount <= 0");
  if (server.vm_enabled && o->storage == SHM_VM_SWAPPING)
    vmStopThreadedIOJob(o);
  if (--(o->refcount) == 0) {
    switch (o->type) {
      case SHM_STRING:
        objFreeString(o);
        break;
      case SHM_LIST:
        objFreeList(o);
        break;
      case SHM_SET:
        objFreeSet(o);
        break;
      case SHM_SORTSET:
        objFreeSortSet(o);
        break;
      case SHM_HASH:
        objFreeHash(o);
        break;
      default:
        shmPanic("Unknown object type");
        break;
    }
    o->ptr = NULL;
    dbfree(o);
  }
}

int objCheckType(shmClient *c, robj *o, int type) {
  if (o->type != type) {
    netAdd(c, shared.wrongtypeerr);
    return 1;
  }
  return 0;
}

robj *objTryEncoding(robj *o) {
  long value;
  dstr s = o->ptr;
  if (o->encoding != SHM_ENCODING_RAW) return o;
  if (o->refcount > 1) return o;
  shmAssert(o->type == SHM_STRING);
  if (dstrToLong(s, &value) == SHM_ERR) return o;
  if (server.maxmemory == 0 && value >= 0 && value < SHM_SHARED_INTEGERS &&
      pthread_equal(pthread_self(), server.mainthread)) {
    objDecRefCount(o);
    objIncRefCount(shared.integers[value]);
    return shared.integers[value];
  } else {
    o->encoding = SHM_ENCODING_INT;
    dstrfree(o->ptr);
    o->ptr = (void *)value;
    return o;
  }
}

robj *objGetDecoded(robj *o) {
  robj *dec;
  if (o->encoding == SHM_ENCODING_RAW) {
    objIncRefCount(o);
    return o;
  }
  if (o->type == SHM_STRING && o->encoding == SHM_ENCODING_INT) {
    char buf[32];
    longLongToStr(buf, 32, (long)o->ptr);
    dec = objCreateString(buf, strlen(buf));
    return dec;
  } else {
    shmPanic("Unknown encoding type");
  }
}

int objCompareString(robj *a, robj *b) {
  shmAssert(a->type == SHM_STRING && b->type == SHM_STRING);
  char bufa[128], bufb[128], *astr, *bstr;
  int bothdstr = 1;
  if (a == b) return 0;
  if (a->encoding != SHM_ENCODING_RAW) {
    longLongToStr(bufa, sizeof(bufa), (long)a->ptr);
    astr = bufa;
    bothdstr = 0;
  } else {
    astr = a->ptr;
  }
  if (b->encoding != SHM_ENCODING_RAW) {
    longLongToStr(bufb, sizeof(bufb), (long)b->ptr);
    bstr = bufb;
    bothdstr = 0;
  } else {
    bstr = b->ptr;
  }
  return bothdstr ? dstrcmp(astr, bstr) : strcmp(astr, bstr);
}

int objEqualString(robj *a, robj *b) {
  if (a->encoding != SHM_ENCODING_RAW && b->encoding != SHM_ENCODING_RAW) {
    return a->ptr == b->ptr;
  } else {
    return objCompareString(a, b) == 0;
  }
}

size_t objStringLen(robj *o) {
  shmAssert(o->type == SHM_STRING);
  if (o->encoding == SHM_ENCODING_RAW) {
    return dstrlen(o->ptr);
  } else {
    char buf[32];
    return longLongToStr(buf, 32, (long)o->ptr);
  }
}

int objGetDouble(robj *o, double *target) {
  double value;
  char *eptr;
  if (o == NULL) {
    value = 0;
  } else {
    shmAssert(o->type == SHM_STRING);
    if (o->encoding == SHM_ENCODING_RAW) {
      value = strtod(o->ptr, &eptr);
      if (eptr[0] != '\0' || isnan(value)) return SHM_ERR;
    } else if (o->encoding == SHM_ENCODING_INT) {
      value = (long)o->ptr;
    } else {
      shmPanic("Unknown string encoding");
    }
  }
  *target = value;
  return SHM_OK;
}

int objGetDoubleOrReply(shmClient *c, robj *o, double *target,
                               const char *msg) {
  double value;
  if (objGetDouble(o, &value) != SHM_OK) {
    if (msg != NULL) {
      netAddError(c, (char *)msg);
    } else {
      netAddError(c, "value is not a double");
    }
    return SHM_ERR;
  }
  *target = value;
  return SHM_OK;
}

int objGetLongLong(robj *o, long long *target) {
  long long value;
  char *eptr;
  if (o == NULL) {
    value = 0;
  } else {
    shmAssert(o->type == SHM_STRING);
    if (o->encoding == SHM_ENCODING_RAW) {
      value = strtoll(o->ptr, &eptr, 10);
      if (eptr[0] != '\0') return SHM_ERR;
      if (errno == ERANGE && (value == LLONG_MIN || value == LLONG_MAX))
        return SHM_ERR;
    } else if (o->encoding == SHM_ENCODING_INT) {
      value = (long)o->ptr;
    } else {
      shmPanic("Unknown string encoding");
    }
  }
  if (target) *target = value;
  return SHM_OK;
}

int objGetLongLongOrReply(shmClient *c, robj *o, long long *target,
                                 const char *msg) {
  long long value;
  if (objGetLongLong(o, &value) != SHM_OK) {
    if (msg != NULL) {
      netAddError(c, (char *)msg);
    } else {
      netAddError(c, "value is not an integer or out of range");
    }
    return SHM_ERR;
  }
  *target = value;
  return SHM_OK;
}

int objGetLongFromOrReply(shmClient *c, robj *o, long *target,
                             const char *msg) {
  long long value;
  if (objGetLongLongOrReply(c, o, &value, msg) != SHM_OK) return SHM_ERR;
  if (value < LONG_MIN || value > LONG_MAX) {
    if (msg != NULL) {
      netAddError(c, (char *)msg);
    } else {
      netAddError(c, "value is out of range");
    }
    return SHM_ERR;
  }
  *target = value;
  return SHM_OK;
}

char *objStrEncoding(int encoding) {
  switch (encoding) {
    case SHM_ENCODING_RAW:
      return "raw";
    case SHM_ENCODING_INT:
      return "int";
    case SHM_ENCODING_HT:
      return "hashtable";
    case SHM_ENCODING_ZIPMAP:
      return "zipmap";
    case SHM_ENCODING_LINKEDLIST:
      return "linkedlist";
    case SHM_ENCODING_SORTLIST:
      return "sortlist";
    case SHM_ENCODING_IS:
      return "is";
    case SHM_ENCODING_SKIPLIST:
      return "skiplist";
    default:
      return "unknown";
  }
}

unsigned long objEstimateIdleTime(robj *o) {
  if (server.lruclock >= o->lru) {
    return (server.lruclock - o->lru) * SHM_LRU_CLOCK_RESOLUTION;
  } else {
    return ((SHM_LRU_CLOCK_MAX - o->lru) + server.lruclock) *
           SHM_LRU_CLOCK_RESOLUTION;
  }
}

robj *objCommandLookup(shmClient *c, robj *key) {
  hashEntry *de;
  if ((de = hashFind(c->db->hash, key->ptr)) == NULL) return NULL;
  return (robj *)hashGetEntryVal(de);
}

robj *objCommandLookupOrReply(shmClient *c, robj *key, robj *reply) {
  robj *o = objCommandLookup(c, key);
  if (!o) netAdd(c, reply);
  return o;
}

void objCmd(shmClient *c) {
  robj *o;
  if (!strcasecmp(c->argv[1]->ptr, "refcount") && c->argc == 3) {
    if ((o = objCommandLookupOrReply(c, c->argv[2], shared.nullbulk)) ==
        NULL)
      return;
    netAddLongLong(c, o->refcount);
  } else if (!strcasecmp(c->argv[1]->ptr, "encoding") && c->argc == 3) {
    if ((o = objCommandLookupOrReply(c, c->argv[2], shared.nullbulk)) ==
        NULL)
      return;
    netAddBulkCString(c, objStrEncoding(o->encoding));
  } else if (!strcasecmp(c->argv[1]->ptr, "idletime") && c->argc == 3) {
    if ((o = objCommandLookupOrReply(c, c->argv[2], shared.nullbulk)) ==
        NULL)
      return;
    netAddLongLong(c, objEstimateIdleTime(o));
  } else {
    netAddError(c, "Syntax error. Try OBJECT (refcount|encoding|idletime)");
  }
}
