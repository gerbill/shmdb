#include "shm.h"

int yesnotoi(char *s) {
				if (!strcasecmp(s, "yes"))
								return 1;
				else if (!strcasecmp(s, "no"))
								return 0;
				else
								return -1;
}

void appendServerSaveParams(time_t seconds, int changes) {
				server.saveparams = dbrealloc(
								server.saveparams, sizeof(struct saveparam) * (server.saveparamslen + 1));
				server.saveparams[server.saveparamslen].seconds = seconds;
				server.saveparams[server.saveparamslen].changes = changes;
				server.saveparamslen++;
}

void resetServerSaveParams() {
				dbfree(server.saveparams);
				server.saveparams = NULL;
				server.saveparamslen = 0;
}

void loadServerConfig(char *filename) {
				FILE *fp;
				char buf[SHM_CONFIGLINE_MAX + 1], *err = NULL;
				int linenum = 0;
				dstr line = NULL;
				int really_use_vm = 0;
				if (filename[0] == '-' && filename[1] == '\0')
								fp = stdin;
				else {
								if ((fp = fopen(filename, "r")) == NULL) {
												shmLog(SHM_WARNING, "Fatal error, can't open config file '%s'", filename);
												exit(1);
								}
				}
				while (fgets(buf, SHM_CONFIGLINE_MAX + 1, fp) != NULL) {
								dstr *argv;
								int argc, j;
								linenum++;
								line = dstrnew(buf);
								line = dstrtrim(line, " \t\r\n");
								if (line[0] == '#' || line[0] == '\0') {
												dstrfree(line);
												continue;
								}
								argv = dstrsplitargs(line, &argc);
								dstrtolower(argv[0]);
								if (!strcasecmp(argv[0], "timeout") && argc == 2) {
												server.maxidletime = atoi(argv[1]);
												if (server.maxidletime < 0) {
																err = "Invalid timeout value";
																goto loaderr;
												}
								} else if (!strcasecmp(argv[0], "port") && argc == 2) {
												server.port = atoi(argv[1]);
												if (server.port < 0 || server.port > 65535) {
																err = "Invalid port";
																goto loaderr;
												}
								} else if (!strcasecmp(argv[0], "bind") && argc == 2) {
												server.bindaddr = dbstrdup(argv[1]);
								} else if (!strcasecmp(argv[0], "socketpath") && argc == 2) {
												server.socketpath = dbstrdup(argv[1]);
								} else if (!strcasecmp(argv[0], "save") && argc == 3) {
												int seconds = atoi(argv[1]);
												int changes = atoi(argv[2]);
												if (seconds < 1 || changes < 0) {
																err = "Invalid save parameters";
																goto loaderr;
												}
												appendServerSaveParams(seconds, changes);
								} else if (!strcasecmp(argv[0], "dir") && argc == 2) {
												if (chdir(argv[1]) == -1) {
																shmLog(SHM_WARNING, "Can't chdir to '%s': %s", argv[1],
																       strerror(errno));
																exit(1);
												}
								} else if (!strcasecmp(argv[0], "loglevel") && argc == 2) {
												if (!strcasecmp(argv[1], "debug"))
																server.verbosity = SHM_DEBUG;
												else if (!strcasecmp(argv[1], "verbose"))
																server.verbosity = SHM_VERBOSE;
												else if (!strcasecmp(argv[1], "notice"))
																server.verbosity = SHM_NOTICE;
												else if (!strcasecmp(argv[1], "warning"))
																server.verbosity = SHM_WARNING;
												else {
																err = "Invalid log level. Must be one of debug, notice, warning";
																goto loaderr;
												}
								} else if (!strcasecmp(argv[0], "logfile") && argc == 2) {
												FILE *logfp;
												server.logfile = dbstrdup(argv[1]);
												if (!strcasecmp(server.logfile, "stdout")) {
																dbfree(server.logfile);
																server.logfile = NULL;
												}
												if (server.logfile) {
																logfp = fopen(server.logfile, "a");
																if (logfp == NULL) {
																				err = dstrcatprintf(dstrempty(), "Can't open the log file: %s",
																				                   strerror(errno));
																				goto loaderr;
																}
																fclose(logfp);
												}
								} else if (!strcasecmp(argv[0], "syslog-enabled") && argc == 2) {
												if ((server.syslog_enabled = yesnotoi(argv[1])) == -1) {
																err = "argument must be 'yes' or 'no'";
																goto loaderr;
												}
								} else if (!strcasecmp(argv[0], "syslog-ident") && argc == 2) {
												if (server.syslog_ident) dbfree(server.syslog_ident);
												server.syslog_ident = dbstrdup(argv[1]);
								} else if (!strcasecmp(argv[0], "syslog-facility") && argc == 2) {
												struct {
																const char *name;
																const int value;
												} validSyslogFacilities[] = {
																{"user", LOG_USER},     {"local0", LOG_LOCAL0},
																{"local1", LOG_LOCAL1}, {"local2", LOG_LOCAL2},
																{"local3", LOG_LOCAL3}, {"local4", LOG_LOCAL4},
																{"local5", LOG_LOCAL5}, {"local6", LOG_LOCAL6},
																{"local7", LOG_LOCAL7}, {NULL, 0}
												};
												int i;
												for (i = 0; validSyslogFacilities[i].name; i++) {
																if (!strcasecmp(validSyslogFacilities[i].name, argv[1])) {
																				server.syslog_facility = validSyslogFacilities[i].value;
																				break;
																}
												}
												if (!validSyslogFacilities[i].name) {
																err =
																				"Invalid log facility. Must be one of USER or between "
																				"LOCAL0-LOCAL7";
																goto loaderr;
												}
								} else if (!strcasecmp(argv[0], "databases") && argc == 2) {
												server.dbnum = atoi(argv[1]);
												if (server.dbnum < 1) {
																err = "Invalid number of databases";
																goto loaderr;
												}
								} else if (!strcasecmp(argv[0], "include") && argc == 2) {
												loadServerConfig(argv[1]);
								} else if (!strcasecmp(argv[0], "maxclients") && argc == 2) {
												server.maxclients = atoi(argv[1]);
								} else if (!strcasecmp(argv[0], "maxmemory") && argc == 2) {
												server.maxmemory = memToLongLong(argv[1], NULL);
								} else if (!strcasecmp(argv[0], "maxmemory-policy") && argc == 2) {
												if (!strcasecmp(argv[1], "volatile-lru")) {
																server.maxmemory_policy = SHM_MAXMEMORY_VOLATILE_LRU;
												} else if (!strcasecmp(argv[1], "volatile-random")) {
																server.maxmemory_policy = SHM_MAXMEMORY_VOLATILE_RANDOM;
												} else if (!strcasecmp(argv[1], "volatile-ttl")) {
																server.maxmemory_policy = SHM_MAXMEMORY_VOLATILE_TTL;
												} else if (!strcasecmp(argv[1], "allkeys-lru")) {
																server.maxmemory_policy = SHM_MAXMEMORY_ALLKEYS_LRU;
												} else if (!strcasecmp(argv[1], "allkeys-random")) {
																server.maxmemory_policy = SHM_MAXMEMORY_ALLKEYS_RANDOM;
												} else if (!strcasecmp(argv[1], "noeviction")) {
																server.maxmemory_policy = SHM_MAXMEMORY_NO_EVICTION;
												} else {
																err = "Invalid maxmemory policy";
																goto loaderr;
												}
								} else if (!strcasecmp(argv[0], "maxmemory-samples") && argc == 2) {
												server.maxmemory_samples = atoi(argv[1]);
												if (server.maxmemory_samples <= 0) {
																err = "maxmemory-samples must be 1 or greater";
																goto loaderr;
												}
								} else if (!strcasecmp(argv[0], "slaveof") && argc == 3) {
												server.masterhost = dstrnew(argv[1]);
												server.masterport = atoi(argv[2]);
												server.replstate = SHM_REPL_CONNECT;
								} else if (!strcasecmp(argv[0], "masterauth") && argc == 2) {
												server.masterauth = dbstrdup(argv[1]);
								} else if (!strcasecmp(argv[0], "slave-serve-stale-data") && argc == 2) {
												if ((server.repl_serve_stale_data = yesnotoi(argv[1])) == -1) {
																err = "argument must be 'yes' or 'no'";
																goto loaderr;
												}
								} else if (!strcasecmp(argv[0], "glueoutputbuf")) {
												shmLog(SHM_WARNING, "Deprecated configuration directive: \"%s\"",
												       argv[0]);
								} else if (!strcasecmp(argv[0], "sdbcompression") && argc == 2) {
												if ((server.sdbcompression = yesnotoi(argv[1])) == -1) {
																err = "argument must be 'yes' or 'no'";
																goto loaderr;
												}
								} else if (!strcasecmp(argv[0], "activerehashing") && argc == 2) {
												if ((server.activerehashing = yesnotoi(argv[1])) == -1) {
																err = "argument must be 'yes' or 'no'";
																goto loaderr;
												}
								} else if (!strcasecmp(argv[0], "daemonize") && argc == 2) {
												if ((server.daemonize = yesnotoi(argv[1])) == -1) {
																err = "argument must be 'yes' or 'no'";
																goto loaderr;
												}
								} else if (!strcasecmp(argv[0], "appendonly") && argc == 2) {
												if ((server.appendonly = yesnotoi(argv[1])) == -1) {
																err = "argument must be 'yes' or 'no'";
																goto loaderr;
												}
								} else if (!strcasecmp(argv[0], "appendfilename") && argc == 2) {
												dbfree(server.appendfilename);
												server.appendfilename = dbstrdup(argv[1]);
								} else if (!strcasecmp(argv[0], "no-appendfsync-on-rewrite") && argc == 2) {
												if ((server.no_appendfsync_on_rewrite = yesnotoi(argv[1])) == -1) {
																err = "argument must be 'yes' or 'no'";
																goto loaderr;
												}
								} else if (!strcasecmp(argv[0], "appendfsync") && argc == 2) {
												if (!strcasecmp(argv[1], "no")) {
																server.appendfsync = APPENDFSYNC_NO;
												} else if (!strcasecmp(argv[1], "always")) {
																server.appendfsync = APPENDFSYNC_ALWAYS;
												} else if (!strcasecmp(argv[1], "everysec")) {
																server.appendfsync = APPENDFSYNC_EVERYSEC;
												} else {
																err = "argument must be 'no', 'always' or 'everysec'";
																goto loaderr;
												}
								} else if (!strcasecmp(argv[0], "requirepass") && argc == 2) {
												server.requirepass = dbstrdup(argv[1]);
								} else if (!strcasecmp(argv[0], "pidfile") && argc == 2) {
												dbfree(server.pidfile);
												server.pidfile = dbstrdup(argv[1]);
								} else if (!strcasecmp(argv[0], "dbfilename") && argc == 2) {
												dbfree(server.dbfilename);
												server.dbfilename = dbstrdup(argv[1]);
								} else if (!strcasecmp(argv[0], "vm-enabled") && argc == 2) {
												if ((server.vm_enabled = yesnotoi(argv[1])) == -1) {
																err = "argument must be 'yes' or 'no'";
																goto loaderr;
												}
								} else if (!strcasecmp(argv[0], "really-use-vm") && argc == 2) {
												if ((really_use_vm = yesnotoi(argv[1])) == -1) {
																err = "argument must be 'yes' or 'no'";
																goto loaderr;
												}
								} else if (!strcasecmp(argv[0], "vm-swap-file") && argc == 2) {
												dbfree(server.vm_swap_file);
												server.vm_swap_file = dbstrdup(argv[1]);
								} else if (!strcasecmp(argv[0], "vm-max-memory") && argc == 2) {
												server.vm_max_memory = memToLongLong(argv[1], NULL);
								} else if (!strcasecmp(argv[0], "vm-page-size") && argc == 2) {
												server.vm_page_size = memToLongLong(argv[1], NULL);
								} else if (!strcasecmp(argv[0], "vm-pages") && argc == 2) {
												server.vm_pages = memToLongLong(argv[1], NULL);
								} else if (!strcasecmp(argv[0], "vm-max-threads") && argc == 2) {
												server.vm_max_threads = strtoll(argv[1], NULL, 10);
								} else if (!strcasecmp(argv[0], "hash-max-zipmap-entries") && argc == 2) {
												server.hash_max_zipmap_entries = memToLongLong(argv[1], NULL);
								} else if (!strcasecmp(argv[0], "hash-max-zipmap-value") && argc == 2) {
												server.hash_max_zipmap_value = memToLongLong(argv[1], NULL);
								} else if (!strcasecmp(argv[0], "list-max-sortlist-entries") && argc == 2) {
												server.list_max_sortlist_entries = memToLongLong(argv[1], NULL);
								} else if (!strcasecmp(argv[0], "list-max-sortlist-value") && argc == 2) {
												server.list_max_sortlist_value = memToLongLong(argv[1], NULL);
								} else if (!strcasecmp(argv[0], "set-max-is-entries") && argc == 2) {
												server.set_max_is_entries = memToLongLong(argv[1], NULL);
								} else if (!strcasecmp(argv[0], "rename-command") && argc == 3) {
												struct cmdShm *cmd = lookupCmd(argv[1]);
												int retval;
												if (!cmd) {
																err = "No such command in rename-command";
																goto loaderr;
												}
												retval = hashDelete(server.commands, argv[1]);
												shmAssert(retval == HASH_OK);
												if (dstrlen(argv[2]) != 0) {
																dstr copy = dstrdup(argv[2]);
																retval = hashAdd(server.commands, copy, cmd);
																if (retval != HASH_OK) {
																				dstrfree(copy);
																				err = "Target command name already exists";
																				goto loaderr;
																}
												}
								} else if (!strcasecmp(argv[0], "slog-log-slower-than") && argc == 2) {
												server.slog_log_slower_than = strtoll(argv[1], NULL, 10);
								} else if (!strcasecmp(argv[0], "slog-max-len") && argc == 2) {
												server.slog_max_len = strtoll(argv[1], NULL, 10);
								} else {
												err = "Bad directive or wrong number of arguments";
												goto loaderr;
								}
								for (j = 0; j < argc; j++) dstrfree(argv[j]);
								dbfree(argv);
								dstrfree(line);
				}
				if (fp != stdin) fclose(fp);
				if (server.vm_enabled && !really_use_vm) goto vm_warning;
				return;
loaderr:
				fprintf(stderr, "\n*** FATAL CONFIG FILE ERROR ***\n");
				fprintf(stderr, "Reading the configuration file, at line %d\n", linenum);
				fprintf(stderr, ">>> '%s'\n", line);
				fprintf(stderr, "%s\n", err);
				exit(1);
vm_warning:
				fprintf(stderr, "\nARE YOU SURE YOU WANT TO USE VM?\n\n");
				fprintf(stderr, "Shm Virtual Memory is going to be deprecated soon,\n");
				fprintf(stderr, "we think you should NOT use it, but use Shm only if\n");
				fprintf(stderr, "your data is suitable for an in-memory database.\n");
				fprintf(stderr, "If you *really* want VM add this in the config file:\n");
				fprintf(stderr, "\n    really-use-vm yes\n\n");
				exit(1);
}

void configSetCmd(shmClient *c) {
				robj *o;
				long long ll;
				shmAssert(c->argv[2]->encoding == SHM_ENCODING_RAW);
				shmAssert(c->argv[3]->encoding == SHM_ENCODING_RAW);
				o = c->argv[3];
				if (!strcasecmp(c->argv[2]->ptr, "dbfilename")) {
								dbfree(server.dbfilename);
								server.dbfilename = dbstrdup(o->ptr);
				} else if (!strcasecmp(c->argv[2]->ptr, "requirepass")) {
								dbfree(server.requirepass);
								server.requirepass = dbstrdup(o->ptr);
				} else if (!strcasecmp(c->argv[2]->ptr, "masterauth")) {
								dbfree(server.masterauth);
								server.masterauth = dbstrdup(o->ptr);
				} else if (!strcasecmp(c->argv[2]->ptr, "maxmemory")) {
								if (objGetLongLong(o, &ll) == SHM_ERR || ll < 0) goto badfmt;
								server.maxmemory = ll;
								if (server.maxmemory) freeMemoryIfNeeded();
				} else if (!strcasecmp(c->argv[2]->ptr, "maxmemory-policy")) {
								if (!strcasecmp(o->ptr, "volatile-lru")) {
												server.maxmemory_policy = SHM_MAXMEMORY_VOLATILE_LRU;
								} else if (!strcasecmp(o->ptr, "volatile-random")) {
												server.maxmemory_policy = SHM_MAXMEMORY_VOLATILE_RANDOM;
								} else if (!strcasecmp(o->ptr, "volatile-ttl")) {
												server.maxmemory_policy = SHM_MAXMEMORY_VOLATILE_TTL;
								} else if (!strcasecmp(o->ptr, "allkeys-lru")) {
												server.maxmemory_policy = SHM_MAXMEMORY_ALLKEYS_LRU;
								} else if (!strcasecmp(o->ptr, "allkeys-random")) {
												server.maxmemory_policy = SHM_MAXMEMORY_ALLKEYS_RANDOM;
								} else if (!strcasecmp(o->ptr, "noeviction")) {
												server.maxmemory_policy = SHM_MAXMEMORY_NO_EVICTION;
								} else {
												goto badfmt;
								}
				} else if (!strcasecmp(c->argv[2]->ptr, "maxmemory-samples")) {
								if (objGetLongLong(o, &ll) == SHM_ERR || ll <= 0) goto badfmt;
								server.maxmemory_samples = ll;
				} else if (!strcasecmp(c->argv[2]->ptr, "timeout")) {
								if (objGetLongLong(o, &ll) == SHM_ERR || ll < 0 || ll > LONG_MAX)
												goto badfmt;
								server.maxidletime = ll;
				} else if (!strcasecmp(c->argv[2]->ptr, "appendfsync")) {
								if (!strcasecmp(o->ptr, "no")) {
												server.appendfsync = APPENDFSYNC_NO;
								} else if (!strcasecmp(o->ptr, "everysec")) {
												server.appendfsync = APPENDFSYNC_EVERYSEC;
								} else if (!strcasecmp(o->ptr, "always")) {
												server.appendfsync = APPENDFSYNC_ALWAYS;
								} else {
												goto badfmt;
								}
				} else if (!strcasecmp(c->argv[2]->ptr, "no-appendfsync-on-rewrite")) {
								int yn = yesnotoi(o->ptr);
								if (yn == -1) goto badfmt;
								server.no_appendfsync_on_rewrite = yn;
				} else if (!strcasecmp(c->argv[2]->ptr, "appendonly")) {
								int old = server.appendonly;
								int new = yesnotoi(o->ptr);
								if (new == -1) goto badfmt;
								if (old != new) {
												if (new == 0) {
													apendOnlyStop();
												} else {
																if (appendOnlyStart() == SHM_ERR) {
																				netAddError(c, "Unable to turn on AOF. Check server logs.");
																				return;
																}
												}
								}
				} else if (!strcasecmp(c->argv[2]->ptr, "save")) {
								int vlen, j;
								dstr *v = dstrsplitlen(o->ptr, dstrlen(o->ptr), " ", 1, &vlen);
								if (vlen & 1) {
												dstrfreesplitres(v, vlen);
												goto badfmt;
								}
								for (j = 0; j < vlen; j++) {
												char *eptr;
												long val;
												val = strtoll(v[j], &eptr, 10);
												if (eptr[0] != '\0' || ((j & 1) == 0 && val < 1) ||
												    ((j & 1) == 1 && val < 0)) {
																dstrfreesplitres(v, vlen);
																goto badfmt;
												}
								}
								resetServerSaveParams();
								for (j = 0; j < vlen; j += 2) {
												time_t seconds;
												int changes;
												seconds = strtoll(v[j], NULL, 10);
												changes = strtoll(v[j + 1], NULL, 10);
												appendServerSaveParams(seconds, changes);
								}
								dstrfreesplitres(v, vlen);
				} else if (!strcasecmp(c->argv[2]->ptr, "slave-serve-stale-data")) {
								int yn = yesnotoi(o->ptr);
								if (yn == -1) goto badfmt;
								server.repl_serve_stale_data = yn;
				} else if (!strcasecmp(c->argv[2]->ptr, "dir")) {
								if (chdir((char *)o->ptr) == -1) {
												netAddErrorFormat(c, "Changing directory: %s", strerror(errno));
												return;
								}
				} else if (!strcasecmp(c->argv[2]->ptr, "hash-max-zipmap-entries")) {
								if (objGetLongLong(o, &ll) == SHM_ERR || ll < 0) goto badfmt;
								server.hash_max_zipmap_entries = ll;
				} else if (!strcasecmp(c->argv[2]->ptr, "hash-max-zipmap-value")) {
								if (objGetLongLong(o, &ll) == SHM_ERR || ll < 0) goto badfmt;
								server.hash_max_zipmap_value = ll;
				} else if (!strcasecmp(c->argv[2]->ptr, "list-max-sortlist-entries")) {
								if (objGetLongLong(o, &ll) == SHM_ERR || ll < 0) goto badfmt;
								server.list_max_sortlist_entries = ll;
				} else if (!strcasecmp(c->argv[2]->ptr, "list-max-sortlist-value")) {
								if (objGetLongLong(o, &ll) == SHM_ERR || ll < 0) goto badfmt;
								server.list_max_sortlist_value = ll;
				} else if (!strcasecmp(c->argv[2]->ptr, "set-max-is-entries")) {
								if (objGetLongLong(o, &ll) == SHM_ERR || ll < 0) goto badfmt;
								server.set_max_is_entries = ll;
				} else if (!strcasecmp(c->argv[2]->ptr, "slog-log-slower-than")) {
								if (objGetLongLong(o, &ll) == SHM_ERR) goto badfmt;
								server.slog_log_slower_than = ll;
				} else if (!strcasecmp(c->argv[2]->ptr, "slog-max-len")) {
								if (objGetLongLong(o, &ll) == SHM_ERR || ll < 0) goto badfmt;
								server.slog_max_len = (unsigned)ll;
				} else {
								netAddErrorFormat(c, "Unsupported CONFIG parameter: %s",
								                    (char *)c->argv[2]->ptr);
								return;
				}
				netAdd(c, shared.ok);
				return;
badfmt:
				netAddErrorFormat(c, "Invalid argument '%s' for CONFIG SET '%s'",
				                    (char *)o->ptr, (char *)c->argv[2]->ptr);
}

void configGetCmd(shmClient *c) {
				robj *o = c->argv[2];
				void *replylen = addDeferredBeginBulkLength(c);
				char *pattern = o->ptr;
				char buf[128];
				int matches = 0;
				shmAssert(o->encoding == SHM_ENCODING_RAW);
				if (strCmp(pattern, "dir", 0)) {
								char buf[1024];
								if (getcwd(buf, sizeof(buf)) == NULL) buf[0] = '\0';
								netAddBulkCString(c, "dir");
								netAddBulkCString(c, buf);
								matches++;
				}
				if (strCmp(pattern, "dbfilename", 0)) {
								netAddBulkCString(c, "dbfilename");
								netAddBulkCString(c, server.dbfilename);
								matches++;
				}
				if (strCmp(pattern, "requirepass", 0)) {
								netAddBulkCString(c, "requirepass");
								netAddBulkCString(c, server.requirepass);
								matches++;
				}
				if (strCmp(pattern, "masterauth", 0)) {
								netAddBulkCString(c, "masterauth");
								netAddBulkCString(c, server.masterauth);
								matches++;
				}
				if (strCmp(pattern, "maxmemory", 0)) {
								longLongToStr(buf, sizeof(buf), server.maxmemory);
								netAddBulkCString(c, "maxmemory");
								netAddBulkCString(c, buf);
								matches++;
				}
				if (strCmp(pattern, "maxmemory-policy", 0)) {
								char *s;
								switch (server.maxmemory_policy) {
								case SHM_MAXMEMORY_VOLATILE_LRU:
												s = "volatile-lru";
												break;
								case SHM_MAXMEMORY_VOLATILE_TTL:
												s = "volatile-ttl";
												break;
								case SHM_MAXMEMORY_VOLATILE_RANDOM:
												s = "volatile-random";
												break;
								case SHM_MAXMEMORY_ALLKEYS_LRU:
												s = "allkeys-lru";
												break;
								case SHM_MAXMEMORY_ALLKEYS_RANDOM:
												s = "allkeys-random";
												break;
								case SHM_MAXMEMORY_NO_EVICTION:
												s = "noeviction";
												break;
								default:
												s = "unknown";
												break;
								}
								netAddBulkCString(c, "maxmemory-policy");
								netAddBulkCString(c, s);
								matches++;
				}
				if (strCmp(pattern, "maxmemory-samples", 0)) {
								longLongToStr(buf, sizeof(buf), server.maxmemory_samples);
								netAddBulkCString(c, "maxmemory-samples");
								netAddBulkCString(c, buf);
								matches++;
				}
				if (strCmp(pattern, "timeout", 0)) {
								longLongToStr(buf, sizeof(buf), server.maxidletime);
								netAddBulkCString(c, "timeout");
								netAddBulkCString(c, buf);
								matches++;
				}
				if (strCmp(pattern, "appendonly", 0)) {
								netAddBulkCString(c, "appendonly");
								netAddBulkCString(c, server.appendonly ? "yes" : "no");
								matches++;
				}
				if (strCmp(pattern, "no-appendfsync-on-rewrite", 0)) {
								netAddBulkCString(c, "no-appendfsync-on-rewrite");
								netAddBulkCString(c, server.no_appendfsync_on_rewrite ? "yes" : "no");
								matches++;
				}
				if (strCmp(pattern, "appendfsync", 0)) {
								char *policy;
								switch (server.appendfsync) {
								case APPENDFSYNC_NO:
												policy = "no";
												break;
								case APPENDFSYNC_EVERYSEC:
												policy = "everysec";
												break;
								case APPENDFSYNC_ALWAYS:
												policy = "always";
												break;
								default:
												policy = "unknown";
												break;
								}
								netAddBulkCString(c, "appendfsync");
								netAddBulkCString(c, policy);
								matches++;
				}
				if (strCmp(pattern, "save", 0)) {
								dstr buf = dstrempty();
								int j;
								for (j = 0; j < server.saveparamslen; j++) {
												buf = dstrcatprintf(buf, "%ld %d", server.saveparams[j].seconds,
												                   server.saveparams[j].changes);
												if (j != server.saveparamslen - 1) buf = dstrcatlen(buf, " ", 1);
								}
								netAddBulkCString(c, "save");
								netAddBulkCString(c, buf);
								dstrfree(buf);
								matches++;
				}
				if (strCmp(pattern, "slave-serve-stale-data", 0)) {
								netAddBulkCString(c, "slave-serve-stale-data");
								netAddBulkCString(c, server.repl_serve_stale_data ? "yes" : "no");
								matches++;
				}
				if (strCmp(pattern, "hash-max-zipmap-entries", 0)) {
								netAddBulkCString(c, "hash-max-zipmap-entries");
								netAddBulkLongLong(c, server.hash_max_zipmap_entries);
								matches++;
				}
				if (strCmp(pattern, "hash-max-zipmap-value", 0)) {
								netAddBulkCString(c, "hash-max-zipmap-value");
								netAddBulkLongLong(c, server.hash_max_zipmap_value);
								matches++;
				}
				if (strCmp(pattern, "list-max-sortlist-entries", 0)) {
								netAddBulkCString(c, "list-max-sortlist-entries");
								netAddBulkLongLong(c, server.list_max_sortlist_entries);
								matches++;
				}
				if (strCmp(pattern, "list-max-sortlist-value", 0)) {
								netAddBulkCString(c, "list-max-sortlist-value");
								netAddBulkLongLong(c, server.list_max_sortlist_value);
								matches++;
				}
				if (strCmp(pattern, "set-max-is-entries", 0)) {
								netAddBulkCString(c, "set-max-is-entries");
								netAddBulkLongLong(c, server.set_max_is_entries);
								matches++;
				}
				if (strCmp(pattern, "slog-log-slower-than", 0)) {
								netAddBulkCString(c, "slog-log-slower-than");
								netAddBulkLongLong(c, server.slog_log_slower_than);
								matches++;
				}
				if (strCmp(pattern, "slog-max-len", 0)) {
								netAddBulkCString(c, "slog-max-len");
								netAddBulkLongLong(c, server.slog_max_len);
								matches++;
				}
				setDeferredBeginBulkLength(c, replylen, matches * 2);
}

void configCmd(shmClient *c) {
				if (!strcasecmp(c->argv[1]->ptr, "set")) {
								if (c->argc != 4) goto badarity;
								configSetCmd(c);
				} else if (!strcasecmp(c->argv[1]->ptr, "get")) {
								if (c->argc != 3) goto badarity;
								configGetCmd(c);
				} else if (!strcasecmp(c->argv[1]->ptr, "resetstat")) {
								if (c->argc != 2) goto badarity;
								server.stat_keyspace_hits = 0;
								server.stat_keyspace_misses = 0;
								server.stat_numcommands = 0;
								server.stat_numconnections = 0;
								server.stat_expiredkeys = 0;
								netAdd(c, shared.ok);
				} else {
								netAddError(c, "CONFIG subcommand must be one of GET, SET, RESETSTAT");
				}
				return;
badarity:
				netAddErrorFormat(c, "Wrong number of arguments for CONFIG %s",
				                    (char *)c->argv[1]->ptr);
}
