#pragma once

typedef struct shmObject {
  unsigned type : 4;
  unsigned storage : 2;
  unsigned encoding : 4;
  unsigned lru : 22;
  int refcount;
  void *ptr;
} robj;

