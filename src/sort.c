#include "shm.h"
#include "psort.h"

shmSortOperation *createSortOperation(int type, robj *pattern) {
				shmSortOperation *so = dbmalloc(sizeof(*so));
				so->type = type;
				so->pattern = pattern;
				return so;
}

robj *lookupKeyByPattern(shmDb *db, robj *pattern, robj *subst) {
				char *p, *f;
				dstr spat, ssub;
				robj keyobj, fieldobj, *o;
				int prefixlen, sublen, postfixlen, fieldlen;
				struct {
								int len;
								int free;
								char buf[SHM_SORTKEY_MAX + 1];
				} keyname, fieldname;
				spat = pattern->ptr;
				if (spat[0] == '#' && spat[1] == '\0') {
								objIncRefCount(subst);
								return subst;
				}
				subst = objGetDecoded(subst);
				ssub = subst->ptr;
				if (dstrlen(spat) + dstrlen(ssub) - 1 > SHM_SORTKEY_MAX) return NULL;
				p = strchr(spat, '*');
				if (!p) {
								objDecRefCount(subst);
								return NULL;
				}
				if ((f = strstr(p + 1, "->")) != NULL) {
								fieldlen = dstrlen(spat) - (f - spat);
								memcpy(fieldname.buf, f + 2, fieldlen - 1);
								fieldname.len = fieldlen - 2;
				} else {
								fieldlen = 0;
				}
				prefixlen = p - spat;
				sublen = dstrlen(ssub);
				postfixlen = dstrlen(spat) - (prefixlen + 1) - fieldlen;
				memcpy(keyname.buf, spat, prefixlen);
				memcpy(keyname.buf + prefixlen, ssub, sublen);
				memcpy(keyname.buf + prefixlen + sublen, p + 1, postfixlen);
				keyname.buf[prefixlen + sublen + postfixlen] = '\0';
				keyname.len = prefixlen + sublen + postfixlen;
				objDecRefCount(subst);
				initStaticStringObject(keyobj, ((char *)&keyname) + (sizeof(struct dstrhdr)));
				o = lookupKeyRead(db, &keyobj);
				if (o == NULL) return NULL;
				if (fieldlen > 0) {
								if (o->type != SHM_HASH || fieldname.len < 1) return NULL;
								initStaticStringObject(fieldobj,
								                       ((char *)&fieldname) + (sizeof(struct dstrhdr)));
								o = hashTypeGetObject(o, &fieldobj);
				} else {
								if (o->type != SHM_STRING) return NULL;
								objIncRefCount(o);
				}
				return o;
}

int sortCompare(const void *s1, const void *s2) {
				const shmSortObject *so1 = s1, *so2 = s2;
				int cmp;
				if (!server.sort_alpha) {
								if (so1->u.score > so2->u.score) {
												cmp = 1;
								} else if (so1->u.score < so2->u.score) {
												cmp = -1;
								} else {
												cmp = 0;
								}
				} else {
								if (server.sort_bypattern) {
												if (!so1->u.cmpobj || !so2->u.cmpobj) {
																if (so1->u.cmpobj == so2->u.cmpobj)
																				cmp = 0;
																else if (so1->u.cmpobj == NULL)
																				cmp = -1;
																else
																				cmp = 1;
												} else {
																cmp = strcoll(so1->u.cmpobj->ptr, so2->u.cmpobj->ptr);
												}
								} else {
												cmp = objCompareString(so1->obj, so2->obj);
								}
				}
				return server.sort_desc ? -cmp : cmp;
}

void sortCmd(shmClient *c) {
				list *operations;
				unsigned int outputlen = 0;
				int desc = 0, alpha = 0;
				int limit_start = 0, limit_count = -1, start, end;
				int j, dontsort = 0, vectorlen;
				int getop = 0;
				robj *sortval, *sortby = NULL, *storekey = NULL;
				shmSortObject *vector;
				sortval = lookupKeyRead(c->db, c->argv[1]);
				if (sortval == NULL) {
								netAdd(c, shared.emptybeginbulk);
								return;
				}
				if (sortval->type != SHM_SET && sortval->type != SHM_LIST &&
				    sortval->type != SHM_SORTSET) {
								netAdd(c, shared.wrongtypeerr);
								return;
				}
				operations = lCreate();
				lSetFreeMethod(operations, dbfree);
				j = 2;
				objIncRefCount(sortval);
				while (j < c->argc) {
								int leftargs = c->argc - j - 1;
								if (!strcasecmp(c->argv[j]->ptr, "asc")) {
												desc = 0;
								} else if (!strcasecmp(c->argv[j]->ptr, "desc")) {
												desc = 1;
								} else if (!strcasecmp(c->argv[j]->ptr, "alpha")) {
												alpha = 1;
								} else if (!strcasecmp(c->argv[j]->ptr, "limit") && leftargs >= 2) {
												limit_start = atoi(c->argv[j + 1]->ptr);
												limit_count = atoi(c->argv[j + 2]->ptr);
												j += 2;
								} else if (!strcasecmp(c->argv[j]->ptr, "store") && leftargs >= 1) {
												storekey = c->argv[j + 1];
												j++;
								} else if (!strcasecmp(c->argv[j]->ptr, "by") && leftargs >= 1) {
												sortby = c->argv[j + 1];
												if (strchr(c->argv[j + 1]->ptr, '*') == NULL) dontsort = 1;
												j++;
								} else if (!strcasecmp(c->argv[j]->ptr, "get") && leftargs >= 1) {
									lAddNodeTail(operations,
												 createSortOperation(SHM_SORT_GET, c->argv[j + 1]));
												getop++;
												j++;
								} else {
												objDecRefCount(sortval);
									lRelease(operations);
												netAdd(c, shared.syntaxerr);
												return;
								}
								j++;
				}
				switch (sortval->type) {
				case SHM_LIST:
								vectorlen = listTypeLength(sortval);
								break;
				case SHM_SET:
								vectorlen = setTypeSize(sortval);
								break;
				case SHM_SORTSET:
								vectorlen = hashSize(((sortset *)sortval->ptr)->hash);
								break;
				default:
								vectorlen = 0;
								shmPanic("Bad SORT type");
				}
				vector = dbmalloc(sizeof(shmSortObject) * vectorlen);
				j = 0;
				if (sortval->type == SHM_LIST) {
								listTypeIterator *li = listTypeInitIterator(sortval, 0, SHM_TAIL);
								listTypeEntry entry;
								while (listTypeNext(li, &entry)) {
												vector[j].obj = listTypeGet(&entry);
												vector[j].u.score = 0;
												vector[j].u.cmpobj = NULL;
												j++;
								}
								listTypeReleaseIterator(li);
				} else if (sortval->type == SHM_SET) {
								setTypeIterator *si = setTypeInitIterator(sortval);
								robj *ele;
								while ((ele = setTypeNextObject(si)) != NULL) {
												vector[j].obj = ele;
												vector[j].u.score = 0;
												vector[j].u.cmpobj = NULL;
												j++;
								}
								setTypeReleaseIterator(si);
				} else if (sortval->type == SHM_SORTSET) {
								hash *set = ((sortset *)sortval->ptr)->hash;
								hashIterator *di;
								hashEntry *setele;
								di = hashGetIterator(set);
								while ((setele = hashNext(di)) != NULL) {
												vector[j].obj = hashGetEntryKey(setele);
												vector[j].u.score = 0;
												vector[j].u.cmpobj = NULL;
												j++;
								}
								hashReleaseIterator(di);
				} else {
								shmPanic("Unknown type");
				}
				shmAssert(j == vectorlen);
				if (dontsort == 0) {
								for (j = 0; j < vectorlen; j++) {
												robj *byval;
												if (sortby) {
																byval = lookupKeyByPattern(c->db, sortby, vector[j].obj);
																if (!byval) continue;
												} else {
																byval = vector[j].obj;
												}
												if (alpha) {
																if (sortby) vector[j].u.cmpobj = objGetDecoded(byval);
												} else {
																if (byval->encoding == SHM_ENCODING_RAW) {
																				vector[j].u.score = strtod(byval->ptr, NULL);
																} else if (byval->encoding == SHM_ENCODING_INT) {
																				vector[j].u.score = (long)byval->ptr;
																} else {
																				shmAssert(1 != 1);
																}
												}
												if (sortby) {
																objDecRefCount(byval);
												}
								}
				}
				start = (limit_start < 0) ? 0 : limit_start;
				end = (limit_count < 0) ? vectorlen - 1 : start + limit_count - 1;
				if (start >= vectorlen) {
								start = vectorlen - 1;
								end = vectorlen - 2;
				}
				if (end >= vectorlen) end = vectorlen - 1;
				if (dontsort == 0) {
								server.sort_desc = desc;
								server.sort_alpha = alpha;
								server.sort_bypattern = sortby ? 1 : 0;
								if (sortby && (start != 0 || end != vectorlen - 1))
												psort(vector, vectorlen, sizeof(shmSortObject), sortCompare, start, end);
								else
												qsort(vector, vectorlen, sizeof(shmSortObject), sortCompare);
				}
				outputlen = getop ? getop * (end - start + 1) : end - start + 1;
				if (storekey == NULL) {
								netAddBeginBulkLen(c, outputlen);
								for (j = start; j <= end; j++) {
												lNode *ln;
												lIter li;
												if (!getop) netAddBulk(c, vector[j].obj);
									lRewind(operations, &li);
												while ((ln = lNext(&li))) {
																shmSortOperation *sop = ln->value;
																robj *val = lookupKeyByPattern(c->db, sop->pattern, vector[j].obj);
																if (sop->type == SHM_SORT_GET) {
																				if (!val) {
																								netAdd(c, shared.nullbulk);
																				} else {
																								netAddBulk(c, val);
																								objDecRefCount(val);
																				}
																} else {
																				shmAssert(sop->type == SHM_SORT_GET);
																}
												}
								}
				} else {
								robj *sobj = objCreateSortlIst();
								for (j = start; j <= end; j++) {
												lNode *ln;
												lIter li;
												if (!getop) {
																listTypePush(sobj, vector[j].obj, SHM_TAIL);
												} else {
													lRewind(operations, &li);
																while ((ln = lNext(&li))) {
																				shmSortOperation *sop = ln->value;
																				robj *val = lookupKeyByPattern(c->db, sop->pattern, vector[j].obj);
																				if (sop->type == SHM_SORT_GET) {
																								if (!val) val = objCreateString("", 0);
																								listTypePush(sobj, val, SHM_TAIL);
																								objDecRefCount(val);
																				} else {
																								shmAssert(sop->type == SHM_SORT_GET);
																				}
																}
												}
								}
								lookupKeyWrite(c->db, storekey);
								dbReplace(c->db, storekey, sobj);
								server.dirty += 1 + outputlen;
								touchWatchedKey(c->db, storekey);
								netAddLongLong(c, outputlen);
				}
				if (sortval->type == SHM_LIST || sortval->type == SHM_SET)
								for (j = 0; j < vectorlen; j++) objDecRefCount(vector[j].obj);
				objDecRefCount(sortval);
	lRelease(operations);
				for (j = 0; j < vectorlen; j++) {
								if (alpha && vector[j].u.cmpobj) objDecRefCount(vector[j].u.cmpobj);
				}
				dbfree(vector);
}
