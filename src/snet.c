#include "fmacros.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <netdb.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include "snet.h"

static void snetSetError(char *err, const char *fmt, ...) {
				va_list ap;
				if (!err) return;
				va_start(ap, fmt);
				vsnprintf(err, SNET_ERR_LEN, fmt, ap);
				va_end(ap);
}

int snetNonBlock(char *err, int fd) {
				int flags;
				if ((flags = fcntl(fd, F_GETFL)) == -1) {
								snetSetError(err, "fcntl(F_GETFL): %s", strerror(errno));
								return SNET_ERR;
				}
				if (fcntl(fd, F_SETFL, flags | O_NONBLOCK) == -1) {
								snetSetError(err, "fcntl(F_SETFL,O_NONBLOCK): %s", strerror(errno));
								return SNET_ERR;
				}
				return SNET_OK;
}

int snetTcpNoDelay(char *err, int fd) {
				int yes = 1;
				if (setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &yes, sizeof(yes)) == -1) {
								snetSetError(err, "setsockopt TCP_NODELAY: %s", strerror(errno));
								return SNET_ERR;
				}
				return SNET_OK;
}

int snetSetSendBuffer(char *err, int fd, int buffsize) {
				if (setsockopt(fd, SOL_SOCKET, SO_SNDBUF, &buffsize, sizeof(buffsize)) ==
				    -1) {
								snetSetError(err, "setsockopt SO_SNDBUF: %s", strerror(errno));
								return SNET_ERR;
				}
				return SNET_OK;
}

int snetTcpKeepAlive(char *err, int fd) {
				int yes = 1;
				if (setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, &yes, sizeof(yes)) == -1) {
								snetSetError(err, "setsockopt SO_KEEPALIVE: %s", strerror(errno));
								return SNET_ERR;
				}
				return SNET_OK;
}

int snetResolve(char *err, char *host, char *ipbuf) {
				struct sockaddr_in sa;
				sa.sin_family = AF_INET;
				if (inet_aton(host, &sa.sin_addr) == 0) {
								struct hostent *he;
								he = gethostbyname(host);
								if (he == NULL) {
												snetSetError(err, "can't resolve: %s", host);
												return SNET_ERR;
								}
								memcpy(&sa.sin_addr, he->h_addr, sizeof(struct in_addr));
				}
				strcpy(ipbuf, inet_ntoa(sa.sin_addr));
				return SNET_OK;
}

static int snetCreateSocket(char *err, int domain) {
				int s, on = 1;
				if ((s = socket(domain, SOCK_STREAM, 0)) == -1) {
								snetSetError(err, "creating socket: %s", strerror(errno));
								return SNET_ERR;
				}
				if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) == -1) {
								snetSetError(err, "setsockopt SO_REUSEADDR: %s", strerror(errno));
								return SNET_ERR;
				}
				return s;
}
#define SNET_CONNECT_NONE 0
#define SNET_CONNECT_NONBLOCK 1

static int snetTcpGenericConnect(char *err, char *addr, int port, int flags) {
				int s;
				struct sockaddr_in sa;
				if ((s = snetCreateSocket(err, AF_INET)) == SNET_ERR) return SNET_ERR;
				sa.sin_family = AF_INET;
				sa.sin_port = htons(port);
				if (inet_aton(addr, &sa.sin_addr) == 0) {
								struct hostent *he;
								he = gethostbyname(addr);
								if (he == NULL) {
												snetSetError(err, "can't resolve: %s", addr);
												close(s);
												return SNET_ERR;
								}
								memcpy(&sa.sin_addr, he->h_addr, sizeof(struct in_addr));
				}
				if (flags & SNET_CONNECT_NONBLOCK) {
								if (snetNonBlock(err, s) != SNET_OK) return SNET_ERR;
				}
				if (connect(s, (struct sockaddr *)&sa, sizeof(sa)) == -1) {
								if (errno == EINPROGRESS && flags & SNET_CONNECT_NONBLOCK) return s;
								snetSetError(err, "connect: %s", strerror(errno));
								close(s);
								return SNET_ERR;
				}
				return s;
}

int snetTcpConnect(char *err, char *addr, int port) {
				return snetTcpGenericConnect(err, addr, port, SNET_CONNECT_NONE);
}

int snetTcpNonBlockConnect(char *err, char *addr, int port) {
				return snetTcpGenericConnect(err, addr, port, SNET_CONNECT_NONBLOCK);
}

int snetUnixGenericConnect(char *err, char *path, int flags) {
				int s;
				struct sockaddr_un sa;
				if ((s = snetCreateSocket(err, AF_LOCAL)) == SNET_ERR) return SNET_ERR;
				sa.sun_family = AF_LOCAL;
				strncpy(sa.sun_path, path, sizeof(sa.sun_path) - 1);
				if (flags & SNET_CONNECT_NONBLOCK) {
								if (snetNonBlock(err, s) != SNET_OK) return SNET_ERR;
				}
				if (connect(s, (struct sockaddr *)&sa, sizeof(sa)) == -1) {
								if (errno == EINPROGRESS && flags & SNET_CONNECT_NONBLOCK) return s;
								snetSetError(err, "connect: %s", strerror(errno));
								close(s);
								return SNET_ERR;
				}
				return s;
}

int snetUnixConnect(char *err, char *path) {
				return snetUnixGenericConnect(err, path, SNET_CONNECT_NONE);
}

int snetUnixNonBlockConnect(char *err, char *path) {
				return snetUnixGenericConnect(err, path, SNET_CONNECT_NONBLOCK);
}

int snetRead(int fd, char *buf, int count) {
				int nread, totlen = 0;
				while (totlen != count) {
								nread = read(fd, buf, count - totlen);
								if (nread == 0) return totlen;
								if (nread == -1) return -1;
								totlen += nread;
								buf += nread;
				}
				return totlen;
}

int snetWrite(int fd, char *buf, int count) {
				int nwritten, totlen = 0;
				while (totlen != count) {
								nwritten = write(fd, buf, count - totlen);
								if (nwritten == 0) return totlen;
								if (nwritten == -1) return -1;
								totlen += nwritten;
								buf += nwritten;
				}
				return totlen;
}

static int snetListen(char *err, int s, struct sockaddr *sa, socklen_t len) {
				if (bind(s, sa, len) == -1) {
								snetSetError(err, "bind: %s", strerror(errno));
								close(s);
								return SNET_ERR;
				}
				if (listen(s, 511) == -1) {
								snetSetError(err, "listen: %s", strerror(errno));
								close(s);
								return SNET_ERR;
				}
				return SNET_OK;
}

int snetTcpServer(char *err, int port, char *bindaddr) {
				int s;
				struct sockaddr_in sa;
				if ((s = snetCreateSocket(err, AF_INET)) == SNET_ERR) return SNET_ERR;
				memset(&sa, 0, sizeof(sa));
				sa.sin_family = AF_INET;
				sa.sin_port = htons(port);
				sa.sin_addr.s_addr = htonl(INADDR_ANY);
				if (bindaddr && inet_aton(bindaddr, &sa.sin_addr) == 0) {
								snetSetError(err, "invalid bind address");
								close(s);
								return SNET_ERR;
				}
				if (snetListen(err, s, (struct sockaddr *)&sa, sizeof(sa)) == SNET_ERR)
								return SNET_ERR;
				return s;
}

int snetUnixServer(char *err, char *path) {
				int s;
				struct sockaddr_un sa;
				if ((s = snetCreateSocket(err, AF_LOCAL)) == SNET_ERR) return SNET_ERR;
				memset(&sa, 0, sizeof(sa));
				sa.sun_family = AF_LOCAL;
				strncpy(sa.sun_path, path, sizeof(sa.sun_path) - 1);
				if (snetListen(err, s, (struct sockaddr *)&sa, sizeof(sa)) == SNET_ERR)
								return SNET_ERR;
				return s;
}

static int snetGenericAccept(char *err, int s, struct sockaddr *sa,
                             socklen_t *len) {
				int fd;
				while (1) {
								fd = accept(s, sa, len);
								if (fd == -1) {
												if (errno == EINTR)
																continue;
												else {
																snetSetError(err, "accept: %s", strerror(errno));
																return SNET_ERR;
												}
								}
								break;
				}
				return fd;
}

int snetTcpAccept(char *err, int s, char *ip, int *port) {
				int fd;
				struct sockaddr_in sa;
				socklen_t salen = sizeof(sa);
				if ((fd = snetGenericAccept(err, s, (struct sockaddr *)&sa, &salen)) ==
				    SNET_ERR)
								return SNET_ERR;
				if (ip) strcpy(ip, inet_ntoa(sa.sin_addr));
				if (port) *port = ntohs(sa.sin_port);
				return fd;
}

int snetUnixAccept(char *err, int s) {
				int fd;
				struct sockaddr_un sa;
				socklen_t salen = sizeof(sa);
				if ((fd = snetGenericAccept(err, s, (struct sockaddr *)&sa, &salen)) ==
				    SNET_ERR)
								return SNET_ERR;
				return fd;
}
