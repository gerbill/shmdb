#ifndef _DBMALLOC_H
#define _DBMALLOC_H

void *dbmalloc(size_t size);

void *dbcalloc(size_t size);

void *dbrealloc(void *ptr, size_t size);

void dbfree(void *ptr);

char *dbstrdup(const char *s);

size_t dbmalloc_used_memory(void);

void dbmalloc_enable_thread_safeness(void);

float dbmalloc_get_fragmentation_ratio(void);

size_t dbmalloc_get_rss(void);

#endif
