#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>
#include <limits.h>
#include "dbmalloc.h"
#include "sortlist.h"

int longLongToStr(char *s, size_t len, long long value);

#define ZIP_END 255
#define ZIP_BIGLEN 254
#define ZIP_STR_06B (0 << 6)
#define ZIP_STR_14B (1 << 6)
#define ZIP_STR_32B (2 << 6)
#define ZIP_INT_16B (0xc0 | 0 << 4)
#define ZIP_INT_32B (0xc0 | 1 << 4)
#define ZIP_INT_64B (0xc0 | 2 << 4)
#define ZIP_IS_STR(enc) (((enc)&0xc0) < 0xc0)
#define ZIP_IS_INT(enc) (!ZIP_IS_STR(enc) && ((enc)&0x30) < 0x30)
#define SORTLIST_BYTES(zl) (*((uint32_t *)(zl)))
#define SORTLIST_TAIL_OFFSET(zl) (*((uint32_t *)((zl) + sizeof(uint32_t))))
#define SORTLIST_LENGTH(zl) (*((uint16_t *)((zl) + sizeof(uint32_t) * 2)))
#define SORTLIST_HEADER_SIZE (sizeof(uint32_t) * 2 + sizeof(uint16_t))
#define SORTLIST_ENTRY_HEAD(zl) ((zl) + SORTLIST_HEADER_SIZE)
#define SORTLIST_ENTRY_TAIL(zl) ((zl) + SORTLIST_TAIL_OFFSET(zl))
#define SORTLIST_ENTRY_END(zl) ((zl) + SORTLIST_BYTES(zl) - 1)
#define SORTLIST_INCR_LENGTH(zl, incr)                                \
				{                                                                  \
								if (SORTLIST_LENGTH(zl) < UINT16_MAX) SORTLIST_LENGTH(zl) += incr; \
				}
typedef struct zlentry {
				unsigned int prevrawlensize, prevrawlen;
				unsigned int lensize, len;
				unsigned int headersize;
				unsigned char encoding;
				unsigned char *p;
} zlentry;

static unsigned int zipEntryEncoding(unsigned char *p) {
				unsigned char b = p[0] & 0xc0;
				if (b < 0xc0) {
								return b;
				} else {
								return p[0] & 0xf0;
				}
				assert(NULL);
				return 0;
}

static unsigned int zipIntSize(unsigned char encoding) {
				switch (encoding) {
				case ZIP_INT_16B:
								return sizeof(int16_t);
				case ZIP_INT_32B:
								return sizeof(int32_t);
				case ZIP_INT_64B:
								return sizeof(int64_t);
				}
				assert(NULL);
				return 0;
}

static unsigned int zipDecodeLength(unsigned char *p, unsigned int *lensize) {
				unsigned char encoding = zipEntryEncoding(p);
				unsigned int len = 0;
				if (ZIP_IS_STR(encoding)) {
								switch (encoding) {
								case ZIP_STR_06B:
												len = p[0] & 0x3f;
												if (lensize) *lensize = 1;
												break;
								case ZIP_STR_14B:
												len = ((p[0] & 0x3f) << 8) | p[1];
												if (lensize) *lensize = 2;
												break;
								case ZIP_STR_32B:
												len = (p[1] << 24) | (p[2] << 16) | (p[3] << 8) | p[4];
												if (lensize) *lensize = 5;
												break;
								default:
												assert(NULL);
								}
				} else {
								len = zipIntSize(encoding);
								if (lensize) *lensize = 1;
				}
				return len;
}

static unsigned int zipEncodeLength(unsigned char *p, unsigned char encoding,
                                    unsigned int rawlen) {
				unsigned char len = 1, buf[5];
				if (ZIP_IS_STR(encoding)) {
								if (rawlen <= 0x3f) {
												if (!p) return len;
												buf[0] = ZIP_STR_06B | rawlen;
								} else if (rawlen <= 0x3fff) {
												len += 1;
												if (!p) return len;
												buf[0] = ZIP_STR_14B | ((rawlen >> 8) & 0x3f);
												buf[1] = rawlen & 0xff;
								} else {
												len += 4;
												if (!p) return len;
												buf[0] = ZIP_STR_32B;
												buf[1] = (rawlen >> 24) & 0xff;
												buf[2] = (rawlen >> 16) & 0xff;
												buf[3] = (rawlen >> 8) & 0xff;
												buf[4] = rawlen & 0xff;
								}
				} else {
								if (!p) return len;
								buf[0] = encoding;
				}
				memcpy(p, buf, len);
				return len;
}

static unsigned int zipPrevDecodeLength(unsigned char *p,
                                        unsigned int *lensize) {
				unsigned int len = *p;
				if (len < ZIP_BIGLEN) {
								if (lensize) *lensize = 1;
				} else {
								if (lensize) *lensize = 1 + sizeof(len);
								memcpy(&len, p + 1, sizeof(len));
				}
				return len;
}

static unsigned int zipPrevEncodeLength(unsigned char *p, unsigned int len) {
				if (p == NULL) {
								return (len < ZIP_BIGLEN) ? 1 : sizeof(len) + 1;
				} else {
								if (len < ZIP_BIGLEN) {
												p[0] = len;
												return 1;
								} else {
												p[0] = ZIP_BIGLEN;
												memcpy(p + 1, &len, sizeof(len));
												return 1 + sizeof(len);
								}
				}
}

static void zipPrevEncodeLengthForceLarge(unsigned char *p, unsigned int len) {
				if (p == NULL) return;
				p[0] = ZIP_BIGLEN;
				memcpy(p + 1, &len, sizeof(len));
}

static int zipPrevLenByteDiff(unsigned char *p, unsigned int len) {
				unsigned int prevlensize;
				zipPrevDecodeLength(p, &prevlensize);
				return zipPrevEncodeLength(NULL, len) - prevlensize;
}

static int zipTryEncoding(unsigned char *entry, unsigned int entrylen,
                          long long *v, unsigned char *encoding) {
				long long value;
				char *eptr;
				char buf[32];
				if (entrylen >= 32 || entrylen == 0) return 0;
				if (entry[0] == '-' || (entry[0] >= '0' && entry[0] <= '9')) {
								int slen;
								memcpy(buf, entry, entrylen);
								buf[entrylen] = '\0';
								value = strtoll(buf, &eptr, 10);
								if (eptr[0] != '\0') return 0;
								slen = longLongToStr(buf, 32, value);
								if (entrylen != (unsigned)slen || memcmp(buf, entry, slen)) return 0;
								if (value >= INT16_MIN && value <= INT16_MAX) {
												*encoding = ZIP_INT_16B;
								} else if (value >= INT32_MIN && value <= INT32_MAX) {
												*encoding = ZIP_INT_32B;
								} else {
												*encoding = ZIP_INT_64B;
								}
								*v = value;
								return 1;
				}
				return 0;
}

static void zipSaveInteger(unsigned char *p, int64_t value,
                           unsigned char encoding) {
				int16_t i16;
				int32_t i32;
				int64_t i64;
				if (encoding == ZIP_INT_16B) {
								i16 = value;
								memcpy(p, &i16, sizeof(i16));
				} else if (encoding == ZIP_INT_32B) {
								i32 = value;
								memcpy(p, &i32, sizeof(i32));
				} else if (encoding == ZIP_INT_64B) {
								i64 = value;
								memcpy(p, &i64, sizeof(i64));
				} else {
								assert(NULL);
				}
}

static int64_t zipLoadInteger(unsigned char *p, unsigned char encoding) {
				int16_t i16;
				int32_t i32;
				int64_t i64, ret = 0;
				if (encoding == ZIP_INT_16B) {
								memcpy(&i16, p, sizeof(i16));
								ret = i16;
				} else if (encoding == ZIP_INT_32B) {
								memcpy(&i32, p, sizeof(i32));
								ret = i32;
				} else if (encoding == ZIP_INT_64B) {
								memcpy(&i64, p, sizeof(i64));
								ret = i64;
				} else {
								assert(NULL);
				}
				return ret;
}

static zlentry zipEntry(unsigned char *p) {
				zlentry e;
				e.prevrawlen = zipPrevDecodeLength(p, &e.prevrawlensize);
				e.len = zipDecodeLength(p + e.prevrawlensize, &e.lensize);
				e.headersize = e.prevrawlensize + e.lensize;
				e.encoding = zipEntryEncoding(p + e.prevrawlensize);
				e.p = p;
				return e;
}

static unsigned int zipRawEntryLength(unsigned char *p) {
				zlentry e = zipEntry(p);
				return e.headersize + e.len;
}

unsigned char *sortlistNew(void) {
				unsigned int bytes = SORTLIST_HEADER_SIZE + 1;
				unsigned char *zl = dbmalloc(bytes);
				SORTLIST_BYTES(zl) = bytes;
				SORTLIST_TAIL_OFFSET(zl) = SORTLIST_HEADER_SIZE;
				SORTLIST_LENGTH(zl) = 0;
				zl[bytes - 1] = ZIP_END;
				return zl;
}

static unsigned char *sortlistResize(unsigned char *zl, unsigned int len) {
				zl = dbrealloc(zl, len);
				SORTLIST_BYTES(zl) = len;
				zl[len - 1] = ZIP_END;
				return zl;
}

static unsigned char *__sortlistCascadeUpdate(unsigned char *zl,
                                             unsigned char *p) {
				unsigned int curlen = SORTLIST_BYTES(zl), rawlen, rawlensize;
				unsigned int offset, noffset, extra;
				unsigned char *np;
				zlentry cur, next;
				while (p[0] != ZIP_END) {
								cur = zipEntry(p);
								rawlen = cur.headersize + cur.len;
								rawlensize = zipPrevEncodeLength(NULL, rawlen);
								if (p[rawlen] == ZIP_END) break;
								next = zipEntry(p + rawlen);
								if (next.prevrawlen == rawlen) break;
								if (next.prevrawlensize < rawlensize) {
												offset = p - zl;
												extra = rawlensize - next.prevrawlensize;
												zl = sortlistResize(zl, curlen + extra);
												p = zl + offset;
												np = p + rawlen;
												noffset = np - zl;
												if ((zl + SORTLIST_TAIL_OFFSET(zl)) != np)
																SORTLIST_TAIL_OFFSET(zl) += extra;
												memmove(np + rawlensize, np + next.prevrawlensize,
												        curlen - noffset - next.prevrawlensize - 1);
												zipPrevEncodeLength(np, rawlen);
												p += rawlen;
												curlen += extra;
								} else {
												if (next.prevrawlensize > rawlensize) {
																zipPrevEncodeLengthForceLarge(p + rawlen, rawlen);
												} else {
																zipPrevEncodeLength(p + rawlen, rawlen);
												}
												break;
								}
				}
				return zl;
}

static unsigned char *__sortlistDelete(unsigned char *zl, unsigned char *p,
                                      unsigned int num) {
				unsigned int i, totlen, deleted = 0;
				int offset, nextdiff = 0;
				zlentry first, tail;
				first = zipEntry(p);
				for (i = 0; p[0] != ZIP_END && i < num; i++) {
								p += zipRawEntryLength(p);
								deleted++;
				}
				totlen = p - first.p;
				if (totlen > 0) {
								if (p[0] != ZIP_END) {
												nextdiff = zipPrevLenByteDiff(p, first.prevrawlen);
												zipPrevEncodeLength(p - nextdiff, first.prevrawlen);
												SORTLIST_TAIL_OFFSET(zl) -= totlen;
												tail = zipEntry(p);
												if (p[tail.headersize + tail.len] != ZIP_END)
																SORTLIST_TAIL_OFFSET(zl) += nextdiff;
												memmove(first.p, p - nextdiff,
												        SORTLIST_BYTES(zl) - (p - zl) - 1 + nextdiff);
								} else {
												SORTLIST_TAIL_OFFSET(zl) = (first.p - zl) - first.prevrawlen;
								}
								offset = first.p - zl;
								zl = sortlistResize(zl, SORTLIST_BYTES(zl) - totlen + nextdiff);
								SORTLIST_INCR_LENGTH(zl, -deleted);
								p = zl + offset;
								if (nextdiff != 0) zl = __sortlistCascadeUpdate(zl, p);
				}
				return zl;
}

static unsigned char *__sortlistInsert(unsigned char *zl, unsigned char *p,
                                      unsigned char *s, unsigned int slen) {
				unsigned int curlen = SORTLIST_BYTES(zl), reqlen, prevlen = 0;
				unsigned int offset, nextdiff = 0;
				unsigned char encoding = 0;
				long long value;
				zlentry entry, tail;
				if (p[0] != ZIP_END) {
								entry = zipEntry(p);
								prevlen = entry.prevrawlen;
				} else {
								unsigned char *ptail = SORTLIST_ENTRY_TAIL(zl);
								if (ptail[0] != ZIP_END) {
												prevlen = zipRawEntryLength(ptail);
								}
				}
				if (zipTryEncoding(s, slen, &value, &encoding)) {
								reqlen = zipIntSize(encoding);
				} else {
								reqlen = slen;
				}
				reqlen += zipPrevEncodeLength(NULL, prevlen);
				reqlen += zipEncodeLength(NULL, encoding, slen);
				nextdiff = (p[0] != ZIP_END) ? zipPrevLenByteDiff(p, reqlen) : 0;
				offset = p - zl;
				zl = sortlistResize(zl, curlen + reqlen + nextdiff);
				p = zl + offset;
				if (p[0] != ZIP_END) {
								memmove(p + reqlen, p - nextdiff, curlen - offset - 1 + nextdiff);
								zipPrevEncodeLength(p + reqlen, reqlen);
								SORTLIST_TAIL_OFFSET(zl) += reqlen;
								tail = zipEntry(p + reqlen);
								if (p[reqlen + tail.headersize + tail.len] != ZIP_END)
												SORTLIST_TAIL_OFFSET(zl) += nextdiff;
				} else {
								SORTLIST_TAIL_OFFSET(zl) = p - zl;
				}
				if (nextdiff != 0) {
								offset = p - zl;
								zl = __sortlistCascadeUpdate(zl, p + reqlen);
								p = zl + offset;
				}
				p += zipPrevEncodeLength(p, prevlen);
				p += zipEncodeLength(p, encoding, slen);
				if (ZIP_IS_STR(encoding)) {
								memcpy(p, s, slen);
				} else {
								zipSaveInteger(p, value, encoding);
				}
				SORTLIST_INCR_LENGTH(zl, 1);
				return zl;
}

unsigned char *sortlistPush(unsigned char *zl, unsigned char *s,
                           unsigned int slen, int where) {
				unsigned char *p;
				p = (where == SORTLIST_HEAD) ? SORTLIST_ENTRY_HEAD(zl) : SORTLIST_ENTRY_END(zl);
				return __sortlistInsert(zl, p, s, slen);
}

unsigned char *sortlistIndex(unsigned char *zl, int index) {
				unsigned char *p;
				zlentry entry;
				if (index < 0) {
								index = (-index) - 1;
								p = SORTLIST_ENTRY_TAIL(zl);
								if (p[0] != ZIP_END) {
												entry = zipEntry(p);
												while (entry.prevrawlen > 0 && index--) {
																p -= entry.prevrawlen;
																entry = zipEntry(p);
												}
								}
				} else {
								p = SORTLIST_ENTRY_HEAD(zl);
								while (p[0] != ZIP_END && index--) {
												p += zipRawEntryLength(p);
								}
				}
				return (p[0] == ZIP_END || index > 0) ? NULL : p;
}

unsigned char *sortlistNext(unsigned char *zl, unsigned char *p) {
				((void)zl);
				if (p[0] == ZIP_END) {
								return NULL;
				} else {
								p = p + zipRawEntryLength(p);
								return (p[0] == ZIP_END) ? NULL : p;
				}
}

unsigned char *sortlistPrev(unsigned char *zl, unsigned char *p) {
				zlentry entry;
				if (p[0] == ZIP_END) {
								p = SORTLIST_ENTRY_TAIL(zl);
								return (p[0] == ZIP_END) ? NULL : p;
				} else if (p == SORTLIST_ENTRY_HEAD(zl)) {
								return NULL;
				} else {
								entry = zipEntry(p);
								assert(entry.prevrawlen > 0);
								return p - entry.prevrawlen;
				}
}

unsigned int sortlistGet(unsigned char *p, unsigned char **sstr,
                        unsigned int *slen, long long *sval) {
				zlentry entry;
				if (p == NULL || p[0] == ZIP_END) return 0;
				if (sstr) *sstr = NULL;
				entry = zipEntry(p);
				if (ZIP_IS_STR(entry.encoding)) {
								if (sstr) {
												*slen = entry.len;
												*sstr = p + entry.headersize;
								}
				} else {
								if (sval) {
												*sval = zipLoadInteger(p + entry.headersize, entry.encoding);
								}
				}
				return 1;
}

unsigned char *sortlistInsert(unsigned char *zl, unsigned char *p,
                             unsigned char *s, unsigned int slen) {
				return __sortlistInsert(zl, p, s, slen);
}

unsigned char *sortlistDelete(unsigned char *zl, unsigned char **p) {
				unsigned int offset = *p - zl;
				zl = __sortlistDelete(zl, *p, 1);
				*p = zl + offset;
				return zl;
}

unsigned char *sortlistDeleteRange(unsigned char *zl, unsigned int index,
                                  unsigned int num) {
				unsigned char *p = sortlistIndex(zl, index);
				return (p == NULL) ? zl : __sortlistDelete(zl, p, num);
}

unsigned int sortlistCompare(unsigned char *p, unsigned char *sstr,
                            unsigned int slen) {
				zlentry entry;
				unsigned char sencoding;
				long long zval, sval;
				if (p[0] == ZIP_END) return 0;
				entry = zipEntry(p);
				if (ZIP_IS_STR(entry.encoding)) {
								if (entry.len == slen) {
												return memcmp(p + entry.headersize, sstr, slen) == 0;
								} else {
												return 0;
								}
				} else {
								if (zipTryEncoding(sstr, slen, &sval, &sencoding)) {
												if (entry.encoding == sencoding) {
																zval = zipLoadInteger(p + entry.headersize, entry.encoding);
																return zval == sval;
												}
								}
				}
				return 0;
}

unsigned int sortlistLen(unsigned char *zl) {
				unsigned int len = 0;
				if (SORTLIST_LENGTH(zl) < UINT16_MAX) {
								len = SORTLIST_LENGTH(zl);
				} else {
								unsigned char *p = zl + SORTLIST_HEADER_SIZE;
								while (*p != ZIP_END) {
												p += zipRawEntryLength(p);
												len++;
								}
								if (len < UINT16_MAX) SORTLIST_LENGTH(zl) = len;
				}
				return len;
}

unsigned int sortlistSize(unsigned char *zl) {
				return SORTLIST_BYTES(zl);
}

void sortlistRepr(unsigned char *zl) {
				unsigned char *p;
				int index = 0;
				zlentry entry;
				printf(
								"{total bytes %d} "
								"{length %u}\n"
								"{tail offset %u}\n",
								SORTLIST_BYTES(zl), SORTLIST_LENGTH(zl), SORTLIST_TAIL_OFFSET(zl));
				p = SORTLIST_ENTRY_HEAD(zl);
				while (*p != ZIP_END) {
								entry = zipEntry(p);
								printf(
												"{"
												"addr 0x%08lx, "
												"index %2d, "
												"offset %5ld, "
												"rl: %5u, "
												"hs %2u, "
												"pl: %5u, "
												"pls: %2u, "
												"payload %5u"
												"} ",
												(long unsigned)p, index, (unsigned long)(p - zl),
												entry.headersize + entry.len, entry.headersize, entry.prevrawlen,
												entry.prevrawlensize, entry.len);
								p += entry.headersize;
								if (ZIP_IS_STR(entry.encoding)) {
												if (entry.len > 40) {
																if (fwrite(p, 40, 1, stdout) == 0) perror("fwrite");
																printf("...");
												} else {
																if (entry.len && fwrite(p, entry.len, 1, stdout) == 0) perror("fwrite");
												}
								} else {
												printf("%lld", (long long)zipLoadInteger(p, entry.encoding));
								}
								printf("\n");
								p += entry.len;
								index++;
				}
				printf("{end}\n\n");
}
#ifdef SORTLIST_TEST_MAIN
#include <sys/time.h>
#include "adlist.h"
#include "dstr.h"
#define debug(f, ...)                  \
				{                                    \
								if (DEBUG) printf(f, __VA_ARGS__); \
				}

unsigned char *createList() {
				unsigned char *zl = sortlistNew();
				zl = sortlistPush(zl, (unsigned char *)"foo", 3, SORTLIST_TAIL);
				zl = sortlistPush(zl, (unsigned char *)"quux", 4, SORTLIST_TAIL);
				zl = sortlistPush(zl, (unsigned char *)"hello", 5, SORTLIST_HEAD);
				zl = sortlistPush(zl, (unsigned char *)"1024", 4, SORTLIST_TAIL);
				return zl;
}

unsigned char *createIntList() {
				unsigned char *zl = sortlistNew();
				char buf[32];
				sprintf(buf, "100");
				zl = sortlistPush(zl, (unsigned char *)buf, strlen(buf), SORTLIST_TAIL);
				sprintf(buf, "128000");
				zl = sortlistPush(zl, (unsigned char *)buf, strlen(buf), SORTLIST_TAIL);
				sprintf(buf, "-100");
				zl = sortlistPush(zl, (unsigned char *)buf, strlen(buf), SORTLIST_HEAD);
				sprintf(buf, "4294967296");
				zl = sortlistPush(zl, (unsigned char *)buf, strlen(buf), SORTLIST_HEAD);
				sprintf(buf, "non integer");
				zl = sortlistPush(zl, (unsigned char *)buf, strlen(buf), SORTLIST_TAIL);
				sprintf(buf, "much much longer non integer");
				zl = sortlistPush(zl, (unsigned char *)buf, strlen(buf), SORTLIST_TAIL);
				return zl;
}

long long usec(void) {
				struct timeval tv;
				gettimeofday(&tv, NULL);
				return (((long long)tv.tv_sec) * 1000000) + tv.tv_usec;
}

void stress(int pos, int num, int maxsize, int dnum) {
				int i, j, k;
				unsigned char *zl;
				char posstr[2][5] = {"HEAD", "TAIL"};
				long long start;
				for (i = 0; i < maxsize; i += dnum) {
								zl = sortlistNew();
								for (j = 0; j < i; j++) {
												zl = sortlistPush(zl, (unsigned char *)"quux", 4, SORTLIST_TAIL);
								}
								start = usec();
								for (k = 0; k < num; k++) {
												zl = sortlistPush(zl, (unsigned char *)"quux", 4, pos);
												zl = sortlistDeleteRange(zl, 0, 1);
								}
								printf("List size: %8d, bytes: %8d, %dx push+pop (%s): %6lld usec\n", i,
								       SORTLIST_BYTES(zl), num, posstr[pos], usec() - start);
								dbfree(zl);
				}
}

void pop(unsigned char *zl, int where) {
				unsigned char *p, *vstr;
				unsigned int vlen;
				long long vlong;
				p = sortlistIndex(zl, where == SORTLIST_HEAD ? 0 : -1);
				if (sortlistGet(p, &vstr, &vlen, &vlong)) {
								if (where == SORTLIST_HEAD)
												printf("Pop head: ");
								else
												printf("Pop tail: ");
								if (vstr)
												if (vlen && fwrite(vstr, vlen, 1, stdout) == 0)
																perror("fwrite");
												else
																printf("%lld", vlong);
								printf("\n");
								sortlistDeleteRange(zl, -1, 1);
				} else {
								printf("ERROR: Could not pop\n");
								exit(1);
				}
}

int randstring(char *target, unsigned int min, unsigned int max) {
				int p, len = min + rand() % (max - min + 1);
				int minval, maxval;
				switch (rand() % 3) {
				case 0:
								minval = 0;
								maxval = 255;
								break;
				case 1:
								minval = 48;
								maxval = 122;
								break;
				case 2:
								minval = 48;
								maxval = 52;
								break;
				default:
								assert(NULL);
				}
				while (p < len) target[p++] = minval + rand() % (maxval - minval + 1);
				return len;
}

int main(int argc, char **argv) {
				unsigned char *zl, *p;
				unsigned char *entry;
				unsigned int elen;
				long long value;
				if (argc == 2) srand(atoi(argv[1]));
				zl = createIntList();
				sortlistRepr(zl);
				zl = createList();
				sortlistRepr(zl);
				pop(zl, SORTLIST_TAIL);
				sortlistRepr(zl);
				pop(zl, SORTLIST_HEAD);
				sortlistRepr(zl);
				pop(zl, SORTLIST_TAIL);
				sortlistRepr(zl);
				pop(zl, SORTLIST_TAIL);
				sortlistRepr(zl);
				printf("Get element at index 3:\n");
				{
								zl = createList();
								p = sortlistIndex(zl, 3);
								if (!sortlistGet(p, &entry, &elen, &value)) {
												printf("ERROR: Could not access index 3\n");
												return 1;
								}
								if (entry) {
												if (elen && fwrite(entry, elen, 1, stdout) == 0) perror("fwrite");
												printf("\n");
								} else {
												printf("%lld\n", value);
								}
								printf("\n");
				}
				printf("Get element at index 4 (out of range):\n");
				{
								zl = createList();
								p = sortlistIndex(zl, 4);
								if (p == NULL) {
												printf("No entry\n");
								} else {
												printf(
																"ERROR: Out of range index should return NULL, returned offset: "
																"%ld\n",
																p - zl);
												return 1;
								}
								printf("\n");
				}
				printf("Get element at index -1 (last element):\n");
				{
								zl = createList();
								p = sortlistIndex(zl, -1);
								if (!sortlistGet(p, &entry, &elen, &value)) {
												printf("ERROR: Could not access index -1\n");
												return 1;
								}
								if (entry) {
												if (elen && fwrite(entry, elen, 1, stdout) == 0) perror("fwrite");
												printf("\n");
								} else {
												printf("%lld\n", value);
								}
								printf("\n");
				}
				printf("Get element at index -4 (first element):\n");
				{
								zl = createList();
								p = sortlistIndex(zl, -4);
								if (!sortlistGet(p, &entry, &elen, &value)) {
												printf("ERROR: Could not access index -4\n");
												return 1;
								}
								if (entry) {
												if (elen && fwrite(entry, elen, 1, stdout) == 0) perror("fwrite");
												printf("\n");
								} else {
												printf("%lld\n", value);
								}
								printf("\n");
				}
				printf("Get element at index -5 (reverse out of range):\n");
				{
								zl = createList();
								p = sortlistIndex(zl, -5);
								if (p == NULL) {
												printf("No entry\n");
								} else {
												printf(
																"ERROR: Out of range index should return NULL, returned offset: "
																"%ld\n",
																p - zl);
												return 1;
								}
								printf("\n");
				}
				printf("Iterate list from 0 to end:\n");
				{
								zl = createList();
								p = sortlistIndex(zl, 0);
								while (sortlistGet(p, &entry, &elen, &value)) {
												printf("Entry: ");
												if (entry) {
																if (elen && fwrite(entry, elen, 1, stdout) == 0) perror("fwrite");
												} else {
																printf("%lld", value);
												}
												p = sortlistNext(zl, p);
												printf("\n");
								}
								printf("\n");
				}
				printf("Iterate list from 1 to end:\n");
				{
								zl = createList();
								p = sortlistIndex(zl, 1);
								while (sortlistGet(p, &entry, &elen, &value)) {
												printf("Entry: ");
												if (entry) {
																if (elen && fwrite(entry, elen, 1, stdout) == 0) perror("fwrite");
												} else {
																printf("%lld", value);
												}
												p = sortlistNext(zl, p);
												printf("\n");
								}
								printf("\n");
				}
				printf("Iterate list from 2 to end:\n");
				{
								zl = createList();
								p = sortlistIndex(zl, 2);
								while (sortlistGet(p, &entry, &elen, &value)) {
												printf("Entry: ");
												if (entry) {
																if (elen && fwrite(entry, elen, 1, stdout) == 0) perror("fwrite");
												} else {
																printf("%lld", value);
												}
												p = sortlistNext(zl, p);
												printf("\n");
								}
								printf("\n");
				}
				printf("Iterate starting out of range:\n");
				{
								zl = createList();
								p = sortlistIndex(zl, 4);
								if (!sortlistGet(p, &entry, &elen, &value)) {
												printf("No entry\n");
								} else {
												printf("ERROR\n");
								}
								printf("\n");
				}
				printf("Iterate from back to front:\n");
				{
								zl = createList();
								p = sortlistIndex(zl, -1);
								while (sortlistGet(p, &entry, &elen, &value)) {
												printf("Entry: ");
												if (entry) {
																if (elen && fwrite(entry, elen, 1, stdout) == 0) perror("fwrite");
												} else {
																printf("%lld", value);
												}
												p = sortlistPrev(zl, p);
												printf("\n");
								}
								printf("\n");
				}
				printf("Iterate from back to front, deleting all items:\n");
				{
								zl = createList();
								p = sortlistIndex(zl, -1);
								while (sortlistGet(p, &entry, &elen, &value)) {
												printf("Entry: ");
												if (entry) {
																if (elen && fwrite(entry, elen, 1, stdout) == 0) perror("fwrite");
												} else {
																printf("%lld", value);
												}
												zl = sortlistDelete(zl, &p);
												p = sortlistPrev(zl, p);
												printf("\n");
								}
								printf("\n");
				}
				printf("Delete inclusive range 0,0:\n");
				{
								zl = createList();
								zl = sortlistDeleteRange(zl, 0, 1);
								sortlistRepr(zl);
				}
				printf("Delete inclusive range 0,1:\n");
				{
								zl = createList();
								zl = sortlistDeleteRange(zl, 0, 2);
								sortlistRepr(zl);
				}
				printf("Delete inclusive range 1,2:\n");
				{
								zl = createList();
								zl = sortlistDeleteRange(zl, 1, 2);
								sortlistRepr(zl);
				}
				printf("Delete with start index out of range:\n");
				{
								zl = createList();
								zl = sortlistDeleteRange(zl, 5, 1);
								sortlistRepr(zl);
				}
				printf("Delete with num overflow:\n");
				{
								zl = createList();
								zl = sortlistDeleteRange(zl, 1, 5);
								sortlistRepr(zl);
				}
				printf("Delete foo while iterating:\n");
				{
								zl = createList();
								p = sortlistIndex(zl, 0);
								while (sortlistGet(p, &entry, &elen, &value)) {
												if (entry && strncmp("foo", (char *)entry, elen) == 0) {
																printf("Delete foo\n");
																zl = sortlistDelete(zl, &p);
												} else {
																printf("Entry: ");
																if (entry) {
																				if (elen && fwrite(entry, elen, 1, stdout) == 0) perror("fwrite");
																} else {
																				printf("%lld", value);
																}
																p = sortlistNext(zl, p);
																printf("\n");
												}
								}
								printf("\n");
								sortlistRepr(zl);
				}
				printf("Regression test for >255 byte strings:\n");
				{
								char v1[257], v2[257];
								memset(v1, 'x', 256);
								memset(v2, 'y', 256);
								zl = sortlistNew();
								zl = sortlistPush(zl, (unsigned char *)v1, strlen(v1), SORTLIST_TAIL);
								zl = sortlistPush(zl, (unsigned char *)v2, strlen(v2), SORTLIST_TAIL);
								p = sortlistIndex(zl, 0);
								assert(sortlistGet(p, &entry, &elen, &value));
								assert(strncmp(v1, (char *)entry, elen) == 0);
								p = sortlistIndex(zl, 1);
								assert(sortlistGet(p, &entry, &elen, &value));
								assert(strncmp(v2, (char *)entry, elen) == 0);
								printf("SUCCESS\n\n");
				}
				printf("Create long list and check indices:\n");
				{
								zl = sortlistNew();
								char buf[32];
								int i, len;
								for (i = 0; i < 1000; i++) {
												len = sprintf(buf, "%d", i);
												zl = sortlistPush(zl, (unsigned char *)buf, len, SORTLIST_TAIL);
								}
								for (i = 0; i < 1000; i++) {
												p = sortlistIndex(zl, i);
												assert(sortlistGet(p, NULL, NULL, &value));
												assert(i == value);
												p = sortlistIndex(zl, -i - 1);
												assert(sortlistGet(p, NULL, NULL, &value));
												assert(999 - i == value);
								}
								printf("SUCCESS\n\n");
				}
				printf("Compare strings with sortlist entries:\n");
				{
								zl = createList();
								p = sortlistIndex(zl, 0);
								if (!sortlistCompare(p, (unsigned char *)"hello", 5)) {
												printf("ERROR: not \"hello\"\n");
												return 1;
								}
								if (sortlistCompare(p, (unsigned char *)"hella", 5)) {
												printf("ERROR: \"hella\"\n");
												return 1;
								}
								p = sortlistIndex(zl, 3);
								if (!sortlistCompare(p, (unsigned char *)"1024", 4)) {
												printf("ERROR: not \"1024\"\n");
												return 1;
								}
								if (sortlistCompare(p, (unsigned char *)"1025", 4)) {
												printf("ERROR: \"1025\"\n");
												return 1;
								}
								printf("SUCCESS\n\n");
				}
				printf("Stress with random payloads of different encoding:\n");
				{
								int i, j, len, where;
								unsigned char *p;
								char buf[1024];
								int buflen;
								list *ref;
								lNode *refnode;
								unsigned char *sstr;
								unsigned int slen;
								long long sval;
								for (i = 0; i < 20000; i++) {
												zl = sortlistNew();
												ref = listCreate();
												lSetFreeMethod(ref, dstrfree);
												len = rand() % 256;
												for (j = 0; j < len; j++) {
																where = (rand() & 1) ? SORTLIST_HEAD : SORTLIST_TAIL;
																if (rand() % 2) {
																				buflen = randstring(buf, 1, sizeof(buf) - 1);
																} else {
																				switch (rand() % 3) {
																				case 0:
																								buflen = sprintf(buf, "%lld", (0LL + rand()) >> 20);
																								break;
																				case 1:
																								buflen = sprintf(buf, "%lld", (0LL + rand()));
																								break;
																				case 2:
																								buflen = sprintf(buf, "%lld", (0LL + rand()) << 20);
																								break;
																				default:
																								assert(NULL);
																				}
																}
																zl = sortlistPush(zl, (unsigned char *)buf, buflen, where);
																if (where == SORTLIST_HEAD) {
																				listAddNodeHead(ref, dstrnewlen(buf, buflen));
																} else if (where == SORTLIST_TAIL) {
																				listAddNodeTail(ref, dstrnewlen(buf, buflen));
																} else {
																				assert(NULL);
																}
												}
												assert(lLength(ref) == sortlistLen(zl));
												for (j = 0; j < len; j++) {
																p = sortlistIndex(zl, j);
																refnode = listIndex(ref, j);
																assert(sortlistGet(p, &sstr, &slen, &sval));
																if (sstr == NULL) {
																				buflen = sprintf(buf, "%lld", sval);
																} else {
																				buflen = slen;
																				memcpy(buf, sstr, buflen);
																				buf[buflen] = '\0';
																}
																assert(memcmp(buf, lNodeValue(refnode), buflen) == 0);
												}
												dbfree(zl);
												listRelease(ref);
								}
								printf("SUCCESS\n\n");
				}
				printf("Stress with variable sortlist size:\n");
				{
								stress(SORTLIST_HEAD, 100000, 16384, 256);
								stress(SORTLIST_TAIL, 100000, 16384, 256);
				}
				return 0;
}
#endif
