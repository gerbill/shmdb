#include "shm.h"

static int checkStringLength(shmClient *c, long long size) {
				if (size > 512 * 1024 * 1024) {
								netAddError(c, "string exceeds maximum allowed size (512MB)");
								return SHM_ERR;
				}
				return SHM_OK;
}

void setGenericCmd(shmClient *c, int nx, robj *key, robj *val,
                       robj *expire) {
				int retval;
				long seconds = 0;
				if (expire) {
								if (objGetLongFromOrReply(c, expire, &seconds, NULL) != SHM_OK) return;
								if (seconds <= 0) {
												netAddError(c, "invalid expire time in SETEX");
												return;
								}
				}
				lookupKeyWrite(c->db, key);
				retval = dbAdd(c->db, key, val);
				if (retval == SHM_ERR) {
								if (!nx) {
												dbReplace(c->db, key, val);
												objIncRefCount(val);
								} else {
												netAdd(c, shared.czero);
												return;
								}
				} else {
								objIncRefCount(val);
				}
				touchWatchedKey(c->db, key);
				server.dirty++;
				removeExpire(c->db, key);
				if (expire) setExpire(c->db, key, time(NULL) + seconds);
				netAdd(c, nx ? shared.cone : shared.ok);
}

void setCmd(shmClient *c) {
				c->argv[2] = objTryEncoding(c->argv[2]);
				setGenericCmd(c, 0, c->argv[1], c->argv[2], NULL);
}

void setneCmd(shmClient *c) {
				c->argv[2] = objTryEncoding(c->argv[2]);
				setGenericCmd(c, 1, c->argv[1], c->argv[2], NULL);
}

void setxCmd(shmClient *c) {
				c->argv[3] = objTryEncoding(c->argv[3]);
				setGenericCmd(c, 0, c->argv[1], c->argv[3], c->argv[2]);
}

int getGenericCmd(shmClient *c) {
				robj *o;
				if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.nullbulk)) == NULL)
								return SHM_OK;
				if (o->type != SHM_STRING) {
								netAdd(c, shared.wrongtypeerr);
								return SHM_ERR;
				} else {
								netAddBulk(c, o);
								return SHM_OK;
				}
}

void getCmd(shmClient *c) {
				getGenericCmd(c);
}

void keygsCmd(shmClient *c) {
				if (getGenericCmd(c) == SHM_ERR) return;
				c->argv[2] = objTryEncoding(c->argv[2]);
				dbReplace(c->db, c->argv[1], c->argv[2]);
				objIncRefCount(c->argv[2]);
				touchWatchedKey(c->db, c->argv[1]);
				server.dirty++;
				removeExpire(c->db, c->argv[1]);
}

static int getBitOffsetFromArgument(shmClient *c, robj *o, size_t *offset) {
				long long loffset;
				char *err = "bit offset is not an integer or out of range";
				if (objGetLongLongOrReply(c, o, &loffset, err) != SHM_OK)
								return SHM_ERR;
				if ((loffset < 0) ||
				    ((unsigned long long)loffset >> 3) >= (512 * 1024 * 1024)) {
								netAddError(c, err);
								return SHM_ERR;
				}
				*offset = (size_t)loffset;
				return SHM_OK;
}

void setbCmd(shmClient *c) {
				robj *o;
				char *err = "bit is not an integer or out of range";
				size_t bitoffset;
				int byte, bit;
				int byteval, bitval;
				long on;
				if (getBitOffsetFromArgument(c, c->argv[2], &bitoffset) != SHM_OK) return;
				if (objGetLongFromOrReply(c, c->argv[3], &on, err) != SHM_OK) return;
				if (on & ~1) {
								netAddError(c, err);
								return;
				}
				o = lookupKeyWrite(c->db, c->argv[1]);
				if (o == NULL) {
								o = objCreate(SHM_STRING, dstrempty());
								dbAdd(c->db, c->argv[1], o);
				} else {
								if (objCheckType(c, o, SHM_STRING)) return;
								if (o->refcount != 1 || o->encoding != SHM_ENCODING_RAW) {
												robj *decoded = objGetDecoded(o);
												o = objCreateString(decoded->ptr, dstrlen(decoded->ptr));
												objDecRefCount(decoded);
												dbReplace(c->db, c->argv[1], o);
								}
				}
				byte = bitoffset >> 3;
				o->ptr = dstrgrowzero(o->ptr, byte + 1);
				byteval = ((char *)o->ptr)[byte];
				bit = 7 - (bitoffset & 0x7);
				bitval = byteval & (1 << bit);
				byteval &= ~(1 << bit);
				byteval |= ((on & 0x1) << bit);
				((char *)o->ptr)[byte] = byteval;
				touchWatchedKey(c->db, c->argv[1]);
				server.dirty++;
				netAdd(c, bitval ? shared.cone : shared.czero);
}

void getbCmd(shmClient *c) {
				robj *o;
				char llbuf[32];
				size_t bitoffset;
				size_t byte, bit;
				size_t bitval = 0;
				if (getBitOffsetFromArgument(c, c->argv[2], &bitoffset) != SHM_OK) return;
				if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.czero)) == NULL ||
				    objCheckType(c, o, SHM_STRING))
								return;
				byte = bitoffset >> 3;
				bit = 7 - (bitoffset & 0x7);
				if (o->encoding != SHM_ENCODING_RAW) {
								if (byte < (size_t)longLongToStr(llbuf, sizeof(llbuf), (long)o->ptr))
												bitval = llbuf[byte] & (1 << bit);
				} else {
								if (byte < dstrlen(o->ptr)) bitval = ((char *)o->ptr)[byte] & (1 << bit);
				}
				netAdd(c, bitval ? shared.cone : shared.czero);
}

void owrangeCmd(shmClient *c) {
				robj *o;
				long offset;
				dstr value = c->argv[3]->ptr;
				if (objGetLongFromOrReply(c, c->argv[2], &offset, NULL) != SHM_OK) return;
				if (offset < 0) {
								netAddError(c, "offset is out of range");
								return;
				}
				o = lookupKeyWrite(c->db, c->argv[1]);
				if (o == NULL) {
								if (dstrlen(value) == 0) {
												netAdd(c, shared.czero);
												return;
								}
								if (checkStringLength(c, offset + dstrlen(value)) != SHM_OK) return;
								o = objCreate(SHM_STRING, dstrempty());
								dbAdd(c->db, c->argv[1], o);
				} else {
								size_t olen;
								if (objCheckType(c, o, SHM_STRING)) return;
								olen = objStringLen(o);
								if (dstrlen(value) == 0) {
												netAddLongLong(c, olen);
												return;
								}
								if (checkStringLength(c, offset + dstrlen(value)) != SHM_OK) return;
								if (o->refcount != 1 || o->encoding != SHM_ENCODING_RAW) {
												robj *decoded = objGetDecoded(o);
												o = objCreateString(decoded->ptr, dstrlen(decoded->ptr));
												objDecRefCount(decoded);
												dbReplace(c->db, c->argv[1], o);
								}
				}
				if (dstrlen(value) > 0) {
								o->ptr = dstrgrowzero(o->ptr, offset + dstrlen(value));
								memcpy((char *)o->ptr + offset, value, dstrlen(value));
								touchWatchedKey(c->db, c->argv[1]);
								server.dirty++;
				}
				netAddLongLong(c, dstrlen(o->ptr));
}

void getrangeCmd(shmClient *c) {
				robj *o;
				long start, end;
				char *str, llbuf[32];
				size_t strlen;
				if (objGetLongFromOrReply(c, c->argv[2], &start, NULL) != SHM_OK) return;
				if (objGetLongFromOrReply(c, c->argv[3], &end, NULL) != SHM_OK) return;
				if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.emptybulk)) == NULL ||
				    objCheckType(c, o, SHM_STRING))
								return;
				if (o->encoding == SHM_ENCODING_INT) {
								str = llbuf;
								strlen = longLongToStr(llbuf, sizeof(llbuf), (long)o->ptr);
				} else {
								str = o->ptr;
								strlen = dstrlen(str);
				}
				if (start < 0) start = strlen + start;
				if (end < 0) end = strlen + end;
				if (start < 0) start = 0;
				if (end < 0) end = 0;
				if ((unsigned)end >= strlen) end = strlen - 1;
				if (start > end) {
								netAdd(c, shared.emptybulk);
				} else {
								netAddBulkCBuffer(c, (char *)str + start, end - start + 1);
				}
}

void multigetCmd(shmClient *c) {
				int j;
				netAddBeginBulkLen(c, c->argc - 1);
				for (j = 1; j < c->argc; j++) {
								robj *o = lookupKeyRead(c->db, c->argv[j]);
								if (o == NULL) {
												netAdd(c, shared.nullbulk);
								} else {
												if (o->type != SHM_STRING) {
																netAdd(c, shared.nullbulk);
												} else {
																netAddBulk(c, o);
												}
								}
				}
}

void msetGenericCmd(shmClient *c, int nx) {
				int j, busykeys = 0;
				if ((c->argc % 2) == 0) {
								netAddError(c, "wrong number of arguments for MSET");
								return;
				}
				if (nx) {
								for (j = 1; j < c->argc; j += 2) {
												if (lookupKeyWrite(c->db, c->argv[j]) != NULL) {
																busykeys++;
												}
								}
				}
				if (busykeys) {
								netAdd(c, shared.czero);
								return;
				}
				for (j = 1; j < c->argc; j += 2) {
								c->argv[j + 1] = objTryEncoding(c->argv[j + 1]);
								dbReplace(c->db, c->argv[j], c->argv[j + 1]);
								objIncRefCount(c->argv[j + 1]);
								removeExpire(c->db, c->argv[j]);
								touchWatchedKey(c->db, c->argv[j]);
				}
				server.dirty += (c->argc - 1) / 2;
				netAdd(c, nx ? shared.cone : shared.ok);
}

void multisetCmd(shmClient *c) {
				msetGenericCmd(c, 0);
}

void multiseteCmd(shmClient *c) {
				msetGenericCmd(c, 1);
}

void incrDecrCmd(shmClient *c, long long incr) {
				long long value, oldvalue;
				robj *o;
				o = lookupKeyWrite(c->db, c->argv[1]);
				if (o != NULL && objCheckType(c, o, SHM_STRING)) return;
				if (objGetLongLongOrReply(c, o, &value, NULL) != SHM_OK) return;
				oldvalue = value;
				value += incr;
				if ((incr < 0 && value > oldvalue) || (incr > 0 && value < oldvalue)) {
								netAddError(c, "increment or decrement would overflow");
								return;
				}
				o = objCreateStringFromLongLong(value);
				dbReplace(c->db, c->argv[1], o);
				touchWatchedKey(c->db, c->argv[1]);
				server.dirty++;
				netAdd(c, shared.colon);
				netAdd(c, o);
				netAdd(c, shared.crlf);
}

void raiseCmd(shmClient *c) {
				incrDecrCmd(c, 1);
}

void dcrCmd(shmClient *c) {
				incrDecrCmd(c, -1);
}

void raisebyCmd(shmClient *c) {
				long long incr;
				if (objGetLongLongOrReply(c, c->argv[2], &incr, NULL) != SHM_OK)
								return;
				incrDecrCmd(c, incr);
}

void dcrbyCmd(shmClient *c) {
				long long incr;
				if (objGetLongLongOrReply(c, c->argv[2], &incr, NULL) != SHM_OK)
								return;
				incrDecrCmd(c, -incr);
}

void addCmd(shmClient *c) {
				size_t totlen;
				robj *o, *append;
				o = lookupKeyWrite(c->db, c->argv[1]);
				if (o == NULL) {
								c->argv[2] = objTryEncoding(c->argv[2]);
								dbAdd(c->db, c->argv[1], c->argv[2]);
								objIncRefCount(c->argv[2]);
								totlen = objStringLen(c->argv[2]);
				} else {
								if (objCheckType(c, o, SHM_STRING)) return;
								append = c->argv[2];
								totlen = objStringLen(o) + dstrlen(append->ptr);
								if (checkStringLength(c, totlen) != SHM_OK) return;
								if (o->refcount != 1 || o->encoding != SHM_ENCODING_RAW) {
												robj *decoded = objGetDecoded(o);
												o = objCreateString(decoded->ptr, dstrlen(decoded->ptr));
												objDecRefCount(decoded);
												dbReplace(c->db, c->argv[1], o);
								}
								o->ptr = dstrcatlen(o->ptr, append->ptr, dstrlen(append->ptr));
								totlen = dstrlen(o->ptr);
				}
				touchWatchedKey(c->db, c->argv[1]);
				server.dirty++;
				netAddLongLong(c, totlen);
}

void getlenCmd(shmClient *c) {
				robj *o;
				if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.czero)) == NULL ||
				    objCheckType(c, o, SHM_STRING))
								return;
				netAddLongLong(c, objStringLen(o));
}
