#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include "el.h"
#include "dbmalloc.h"
#include "config.h"
#ifdef HAVE_EPOLL
#include "el_epoll.c"
#else
#ifdef HAVE_KQUEUE
#include "el_kqueue.c"
#else
#include "el_select.c"
#endif
#endif

elLoop *elCreateLoop(void) {
				elLoop *l;
				int i;
				l = dbmalloc(sizeof(*l));
				if (!l) return NULL;
				l->timeEventHead = NULL;
				l->nextId = 0;
				l->stop = 0;
				l->maxfd = -1;
				l->beforesleep = NULL;
				if (elApiCreate(l) == -1) {
								dbfree(l);
								return NULL;
				}
				for (i = 0; i < EL_SETSIZE; i++) l->events[i].mask = EL_NONE;
				return l;
}

void elDeleteLoop(elLoop *l) {
				elApiFree(l);
				dbfree(l);
}

void elStop(elLoop *l) {
				l->stop = 1;
}

int elCreateFileEvent(elLoop *l, int fd, int mask,
                      elFileProc *proc, void *clientData) {
				if (fd >= EL_SETSIZE) return EL_ERR;
				elFileEvent *fe = &l->events[fd];
				if (elApiAddEvent(l, fd, mask) == -1) return EL_ERR;
				fe->mask |= mask;
				if (mask & EL_READABLE) fe->rProc = proc;
				if (mask & EL_WRITABLE) fe->wProc = proc;
				fe->clientData = clientData;
				if (fd > l->maxfd) l->maxfd = fd;
				return EL_OK;
}

void elDeleteFileEvent(elLoop *l, int fd, int mask) {
				if (fd >= EL_SETSIZE) return;
				elFileEvent *fe = &l->events[fd];
				if (fe->mask == EL_NONE) return;
				fe->mask = fe->mask & (~mask);
				if (fd == l->maxfd && fe->mask == EL_NONE) {
								int j;
								for (j = l->maxfd - 1; j >= 0; j--)
												if (l->events[j].mask != EL_NONE) break;
								l->maxfd = j;
				}
				elApiDelEvent(l, fd, mask);
}

static void elGetTime(long *seconds, long *ms) {
				struct timeval tv;
				gettimeofday(&tv, NULL);
				*seconds = tv.tv_sec;
				*ms = tv.tv_usec / 1000;
}

static void elAddMillisecondsToNow(long long milliseconds, long *sec,
                                   long *ms) {
				long cur_sec, cur_ms, when_sec, when_ms;
				elGetTime(&cur_sec, &cur_ms);
				when_sec = cur_sec + milliseconds / 1000;
				when_ms = cur_ms + milliseconds % 1000;
				if (when_ms >= 1000) {
								when_sec++;
								when_ms -= 1000;
				}
				*sec = when_sec;
				*ms = when_ms;
}

long long elCreateTimeEvent(elLoop *l, long long ms,
                            elTimeProc *proc, void *clientData,
                            elEventFinalizerProc *finalizerProc) {
				long long id = l->nextId++;
				elTimeEvent *te;
				te = dbmalloc(sizeof(*te));
				if (te == NULL) return EL_ERR;
				te->id = id;
				elAddMillisecondsToNow(ms, &te->when_s, &te->when_ms);
				te->timeProc = proc;
				te->finalizerProc = finalizerProc;
				te->clientData = clientData;
				te->next = l->timeEventHead;
				l->timeEventHead = te;
				return id;
}

int elDeleteTimeEvent(elLoop *l, long long id) {
				elTimeEvent *te, *prev = NULL;
				te = l->timeEventHead;
				while (te) {
								if (te->id == id) {
												if (prev == NULL)
																l->timeEventHead = te->next;
												else
																prev->next = te->next;
												if (te->finalizerProc) te->finalizerProc(l, te->clientData);
												dbfree(te);
												return EL_OK;
								}
								prev = te;
								te = te->next;
				}
				return EL_ERR;
}

static elTimeEvent *elSearchNearestTimer(elLoop *l) {
				elTimeEvent *te = l->timeEventHead;
				elTimeEvent *nearest = NULL;
				while (te) {
								if (!nearest || te->when_s < nearest->when_s ||
								    (te->when_s == nearest->when_s && te->when_ms < nearest->when_ms))
												nearest = te;
								te = te->next;
				}
				return nearest;
}

static int processTimeEvents(elLoop *l) {
				int processed = 0;
				elTimeEvent *te;
				long long maxId;
				te = l->timeEventHead;
				maxId = l->nextId - 1;
				while (te) {
								long now_sec, now_ms;
								long long id;
								if (te->id > maxId) {
												te = te->next;
												continue;
								}
								elGetTime(&now_sec, &now_ms);
								if (now_sec > te->when_s ||
								    (now_sec == te->when_s && now_ms >= te->when_ms)) {
												int retval;
												id = te->id;
												retval = te->timeProc(l, id, te->clientData);
												processed++;
												if (retval != EL_NOMORE) {
																elAddMillisecondsToNow(retval, &te->when_s, &te->when_ms);
												} else {
																elDeleteTimeEvent(l, id);
												}
												te = l->timeEventHead;
								} else {
												te = te->next;
								}
				}
				return processed;
}

int elProcessEvents(elLoop *l, int flags) {
				int processed = 0, numevents;
				if (!(flags & EL_TIME_EVENTS) && !(flags & EL_FILE_EVENTS)) return 0;
				if (l->maxfd != -1 ||
				    ((flags & EL_TIME_EVENTS) && !(flags & EL_DONT_WAIT))) {
								int j;
								elTimeEvent *shortest = NULL;
								struct timeval tv, *tvp;
								if (flags & EL_TIME_EVENTS && !(flags & EL_DONT_WAIT))
												shortest = elSearchNearestTimer(l);
								if (shortest) {
												long now_sec, now_ms;
												elGetTime(&now_sec, &now_ms);
												tvp = &tv;
												tvp->tv_sec = shortest->when_s - now_sec;
												if (shortest->when_ms < now_ms) {
																tvp->tv_usec = ((shortest->when_ms + 1000) - now_ms) * 1000;
																tvp->tv_sec--;
												} else {
																tvp->tv_usec = (shortest->when_ms - now_ms) * 1000;
												}
												if (tvp->tv_sec < 0) tvp->tv_sec = 0;
												if (tvp->tv_usec < 0) tvp->tv_usec = 0;
								} else {
												if (flags & EL_DONT_WAIT) {
																tv.tv_sec = tv.tv_usec = 0;
																tvp = &tv;
												} else {
																tvp = NULL;
												}
								}
								numevents = elApiPoll(l, tvp);
								for (j = 0; j < numevents; j++) {
												elFileEvent *fe = &l->events[l->fired[j].fd];
												int mask = l->fired[j].mask;
												int fd = l->fired[j].fd;
												int rfired = 0;
												if (fe->mask & mask & EL_READABLE) {
																rfired = 1;
																fe->rProc(l, fd, fe->clientData, mask);
												}
												if (fe->mask & mask & EL_WRITABLE) {
																if (!rfired || fe->wProc != fe->rProc)
																				fe->wProc(l, fd, fe->clientData, mask);
												}
												processed++;
								}
				}
				if (flags & EL_TIME_EVENTS) processed += processTimeEvents(l);
				return processed;
}

int elWait(int fd, int mask, long long ms) {
				struct timeval tv;
				fd_set rfds, wfds, efds;
				int retmask = 0, retval;
				tv.tv_sec = ms / 1000;
				tv.tv_usec = (ms % 1000) * 1000;
				FD_ZERO(&rfds);
				FD_ZERO(&wfds);
				FD_ZERO(&efds);
				if (mask & EL_READABLE) FD_SET(fd, &rfds);
				if (mask & EL_WRITABLE) FD_SET(fd, &wfds);
				if ((retval = select(fd + 1, &rfds, &wfds, &efds, &tv)) > 0) {
								if (FD_ISSET(fd, &rfds)) retmask |= EL_READABLE;
								if (FD_ISSET(fd, &wfds)) retmask |= EL_WRITABLE;
								return retmask;
				} else {
								return retval;
				}
}

void elMain(elLoop *l) {
				l->stop = 0;
				while (!l->stop) {
								if (l->beforesleep != NULL) l->beforesleep(l);
								elProcessEvents(l, EL_ALL_EVENTS);
				}
}

char *elGetApiName(void) {
				return elApiName();
}

void elSetBeforeSleepProc(elLoop *l,
                          elBeforeSleepProc *beforesleep) {
				l->beforesleep = beforesleep;
}
