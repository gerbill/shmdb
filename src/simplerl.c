#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include "simplerl.h"
#define SIMPLERL_DEFAULT_HISTORY_MAX_LEN 100
#define SIMPLERL_MAX_LINE 4096
static char *unsupported_term[] = {"dumb", "cons25", NULL};
static simplerlCompletionCallback *completionCallback = NULL;
static struct termios orig_termios;
static int rawmode = 0;
static int atexit_registered = 0;
static int history_max_len = SIMPLERL_DEFAULT_HISTORY_MAX_LEN;
static int history_len = 0;
char **history = NULL;

static void simplerlAtExit(void);

int simplerlHistoryAdd(const char *line);

static int isUnsupportedTerm(void) {
				char *term = getenv("TERM");
				int j;
				if (term == NULL) return 0;
				for (j = 0; unsupported_term[j]; j++)
								if (!strcasecmp(term, unsupported_term[j])) return 1;
				return 0;
}

static void freeHistory(void) {
				if (history) {
								int j;
								for (j = 0; j < history_len; j++) free(history[j]);
								free(history);
				}
}

static int enableRawMode(int fd) {
				struct termios raw;
				if (!isatty(STDIN_FILENO)) goto fatal;
				if (!atexit_registered) {
								atexit(simplerlAtExit);
								atexit_registered = 1;
				}
				if (tcgetattr(fd, &orig_termios) == -1) goto fatal;
				raw = orig_termios;
				raw.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
				raw.c_oflag &= ~(OPOST);
				raw.c_cflag |= (CS8);
				raw.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
				raw.c_cc[VMIN] = 1;
				raw.c_cc[VTIME] = 0;
				if (tcsetattr(fd, TCSAFLUSH, &raw) < 0) goto fatal;
				rawmode = 1;
				return 0;
fatal:
				errno = ENOTTY;
				return -1;
}

static void disableRawMode(int fd) {
				if (rawmode && tcsetattr(fd, TCSAFLUSH, &orig_termios) != -1) rawmode = 0;
}

static void simplerlAtExit(void) {
				disableRawMode(STDIN_FILENO);
				freeHistory();
}

static int getColumns(void) {
				struct winsize ws;
				if (ioctl(1, TIOCGWINSZ, &ws) == -1) return 80;
				return ws.ws_col;
}

static void refreshLine(int fd, const char *prompt, char *buf, size_t len,
                        size_t pos, size_t cols) {
				char seq[64];
				size_t plen = strlen(prompt);
				while ((plen + pos) >= cols) {
								buf++;
								len--;
								pos--;
				}
				while (plen + len > cols) {
								len--;
				}
				snprintf(seq, 64, "\x1b[0G");
				if (write(fd, seq, strlen(seq)) == -1) return;
				if (write(fd, prompt, strlen(prompt)) == -1) return;
				if (write(fd, buf, len) == -1) return;
				snprintf(seq, 64, "\x1b[0K");
				if (write(fd, seq, strlen(seq)) == -1) return;
				snprintf(seq, 64, "\x1b[0G\x1b[%dC", (int)(pos + plen));
				if (write(fd, seq, strlen(seq)) == -1) return;
}

static void beep() {
				fprintf(stderr, "\x7");
				fflush(stderr);
}

static void freeCompletions(simplerlCompletions *lc) {
				size_t i;
				for (i = 0; i < lc->len; i++) free(lc->cvec[i]);
				if (lc->cvec != NULL) free(lc->cvec);
}

static int completeLine(int fd, const char *prompt, char *buf, size_t buflen,
                        size_t *len, size_t *pos, size_t cols) {
				simplerlCompletions lc = {0, NULL};
				int nread, nwritten;
				char c = 0;
				completionCallback(buf, &lc);
				if (lc.len == 0) {
								beep();
				} else {
								size_t stop = 0, i = 0;
								size_t clen;
								while (!stop) {
												if (i < lc.len) {
																clen = strlen(lc.cvec[i]);
																refreshLine(fd, prompt, lc.cvec[i], clen, clen, cols);
												} else {
																refreshLine(fd, prompt, buf, *len, *pos, cols);
												}
												nread = read(fd, &c, 1);
												if (nread <= 0) {
																freeCompletions(&lc);
																return -1;
												}
												switch (c) {
												case 9:
																i = (i + 1) % (lc.len + 1);
																if (i == lc.len) beep();
																break;
												case 27:
																if (i < lc.len) {
																				refreshLine(fd, prompt, buf, *len, *pos, cols);
																}
																stop = 1;
																break;
												default:
																if (i < lc.len) {
																				nwritten = snprintf(buf, buflen, "%s", lc.cvec[i]);
																				*len = *pos = nwritten;
																}
																stop = 1;
																break;
												}
								}
				}
				freeCompletions(&lc);
				return c;
}

void simplerlClearScreen(void) {
				if (write(STDIN_FILENO, "\x1b[H\x1b[2J", 7) <= 0) {
				}
}

static int simplerlPrompt(int fd, char *buf, size_t buflen,
                          const char *prompt) {
				size_t plen = strlen(prompt);
				size_t pos = 0;
				size_t len = 0;
				size_t cols = getColumns();
				int history_index = 0;
				buf[0] = '\0';
				buflen--;
				simplerlHistoryAdd("");
				if (write(fd, prompt, plen) == -1) return -1;
				while (1) {
								char c;
								int nread;
								char seq[2], seq2[2];
								nread = read(fd, &c, 1);
								if (nread <= 0) return len;
								if (c == 9 && completionCallback != NULL) {
												c = completeLine(fd, prompt, buf, buflen, &len, &pos, cols);
												if (c < 0) return len;
												if (c == 0) continue;
								}
								switch (c) {
								case 13:
												history_len--;
												free(history[history_len]);
												return (int)len;
								case 3:
												errno = EAGAIN;
												return -1;
								case 127:
								case 8:
												if (pos > 0 && len > 0) {
																memmove(buf + pos - 1, buf + pos, len - pos);
																pos--;
																len--;
																buf[len] = '\0';
																refreshLine(fd, prompt, buf, len, pos, cols);
												}
												break;
								case 4:
												if (len > 1 && pos < (len - 1)) {
																memmove(buf + pos, buf + pos + 1, len - pos);
																len--;
																buf[len] = '\0';
																refreshLine(fd, prompt, buf, len, pos, cols);
												} else if (len == 0) {
																history_len--;
																free(history[history_len]);
																return -1;
												}
												break;
								case 20:
												if (pos > 0 && pos < len) {
																int aux = buf[pos - 1];
																buf[pos - 1] = buf[pos];
																buf[pos] = aux;
																if (pos != len - 1) pos++;
																refreshLine(fd, prompt, buf, len, pos, cols);
												}
												break;
								case 2:
												goto left_arrow;
								case 6:
												goto right_arrow;
								case 16:
												seq[1] = 65;
												goto up_down_arrow;
								case 14:
												seq[1] = 66;
												goto up_down_arrow;
												break;
								case 27:
												if (read(fd, seq, 2) == -1) break;
												if (seq[0] == 91 && seq[1] == 68) {
left_arrow:
																if (pos > 0) {
																				pos--;
																				refreshLine(fd, prompt, buf, len, pos, cols);
																}
												} else if (seq[0] == 91 && seq[1] == 67) {
right_arrow:
																if (pos != len) {
																				pos++;
																				refreshLine(fd, prompt, buf, len, pos, cols);
																}
												} else if (seq[0] == 91 && (seq[1] == 65 || seq[1] == 66)) {
up_down_arrow:
																if (history_len > 1) {
																				free(history[history_len - 1 - history_index]);
																				history[history_len - 1 - history_index] = strdup(buf);
																				history_index += (seq[1] == 65) ? 1 : -1;
																				if (history_index < 0) {
																								history_index = 0;
																								break;
																				} else if (history_index >= history_len) {
																								history_index = history_len - 1;
																								break;
																				}
																				strncpy(buf, history[history_len - 1 - history_index], buflen);
																				buf[buflen] = '\0';
																				len = pos = strlen(buf);
																				refreshLine(fd, prompt, buf, len, pos, cols);
																}
												} else if (seq[0] == 91 && seq[1] > 48 && seq[1] < 55) {
																if (read(fd, seq2, 2) == -1) break;
																if (seq[1] == 51 && seq2[0] == 126) {
																				if (len > 0 && pos < len) {
																								memmove(buf + pos, buf + pos + 1, len - pos - 1);
																								len--;
																								buf[len] = '\0';
																								refreshLine(fd, prompt, buf, len, pos, cols);
																				}
																}
												}
												break;
								default:
												if (len < buflen) {
																if (len == pos) {
																				buf[pos] = c;
																				pos++;
																				len++;
																				buf[len] = '\0';
																				if (plen + len < cols) {
																								if (write(fd, &c, 1) == -1) return -1;
																				} else {
																								refreshLine(fd, prompt, buf, len, pos, cols);
																				}
																} else {
																				memmove(buf + pos + 1, buf + pos, len - pos);
																				buf[pos] = c;
																				len++;
																				pos++;
																				buf[len] = '\0';
																				refreshLine(fd, prompt, buf, len, pos, cols);
																}
												}
												break;
								case 21:
												buf[0] = '\0';
												pos = len = 0;
												refreshLine(fd, prompt, buf, len, pos, cols);
												break;
								case 11:
												buf[pos] = '\0';
												len = pos;
												refreshLine(fd, prompt, buf, len, pos, cols);
												break;
								case 1:
												pos = 0;
												refreshLine(fd, prompt, buf, len, pos, cols);
												break;
								case 5:
												pos = len;
												refreshLine(fd, prompt, buf, len, pos, cols);
												break;
								case 12:
												simplerlClearScreen();
												refreshLine(fd, prompt, buf, len, pos, cols);
								}
				}
				return len;
}

static int simplerlRaw(char *buf, size_t buflen, const char *prompt) {
				int fd = STDIN_FILENO;
				int count;
				if (buflen == 0) {
								errno = EINVAL;
								return -1;
				}
				if (!isatty(STDIN_FILENO)) {
								if (fgets(buf, buflen, stdin) == NULL) return -1;
								count = strlen(buf);
								if (count && buf[count - 1] == '\n') {
												count--;
												buf[count] = '\0';
								}
				} else {
								if (enableRawMode(fd) == -1) return -1;
								count = simplerlPrompt(fd, buf, buflen, prompt);
								disableRawMode(fd);
								printf("\n");
				}
				return count;
}

char *simplerl(const char *prompt) {
				char buf[SIMPLERL_MAX_LINE];
				int count;
				if (isUnsupportedTerm()) {
								size_t len;
								printf("%s", prompt);
								fflush(stdout);
								if (fgets(buf, SIMPLERL_MAX_LINE, stdin) == NULL) return NULL;
								len = strlen(buf);
								while (len && (buf[len - 1] == '\n' || buf[len - 1] == '\r')) {
												len--;
												buf[len] = '\0';
								}
								return strdup(buf);
				} else {
								count = simplerlRaw(buf, SIMPLERL_MAX_LINE, prompt);
								if (count == -1) return NULL;
								return strdup(buf);
				}
}

void simplerlSetCompletionCallback(simplerlCompletionCallback *fn) {
				completionCallback = fn;
}

void simplerlAddCompletion(simplerlCompletions *lc, char *str) {
				size_t len = strlen(str);
				char *copy = malloc(len + 1);
				memcpy(copy, str, len + 1);
				lc->cvec = realloc(lc->cvec, sizeof(char *) * (lc->len + 1));
				lc->cvec[lc->len++] = copy;
}

int simplerlHistoryAdd(const char *line) {
				char *linecopy;
				if (history_max_len == 0) return 0;
				if (history == NULL) {
								history = malloc(sizeof(char *) * history_max_len);
								if (history == NULL) return 0;
								memset(history, 0, (sizeof(char *) * history_max_len));
				}
				linecopy = strdup(line);
				if (!linecopy) return 0;
				if (history_len == history_max_len) {
								free(history[0]);
								memmove(history, history + 1, sizeof(char *) * (history_max_len - 1));
								history_len--;
				}
				history[history_len] = linecopy;
				history_len++;
				return 1;
}

int simplerlHistorySetMaxLen(int len) {
				char **new;
				if (len < 1) return 0;
				if (history) {
								int tocopy = history_len;
								new = malloc(sizeof(char *) * len);
								if (new == NULL) return 0;
								if (len < tocopy) tocopy = len;
								memcpy(new, history + (history_max_len - tocopy), sizeof(char *) * tocopy);
								free(history);
								history = new;
				}
				history_max_len = len;
				if (history_len > history_max_len) history_len = history_max_len;
				return 1;
}

int simplerlHistorySave(char *filename) {
				FILE *fp = fopen(filename, "w");
				int j;
				if (fp == NULL) return -1;
				for (j = 0; j < history_len; j++) fprintf(fp, "%s\n", history[j]);
				fclose(fp);
				return 0;
}

int simplerlHistoryLoad(char *filename) {
				FILE *fp = fopen(filename, "r");
				char buf[SIMPLERL_MAX_LINE];
				if (fp == NULL) return -1;
				while (fgets(buf, SIMPLERL_MAX_LINE, fp) != NULL) {
								char *p;
								p = strchr(buf, '\r');
								if (!p) p = strchr(buf, '\n');
								if (p) *p = '\0';
								simplerlHistoryAdd(buf);
				}
				fclose(fp);
				return 0;
}
