#pragma once

/**
 * Event library.
 */

#define EL_SETSIZE (1024 * 10)
#define EL_OK 0
#define EL_ERR -1
#define EL_NONE 0
#define EL_READABLE 1
#define EL_WRITABLE 2
#define EL_FILE_EVENTS 1
#define EL_TIME_EVENTS 2
#define EL_ALL_EVENTS (EL_FILE_EVENTS | EL_TIME_EVENTS)
#define EL_DONT_WAIT 4
#define EL_NOMORE -1
#define EL_UNUSED(V) ((void)V)

struct elLoop;

typedef void elFileProc(struct elLoop *eventLoop, int fd, void *clientData,
                        int mask);

typedef int elTimeProc(struct elLoop *eventLoop, long long id,
                       void *clientData);

typedef void elEventFinalizerProc(struct elLoop *eventLoop,
                                  void *clientData);

typedef void elBeforeSleepProc(struct elLoop *eventLoop);

typedef struct elFileEvent {
  int mask;
  elFileProc *rProc;
  elFileProc *wProc;
  void *clientData;
} elFileEvent;

typedef struct elTimeEvent {
  long long id;
  long when_s;
  long when_ms;
  elTimeProc *timeProc;
  elEventFinalizerProc *finalizerProc;
  void *clientData;
  struct elTimeEvent *next;
} elTimeEvent;

typedef struct elFiredEvent {
  int fd;
  int mask;
} elFiredEvent;

typedef struct elLoop {
  int maxfd;
  long long nextId;
  elFileEvent events[EL_SETSIZE];
  elFiredEvent fired[EL_SETSIZE];
  elTimeEvent *timeEventHead;
  int stop;
  void *apidata;
  elBeforeSleepProc *beforesleep;
} elLoop;

elLoop *elCreateLoop(void);

void elDeleteLoop(elLoop *l);

void elStop(elLoop *l);

int elCreateFileEvent(elLoop *l, int fd, int mask,
                      elFileProc *proc, void *clientData);

void elDeleteFileEvent(elLoop *l, int fd, int mask);

long long elCreateTimeEvent(elLoop *l, long long ms,
                            elTimeProc *proc, void *clientData,
                            elEventFinalizerProc *finalizerProc);

int elDeleteTimeEvent(elLoop *l, long long id);

int elProcessEvents(elLoop *l, int flags);

int elWait(int fd, int mask, long long ms);

void elMain(elLoop *l);

char *elGetApiName(void);

void elSetBeforeSleepProc(elLoop *l,
                          elBeforeSleepProc *beforesleep);

