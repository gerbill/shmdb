#include <stdlib.h>
#include "list.h"
#include "dbmalloc.h"

list *lCreate(void) {
  struct list *l;
  if ((l = dbmalloc(sizeof(*l))) == NULL) return NULL;
  l->head = l->tail = NULL;
  l->len = 0;
  l->dup = NULL;
  l->free = NULL;
  l->match = NULL;
  return l;
}

void lRelease(list *l) {
  unsigned int len;
  lNode *current, *next;
  current = l->head;
  len = l->len;
  while (len--) {
    next = current->next;
    if (l->free) l->free(current->value);
    dbfree(current);
    current = next;
  }
  dbfree(l);
}

list *lAddNodeHead(list *l, void *v) {
  lNode *node;
  if ((node = dbmalloc(sizeof(*node))) == NULL) return NULL;
  node->value = v;
  if (l->len == 0) {
    l->head = l->tail = node;
    node->prev = node->next = NULL;
  } else {
    node->prev = NULL;
    node->next = l->head;
    l->head->prev = node;
    l->head = node;
  }
  l->len++;
  return l;
}

list *lAddNodeTail(list *l, void *v) {
  lNode *node;
  if ((node = dbmalloc(sizeof(*node))) == NULL) return NULL;
  node->value = v;
  if (l->len == 0) {
    l->head = l->tail = node;
    node->prev = node->next = NULL;
  } else {
    node->prev = l->tail;
    node->next = NULL;
    l->tail->next = node;
    l->tail = node;
  }
  l->len++;
  return l;
}

list *lInsertNode(list *l, lNode *old, void *v, int after) {
  lNode *node;
  if ((node = dbmalloc(sizeof(*node))) == NULL) return NULL;
  node->value = v;
  if (after) {
    node->prev = old;
    node->next = old->next;
    if (l->tail == old) {
      l->tail = node;
    }
  } else {
    node->next = old;
    node->prev = old->prev;
    if (l->head == old) {
      l->head = node;
    }
  }
  if (node->prev != NULL) {
    node->prev->next = node;
  }
  if (node->next != NULL) {
    node->next->prev = node;
  }
  l->len++;
  return l;
}

void lDelNode(list *l, lNode *node) {
  if (node->prev)
    node->prev->next = node->next;
  else
    l->head = node->next;
  if (node->next)
    node->next->prev = node->prev;
  else
    l->tail = node->prev;
  if (l->free) l->free(node->value);
  dbfree(node);
  l->len--;
}

lIter *lGetIterator(list *l, int dir) {
  lIter *iter;
  if ((iter = dbmalloc(sizeof(*iter))) == NULL) return NULL;
  if (dir == L_START_HEAD)
    iter->next = l->head;
  else
    iter->next = l->tail;
  iter->direction = dir;
  return iter;
}

void lReleaseIterator(lIter *i) { dbfree(i); }

void lRewind(list *list, lIter *li) {
  li->next = list->head;
  li->direction = L_START_HEAD;
}

void listRewindTail(list *l, lIter *i) {
  i->next = l->tail;
  i->direction = L_START_TAIL;
}

lNode *lNext(lIter *i) {
  lNode *current = i->next;
  if (current != NULL) {
    if (i->direction == L_START_HEAD)
      i->next = current->next;
    else
      i->next = current->prev;
  }
  return current;
}

list *lDup(list *src) {
  list *copy;
  lIter *i;
  lNode *node;
  if ((copy = lCreate()) == NULL) return NULL;
  copy->dup = src->dup;
  copy->free = src->free;
  copy->match = src->match;
  i = lGetIterator(src, L_START_HEAD);
  while ((node = lNext(i)) != NULL) {
    void *value;
    if (copy->dup) {
      value = copy->dup(node->value);
      if (value == NULL) {
        lRelease(copy);
        lReleaseIterator(i);
        return NULL;
      }
    } else
      value = node->value;
    if (lAddNodeTail(copy, value) == NULL) {
      lRelease(copy);
      lReleaseIterator(i);
      return NULL;
    }
  }
  lReleaseIterator(i);
  return copy;
}

lNode *lSearchKey(list *l, void *key) {
  lIter *i;
  lNode *node;
  i = lGetIterator(l, L_START_HEAD);
  while ((node = lNext(i)) != NULL) {
    if (l->match) {
      if (l->match(node->value, key)) {
        lReleaseIterator(i);
        return node;
      }
    } else {
      if (key == node->value) {
        lReleaseIterator(i);
        return node;
      }
    }
  }
  lReleaseIterator(i);
  return NULL;
}

lNode *lIndex(list *l, int idx) {
  lNode *n;
  if (idx < 0) {
    idx = (-idx) - 1;
    n = l->tail;
    while (idx-- && n) n = n->prev;
  } else {
    n = l->head;
    while (idx-- && n) n = n->next;
  }
  return n;
}

