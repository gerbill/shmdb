#include "shm.h"

int syncWrite(int fd, char *ptr, ssize_t size, int timeout) {
				ssize_t nwritten, ret = size;
				time_t start = time(NULL);
				timeout++;
				while (size) {
								if (elWait(fd, EL_WRITABLE, 1000) & EL_WRITABLE) {
												nwritten = write(fd, ptr, size);
												if (nwritten == -1) return -1;
												ptr += nwritten;
												size -= nwritten;
								}
								if ((time(NULL) - start) > timeout) {
												errno = ETIMEDOUT;
												return -1;
								}
				}
				return ret;
}

int syncRead(int fd, char *ptr, ssize_t size, int timeout) {
				ssize_t nread, totread = 0;
				time_t start = time(NULL);
				timeout++;
				while (size) {
								if (elWait(fd, EL_READABLE, 1000) & EL_READABLE) {
												nread = read(fd, ptr, size);
												if (nread <= 0) return -1;
												ptr += nread;
												size -= nread;
												totread += nread;
								}
								if ((time(NULL) - start) > timeout) {
												errno = ETIMEDOUT;
												return -1;
								}
				}
				return totread;
}

int syncReadLine(int fd, char *ptr, ssize_t size, int timeout) {
				ssize_t nread = 0;
				size--;
				while (size) {
								char c;
								if (syncRead(fd, &c, 1, timeout) == -1) return -1;
								if (c == '\n') {
												*ptr = '\0';
												if (nread && *(ptr - 1) == '\r') *(ptr - 1) = '\0';
												return nread;
								} else {
												*ptr++ = c;
												*ptr = '\0';
												nread++;
								}
				}
				return nread;
}

int fwriteBulkString(FILE *fp, char *s, unsigned long len) {
				char cbuf[128];
				int clen;
				cbuf[0] = '$';
				clen = 1 + longLongToStr(cbuf + 1, sizeof(cbuf) - 1, len);
				cbuf[clen++] = '\r';
				cbuf[clen++] = '\n';
				if (fwrite(cbuf, clen, 1, fp) == 0) return 0;
				if (len > 0 && fwrite(s, len, 1, fp) == 0) return 0;
				if (fwrite("\r\n", 2, 1, fp) == 0) return 0;
				return 1;
}

int fwriteBulkDouble(FILE *fp, double d) {
				char buf[128], dbuf[128];
				snprintf(dbuf, sizeof(dbuf), "%.17g\r\n", d);
				snprintf(buf, sizeof(buf), "$%lu\r\n", (unsigned long)strlen(dbuf) - 2);
				if (fwrite(buf, strlen(buf), 1, fp) == 0) return 0;
				if (fwrite(dbuf, strlen(dbuf), 1, fp) == 0) return 0;
				return 1;
}

int fwriteBulkLongLong(FILE *fp, long long l) {
				char bbuf[128], lbuf[128];
				unsigned int blen, llen;
				llen = longLongToStr(lbuf, 32, l);
				blen = snprintf(bbuf, sizeof(bbuf), "$%u\r\n%s\r\n", llen, lbuf);
				if (fwrite(bbuf, blen, 1, fp) == 0) return 0;
				return 1;
}

int fwriteBulkObject(FILE *fp, robj *obj) {
				if (obj->encoding == SHM_ENCODING_INT) {
								return fwriteBulkLongLong(fp, (long)obj->ptr);
				} else if (obj->encoding == SHM_ENCODING_RAW) {
								return fwriteBulkString(fp, obj->ptr, dstrlen(obj->ptr));
				} else {
								shmPanic("Unknown string encoding");
				}
}
