#include "shm.h"

robj *setTypeCreate(robj *value) {
				if (objToLongLong(value, NULL) == SHM_OK)
								return objCreateIs();
				return objCreateSet();
}

int setTypeAdd(robj *subject, robj *value) {
				long long llval;
				if (subject->encoding == SHM_ENCODING_HT) {
								if (hashAdd(subject->ptr, value, NULL) == HASH_OK) {
												objIncRefCount(value);
												return 1;
								}
				} else if (subject->encoding == SHM_ENCODING_IS) {
								if (objToLongLong(value, &llval) == SHM_OK) {
												uint8_t success = 0;
												subject->ptr = isAdd(subject->ptr, llval, &success);
												if (success) {
																if (isLen(subject->ptr) > server.set_max_is_entries)
																				setTypeConvert(subject, SHM_ENCODING_HT);
																return 1;
												}
								} else {
												setTypeConvert(subject, SHM_ENCODING_HT);
												shmAssert(hashAdd(subject->ptr, value, NULL) == HASH_OK);
												objIncRefCount(value);
												return 1;
								}
				} else {
								shmPanic("Unknown set encoding");
				}
				return 0;
}

int setTypeRemove(robj *setobj, robj *value) {
				long long llval;
				if (setobj->encoding == SHM_ENCODING_HT) {
								if (hashDelete(setobj->ptr, value) == HASH_OK) {
												if (htNeedsResize(setobj->ptr)) hashResize(setobj->ptr);
												return 1;
								}
				} else if (setobj->encoding == SHM_ENCODING_IS) {
								if (objToLongLong(value, &llval) == SHM_OK) {
												int success;
												setobj->ptr = isRemove(setobj->ptr, llval, &success);
												if (success) return 1;
								}
				} else {
								shmPanic("Unknown set encoding");
				}
				return 0;
}

int setTypeIsMember(robj *subject, robj *value) {
				long long llval;
				if (subject->encoding == SHM_ENCODING_HT) {
								return hashFind((hash *)subject->ptr, value) != NULL;
				} else if (subject->encoding == SHM_ENCODING_IS) {
								if (objToLongLong(value, &llval) == SHM_OK) {
												return isFind((is *)subject->ptr, llval);
								}
				} else {
								shmPanic("Unknown set encoding");
				}
				return 0;
}

setTypeIterator *setTypeInitIterator(robj *subject) {
				setTypeIterator *si = dbmalloc(sizeof(setTypeIterator));
				si->subject = subject;
				si->encoding = subject->encoding;
				if (si->encoding == SHM_ENCODING_HT) {
								si->di = hashGetIterator(subject->ptr);
				} else if (si->encoding == SHM_ENCODING_IS) {
								si->ii = 0;
				} else {
								shmPanic("Unknown set encoding");
				}
				return si;
}

void setTypeReleaseIterator(setTypeIterator *si) {
				if (si->encoding == SHM_ENCODING_HT) hashReleaseIterator(si->di);
				dbfree(si);
}

int setTypeNext(setTypeIterator *si, robj **objele, int64_t *llele) {
				if (si->encoding == SHM_ENCODING_HT) {
								hashEntry *de = hashNext(si->di);
								if (de == NULL) return -1;
								*objele = hashGetEntryKey(de);
				} else if (si->encoding == SHM_ENCODING_IS) {
								if (!isGet(si->subject->ptr, si->ii++, llele)) return -1;
				}
				return si->encoding;
}

robj *setTypeNextObject(setTypeIterator *si) {
				int64_t intele;
				robj *objele;
				int encoding;
				encoding = setTypeNext(si, &objele, &intele);
				switch (encoding) {
				case -1:
								return NULL;
				case SHM_ENCODING_IS:
								return objCreateStringFromLongLong(intele);
				case SHM_ENCODING_HT:
								objIncRefCount(objele);
								return objele;
				default:
								shmPanic("Unsupported encoding");
				}
				return NULL;
}

int setTypeRandomElement(robj *setobj, robj **objele, int64_t *llele) {
				if (setobj->encoding == SHM_ENCODING_HT) {
								hashEntry *de = hashGetRandomKey(setobj->ptr);
								*objele = hashGetEntryKey(de);
				} else if (setobj->encoding == SHM_ENCODING_IS) {
								*llele = isRandom(setobj->ptr);
				} else {
								shmPanic("Unknown set encoding");
				}
				return setobj->encoding;
}

unsigned long setTypeSize(robj *subject) {
				if (subject->encoding == SHM_ENCODING_HT) {
								return hashSize((hash *)subject->ptr);
				} else if (subject->encoding == SHM_ENCODING_IS) {
								return isLen((is *)subject->ptr);
				} else {
								shmPanic("Unknown set encoding");
				}
}

void setTypeConvert(robj *setobj, int enc) {
				setTypeIterator *si;
				shmAssert(setobj->type == SHM_SET && setobj->encoding == SHM_ENCODING_IS);
				if (enc == SHM_ENCODING_HT) {
								int64_t intele;
								hash *d = hashCreate(&setHashType, NULL);
								robj *element;
								hashExpand(d, isLen(setobj->ptr));
								si = setTypeInitIterator(setobj);
								while (setTypeNext(si, NULL, &intele) != -1) {
												element = objCreateStringFromLongLong(intele);
												shmAssert(hashAdd(d, element, NULL) == HASH_OK);
								}
								setTypeReleaseIterator(si);
								setobj->encoding = SHM_ENCODING_HT;
								dbfree(setobj->ptr);
								setobj->ptr = d;
				} else {
								shmPanic("Unsupported set conversion");
				}
}

void setaddCmd(shmClient *c) {
				robj *set;
				set = lookupKeyWrite(c->db, c->argv[1]);
				c->argv[2] = objTryEncoding(c->argv[2]);
				if (set == NULL) {
								set = setTypeCreate(c->argv[2]);
								dbAdd(c->db, c->argv[1], set);
				} else {
								if (set->type != SHM_SET) {
												netAdd(c, shared.wrongtypeerr);
												return;
								}
				}
				if (setTypeAdd(set, c->argv[2])) {
								touchWatchedKey(c->db, c->argv[1]);
								server.dirty++;
								netAdd(c, shared.cone);
				} else {
								netAdd(c, shared.czero);
				}
}

void setremCmd(shmClient *c) {
				robj *set;
				if ((set = lookupKeyWriteOrReply(c, c->argv[1], shared.czero)) == NULL ||
				    objCheckType(c, set, SHM_SET))
								return;
				c->argv[2] = objTryEncoding(c->argv[2]);
				if (setTypeRemove(set, c->argv[2])) {
								if (setTypeSize(set) == 0) dbDelete(c->db, c->argv[1]);
								touchWatchedKey(c->db, c->argv[1]);
								server.dirty++;
								netAdd(c, shared.cone);
				} else {
								netAdd(c, shared.czero);
				}
}

void smvkeyCmd(shmClient *c) {
				robj *srcset, *dstset, *ele;
				srcset = lookupKeyWrite(c->db, c->argv[1]);
				dstset = lookupKeyWrite(c->db, c->argv[2]);
				ele = c->argv[3] = objTryEncoding(c->argv[3]);
				if (srcset == NULL) {
								netAdd(c, shared.czero);
								return;
				}
				if (objCheckType(c, srcset, SHM_SET) ||
				    (dstset && objCheckType(c, dstset, SHM_SET)))
								return;
				if (srcset == dstset) {
								netAdd(c, shared.cone);
								return;
				}
				if (!setTypeRemove(srcset, ele)) {
								netAdd(c, shared.czero);
								return;
				}
				if (setTypeSize(srcset) == 0) dbDelete(c->db, c->argv[1]);
				touchWatchedKey(c->db, c->argv[1]);
				touchWatchedKey(c->db, c->argv[2]);
				server.dirty++;
				if (!dstset) {
								dstset = setTypeCreate(ele);
								dbAdd(c->db, c->argv[2], dstset);
				}
				if (setTypeAdd(dstset, ele)) server.dirty++;
				netAdd(c, shared.cone);
}

void setmemberofCmd(shmClient *c) {
				robj *set;
				if ((set = lookupKeyReadOrReply(c, c->argv[1], shared.czero)) == NULL ||
				    objCheckType(c, set, SHM_SET))
								return;
				c->argv[2] = objTryEncoding(c->argv[2]);
				if (setTypeIsMember(set, c->argv[2]))
								netAdd(c, shared.cone);
				else
								netAdd(c, shared.czero);
}

void setcardCmd(shmClient *c) {
				robj *o;
				if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.czero)) == NULL ||
				    objCheckType(c, o, SHM_SET))
								return;
				netAddLongLong(c, setTypeSize(o));
}

void setpopCmd(shmClient *c) {
				robj *set, *ele, *aux;
				int64_t llele;
				int encoding;
				if ((set = lookupKeyWriteOrReply(c, c->argv[1], shared.nullbulk)) == NULL ||
				    objCheckType(c, set, SHM_SET))
								return;
				encoding = setTypeRandomElement(set, &ele, &llele);
				if (encoding == SHM_ENCODING_IS) {
								ele = objCreateStringFromLongLong(llele);
								set->ptr = isRemove(set->ptr, llele, NULL);
				} else {
								objIncRefCount(ele);
								setTypeRemove(set, ele);
				}
				aux = objCreateString("SREM", 4);
				netRewriteClientCommandVector(c, 3, aux, c->argv[1], ele);
				objDecRefCount(ele);
				objDecRefCount(aux);
				netAddBulk(c, ele);
				if (setTypeSize(set) == 0) dbDelete(c->db, c->argv[1]);
				touchWatchedKey(c->db, c->argv[1]);
				server.dirty++;
}

void setranditemCmd(shmClient *c) {
				robj *set, *ele;
				int64_t llele;
				int encoding;
				if ((set = lookupKeyReadOrReply(c, c->argv[1], shared.nullbulk)) == NULL ||
				    objCheckType(c, set, SHM_SET))
								return;
				encoding = setTypeRandomElement(set, &ele, &llele);
				if (encoding == SHM_ENCODING_IS) {
								netAddBulkLongLong(c, llele);
				} else {
								netAddBulk(c, ele);
				}
}

int qsortCompareSetsByCardinality(const void *s1, const void *s2) {
				return setTypeSize(*(robj **)s1) - setTypeSize(*(robj **)s2);
}

void sinterGenericCmd(shmClient *c, robj **setkeys, unsigned long setnum,
                          robj *dstkey) {
				robj **sets = dbmalloc(sizeof(robj *) * setnum);
				setTypeIterator *si;
				robj *eleobj, *dstset = NULL;
				int64_t intobj;
				void *replylen = NULL;
				unsigned long j, cardinality = 0;
				int encoding;
				for (j = 0; j < setnum; j++) {
								robj *setobj = dstkey ? lookupKeyWrite(c->db, setkeys[j])
								               : lookupKeyRead(c->db, setkeys[j]);
								if (!setobj) {
												dbfree(sets);
												if (dstkey) {
																if (dbDelete(c->db, dstkey)) {
																				touchWatchedKey(c->db, dstkey);
																				server.dirty++;
																}
																netAdd(c, shared.czero);
												} else {
																netAdd(c, shared.emptybeginbulk);
												}
												return;
								}
								if (objCheckType(c, setobj, SHM_SET)) {
												dbfree(sets);
												return;
								}
								sets[j] = setobj;
				}
				qsort(sets, setnum, sizeof(robj *), qsortCompareSetsByCardinality);
				if (!dstkey) {
								replylen = addDeferredBeginBulkLength(c);
				} else {
								dstset = objCreateIs();
				}
				si = setTypeInitIterator(sets[0]);
				while ((encoding = setTypeNext(si, &eleobj, &intobj)) != -1) {
								for (j = 1; j < setnum; j++) {
												if (sets[j] == sets[0]) continue;
												if (encoding == SHM_ENCODING_IS) {
																if (sets[j]->encoding == SHM_ENCODING_IS &&
																    !isFind((is *)sets[j]->ptr, intobj)) {
																				break;
																} else if (sets[j]->encoding == SHM_ENCODING_HT) {
																				eleobj = objCreateStringFromLongLong(intobj);
																				if (!setTypeIsMember(sets[j], eleobj)) {
																								objDecRefCount(eleobj);
																								break;
																				}
																				objDecRefCount(eleobj);
																}
												} else if (encoding == SHM_ENCODING_HT) {
																if (eleobj->encoding == SHM_ENCODING_INT &&
																    sets[j]->encoding == SHM_ENCODING_IS &&
																    !isFind((is *)sets[j]->ptr, (long)eleobj->ptr)) {
																				break;
																} else if (!setTypeIsMember(sets[j], eleobj)) {
																				break;
																}
												}
								}
								if (j == setnum) {
												if (!dstkey) {
																if (encoding == SHM_ENCODING_HT)
																				netAddBulk(c, eleobj);
																else
																				netAddBulkLongLong(c, intobj);
																cardinality++;
												} else {
																if (encoding == SHM_ENCODING_IS) {
																				eleobj = objCreateStringFromLongLong(intobj);
																				setTypeAdd(dstset, eleobj);
																				objDecRefCount(eleobj);
																} else {
																				setTypeAdd(dstset, eleobj);
																}
												}
								}
				}
				setTypeReleaseIterator(si);
				if (dstkey) {
								dbDelete(c->db, dstkey);
								if (setTypeSize(dstset) > 0) {
												dbAdd(c->db, dstkey, dstset);
												netAddLongLong(c, setTypeSize(dstset));
								} else {
												objDecRefCount(dstset);
												netAdd(c, shared.czero);
								}
								touchWatchedKey(c->db, dstkey);
								server.dirty++;
				} else {
								setDeferredBeginBulkLength(c, replylen, cardinality);
				}
				dbfree(sets);
}

void setinterCmd(shmClient *c) {
				sinterGenericCmd(c, c->argv + 1, c->argc - 1, NULL);
}

void setintertokeyCmd(shmClient *c) {
				sinterGenericCmd(c, c->argv + 2, c->argc - 2, c->argv[1]);
}
#define SHM_OP_UNION 0
#define SHM_OP_DIFF 1
#define SHM_OP_INTER 2

void sunionDiffGenericCmd(shmClient *c, robj **setkeys, int setnum,
                              robj *dstkey, int op) {
				robj **sets = dbmalloc(sizeof(robj *) * setnum);
				setTypeIterator *si;
				robj *ele, *dstset = NULL;
				int j, cardinality = 0;
				for (j = 0; j < setnum; j++) {
								robj *setobj = dstkey ? lookupKeyWrite(c->db, setkeys[j])
								               : lookupKeyRead(c->db, setkeys[j]);
								if (!setobj) {
												sets[j] = NULL;
												continue;
								}
								if (objCheckType(c, setobj, SHM_SET)) {
												dbfree(sets);
												return;
								}
								sets[j] = setobj;
				}
				dstset = objCreateIs();
				for (j = 0; j < setnum; j++) {
								if (op == SHM_OP_DIFF && j == 0 && !sets[j]) break;
								if (!sets[j]) continue;
								si = setTypeInitIterator(sets[j]);
								while ((ele = setTypeNextObject(si)) != NULL) {
												if (op == SHM_OP_UNION || j == 0) {
																if (setTypeAdd(dstset, ele)) {
																				cardinality++;
																}
												} else if (op == SHM_OP_DIFF) {
																if (setTypeRemove(dstset, ele)) {
																				cardinality--;
																}
												}
												objDecRefCount(ele);
								}
								setTypeReleaseIterator(si);
								if (op == SHM_OP_DIFF && cardinality == 0) break;
				}
				if (!dstkey) {
								netAddBeginBulkLen(c, cardinality);
								si = setTypeInitIterator(dstset);
								while ((ele = setTypeNextObject(si)) != NULL) {
												netAddBulk(c, ele);
												objDecRefCount(ele);
								}
								setTypeReleaseIterator(si);
								objDecRefCount(dstset);
				} else {
								dbDelete(c->db, dstkey);
								if (setTypeSize(dstset) > 0) {
												dbAdd(c->db, dstkey, dstset);
												netAddLongLong(c, setTypeSize(dstset));
								} else {
												objDecRefCount(dstset);
												netAdd(c, shared.czero);
								}
								touchWatchedKey(c->db, dstkey);
								server.dirty++;
				}
				dbfree(sets);
}

void setunionCmd(shmClient *c) {
				sunionDiffGenericCmd(c, c->argv + 1, c->argc - 1, NULL, SHM_OP_UNION);
}

void setuniontokeyCmd(shmClient *c) {
				sunionDiffGenericCmd(c, c->argv + 2, c->argc - 2, c->argv[1],
				                         SHM_OP_UNION);
}

void setdiffCmd(shmClient *c) {
				sunionDiffGenericCmd(c, c->argv + 1, c->argc - 1, NULL, SHM_OP_DIFF);
}

void setdifftokeyCmd(shmClient *c) {
				sunionDiffGenericCmd(c, c->argv + 2, c->argc - 2, c->argv[1],
				                         SHM_OP_DIFF);
}
