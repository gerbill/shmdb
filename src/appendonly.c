#include "shm.h"
#include <signal.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>

int appendOnlyStart(void) {
				server.appendonly = 1;
				server.lastfsync = time(NULL);
				server.appendfd =
								open(server.appendfilename, O_WRONLY | O_APPEND | O_CREAT, 0644);
				if (server.appendfd == -1) {
								shmLog(SHM_WARNING,
								       "Used tried to switch on AOF via CONFIG, but I can't open the AOF "
								       "file: %s",
								       strerror(errno));
								return SHM_ERR;
				}
				if (appendOnlyRewriteAsync() == SHM_ERR) {
								server.appendonly = 0;
								close(server.appendfd);
								shmLog(SHM_WARNING,
								       "Used tried to switch on AOF via CONFIG, I can't trigger a "
								       "background AOF rewrite operation. Check the above logs for more "
								       "info about the error.",
								       strerror(errno));
								return SHM_ERR;
				}
				return SHM_OK;
}

void apendOnlyStop(void) {
	appendOnlyFlush();
	appendonly_fsync(server.appendfd);
	close(server.appendfd);
	server.appendfd = -1;
	server.appendseldb = -1;
	server.appendonly = 0;
	if (server.bgrewritechildpid != -1) {
		int statloc;
		if (kill(server.bgrewritechildpid, SIGKILL) != -1) wait3(&statloc, 0, NULL);
		dstrfree(server.bgrewritebuf);
		server.bgrewritebuf = dstrempty();
		server.bgrewritechildpid = -1;
	}
}

void appendOnlyFlush(void) {
				time_t now;
				ssize_t nwritten;
				if (dstrlen(server.aofbuf) == 0) return;
				nwritten = write(server.appendfd, server.aofbuf, dstrlen(server.aofbuf));
				if (nwritten != (signed)dstrlen(server.aofbuf)) {
								if (nwritten == -1) {
												shmLog(SHM_WARNING,
												       "Exiting on error writing to the append-only file: %s",
												       strerror(errno));
								} else {
												shmLog(SHM_WARNING,
												       "Exiting on short write while writing to the append-only file: %s",
												       strerror(errno));
								}
								exit(1);
				}
				dstrfree(server.aofbuf);
				server.aofbuf = dstrempty();
				if (server.no_appendfsync_on_rewrite &&
				    (server.bgrewritechildpid != -1 || server.asavechildpid != -1))
								return;
				now = time(NULL);
				if (server.appendfsync == APPENDFSYNC_ALWAYS ||
				    (server.appendfsync == APPENDFSYNC_EVERYSEC &&
				     now - server.lastfsync > 1)) {
								appendonly_fsync(server.appendfd);
								server.lastfsync = now;
				}
}

dstr appendOnlyCatGenericCmd(dstr buf, int argc, robj **argv) {
				int j;
				buf = dstrcatprintf(buf, "*%d\r\n", argc);
				for (j = 0; j < argc; j++) {
								robj *o = objGetDecoded(argv[j]);
								buf = dstrcatprintf(buf, "$%lu\r\n", (unsigned long)dstrlen(o->ptr));
								buf = dstrcatlen(buf, o->ptr, dstrlen(o->ptr));
								buf = dstrcatlen(buf, "\r\n", 2);
								objDecRefCount(o);
				}
				return buf;
}

dstr appendOnlyCatExpireAtCmd(dstr buf, robj *key, robj *seconds) {
				int argc = 3;
				long when;
				robj *argv[3];
				seconds = objGetDecoded(seconds);
				when = time(NULL) + strtol(seconds->ptr, NULL, 10);
				objDecRefCount(seconds);
				argv[0] = objCreateString("EXPIREAT", 8);
				argv[1] = key;
				argv[2] = objCreate(SHM_STRING, dstrcatprintf(dstrempty(), "%ld", when));
				buf = appendOnlyCatGenericCmd(buf, argc, argv);
				objDecRefCount(argv[0]);
				objDecRefCount(argv[2]);
				return buf;
}

void appendOnlyFeed(struct cmdShm *cmd, int hashid, robj **argv,
										int argc) {
				dstr buf = dstrempty();
				robj *tmpargv[3];
				if (hashid != server.appendseldb) {
								char seldb[64];
								snprintf(seldb, sizeof(seldb), "%d", hashid);
								buf = dstrcatprintf(buf, "*2\r\n$6\r\nSELECT\r\n$%lu\r\n%s\r\n",
								                   (unsigned long)strlen(seldb), seldb);
								server.appendseldb = hashid;
				}
				if (cmd->proc == expireCmd) {
								buf = appendOnlyCatExpireAtCmd(buf, argv[1], argv[2]);
				} else if (cmd->proc == setxCmd) {
								tmpargv[0] = objCreateString("SET", 3);
								tmpargv[1] = argv[1];
								tmpargv[2] = argv[3];
								buf = appendOnlyCatGenericCmd(buf, 3, tmpargv);
								objDecRefCount(tmpargv[0]);
								buf = appendOnlyCatExpireAtCmd(buf, argv[1], argv[2]);
				} else {
								buf = appendOnlyCatGenericCmd(buf, argc, argv);
				}
				server.aofbuf = dstrcatlen(server.aofbuf, buf, dstrlen(buf));
				if (server.bgrewritechildpid != -1)
								server.bgrewritebuf = dstrcatlen(server.bgrewritebuf, buf, dstrlen(buf));
				dstrfree(buf);
}
struct shmClient *fakeClientCreate(void) {
				struct shmClient *c = dbmalloc(sizeof(*c));
				selectDb(c, 0);
				c->fd = -1;
				c->querybuf = dstrempty();
				c->argc = 0;
				c->argv = NULL;
				c->bufpos = 0;
				c->flags = 0;
				c->replstate = SHM_REPL_WAIT_ASAVE_START;
				c->reply = lCreate();
				c->watched_keys = lCreate();
				lSetFreeMethod(c->reply, objDecRefCount);
				lSetDupMethod(c->reply, netDupReply);
				initClientBeginState(c);
				return c;
}

void fakeClientFree(struct shmClient *c) {
				dstrfree(c->querybuf);
	lRelease(c->reply);
	lRelease(c->watched_keys);
				netFreeClientBeginState(c);
				dbfree(c);
}

int appendOnlyLoad(char *filename) {
				struct shmClient *fakeClient;
				FILE *fp = fopen(filename, "r");
				struct shm_stat sb;
				int appendonly = server.appendonly;
				long loops = 0;
				if (fp && shm_fstat(fileno(fp), &sb) != -1 && sb.st_size == 0) {
								fclose(fp);
								return SHM_ERR;
				}
				if (fp == NULL) {
								shmLog(SHM_WARNING,
								       "Fatal error: can't open the append log file for reading: %s",
								       strerror(errno));
								exit(1);
				}
				server.appendonly = 0;
				fakeClient = fakeClientCreate();
				startLoading(fp);
				while (1) {
								int argc, j;
								unsigned long len;
								robj **argv;
								char buf[128];
								dstr argdstr;
								struct cmdShm *cmd;
								int force_swapout;
								if (!(loops++ % 1000)) {
												loadingProgress(ftello(fp));
												elProcessEvents(server.el, EL_FILE_EVENTS | EL_DONT_WAIT);
								}
								if (fgets(buf, sizeof(buf), fp) == NULL) {
												if (feof(fp))
																break;
												else
																goto readerr;
								}
								if (buf[0] != '*') goto fmterr;
								argc = atoi(buf + 1);
								argv = dbmalloc(sizeof(robj *) * argc);
								for (j = 0; j < argc; j++) {
												if (fgets(buf, sizeof(buf), fp) == NULL) goto readerr;
												if (buf[0] != '$') goto fmterr;
												len = strtol(buf + 1, NULL, 10);
												argdstr = dstrnewlen(NULL, len);
												if (len && fread(argdstr, len, 1, fp) == 0) goto fmterr;
												argv[j] = objCreate(SHM_STRING, argdstr);
												if (fread(buf, 2, 1, fp) == 0) goto fmterr;
								}
								cmd = lookupCmd(argv[0]->ptr);
								if (!cmd) {
												shmLog(SHM_WARNING, "Unknown command '%s' reading the append only file",
												       argv[0]->ptr);
												exit(1);
								}
								fakeClient->argc = argc;
								fakeClient->argv = argv;
								cmd->proc(fakeClient);
								shmAssert(fakeClient->bufpos == 0 && lLength(fakeClient->reply) == 0);
								shmAssert((fakeClient->flags & SHM_BLOCKED) == 0);
								for (j = 0; j < fakeClient->argc; j++) objDecRefCount(fakeClient->argv[j]);
								dbfree(fakeClient->argv);
								force_swapout = 0;
								if ((dbmalloc_used_memory() - server.vm_max_memory) > 1024 * 1024 * 32)
												force_swapout = 1;
								if (server.vm_enabled && force_swapout) {
												while (dbmalloc_used_memory() > server.vm_max_memory) {
																if (vmSwapOutOneObjBlocking() == SHM_ERR) break;
												}
								}
				}
				if (fakeClient->flags & SHM_BEGIN) goto readerr;
				fclose(fp);
	fakeClientFree(fakeClient);
				server.appendonly = appendonly;
				stopLoading();
				return SHM_OK;
readerr:
				if (feof(fp)) {
								shmLog(SHM_WARNING, "Unexpected end of file reading the append only file");
				} else {
								shmLog(SHM_WARNING, "Unrecoverable error reading the append only file: %s",
								       strerror(errno));
				}
				exit(1);
fmterr:
				shmLog(SHM_WARNING,
				       "Bad file format reading the append only file: make a backup of "
				       "your AOF file, then use ./shm-check-aof --fix <filename>");
				exit(1);
}

int appendOnlyRewrite(char *filename) {
				hashIterator *di = NULL;
				hashEntry *de;
				FILE *fp;
				char tmpfile[256];
				int j;
				time_t now = time(NULL);
				snprintf(tmpfile, 256, "temp-rewriteaof-%d.aof", (int)getpid());
				fp = fopen(tmpfile, "w");
				if (!fp) {
								shmLog(SHM_WARNING, "Failed rewriting the append only file: %s",
								       strerror(errno));
								return SHM_ERR;
				}
				for (j = 0; j < server.dbnum; j++) {
								char selectcmd[] = "*2\r\n$6\r\nSELECT\r\n";
								shmDb *db = server.db + j;
								hash *d = db->hash;
								if (hashSize(d) == 0) continue;
								di = hashGetSafeIterator(d);
								if (!di) {
												fclose(fp);
												return SHM_ERR;
								}
								if (fwrite(selectcmd, sizeof(selectcmd) - 1, 1, fp) == 0) goto werr;
								if (fwriteBulkLongLong(fp, j) == 0) goto werr;
								while ((de = hashNext(di)) != NULL) {
												dstr keystr = hashGetEntryKey(de);
												robj key, *o;
												time_t expiretime;
												int swapped;
												keystr = hashGetEntryKey(de);
												o = hashGetEntryVal(de);
												initStaticStringObject(key, keystr);
												if (!server.vm_enabled || o->storage == SHM_VM_MEMORY ||
												    o->storage == SHM_VM_SWAPPING) {
																swapped = 0;
												} else {
																o = vmPreviewObj(o);
																swapped = 1;
												}
												expiretime = getExpire(db, &key);
												if (o->type == SHM_STRING) {
																char cmd[] = "*3\r\n$3\r\nSET\r\n";
																if (fwrite(cmd, sizeof(cmd) - 1, 1, fp) == 0) goto werr;
																if (fwriteBulkObject(fp, &key) == 0) goto werr;
																if (fwriteBulkObject(fp, o) == 0) goto werr;
												} else if (o->type == SHM_LIST) {
																char cmd[] = "*3\r\n$5\r\nRPUSH\r\n";
																if (o->encoding == SHM_ENCODING_SORTLIST) {
																				unsigned char *zl = o->ptr;
																				unsigned char *p = sortlistIndex(zl, 0);
																				unsigned char *vstr;
																				unsigned int vlen;
																				long long vlong;
																				while (sortlistGet(p, &vstr, &vlen, &vlong)) {
																								if (fwrite(cmd, sizeof(cmd) - 1, 1, fp) == 0) goto werr;
																								if (fwriteBulkObject(fp, &key) == 0) goto werr;
																								if (vstr) {
																												if (fwriteBulkString(fp, (char *)vstr, vlen) == 0) goto werr;
																								} else {
																												if (fwriteBulkLongLong(fp, vlong) == 0) goto werr;
																								}
																								p = sortlistNext(zl, p);
																				}
																} else if (o->encoding == SHM_ENCODING_LINKEDLIST) {
																				list *list = o->ptr;
																				lNode *ln;
																				lIter li;
																	lRewind(list, &li);
																				while ((ln = lNext(&li))) {
																								robj *eleobj = lNodeValue(ln);
																								if (fwrite(cmd, sizeof(cmd) - 1, 1, fp) == 0) goto werr;
																								if (fwriteBulkObject(fp, &key) == 0) goto werr;
																								if (fwriteBulkObject(fp, eleobj) == 0) goto werr;
																				}
																} else {
																				shmPanic("Unknown list encoding");
																}
												} else if (o->type == SHM_SET) {
																char cmd[] = "*3\r\n$4\r\nSADD\r\n";
																if (o->encoding == SHM_ENCODING_IS) {
																				int ii = 0;
																				int64_t llval;
																				while (isGet(o->ptr, ii++, &llval)) {
																								if (fwrite(cmd, sizeof(cmd) - 1, 1, fp) == 0) goto werr;
																								if (fwriteBulkObject(fp, &key) == 0) goto werr;
																								if (fwriteBulkLongLong(fp, llval) == 0) goto werr;
																				}
																} else if (o->encoding == SHM_ENCODING_HT) {
																				hashIterator *di = hashGetIterator(o->ptr);
																				hashEntry *de;
																				while ((de = hashNext(di)) != NULL) {
																								robj *eleobj = hashGetEntryKey(de);
																								if (fwrite(cmd, sizeof(cmd) - 1, 1, fp) == 0) goto werr;
																								if (fwriteBulkObject(fp, &key) == 0) goto werr;
																								if (fwriteBulkObject(fp, eleobj) == 0) goto werr;
																				}
																				hashReleaseIterator(di);
																} else {
																				shmPanic("Unknown set encoding");
																}
												} else if (o->type == SHM_SORTSET) {
																sortset *zs = o->ptr;
																hashIterator *di = hashGetIterator(zs->hash);
																hashEntry *de;
																while ((de = hashNext(di)) != NULL) {
																				char cmd[] = "*4\r\n$4\r\nZADD\r\n";
																				robj *eleobj = hashGetEntryKey(de);
																				double *score = hashGetEntryVal(de);
																				if (fwrite(cmd, sizeof(cmd) - 1, 1, fp) == 0) goto werr;
																				if (fwriteBulkObject(fp, &key) == 0) goto werr;
																				if (fwriteBulkDouble(fp, *score) == 0) goto werr;
																				if (fwriteBulkObject(fp, eleobj) == 0) goto werr;
																}
																hashReleaseIterator(di);
												} else if (o->type == SHM_HASH) {
																char cmd[] = "*4\r\n$4\r\nHSET\r\n";
																if (o->encoding == SHM_ENCODING_ZIPMAP) {
																				unsigned char *p = zipmapRewind(o->ptr);
																				unsigned char *field, *val;
																				unsigned int flen, vlen;
																				while ((p = zipmapNext(p, &field, &flen, &val, &vlen)) != NULL) {
																								if (fwrite(cmd, sizeof(cmd) - 1, 1, fp) == 0) goto werr;
																								if (fwriteBulkObject(fp, &key) == 0) goto werr;
																								if (fwriteBulkString(fp, (char *)field, flen) == 0) goto werr;
																								if (fwriteBulkString(fp, (char *)val, vlen) == 0) goto werr;
																				}
																} else {
																				hashIterator *di = hashGetIterator(o->ptr);
																				hashEntry *de;
																				while ((de = hashNext(di)) != NULL) {
																								robj *field = hashGetEntryKey(de);
																								robj *val = hashGetEntryVal(de);
																								if (fwrite(cmd, sizeof(cmd) - 1, 1, fp) == 0) goto werr;
																								if (fwriteBulkObject(fp, &key) == 0) goto werr;
																								if (fwriteBulkObject(fp, field) == 0) goto werr;
																								if (fwriteBulkObject(fp, val) == 0) goto werr;
																				}
																				hashReleaseIterator(di);
																}
												} else {
																shmPanic("Unknown object type");
												}
												if (expiretime != -1) {
																char cmd[] = "*3\r\n$8\r\nEXPIREAT\r\n";
																if (expiretime < now) continue;
																if (fwrite(cmd, sizeof(cmd) - 1, 1, fp) == 0) goto werr;
																if (fwriteBulkObject(fp, &key) == 0) goto werr;
																if (fwriteBulkLongLong(fp, expiretime) == 0) goto werr;
												}
												if (swapped) objDecRefCount(o);
								}
								hashReleaseIterator(di);
				}
				fflush(fp);
				appendonly_fsync(fileno(fp));
				fclose(fp);
				if (rename(tmpfile, filename) == -1) {
								shmLog(SHM_WARNING,
								       "Error moving temp append only file on the final destination: %s",
								       strerror(errno));
								unlink(tmpfile);
								return SHM_ERR;
				}
				shmLog(SHM_NOTICE, "SYNC append only file rewrite performed");
				return SHM_OK;
werr:
				fclose(fp);
				unlink(tmpfile);
				shmLog(SHM_WARNING, "Write error writing append only file on disk: %s",
				       strerror(errno));
				if (di) hashReleaseIterator(di);
				return SHM_ERR;
}

int appendOnlyRewriteAsync(void) {
				pid_t childpid;
				if (server.bgrewritechildpid != -1) return SHM_ERR;
				if (server.vm_enabled) vmWaitIOJobQueueEmpty();
				if ((childpid = fork()) == 0) {
								char tmpfile[256];
								if (server.vm_enabled) vmReopenSwap();
								if (server.ipfd > 0) close(server.ipfd);
								if (server.sofd > 0) close(server.sofd);
								snprintf(tmpfile, 256, "temp-rewriteaof-bg-%d.aof", (int)getpid());
								if (appendOnlyRewrite(tmpfile) == SHM_OK) {
												_exit(0);
								} else {
												_exit(1);
								}
				} else {
								if (childpid == -1) {
												shmLog(SHM_WARNING,
												       "Can't rewrite append only file in background: fork: %s",
												       strerror(errno));
												return SHM_ERR;
								}
								shmLog(SHM_NOTICE,
								       "Background append only file rewriting started by pid %d", childpid);
								server.bgrewritechildpid = childpid;
								updateHashResizePolicy();
								server.appendseldb = -1;
								return SHM_OK;
				}
				return SHM_OK;
}

void asyncrwCmd(shmClient *c) {
				if (server.bgrewritechildpid != -1) {
								netAddError(c,
								              "Background append only file rewriting already in progress");
								return;
				}
				if (appendOnlyRewriteAsync() == SHM_OK) {
								netAddStatus(c, "Background append only file rewriting started");
				} else {
								netAdd(c, shared.err);
				}
}

void apendOnlyRemoveTempFile(pid_t childpid) {
				char tmpfile[256];
				snprintf(tmpfile, 256, "temp-rewriteaof-bg-%d.aof", (int)childpid);
				unlink(tmpfile);
}

void asyncRewriteDoneHandler(int statloc) {
				int exitcode = WEXITSTATUS(statloc);
				int bysignal = WIFSIGNALED(statloc);
				if (!bysignal && exitcode == 0) {
								int fd;
								char tmpfile[256];
								shmLog(SHM_NOTICE,
								       "Background append only file rewriting terminated with success");
								snprintf(tmpfile, 256, "temp-rewriteaof-bg-%d.aof",
								         (int)server.bgrewritechildpid);
								fd = open(tmpfile, O_WRONLY | O_APPEND);
								if (fd == -1) {
												shmLog(SHM_WARNING,
												       "Not able to open the temp append only file produced by the "
												       "child: %s",
												       strerror(errno));
												goto cleanup;
								}
								if (write(fd, server.bgrewritebuf, dstrlen(server.bgrewritebuf)) !=
								    (signed)dstrlen(server.bgrewritebuf)) {
												shmLog(SHM_WARNING,
												       "Error or short write trying to flush the parent diff of the "
												       "append log file in the child temp file: %s",
												       strerror(errno));
												close(fd);
												goto cleanup;
								}
								shmLog(SHM_NOTICE,
								       "Parent diff flushed into the new append log file with success "
								       "(%lu bytes)",
								       dstrlen(server.bgrewritebuf));
								if (rename(tmpfile, server.appendfilename) == -1) {
												shmLog(SHM_WARNING,
												       "Can't rename the temp append only file into the stable one: %s",
												       strerror(errno));
												close(fd);
												goto cleanup;
								}
								shmLog(SHM_NOTICE, "Append only file successfully rewritten.");
								if (server.appendfd != -1) {
												close(server.appendfd);
												server.appendfd = fd;
												if (server.appendfsync != APPENDFSYNC_NO) appendonly_fsync(fd);
												server.appendseldb = -1;
												shmLog(SHM_NOTICE,
												       "The new append only file was selected for future appends.");
												dstrfree(server.aofbuf);
												server.aofbuf = dstrempty();
								} else {
												close(fd);
								}
				} else if (!bysignal && exitcode != 0) {
								shmLog(SHM_WARNING, "Background append only file rewriting error");
				} else {
								shmLog(SHM_WARNING,
								       "Background append only file rewriting terminated by signal %d",
								       WTERMSIG(statloc));
				}
cleanup:
				dstrfree(server.bgrewritebuf);
				server.bgrewritebuf = dstrempty();
	apendOnlyRemoveTempFile(server.bgrewritechildpid);
				server.bgrewritechildpid = -1;
}
