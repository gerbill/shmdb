#ifndef __IS_H
#define __IS_H
#include <stdint.h>
typedef struct is {
				uint32_t encoding;
				uint32_t length;
				int8_t contents[];
} is;

is *isNew(void);

is *isAdd(is *is, int64_t value, uint8_t *success);

is *isRemove(is *is, int64_t value, int *success);

uint8_t isFind(is *is, int64_t value);

int64_t isRandom(is *is);

uint8_t isGet(is *is, uint32_t pos, int64_t *value);

uint32_t isLen(is *is);

#endif
