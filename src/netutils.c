#include "shm.h"
#include <sys/uio.h>

void *netDupReply(void *o) {
  objIncRefCount((robj *) o);
  return o;
}

int _installWriteEvent(shmClient *c) {
  if (c->fd <= 0) return SHM_ERR;
  if (c->bufpos == 0 && lLength(c->reply) == 0 &&
      (c->replstate == SHM_REPL_NONE || c->replstate == SHM_REPL_ONLINE) &&
      elCreateFileEvent(server.el, c->fd, EL_WRITABLE, netSendReply, c) ==
      EL_ERR)
    return SHM_ERR;
  return SHM_OK;
}

robj *netDupLastObjectIfNeeded(list *reply) {
  robj *new, *cur;
  lNode *ln;
  shmAssert(lLength(reply) > 0);
  ln = lLast(reply);
  cur = lNodeValue(ln);
  if (cur->refcount > 1) {
    new = objStringDup(cur);
    objDecRefCount(cur);
    lNodeValue(ln) = new;
  }
  return lNodeValue(ln);
}

int listMatchObjects(void *a, void *b) {
  return objEqualString(a, b);
}

shmClient *netCreateClient(int fd) {
  shmClient *c = dbmalloc(sizeof(shmClient));
  c->bufpos = 0;
  snetNonBlock(NULL, fd);
  snetTcpNoDelay(NULL, fd);
  if (!c) return NULL;
  if (elCreateFileEvent(server.el, fd, EL_READABLE, netReadQueryFromClient, c) ==
      EL_ERR) {
    close(fd);
    dbfree(c);
    return NULL;
  }
  selectDb(c, 0);
  c->fd = fd;
  c->querybuf = dstrempty();
  c->reqtype = 0;
  c->argc = 0;
  c->argv = NULL;
  c->cmd = NULL;
  c->beginbulklen = 0;
  c->bulklen = -1;
  c->sentlen = 0;
  c->flags = 0;
  c->lastinteraction = time(NULL);
  c->authenticated = 0;
  c->replstate = SHM_REPL_NONE;
  c->reply = lCreate();
  lSetFreeMethod(c->reply, objDecRefCount);
  lSetDupMethod(c->reply, netDupReply);
  c->bpop.keys = NULL;
  c->bpop.count = 0;
  c->bpop.timeout = 0;
  c->bpop.target = NULL;
  c->io_keys = lCreate();
  c->watched_keys = lCreate();
  lSetFreeMethod(c->io_keys, objDecRefCount);
  c->msg_channels = hashCreate(&setHashType, NULL);
  c->msg_patterns = lCreate();
  lSetFreeMethod(c->msg_patterns, objDecRefCount);
  lSetMatchMethod(c->msg_patterns, listMatchObjects);
  lAddNodeTail(server.clients, c);
  initClientBeginState(c);
  return c;
}

int _netAddToBuffer(shmClient *c, char *s, size_t len) {
  size_t available = sizeof(c->buf) - c->bufpos;
  if (c->flags & SHM_CLOSE_AFTER_REPLY) return SHM_OK;
  if (lLength(c->reply) > 0) return SHM_ERR;
  if (len > available) return SHM_ERR;
  memcpy(c->buf + c->bufpos, s, len);
  c->bufpos += len;
  return SHM_OK;
}

void _netAddObjectToList(shmClient *c, robj *o) {
  robj *tail;
  if (c->flags & SHM_CLOSE_AFTER_REPLY) return;
  if (lLength(c->reply) == 0) {
    objIncRefCount(o);
    lAddNodeTail(c->reply, o);
  } else {
    tail = lNodeValue(lLast(c->reply));
    if (tail->ptr != NULL &&
        dstrlen(tail->ptr) + dstrlen(o->ptr) <= SHM_REPLY_CHUNK_BYTES) {
      tail = netDupLastObjectIfNeeded(c->reply);
      tail->ptr = dstrcatlen(tail->ptr, o->ptr, dstrlen(o->ptr));
    } else {
      objIncRefCount(o);
      lAddNodeTail(c->reply, o);
    }
  }
}

void _netAddDstRToList(shmClient *c, dstr s) {
  robj *tail;
  if (c->flags & SHM_CLOSE_AFTER_REPLY) {
    dstrfree(s);
    return;
  }
  if (lLength(c->reply) == 0) {
    lAddNodeTail(c->reply, objCreate(SHM_STRING, s));
  } else {
    tail = lNodeValue(lLast(c->reply));
    if (tail->ptr != NULL &&
        dstrlen(tail->ptr) + dstrlen(s) <= SHM_REPLY_CHUNK_BYTES) {
      tail = netDupLastObjectIfNeeded(c->reply);
      tail->ptr = dstrcatlen(tail->ptr, s, dstrlen(s));
      dstrfree(s);
    } else {
      lAddNodeTail(c->reply, objCreate(SHM_STRING, s));
    }
  }
}

void _netAddStringToList(shmClient *c, char *s, size_t len) {
  robj *tail;
  if (c->flags & SHM_CLOSE_AFTER_REPLY) return;
  if (lLength(c->reply) == 0) {
    lAddNodeTail(c->reply, objCreateString(s, len));
  } else {
    tail = lNodeValue(lLast(c->reply));
    if (tail->ptr != NULL && dstrlen(tail->ptr) + len <= SHM_REPLY_CHUNK_BYTES) {
      tail = netDupLastObjectIfNeeded(c->reply);
      tail->ptr = dstrcatlen(tail->ptr, s, len);
    } else {
      lAddNodeTail(c->reply, objCreateString(s, len));
    }
  }
}

void netAdd(shmClient *c, robj *obj) {
  if (_installWriteEvent(c) != SHM_OK) return;
  shmAssert(!server.vm_enabled || obj->storage == SHM_VM_MEMORY);
  if (obj->encoding == SHM_ENCODING_RAW) {
    if (_netAddToBuffer(c, obj->ptr, dstrlen(obj->ptr)) != SHM_OK)
      _netAddObjectToList(c, obj);
  } else {
    obj = objGetDecoded(obj);
    if (_netAddToBuffer(c, obj->ptr, dstrlen(obj->ptr)) != SHM_OK)
      _netAddObjectToList(c, obj);
    objDecRefCount(obj);
  }
}

void netAddDstR(shmClient *c, dstr s) {
  if (_installWriteEvent(c) != SHM_OK) {
    dstrfree(s);
    return;
  }
  if (_netAddToBuffer(c, s, dstrlen(s)) == SHM_OK) {
    dstrfree(s);
  } else {
    _netAddDstRToList(c, s);
  }
}

void netAddString(shmClient *c, char *s, size_t len) {
  if (_installWriteEvent(c) != SHM_OK) return;
  if (_netAddToBuffer(c, s, len) != SHM_OK) _netAddStringToList(c, s, len);
}

void _netAddError(shmClient *c, char *s, size_t len) {
  netAddString(c, "-ERR ", 5);
  netAddString(c, s, len);
  netAddString(c, "\r\n", 2);
}

void netAddError(shmClient *c, char *err) {
  _netAddError(c, err, strlen(err));
}

void netAddErrorFormat(shmClient *c, const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  dstr s = dstrcatvprintf(dstrempty(), fmt, ap);
  va_end(ap);
  _netAddError(c, s, dstrlen(s));
  dstrfree(s);
}

void _netAddStatus(shmClient *c, char *s, size_t len) {
  netAddString(c, "+", 1);
  netAddString(c, s, len);
  netAddString(c, "\r\n", 2);
}

void netAddStatus(shmClient *c, char *status) {
  _netAddStatus(c, status, strlen(status));
}

void netAddStatusFormat(shmClient *c, const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  dstr s = dstrcatvprintf(dstrempty(), fmt, ap);
  va_end(ap);
  _netAddStatus(c, s, dstrlen(s));
  dstrfree(s);
}

void *addDeferredBeginBulkLength(shmClient *c) {
  if (_installWriteEvent(c) != SHM_OK) return NULL;
  lAddNodeTail(c->reply, objCreate(SHM_STRING, NULL));
  return lLast(c->reply);
}

void setDeferredBeginBulkLength(shmClient *c, void *node, long length) {
  lNode *ln = (lNode *) node;
  robj *len, *next;
  if (node == NULL) return;
  len = lNodeValue(ln);
  len->ptr = dstrcatprintf(dstrempty(), "*%ld\r\n", length);
  if (ln->next != NULL) {
    next = lNodeValue(ln->next);
    if (next->ptr != NULL) {
      len->ptr = dstrcatlen(len->ptr, next->ptr, dstrlen(next->ptr));
      lDelNode(c->reply, ln->next);
    }
  }
}

void netAddDouble(shmClient *c, double d) {
  char dbuf[128], sbuf[128];
  int dlen, slen;
  dlen = snprintf(dbuf, sizeof(dbuf), "%.17g", d);
  slen = snprintf(sbuf, sizeof(sbuf), "$%d\r\n%s\r\n", dlen, dbuf);
  netAddString(c, sbuf, slen);
}

void _netAddLongLong(shmClient *c, long long ll, char prefix) {
  char buf[128];
  int len;
  buf[0] = prefix;
  len = longLongToStr(buf + 1, sizeof(buf) - 1, ll);
  buf[len + 1] = '\r';
  buf[len + 2] = '\n';
  netAddString(c, buf, len + 3);
}

void netAddLongLong(shmClient *c, long long ll) {
  _netAddLongLong(c, ll, ':');
}

void netAddBeginBulkLen(shmClient *c, long length) {
  _netAddLongLong(c, length, '*');
}

void netAddBulkLen(shmClient *c, robj *obj) {
  size_t len;
  if (obj->encoding == SHM_ENCODING_RAW) {
    len = dstrlen(obj->ptr);
  } else {
    long n = (long) obj->ptr;
    len = 1;
    if (n < 0) {
      len++;
      n = -n;
    }
    while ((n = n / 10) != 0) {
      len++;
    }
  }
  _netAddLongLong(c, len, '$');
}

void netAddBulk(shmClient *c, robj *obj) {
  netAddBulkLen(c, obj);
  netAdd(c, obj);
  netAdd(c, shared.crlf);
}

void netAddBulkCBuffer(shmClient *c, void *p, size_t len) {
  _netAddLongLong(c, len, '$');
  netAddString(c, p, len);
  netAdd(c, shared.crlf);
}

void netAddBulkCString(shmClient *c, char *s) {
  if (s == NULL) {
    netAdd(c, shared.nullbulk);
  } else {
    netAddBulkCBuffer(c, s, strlen(s));
  }
}

void netAddBulkLongLong(shmClient *c, long long ll) {
  char buf[64];
  int len;
  len = longLongToStr(buf, 64, ll);
  netAddBulkCBuffer(c, buf, len);
}

static void netAcceptCommonHandler(int fd) {
  shmClient *c;
  if ((c = netCreateClient(fd)) == NULL) {
    shmLog(SHM_WARNING, "Error allocating resoures for the client");
    close(fd);
    return;
  }
  if (server.maxclients && lLength(server.clients) > server.maxclients) {
    char *err = "-ERR max number of clients reached\r\n";
    if (write(c->fd, err, strlen(err)) == -1) {
    }
    netFreeClient(c);
    return;
  }
  server.stat_numconnections++;
}

void netAcceptTcpHandler(elLoop *el, int fd, void *privdata, int mask) {
  int cport, cfd;
  char cip[128];
  SHM_NOTUSED(el);
  SHM_NOTUSED(mask);
  SHM_NOTUSED(privdata);
  cfd = snetTcpAccept(server.neterr, fd, cip, &cport);
  if (cfd == EL_ERR) {
    shmLog(SHM_WARNING, "Accepting client connection: %s", server.neterr);
    return;
  }
  shmLog(SHM_VERBOSE, "Accepted %s:%d", cip, cport);
  netAcceptCommonHandler(cfd);
}

void netAcceptUnixHandler(elLoop *el, int fd, void *privdata, int mask) {
  int cfd;
  SHM_NOTUSED(el);
  SHM_NOTUSED(mask);
  SHM_NOTUSED(privdata);
  cfd = snetUnixAccept(server.neterr, fd);
  if (cfd == EL_ERR) {
    shmLog(SHM_WARNING, "Accepting client connection: %s", server.neterr);
    return;
  }
  shmLog(SHM_VERBOSE, "Accepted connection to %s", server.socketpath);
  netAcceptCommonHandler(cfd);
}

static void netFreeClientArgv(shmClient *c) {
  int j;
  for (j = 0; j < c->argc; j++) objDecRefCount(c->argv[j]);
  c->argc = 0;
  c->cmd = NULL;
}

void netFreeClient(shmClient *c) {
  lNode *ln;
  dstrfree(c->querybuf);
  c->querybuf = NULL;
  if (c->flags & SHM_BLOCKED) unblockClientWaitingData(c);
  unwatchAllKeys(c);
  lRelease(c->watched_keys);
  msgUnsubscribeAllChannels(c, 0);
  msgUnsubscribeAllPatterns(c, 0);
  hashRelease(c->msg_channels);
  lRelease(c->msg_patterns);
  elDeleteFileEvent(server.el, c->fd, EL_READABLE);
  elDeleteFileEvent(server.el, c->fd, EL_WRITABLE);
  lRelease(c->reply);
  netFreeClientArgv(c);
  close(c->fd);
  ln = lSearchKey(server.clients, c);
  shmAssert(ln != NULL);
  lDelNode(server.clients, ln);
  if (c->flags & SHM_UNBLOCKED) {
    ln = lSearchKey(server.unblocked_clients, c);
    shmAssert(ln != NULL);
    lDelNode(server.unblocked_clients, ln);
  }
  if (c->flags & SHM_IO_WAIT) {
    shmAssert(server.vm_enabled);
    if (lLength(c->io_keys) == 0) {
      ln = lSearchKey(server.io_ready_clients, c);
      shmAssert(ln != NULL);
      lDelNode(server.io_ready_clients, ln);
    } else {
      while (lLength(c->io_keys)) {
        ln = lFirst(c->io_keys);
        vmDontWaitForSwappedKey(c, ln->value);
      }
    }
    server.vm_blocked_clients--;
  }
  lRelease(c->io_keys);
  if (c->flags & SHM_SLAVE) {
    if (c->replstate == SHM_REPL_SEND_BULK && c->repldbfd != -1)
      close(c->repldbfd);
    list *l = (c->flags & SHM_MONITOR) ? server.monitors : server.slaves;
    ln = lSearchKey(l, c);
    shmAssert(ln != NULL);
    lDelNode(l, ln);
  }
  if (c->flags & SHM_MASTER) {
    server.master = NULL;
    server.replstate = SHM_REPL_CONNECT;
    if (server.masterhost != NULL) {
      while (lLength(server.slaves)) {
        ln = lFirst(server.slaves);
        netFreeClient((shmClient *) ln->value);
      }
    }
  }
  dbfree(c->argv);
  netFreeClientBeginState(c);
  dbfree(c);
}

void netSendReply(elLoop *el, int fd, void *privdata, int mask) {
  shmClient *c = privdata;
  int nwritten = 0, totwritten = 0, objlen;
  robj *o;
  SHM_NOTUSED(el);
  SHM_NOTUSED(mask);
  while (c->bufpos > 0 || lLength(c->reply)) {
    if (c->bufpos > 0) {
      if (c->flags & SHM_MASTER) {
        nwritten = c->bufpos - c->sentlen;
      } else {
        nwritten = write(fd, c->buf + c->sentlen, c->bufpos - c->sentlen);
        if (nwritten <= 0) break;
      }
      c->sentlen += nwritten;
      totwritten += nwritten;
      if (c->sentlen == c->bufpos) {
        c->bufpos = 0;
        c->sentlen = 0;
      }
    } else {
      o = lNodeValue(lFirst(c->reply));
      objlen = dstrlen(o->ptr);
      if (objlen == 0) {
        lDelNode(c->reply, lFirst(c->reply));
        continue;
      }
      if (c->flags & SHM_MASTER) {
        nwritten = objlen - c->sentlen;
      } else {
        nwritten =
            write(fd, ((char *) o->ptr) + c->sentlen, objlen - c->sentlen);
        if (nwritten <= 0) break;
      }
      c->sentlen += nwritten;
      totwritten += nwritten;
      if (c->sentlen == objlen) {
        lDelNode(c->reply, lFirst(c->reply));
        c->sentlen = 0;
      }
    }
    if (totwritten > SHM_MAX_WRITE_PER_EVENT) break;
  }
  if (nwritten == -1) {
    if (errno == EAGAIN) {
      nwritten = 0;
    } else {
      shmLog(SHM_VERBOSE, "Error writing to client: %s", strerror(errno));
      netFreeClient(c);
      return;
    }
  }
  if (totwritten > 0) c->lastinteraction = time(NULL);
  if (c->bufpos == 0 && lLength(c->reply) == 0) {
    c->sentlen = 0;
    elDeleteFileEvent(server.el, c->fd, EL_WRITABLE);
    if (c->flags & SHM_CLOSE_AFTER_REPLY) netFreeClient(c);
  }
}

void netResetClient(shmClient *c) {
  netFreeClientArgv(c);
  c->reqtype = 0;
  c->beginbulklen = 0;
  c->bulklen = -1;
}

void netCloseTimedoutClients(void) {
  shmClient *c;
  lNode *ln;
  time_t now = time(NULL);
  lIter li;
  lRewind(server.clients, &li);
  while ((ln = lNext(&li)) != NULL) {
    c = lNodeValue(ln);
    if (server.maxidletime && !(c->flags & SHM_SLAVE) &&
        !(c->flags & SHM_MASTER) && !(c->flags & SHM_BLOCKED) &&
        hashSize(c->msg_channels) == 0 &&
        lLength(c->msg_patterns) == 0 &&
        (now - c->lastinteraction > server.maxidletime)) {
      shmLog(SHM_VERBOSE, "Closing idle client");
      netFreeClient(c);
    } else if (c->flags & SHM_BLOCKED) {
      if (c->bpop.timeout != 0 && c->bpop.timeout < now) {
        netAdd(c, shared.nullbeginbulk);
        unblockClientWaitingData(c);
      }
    }
  }
}

int netProcessInlineBuffer(shmClient *c) {
  char *newline = strstr(c->querybuf, "\r\n");
  int argc, j;
  dstr *argv;
  size_t querylen;
  if (newline == NULL) return SHM_ERR;
  querylen = newline - (c->querybuf);
  argv = dstrsplitlen(c->querybuf, querylen, " ", 1, &argc);
  c->querybuf = dstrrange(c->querybuf, querylen + 2, -1);
  if (c->argv) dbfree(c->argv);
  c->argv = dbmalloc(sizeof(robj *) * argc);
  for (c->argc = 0, j = 0; j < argc; j++) {
    if (dstrlen(argv[j])) {
      c->argv[c->argc] = objCreate(SHM_STRING, argv[j]);
      c->argc++;
    } else {
      dstrfree(argv[j]);
    }
  }
  dbfree(argv);
  return SHM_OK;
}

static void netSetProtocolError(shmClient *c, int pos) {
  c->flags |= SHM_CLOSE_AFTER_REPLY;
  c->querybuf = dstrrange(c->querybuf, pos, -1);
}

int netProcessBeginbulkBuffer(shmClient *c) {
  char *newline = NULL;
  char *eptr;
  int pos = 0, tolerr;
  long bulklen;
  if (c->beginbulklen == 0) {
    shmAssert(c->argc == 0);
    newline = strstr(c->querybuf, "\r\n");
    if (newline == NULL) return SHM_ERR;
    shmAssert(c->querybuf[0] == '*');
    c->beginbulklen = strtol(c->querybuf + 1, &eptr, 10);
    pos = (newline - c->querybuf) + 2;
    if (c->beginbulklen <= 0) {
      c->querybuf = dstrrange(c->querybuf, pos, -1);
      return SHM_OK;
    } else if (c->beginbulklen > 1024 * 1024) {
      netAddError(c, "Protocol error: invalid beginbulk length");
      netSetProtocolError(c, pos);
      return SHM_ERR;
    }
    if (c->argv) dbfree(c->argv);
    c->argv = dbmalloc(sizeof(robj *) * c->beginbulklen);
    newline = strstr(c->querybuf + pos, "\r\n");
  }
  shmAssert(c->beginbulklen > 0);
  while (c->beginbulklen) {
    if (c->bulklen == -1) {
      newline = strstr(c->querybuf + pos, "\r\n");
      if (newline != NULL) {
        if (c->querybuf[pos] != '$') {
          netAddErrorFormat(c, "Protocol error: expected '$', got '%c'",
                            c->querybuf[pos]);
          netSetProtocolError(c, pos);
          return SHM_ERR;
        }
        bulklen = strtol(c->querybuf + pos + 1, &eptr, 10);
        tolerr = (eptr[0] != '\r');
        if (tolerr || bulklen == LONG_MIN || bulklen == LONG_MAX ||
            bulklen < 0 || bulklen > 512 * 1024 * 1024) {
          netAddError(c, "Protocol error: invalid bulk length");
          netSetProtocolError(c, pos);
          return SHM_ERR;
        }
        pos += eptr - (c->querybuf + pos) + 2;
        c->bulklen = bulklen;
      } else {
        break;
      }
    }
    if (dstrlen(c->querybuf) - pos < (unsigned) (c->bulklen + 2)) {
      break;
    } else {
      c->argv[c->argc++] = objCreateString(c->querybuf + pos, c->bulklen);
      pos += c->bulklen + 2;
      c->bulklen = -1;
      c->beginbulklen--;
    }
  }
  c->querybuf = dstrrange(c->querybuf, pos, -1);
  if (c->beginbulklen == 0) {
    return SHM_OK;
  }
  return SHM_ERR;
}

void netProcessInputBuffer(shmClient *c) {
  while (dstrlen(c->querybuf)) {
    if (c->flags & SHM_BLOCKED || c->flags & SHM_IO_WAIT) return;
    if (c->flags & SHM_CLOSE_AFTER_REPLY) return;
    if (!c->reqtype) {
      if (c->querybuf[0] == '*') {
        c->reqtype = SHM_REQ_BEGINBULK;
      } else {
        c->reqtype = SHM_REQ_INLINE;
      }
    }
    if (c->reqtype == SHM_REQ_INLINE) {
      if (netProcessInlineBuffer(c) != SHM_OK) break;
    } else if (c->reqtype == SHM_REQ_BEGINBULK) {
      if (netProcessBeginbulkBuffer(c) != SHM_OK) break;
    } else {
      shmPanic("Unknown request type");
    }
    if (c->argc == 0) {
      netResetClient(c);
    } else {
      if (processCmd(c) == SHM_OK) netResetClient(c);
    }
  }
}

void netRewriteClientCommandVector(shmClient *c, int argc, ...) {
  va_list ap;
  int j;
  robj **argv;
  argv = dbmalloc(sizeof(robj *) * argc);
  va_start(ap, argc);
  for (j = 0; j < argc; j++) {
    robj *a;
    a = va_arg(ap, robj * );
    argv[j] = a;
    objIncRefCount(a);
  }
  for (j = 0; j < c->argc; j++) objDecRefCount(c->argv[j]);
  dbfree(c->argv);
  c->argv = argv;
  c->argc = argc;
  va_end(ap);
}

void netReadQueryFromClient(elLoop *el, int fd, void *privdata, int mask) {
  shmClient *c = (shmClient *) privdata;
  char buf[SHM_IOBUF_LEN];
  int nread;
  SHM_NOTUSED(el);
  SHM_NOTUSED(mask);
  nread = read(fd, buf, SHM_IOBUF_LEN);
  if (nread == -1) {
    if (errno == EAGAIN) {
      nread = 0;
    } else {
      shmLog(SHM_VERBOSE, "Reading from client: %s", strerror(errno));
      netFreeClient(c);
      return;
    }
  } else if (nread == 0) {
    shmLog(SHM_VERBOSE, "Client closed connection");
    netFreeClient(c);
    return;
  }
  if (nread) {
    c->querybuf = dstrcatlen(c->querybuf, buf, nread);
    c->lastinteraction = time(NULL);
  } else {
    return;
  }
  netProcessInputBuffer(c);
}

void netGetClientsMaxBuffers(unsigned long *longest_outpulisttype,
                          unsigned long *biggest_input_buffer) {
  shmClient *c;
  lNode *ln;
  lIter li;
  unsigned long lol = 0, bib = 0;
  lRewind(server.clients, &li);
  while ((ln = lNext(&li)) != NULL) {
    c = lNodeValue(ln);
    if (lLength(c->reply) > lol) lol = lLength(c->reply);
    if (dstrlen(c->querybuf) > bib) bib = dstrlen(c->querybuf);
  }
  *longest_outpulisttype = lol;
  *biggest_input_buffer = bib;
}

