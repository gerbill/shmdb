#include "shm.h"

void initClientBeginState(shmClient *c) {
				c->beginstate.commands = NULL;
				c->beginstate.count = 0;
}

void netFreeClientBeginState(shmClient *c) {
				int j;
				for (j = 0; j < c->beginstate.count; j++) {
								int i;
								cmdBegin *mc = c->beginstate.commands + j;
								for (i = 0; i < mc->argc; i++) objDecRefCount(mc->argv[i]);
								dbfree(mc->argv);
				}
				dbfree(c->beginstate.commands);
}

void queueBeginCmd(shmClient *c) {
				cmdBegin *mc;
				int j;
				c->beginstate.commands =
								dbrealloc(c->beginstate.commands, sizeof(cmdBegin) * (c->beginstate.count + 1));
				mc = c->beginstate.commands + c->beginstate.count;
				mc->cmd = c->cmd;
				mc->argc = c->argc;
				mc->argv = dbmalloc(sizeof(robj *) * c->argc);
				memcpy(mc->argv, c->argv, sizeof(robj *) * c->argc);
				for (j = 0; j < c->argc; j++) objIncRefCount(mc->argv[j]);
				c->beginstate.count++;
}

void beginCmd(shmClient *c) {
				if (c->flags & SHM_BEGIN) {
								netAddError(c, "BEGIN calls can not be nested");
								return;
				}
				c->flags |= SHM_BEGIN;
				netAdd(c, shared.ok);
}

void rollbackCmd(shmClient *c) {
				if (!(c->flags & SHM_BEGIN)) {
								netAddError(c, "ROLLBACK without BEGIN");
								return;
				}
				netFreeClientBeginState(c);
				initClientBeginState(c);
				c->flags &= (~SHM_BEGIN);
				unwatchAllKeys(c);
				netAdd(c, shared.ok);
}

void commitCommandReplicateBegin(shmClient *c) {
				robj *beginstring = objCreateString("BEGIN", 5);
				if (server.appendonly)
          appendOnlyFeed(server.beginCmd, c->db->id, &beginstring, 1);
				if (lLength(server.slaves))
								replFeedSlaves(server.slaves, c->db->id, &beginstring, 1);
				objDecRefCount(beginstring);
}

void commitCmd(shmClient *c) {
				int j;
				robj **orig_argv;
				int orig_argc;
				struct cmdShm *orig_cmd;
				if (!(c->flags & SHM_BEGIN)) {
								netAddError(c, "COMMIT without BEGIN");
								return;
				}
				if (c->flags & SHM_DIRTY_CAS) {
								netFreeClientBeginState(c);
								initClientBeginState(c);
								c->flags &= ~(SHM_BEGIN | SHM_DIRTY_CAS);
								unwatchAllKeys(c);
								netAdd(c, shared.nullbeginbulk);
								return;
				}
				commitCommandReplicateBegin(c);
				unwatchAllKeys(c);
				orig_argv = c->argv;
				orig_argc = c->argc;
				orig_cmd = c->cmd;
				netAddBeginBulkLen(c, c->beginstate.count);
				for (j = 0; j < c->beginstate.count; j++) {
								c->argc = c->beginstate.commands[j].argc;
								c->argv = c->beginstate.commands[j].argv;
								c->cmd = c->beginstate.commands[j].cmd;
								call(c);
								c->beginstate.commands[j].argc = c->argc;
								c->beginstate.commands[j].argv = c->argv;
								c->beginstate.commands[j].cmd = c->cmd;
				}
				c->argv = orig_argv;
				c->argc = orig_argc;
				c->cmd = orig_cmd;
				netFreeClientBeginState(c);
				initClientBeginState(c);
				c->flags &= ~(SHM_BEGIN | SHM_DIRTY_CAS);
				server.dirty++;
}
typedef struct watchedKey {
				robj *key;
				shmDb *db;
} watchedKey;

void watchForKey(shmClient *c, robj *key) {
				list *clients = NULL;
				lIter li;
				lNode *ln;
				watchedKey *wk;
	lRewind(c->watched_keys, &li);
				while ((ln = lNext(&li))) {
								wk = lNodeValue(ln);
								if (wk->db == c->db && objEqualString(key, wk->key)) return;
				}
				clients = hashFetchValue(c->db->watched_keys, key);
				if (!clients) {
								clients = lCreate();
								hashAdd(c->db->watched_keys, key, clients);
								objIncRefCount(key);
				}
	lAddNodeTail(clients, c);
				wk = dbmalloc(sizeof(*wk));
				wk->key = key;
				wk->db = c->db;
				objIncRefCount(key);
	lAddNodeTail(c->watched_keys, wk);
}

void unwatchAllKeys(shmClient *c) {
				lIter li;
				lNode *ln;
				if (lLength(c->watched_keys) == 0) return;
	lRewind(c->watched_keys, &li);
				while ((ln = lNext(&li))) {
								list *clients;
								watchedKey *wk;
								wk = lNodeValue(ln);
								clients = hashFetchValue(wk->db->watched_keys, wk->key);
								shmAssert(clients != NULL);
					lDelNode(clients, lSearchKey(clients, c));
								if (lLength(clients) == 0) hashDelete(wk->db->watched_keys, wk->key);
					lDelNode(c->watched_keys, ln);
								objDecRefCount(wk->key);
								dbfree(wk);
				}
}

void touchWatchedKey(shmDb *db, robj *key) {
				list *clients;
				lIter li;
				lNode *ln;
				if (hashSize(db->watched_keys) == 0) return;
				clients = hashFetchValue(db->watched_keys, key);
				if (!clients) return;
	lRewind(clients, &li);
				while ((ln = lNext(&li))) {
								shmClient *c = lNodeValue(ln);
								c->flags |= SHM_DIRTY_CAS;
				}
}

void touchWatchedKeysOnFlush(int dbid) {
				lIter li1, li2;
				lNode *ln;
	lRewind(server.clients, &li1);
				while ((ln = lNext(&li1))) {
								shmClient *c = lNodeValue(ln);
					lRewind(c->watched_keys, &li2);
								while ((ln = lNext(&li2))) {
												watchedKey *wk = lNodeValue(ln);
												if (dbid == -1 || wk->db->id == dbid) {
																if (hashFind(wk->db->hash, wk->key->ptr) != NULL)
																				c->flags |= SHM_DIRTY_CAS;
												}
								}
				}
}

void lookCmd(shmClient *c) {
				int j;
				if (c->flags & SHM_BEGIN) {
								netAddError(c, "LOOK inside BEGIN is not allowed");
								return;
				}
				for (j = 1; j < c->argc; j++) watchForKey(c, c->argv[j]);
				netAdd(c, shared.ok);
}

void unlookCmd(shmClient *c) {
				unwatchAllKeys(c);
				c->flags &= (~SHM_DIRTY_CAS);
				netAdd(c, shared.ok);
}
