#include "shm.h"
#include <math.h>

void hashTypeTryConversion(robj *subject, robj **argv, int start, int end) {
				int i;
				if (subject->encoding != SHM_ENCODING_ZIPMAP) return;
				for (i = start; i <= end; i++) {
								if (argv[i]->encoding == SHM_ENCODING_RAW &&
								    dstrlen(argv[i]->ptr) > server.hash_max_zipmap_value) {
												convertToRealHash(subject);
												return;
								}
				}
}

void hashTypeTryObjectEncoding(robj *subject, robj **o1, robj **o2) {
				if (subject->encoding == SHM_ENCODING_HT) {
								if (o1) *o1 = objTryEncoding(*o1);
								if (o2) *o2 = objTryEncoding(*o2);
				}
}

int hashTypeGet(robj *o, robj *key, robj **objval, unsigned char **v,
                unsigned int *vlen) {
				if (o->encoding == SHM_ENCODING_ZIPMAP) {
								int found;
								key = objGetDecoded(key);
								found = zipmapGet(o->ptr, key->ptr, dstrlen(key->ptr), v, vlen);
								objDecRefCount(key);
								if (!found) return -1;
				} else {
								hashEntry *de = hashFind(o->ptr, key);
								if (de == NULL) return -1;
								*objval = hashGetEntryVal(de);
				}
				return o->encoding;
}

robj *hashTypeGetObject(robj *o, robj *key) {
				robj *objval;
				unsigned char *v;
				unsigned int vlen;
				int encoding = hashTypeGet(o, key, &objval, &v, &vlen);
				switch (encoding) {
				case SHM_ENCODING_HT:
								objIncRefCount(objval);
								return objval;
				case SHM_ENCODING_ZIPMAP:
								objval = objCreateString((char *)v, vlen);
								return objval;
				default:
								return NULL;
				}
}

int hashTypeExists(robj *o, robj *key) {
				if (o->encoding == SHM_ENCODING_ZIPMAP) {
								key = objGetDecoded(key);
								if (zipmapExists(o->ptr, key->ptr, dstrlen(key->ptr))) {
												objDecRefCount(key);
												return 1;
								}
								objDecRefCount(key);
				} else {
								if (hashFind(o->ptr, key) != NULL) {
												return 1;
								}
				}
				return 0;
}

int hashTypeSet(robj *o, robj *key, robj *value) {
				int update = 0;
				if (o->encoding == SHM_ENCODING_ZIPMAP) {
								key = objGetDecoded(key);
								value = objGetDecoded(value);
								o->ptr = zipmapSet(o->ptr, key->ptr, dstrlen(key->ptr), value->ptr,
								                   dstrlen(value->ptr), &update);
								objDecRefCount(key);
								objDecRefCount(value);
								if (zipmapLen(o->ptr) > server.hash_max_zipmap_entries)
												convertToRealHash(o);
				} else {
								if (hashReplace(o->ptr, key, value)) {
												objIncRefCount(key);
								} else {
												update = 1;
								}
								objIncRefCount(value);
				}
				return update;
}

int hashTypeDelete(robj *o, robj *key) {
				int deleted = 0;
				if (o->encoding == SHM_ENCODING_ZIPMAP) {
								key = objGetDecoded(key);
								o->ptr = zipmapDel(o->ptr, key->ptr, dstrlen(key->ptr), &deleted);
								objDecRefCount(key);
				} else {
								deleted = hashDelete((hash *)o->ptr, key) == HASH_OK;
								if (deleted && htNeedsResize(o->ptr)) hashResize(o->ptr);
				}
				return deleted;
}

unsigned long hashTypeLength(robj *o) {
				return (o->encoding == SHM_ENCODING_ZIPMAP)
				       ? zipmapLen((unsigned char *)o->ptr)
				       : hashSize((hash *)o->ptr);
}

hashTypeIterator *hashTypeInitIterator(robj *subject) {
				hashTypeIterator *hi = dbmalloc(sizeof(hashTypeIterator));
				hi->encoding = subject->encoding;
				if (hi->encoding == SHM_ENCODING_ZIPMAP) {
								hi->zi = zipmapRewind(subject->ptr);
				} else if (hi->encoding == SHM_ENCODING_HT) {
								hi->di = hashGetIterator(subject->ptr);
				} else {
								shmAssert(NULL);
				}
				return hi;
}

void hashTypeReleaseIterator(hashTypeIterator *hi) {
				if (hi->encoding == SHM_ENCODING_HT) {
								hashReleaseIterator(hi->di);
				}
				dbfree(hi);
}

int hashTypeNext(hashTypeIterator *hi) {
				if (hi->encoding == SHM_ENCODING_ZIPMAP) {
								if ((hi->zi = zipmapNext(hi->zi, &hi->zk, &hi->zklen, &hi->zv,
								                         &hi->zvlen)) == NULL)
												return SHM_ERR;
				} else {
								if ((hi->de = hashNext(hi->di)) == NULL) return SHM_ERR;
				}
				return SHM_OK;
}

int hashTypeCurrent(hashTypeIterator *hi, int what, robj **objval,
                    unsigned char **v, unsigned int *vlen) {
				if (hi->encoding == SHM_ENCODING_ZIPMAP) {
								if (what & SHM_HASH_KEY) {
												*v = hi->zk;
												*vlen = hi->zklen;
								} else {
												*v = hi->zv;
												*vlen = hi->zvlen;
								}
				} else {
								if (what & SHM_HASH_KEY)
												*objval = hashGetEntryKey(hi->de);
								else
												*objval = hashGetEntryVal(hi->de);
				}
				return hi->encoding;
}

robj *hashTypeCurrentObject(hashTypeIterator *hi, int what) {
				robj *obj;
				unsigned char *v = NULL;
				unsigned int vlen = 0;
				int encoding = hashTypeCurrent(hi, what, &obj, &v, &vlen);
				if (encoding == SHM_ENCODING_HT) {
								objIncRefCount(obj);
								return obj;
				} else {
								return objCreateString((char *)v, vlen);
				}
}

robj *hashTypeLookupWriteOrCreate(shmClient *c, robj *key) {
				robj *o = lookupKeyWrite(c->db, key);
				if (o == NULL) {
								o = objCreateHash();
								dbAdd(c->db, key, o);
				} else {
								if (o->type != SHM_HASH) {
												netAdd(c, shared.wrongtypeerr);
												return NULL;
								}
				}
				return o;
}

void convertToRealHash(robj *o) {
				unsigned char *key, *val, *p, *zm = o->ptr;
				unsigned int klen, vlen;
				hash *hash = hashCreate(&hashHashType, NULL);
				shmAssert(o->type == SHM_HASH && o->encoding != SHM_ENCODING_HT);
				p = zipmapRewind(zm);
				while ((p = zipmapNext(p, &key, &klen, &val, &vlen)) != NULL) {
								robj *keyobj, *valobj;
								keyobj = objCreateString((char *)key, klen);
								valobj = objCreateString((char *)val, vlen);
								keyobj = objTryEncoding(keyobj);
								valobj = objTryEncoding(valobj);
								hashAdd(hash, keyobj, valobj);
				}
				o->encoding = SHM_ENCODING_HT;
				o->ptr = hash;
				dbfree(zm);
}

void hashsetCmd(shmClient *c) {
				int update;
				robj *o;
				if ((o = hashTypeLookupWriteOrCreate(c, c->argv[1])) == NULL) return;
				hashTypeTryConversion(o, c->argv, 2, 3);
				hashTypeTryObjectEncoding(o, &c->argv[2], &c->argv[3]);
				update = hashTypeSet(o, c->argv[2], c->argv[3]);
				netAdd(c, update ? shared.czero : shared.cone);
				touchWatchedKey(c->db, c->argv[1]);
				server.dirty++;
}

void hashsetneCmd(shmClient *c) {
				robj *o;
				if ((o = hashTypeLookupWriteOrCreate(c, c->argv[1])) == NULL) return;
				hashTypeTryConversion(o, c->argv, 2, 3);
				if (hashTypeExists(o, c->argv[2])) {
								netAdd(c, shared.czero);
				} else {
								hashTypeTryObjectEncoding(o, &c->argv[2], &c->argv[3]);
								hashTypeSet(o, c->argv[2], c->argv[3]);
								netAdd(c, shared.cone);
								touchWatchedKey(c->db, c->argv[1]);
								server.dirty++;
				}
}

void hmultisetCmd(shmClient *c) {
				int i;
				robj *o;
				if ((c->argc % 2) == 1) {
								netAddError(c, "wrong number of arguments for HMSET");
								return;
				}
				if ((o = hashTypeLookupWriteOrCreate(c, c->argv[1])) == NULL) return;
				hashTypeTryConversion(o, c->argv, 2, c->argc - 1);
				for (i = 2; i < c->argc; i += 2) {
								hashTypeTryObjectEncoding(o, &c->argv[i], &c->argv[i + 1]);
								hashTypeSet(o, c->argv[i], c->argv[i + 1]);
				}
				netAdd(c, shared.ok);
				touchWatchedKey(c->db, c->argv[1]);
				server.dirty++;
}

void hashraisebyCmd(shmClient *c) {
				long long value, incr;
				robj *o, *current, *new;
				if (objGetLongLongOrReply(c, c->argv[3], &incr, NULL) != SHM_OK)
								return;
				if ((o = hashTypeLookupWriteOrCreate(c, c->argv[1])) == NULL) return;
				if ((current = hashTypeGetObject(o, c->argv[2])) != NULL) {
								if (objGetLongLongOrReply(
														c, current, &value, "hash value is not an integer") != SHM_OK) {
												objDecRefCount(current);
												return;
								}
								objDecRefCount(current);
				} else {
								value = 0;
				}
				value += incr;
				new = objCreateStringFromLongLong(value);
				hashTypeTryObjectEncoding(o, &c->argv[2], NULL);
				hashTypeSet(o, c->argv[2], new);
				objDecRefCount(new);
				netAddLongLong(c, value);
				touchWatchedKey(c->db, c->argv[1]);
				server.dirty++;
}

void hashgetCmd(shmClient *c) {
				robj *o, *value;
				unsigned char *v;
				unsigned int vlen;
				int encoding;
				if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.nullbulk)) == NULL ||
				    objCheckType(c, o, SHM_HASH))
								return;
				if ((encoding = hashTypeGet(o, c->argv[2], &value, &v, &vlen)) != -1) {
								if (encoding == SHM_ENCODING_HT)
												netAddBulk(c, value);
								else
												netAddBulkCBuffer(c, v, vlen);
				} else {
								netAdd(c, shared.nullbulk);
				}
}

void hmultigetCmd(shmClient *c) {
				int i, encoding;
				robj *o, *value;
				unsigned char *v;
				unsigned int vlen;
				o = lookupKeyRead(c->db, c->argv[1]);
				if (o != NULL && o->type != SHM_HASH) {
								netAdd(c, shared.wrongtypeerr);
								return;
				}
				netAddBeginBulkLen(c, c->argc - 2);
				for (i = 2; i < c->argc; i++) {
								if (o != NULL &&
								    (encoding = hashTypeGet(o, c->argv[i], &value, &v, &vlen)) != -1) {
												if (encoding == SHM_ENCODING_HT)
																netAddBulk(c, value);
												else
																netAddBulkCBuffer(c, v, vlen);
								} else {
												netAdd(c, shared.nullbulk);
								}
				}
}

void hashdelCmd(shmClient *c) {
				robj *o;
				if ((o = lookupKeyWriteOrReply(c, c->argv[1], shared.czero)) == NULL ||
				    objCheckType(c, o, SHM_HASH))
								return;
				if (hashTypeDelete(o, c->argv[2])) {
								if (hashTypeLength(o) == 0) dbDelete(c->db, c->argv[1]);
								netAdd(c, shared.cone);
								touchWatchedKey(c->db, c->argv[1]);
								server.dirty++;
				} else {
								netAdd(c, shared.czero);
				}
}

void hashlenCmd(shmClient *c) {
				robj *o;
				if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.czero)) == NULL ||
				    objCheckType(c, o, SHM_HASH))
								return;
				netAddLongLong(c, hashTypeLength(o));
}

void genericHgetallCmd(shmClient *c, int flags) {
				robj *o;
				unsigned long count = 0;
				hashTypeIterator *hi;
				void *replylen = NULL;
				if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.emptybeginbulk)) ==
				    NULL ||
				    objCheckType(c, o, SHM_HASH))
								return;
				replylen = addDeferredBeginBulkLength(c);
				hi = hashTypeInitIterator(o);
				while (hashTypeNext(hi) != SHM_ERR) {
								robj *obj;
								unsigned char *v = NULL;
								unsigned int vlen = 0;
								int encoding;
								if (flags & SHM_HASH_KEY) {
												encoding = hashTypeCurrent(hi, SHM_HASH_KEY, &obj, &v, &vlen);
												if (encoding == SHM_ENCODING_HT)
																netAddBulk(c, obj);
												else
																netAddBulkCBuffer(c, v, vlen);
												count++;
								}
								if (flags & SHM_HASH_VALUE) {
												encoding = hashTypeCurrent(hi, SHM_HASH_VALUE, &obj, &v, &vlen);
												if (encoding == SHM_ENCODING_HT)
																netAddBulk(c, obj);
												else
																netAddBulkCBuffer(c, v, vlen);
												count++;
								}
				}
				hashTypeReleaseIterator(hi);
				setDeferredBeginBulkLength(c, replylen, count);
}

void hashallkeysCmd(shmClient *c) {
				genericHgetallCmd(c, SHM_HASH_KEY);
}

void hashvalsCmd(shmClient *c) {
				genericHgetallCmd(c, SHM_HASH_VALUE);
}

void hashgetallCmd(shmClient *c) {
				genericHgetallCmd(c, SHM_HASH_KEY | SHM_HASH_VALUE);
}

void hashexistsCmd(shmClient *c) {
				robj *o;
				if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.czero)) == NULL ||
				    objCheckType(c, o, SHM_HASH))
								return;
				netAdd(c, hashTypeExists(o, c->argv[2]) ? shared.cone : shared.czero);
}
