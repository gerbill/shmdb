#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>
typedef struct elApiState {
				int kqfd;
				struct kevent events[EL_SETSIZE];
} elApiState;

static int elApiCreate(elLoop *eventLoop) {
				elApiState *state = dbmalloc(sizeof(elApiState));
				if (!state) return -1;
				state->kqfd = kqueue();
				if (state->kqfd == -1) return -1;
				eventLoop->apidata = state;
				return 0;
}

static void elApiFree(elLoop *eventLoop) {
				elApiState *state = eventLoop->apidata;
				close(state->kqfd);
				dbfree(state);
}

static int elApiAddEvent(elLoop *eventLoop, int fd, int mask) {
				elApiState *state = eventLoop->apidata;
				struct kevent ke;
				if (mask & EL_READABLE) {
								EV_SET(&ke, fd, EVFILT_READ, EV_ADD, 0, 0, NULL);
								if (kevent(state->kqfd, &ke, 1, NULL, 0, NULL) == -1) return -1;
				}
				if (mask & EL_WRITABLE) {
								EV_SET(&ke, fd, EVFILT_WRITE, EV_ADD, 0, 0, NULL);
								if (kevent(state->kqfd, &ke, 1, NULL, 0, NULL) == -1) return -1;
				}
				return 0;
}

static void elApiDelEvent(elLoop *eventLoop, int fd, int mask) {
				elApiState *state = eventLoop->apidata;
				struct kevent ke;
				if (mask & EL_READABLE) {
								EV_SET(&ke, fd, EVFILT_READ, EV_DELETE, 0, 0, NULL);
								kevent(state->kqfd, &ke, 1, NULL, 0, NULL);
				}
				if (mask & EL_WRITABLE) {
								EV_SET(&ke, fd, EVFILT_WRITE, EV_DELETE, 0, 0, NULL);
								kevent(state->kqfd, &ke, 1, NULL, 0, NULL);
				}
}

static int elApiPoll(elLoop *eventLoop, struct timeval *tvp) {
				elApiState *state = eventLoop->apidata;
				int retval, numevents = 0;
				if (tvp != NULL) {
								struct timespec timeout;
								timeout.tv_sec = tvp->tv_sec;
								timeout.tv_nsec = tvp->tv_usec * 1000;
								retval = kevent(state->kqfd, NULL, 0, state->events, EL_SETSIZE, &timeout);
				} else {
								retval = kevent(state->kqfd, NULL, 0, state->events, EL_SETSIZE, NULL);
				}
				if (retval > 0) {
								int j;
								numevents = retval;
								for (j = 0; j < numevents; j++) {
												int mask = 0;
												struct kevent *e = state->events + j;
												if (e->filter == EVFILT_READ) mask |= EL_READABLE;
												if (e->filter == EVFILT_WRITE) mask |= EL_WRITABLE;
												eventLoop->fired[j].fd = e->ident;
												eventLoop->fired[j].mask = mask;
								}
				}
				return numevents;
}

static char *elApiName(void) {
				return "kqueue";
}
