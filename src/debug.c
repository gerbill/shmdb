#include "shm.h"
#include "sha1.h"
#include <arpa/inet.h>

void xorDigest(unsigned char *digest, void *ptr, size_t len) {
				SHA1_CTX ctx;
				unsigned char hash[20], *s = ptr;
				int j;
				SHA1Init(&ctx);
				SHA1Update(&ctx, s, len);
				SHA1Final(hash, &ctx);
				for (j = 0; j < 20; j++) digest[j] ^= hash[j];
}

void xorObjectDigest(unsigned char *digest, robj *o) {
				o = objGetDecoded(o);
				xorDigest(digest, o->ptr, dstrlen(o->ptr));
				objDecRefCount(o);
}

void mixDigest(unsigned char *digest, void *ptr, size_t len) {
				SHA1_CTX ctx;
				char *s = ptr;
				xorDigest(digest, s, len);
				SHA1Init(&ctx);
				SHA1Update(&ctx, digest, 20);
				SHA1Final(digest, &ctx);
}

void mixObjectDigest(unsigned char *digest, robj *o) {
				o = objGetDecoded(o);
				mixDigest(digest, o->ptr, dstrlen(o->ptr));
				objDecRefCount(o);
}

void computeDatasetDigest(unsigned char *final) {
				unsigned char digest[20];
				char buf[128];
				hashIterator *di = NULL;
				hashEntry *de;
				int j;
				uint32_t aux;
				memset(final, 0, 20);
				for (j = 0; j < server.dbnum; j++) {
								shmDb *db = server.db + j;
								if (hashSize(db->hash) == 0) continue;
								di = hashGetSafeIterator(db->hash);
								aux = htonl(j);
								mixDigest(final, &aux, sizeof(aux));
								while ((de = hashNext(di)) != NULL) {
												dstr key;
												robj *keyobj, *o;
												time_t expiretime;
												memset(digest, 0, 20);
												key = hashGetEntryKey(de);
												keyobj = objCreateString(key, dstrlen(key));
												mixDigest(digest, key, dstrlen(key));
												o = lookupKeyRead(db, keyobj);
												if (o == NULL) {
																objDecRefCount(keyobj);
																continue;
												}
												aux = htonl(o->type);
												mixDigest(digest, &aux, sizeof(aux));
												expiretime = getExpire(db, keyobj);
												if (o->type == SHM_STRING) {
																mixObjectDigest(digest, o);
												} else if (o->type == SHM_LIST) {
																listTypeIterator *li = listTypeInitIterator(o, 0, SHM_TAIL);
																listTypeEntry entry;
																while (listTypeNext(li, &entry)) {
																				robj *eleobj = listTypeGet(&entry);
																				mixObjectDigest(digest, eleobj);
																				objDecRefCount(eleobj);
																}
																listTypeReleaseIterator(li);
												} else if (o->type == SHM_SET) {
																setTypeIterator *si = setTypeInitIterator(o);
																robj *ele;
																while ((ele = setTypeNextObject(si)) != NULL) {
																				xorObjectDigest(digest, ele);
																				objDecRefCount(ele);
																}
																setTypeReleaseIterator(si);
												} else if (o->type == SHM_SORTSET) {
																sortset *zs = o->ptr;
																hashIterator *di = hashGetIterator(zs->hash);
																hashEntry *de;
																while ((de = hashNext(di)) != NULL) {
																				robj *eleobj = hashGetEntryKey(de);
																				double *score = hashGetEntryVal(de);
																				unsigned char eledigest[20];
																				snprintf(buf, sizeof(buf), "%.17g", *score);
																				memset(eledigest, 0, 20);
																				mixObjectDigest(eledigest, eleobj);
																				mixDigest(eledigest, buf, strlen(buf));
																				xorDigest(digest, eledigest, 20);
																}
																hashReleaseIterator(di);
												} else if (o->type == SHM_HASH) {
																hashTypeIterator *hi;
																robj *obj;
																hi = hashTypeInitIterator(o);
																while (hashTypeNext(hi) != SHM_ERR) {
																				unsigned char eledigest[20];
																				memset(eledigest, 0, 20);
																				obj = hashTypeCurrentObject(hi, SHM_HASH_KEY);
																				mixObjectDigest(eledigest, obj);
																				objDecRefCount(obj);
																				obj = hashTypeCurrentObject(hi, SHM_HASH_VALUE);
																				mixObjectDigest(eledigest, obj);
																				objDecRefCount(obj);
																				xorDigest(digest, eledigest, 20);
																}
																hashTypeReleaseIterator(hi);
												} else {
																shmPanic("Unknown object type");
												}
												if (expiretime != -1) xorDigest(digest, "!!expire!!", 10);
												xorDigest(final, digest, 20);
												objDecRefCount(keyobj);
								}
								hashReleaseIterator(di);
				}
}

void debugCmd(shmClient *c) {
				if (!strcasecmp(c->argv[1]->ptr, "segfault")) {
								*((char *)-1) = 'x';
				} else if (!strcasecmp(c->argv[1]->ptr, "reload")) {
								if (sdbSave(server.dbfilename) != SHM_OK) {
												netAdd(c, shared.err);
												return;
								}
								emptyDb();
								if (sdbLoad(server.dbfilename) != SHM_OK) {
												netAdd(c, shared.err);
												return;
								}
								shmLog(SHM_WARNING, "DB reloaded by DEBUG RELOAD");
								netAdd(c, shared.ok);
				} else if (!strcasecmp(c->argv[1]->ptr, "loadaof")) {
								emptyDb();
								if (appendOnlyLoad(server.appendfilename) != SHM_OK) {
												netAdd(c, shared.err);
												return;
								}
								shmLog(SHM_WARNING, "Append Only File loaded by DEBUG LOADAOF");
								netAdd(c, shared.ok);
				} else if (!strcasecmp(c->argv[1]->ptr, "object") && c->argc == 3) {
								hashEntry *de = hashFind(c->db->hash, c->argv[2]->ptr);
								robj *val;
								if (!de) {
												netAdd(c, shared.nokeyerr);
												return;
								}
								val = hashGetEntryVal(de);
								if (!server.vm_enabled ||
								    (val->storage == SHM_VM_MEMORY || val->storage == SHM_VM_SWAPPING)) {
												char *strenc;
												strenc = objStrEncoding(val->encoding);
												netAddStatusFormat(c,
												                     "Value at:%p refcount:%d "
												                     "encoding:%s serializedlength:%lld "
												                     "lru:%d lru_seconds_idle:%lu",
												                     (void *)val, val->refcount, strenc,
												                     (long long)sdbSavedObjectLen(val), val->lru,
												                     objEstimateIdleTime(val));
								} else {
												vmptr *vp = (vmptr *)val;
												netAddStatusFormat(c,
												                     "Value swapped at: page %llu "
												                     "using %llu pages",
												                     (unsigned long long)vp->page,
												                     (unsigned long long)vp->usedpages);
								}
				} else if (!strcasecmp(c->argv[1]->ptr, "swapin") && c->argc == 3) {
								lookupKeyRead(c->db, c->argv[2]);
								netAdd(c, shared.ok);
				} else if (!strcasecmp(c->argv[1]->ptr, "swapout") && c->argc == 3) {
								hashEntry *de = hashFind(c->db->hash, c->argv[2]->ptr);
								robj *val;
								vmptr *vp;
								if (!server.vm_enabled) {
												netAddError(c, "Virtual Memory is disabled");
												return;
								}
								if (!de) {
												netAdd(c, shared.nokeyerr);
												return;
								}
								val = hashGetEntryVal(de);
								if (val->storage != SHM_VM_MEMORY) {
												netAddError(c, "This key is not in memory");
								} else if (val->refcount != 1) {
												netAddError(c, "Object is shared");
								} else if ((vp = vmSwapOutObjBlocking(val)) != NULL) {
												hashGetEntryVal(de) = vp;
												netAdd(c, shared.ok);
								} else {
												netAdd(c, shared.err);
								}
				} else if (!strcasecmp(c->argv[1]->ptr, "populate") && c->argc == 3) {
								long keys, j;
								robj *key, *val;
								char buf[128];
								if (objGetLongFromOrReply(c, c->argv[2], &keys, NULL) != SHM_OK) return;
								for (j = 0; j < keys; j++) {
												snprintf(buf, sizeof(buf), "key:%lu", j);
												key = objCreateString(buf, strlen(buf));
												if (lookupKeyRead(c->db, key) != NULL) {
																objDecRefCount(key);
																continue;
												}
												snprintf(buf, sizeof(buf), "value:%lu", j);
												val = objCreateString(buf, strlen(buf));
												dbAdd(c->db, key, val);
												objDecRefCount(key);
								}
								netAdd(c, shared.ok);
				} else if (!strcasecmp(c->argv[1]->ptr, "digest") && c->argc == 2) {
								unsigned char digest[20];
								dstr d = dstrempty();
								int j;
								computeDatasetDigest(digest);
								for (j = 0; j < 20; j++) d = dstrcatprintf(d, "%02x", digest[j]);
								netAddStatus(c, d);
								dstrfree(d);
				} else if (!strcasecmp(c->argv[1]->ptr, "sleep") && c->argc == 3) {
								double dtime = strtod(c->argv[2]->ptr, NULL);
								long long utime = dtime * 1000000;
								usleep(utime);
								netAdd(c, shared.ok);
				} else {
								netAddError(c,
								              "Syntax error, try DEBUG [SEGFAULT|OBJECT <key>|SWAPIN "
								              "<key>|SWAPOUT <key>|RELOAD]");
				}
}

void _shmAssert(char *estr, char *file, int line) {
				shmLog(SHM_WARNING, "=== ASSERTION FAILED ===");
				shmLog(SHM_WARNING, "==> %s:%d '%s' is not true", file, line, estr);
#ifdef HAVE_BACKTRACE
				shmLog(SHM_WARNING, "(forcing SIGSEGV in order to print the stack trace)");
				*((char *)-1) = 'x';
#endif
}

void _shmPanic(char *msg, char *file, int line) {
				shmLog(SHM_WARNING,
				       "!!! Software Failure. Press left mouse button to continue");
				shmLog(SHM_WARNING, "Guru Meditation: %s #%s:%d", msg, file, line);
#ifdef HAVE_BACKTRACE
				shmLog(SHM_WARNING, "(forcing SIGSEGV in order to print the stack trace)");
				*((char *)-1) = 'x';
#endif
}
