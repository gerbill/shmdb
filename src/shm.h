#pragma once

#include "fmacros.h"
#include "config.h"
#include "begin.h"

#if defined(__sun)
#include "solarisfixes.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <limits.h>
#include <unistd.h>
#include <errno.h>
#include <inttypes.h>
#include <pthread.h>
#include <syslog.h>
#include "el.h"
#include "dstr.h"
#include "hash.h"
#include "list.h"
#include "dbmalloc.h"
#include "snet.h"
#include "zipmap.h"
#include "sortlist.h"
#include "is.h"
#include "version.h"

#define APPENDFSYNC_ALWAYS 1
#define APPENDFSYNC_EVERYSEC 2
#define APPENDFSYNC_NO 0
#define SHM_BEGIN 8
#define SHM_BLOCKED 16
#define SHM_CLOSE_AFTER_REPLY 128
#define SHM_CMD_DENYNOMEM 4
#define SHM_CMD_FORCE_REPL 8
#define SHM_CONFIGLINE_MAX 1024
#define SHM_DEBUG 0
#define SHM_DEFAULT_DBNUM 16
#define SHM_DIRTY_CAS 64
#define SHM_ENCODING_HT 2
#define SHM_ENCODING_INT 1
#define SHM_ENCODING_IS 6
#define SHM_ENCODING_LINKEDLIST 4
#define SHM_ENCODING_RAW 0
#define SHM_ENCODING_SKIPLIST 7
#define SHM_ENCODING_SORTLIST 5
#define SHM_ENCODING_ZIPMAP 3
#define SHM_EOF 255
#define SHM_ERR -1
#define SHM_EXPIRELOOKUPS_PER_CRON 10
#define SHM_EXPIRETIME 253
#define SHM_HASH 4
#define SHM_HASH_MAX_ZIPMAP_ENTRIES 512
#define SHM_HASH_MAX_ZIPMAP_VALUE 64
#define SHM_HEAD 0
#define SHM_HT_MINFILL 10
#define SHM_IO_WAIT 32
#define SHM_IOBUF_LEN 1024
#define SHM_LIST 1
#define SHM_LIST_MAX_SORTLIST_ENTRIES 512
#define SHM_LIST_MAX_SORTLIST_VALUE 64
#define SHM_MASTER 2
#define SHM_MAX_COMPLETED_JOBS_PROCESSED 1
#define SHM_MAX_LOGMSG_LEN 1024
#define SHM_MAX_WRITE_PER_EVENT (1024 * 64)
#define SHM_MAXIDLETIME (60 * 5)
#define SHM_MAXMEMORY_ALLKEYS_LRU 3
#define SHM_MAXMEMORY_ALLKEYS_RANDOM 4
#define SHM_MAXMEMORY_NO_EVICTION 5
#define SHM_MAXMEMORY_VOLATILE_LRU 0
#define SHM_MAXMEMORY_VOLATILE_RANDOM 2
#define SHM_MAXMEMORY_VOLATILE_TTL 1
#define SHM_MONITOR 4
#define SHM_NOTICE 2
#define SHM_NOTUSED(V) ((void)V)
#define SHM_OK 0
#define SHM_OP_DIFF 1
#define SHM_OP_INTER 2
#define SHM_OP_UNION 0
#define SHM_REPL_CONNECT 1
#define SHM_REPL_CONNECTED 3
#define SHM_REPL_NONE 0
#define SHM_REPL_ONLINE 6
#define SHM_REPL_SEND_BULK 5
#define SHM_REPL_TRANSFER 2
#define SHM_REPL_WAIT_ASAVE_END 4
#define SHM_REPL_WAIT_ASAVE_START 3
#define SHM_REPLY_CHUNK_BYTES (5 * 1500)
#define SHM_REQ_INLINE 1
#define SHM_REQ_BEGINBULK 2
#define SHM_RPORT 20971
#define SHM_SDB_14BITLEN 1
#define SHM_SDB_32BITLEN 2
#define SHM_SDB_6BITLEN 0
#define SHM_SDB_ENC_INT16 1
#define SHM_SDB_ENC_INT32 2
#define SHM_SDB_ENC_INT8 0
#define SHM_SDB_ENC_LZF 3
#define SHM_SDB_ENCVAL 3
#define SHM_SDB_LENERR UINT_MAX
#define SHM_SELECTDB 254
#define SHM_SET 2
#define SHM_SET_MAX_IS_ENTRIES 512
#define SHM_SHARED_INTEGERS 10000
#define SHM_SLAVE 1
#define SHM_SLOG_LOG_SLOWER_THAN 10000
#define SHM_SLOG_MAX_LEN 64
#define SHM_SORT_GET 0
#define SHM_SORTKEY_MAX 1024
#define SHM_STATIC_ARGS 8
#define SHM_STRING 0
#define SHM_TAIL 1
#define SHM_THREAD_STACK_SIZE (1024 * 1024 * 4)
#define SHM_UNBLOCKED 256
#define SHM_VERBOSE 1
#define SHM_VM_LOADING 3
#define SHM_VM_MAX_NEAR_PAGES 65536
#define SHM_VM_MAX_RANDOM_JUMP 4096
#define SHM_VM_MEMORY 0
#define SHM_VM_SWAPPED 1
#define SHM_VM_SWAPPING 2
#define SHM_VMPTR 8
#define SHM_WARNING 3
#define SHM_SORTSET 3
#define SORTSKIPLIST_MAXLEVEL 32
#define SORTSKIPLIST_P 0.25

#define shmAssert(_e) \
        ((_e) ? (void)0 : (_shmAssert(#_e, __FILE__, __LINE__), _exit(1)))
#define shmPanic(_e) _shmPanic(#_e, __FILE__, __LINE__), _exit(1)

void _shmAssert(char *estr, char *file, int line);

void _shmPanic(char *msg, char *file, int line);

#define SHM_LRU_CLOCK_MAX ((1 << 21) - 1)
#define SHM_LRU_CLOCK_RESOLUTION 10

typedef struct vmPtr {
  unsigned type : 4;
  unsigned storage : 2;
  unsigned notused : 26;
  unsigned int vtype;
  off_t page;
  off_t usedpages;
} vmptr;

#define initStaticStringObject(_var, _ptr) \
        do {                                     \
                _var.refcount = 1;                     \
                _var.type = SHM_STRING;                \
                _var.encoding = SHM_ENCODING_RAW;      \
                _var.ptr = _ptr;                       \
                _var.storage = SHM_VM_MEMORY;          \
        } while (0);

typedef struct shmDb {
  hash *hash;
  hash *expires;
  hash *blocking_keys;
  hash *io_keys;
  hash *watched_keys;
  int id;
} shmDb;

typedef struct blockingState {
  robj **keys;
  int count;
  time_t timeout;
  robj *target;
} blockingState;

typedef struct shmClient {
  int fd;
  shmDb *db;
  int hashid;
  dstr querybuf;
  int argc;
  robj **argv;
  struct cmdShm *cmd;
  int reqtype;
  int beginbulklen;
  long bulklen;
  list *reply;
  int sentlen;
  time_t lastinteraction;
  int flags;
  int slaveseldb;
  int authenticated;
  int replstate;
  int repldbfd;
  long repldboff;
  off_t repldbsize;
  beginState beginstate;
  blockingState bpop;
  list *io_keys;
  list *watched_keys;
  hash *msg_channels;
  list *msg_patterns;
  int bufpos;
  char buf[SHM_REPLY_CHUNK_BYTES];
} shmClient;

struct saveparam {
  time_t seconds;
  int changes;
};

struct soStruct {
  robj *crlf, *ok, *err, *emptybulk, *czero, *cone, *cnegone, *world, *space,
      *colon, *nullbulk, *nullbeginbulk, *queued, *emptybeginbulk,
      *wrongtypeerr, *nokeyerr, *syntaxerr, *sameobjecterr, *outofrangeerr,
      *loadingerr, *plus, *select0, *select1, *select2, *select3, *select4,
      *select5, *select6, *select7, *select8, *select9, *messagebulk,
      *pmessagebulk, *subscribebulk, *unsubscribebulk, *mbulk3, *mbulk4,
      *psubscribebulk, *punsubscribebulk, *integers[SHM_SHARED_INTEGERS];
};

struct shmServer {
  pthread_t mainthread;
  int port;
  char *bindaddr;
  char *socketpath;
  int ipfd;
  int sofd;
  shmDb *db;
  long long dirty;
  long long dirty_before_asave;
  list *clients;
  hash *commands;
  int loading;
  off_t loading_total_bytes;
  off_t loading_loaded_bytes;
  time_t loading_start_time;
  struct cmdShm *delCmd, *beginCmd;
  list *slaves, *monitors;
  char neterr[SNET_ERR_LEN];
  elLoop *el;
  int cronloops;
  time_t lastsave;
  time_t stat_starttime;
  long long stat_numcommands;
  long long stat_numconnections;
  long long stat_expiredkeys;
  long long stat_evictedkeys;
  long long stat_keyspace_hits;
  long long stat_keyspace_misses;
  list *slog;
  long long slog_entry_id;
  long long slog_log_slower_than;
  unsigned long slog_max_len;
  int verbosity;
  int maxidletime;
  int dbnum;
  int daemonize;
  int appendonly;
  int appendfsync;
  int no_appendfsync_on_rewrite;
  int shutdown_asap;
  time_t lastfsync;
  int appendfd;
  int appendseldb;
  char *pidfile;
  pid_t asavechildpid;
  pid_t bgrewritechildpid;
  dstr bgrewritebuf;
  dstr aofbuf;
  struct saveparam *saveparams;
  int saveparamslen;
  char *logfile;
  int syslog_enabled;
  char *syslog_ident;
  int syslog_facility;
  char *dbfilename;
  char *appendfilename;
  char *requirepass;
  int sdbcompression;
  int activerehashing;
  char *masterauth;
  char *masterhost;
  int masterport;
  shmClient *master;
  int replstate;
  off_t repl_transfer_left;
  int repl_transfer_s;
  int repl_transfer_fd;
  char *repl_transfer_tmpfile;
  time_t repl_transfer_lastio;
  int repl_serve_stale_data;
  unsigned int maxclients;
  unsigned long long maxmemory;
  int maxmemory_policy;
  int maxmemory_samples;
  unsigned int bpop_blocked_clients;
  unsigned int vm_blocked_clients;
  list *unblocked_clients;
  int sort_desc;
  int sort_alpha;
  int sort_bypattern;
  int vm_enabled;
  char *vm_swap_file;
  off_t vm_page_size;
  off_t vm_pages;
  unsigned long long vm_max_memory;
  size_t hash_max_zipmap_entries;
  size_t hash_max_zipmap_value;
  size_t list_max_sortlist_entries;
  size_t list_max_sortlist_value;
  size_t set_max_is_entries;
  FILE *vm_fp;
  int vm_fd;
  off_t vm_next_page;
  off_t vm_near_pages;
  unsigned char *vm_bitmap;
  time_t unixtime;
  list *io_newjobs;
  list *io_processing;
  list *io_processed;
  list *io_ready_clients;
  pthread_mutex_t io_mutex;
  pthread_mutex_t io_swapfile_mutex;
  pthread_attr_t io_threads_attr;
  int io_active_threads;
  int vm_max_threads;
  int io_ready_pipe_read;
  int io_ready_pipe_write;
  unsigned long long vm_stats_used_pages;
  unsigned long long vm_stats_swapped_objects;
  unsigned long long vm_stats_swapouts;
  unsigned long long vm_stats_swapins;
  hash *msg_channels;
  list *msg_patterns;
  unsigned lruclock : 22;
  unsigned lruclock_padding : 10;
};
typedef struct msgPattern {
  shmClient *client;
  robj *pattern;
} msgPattern;

typedef void cmdShmProc(shmClient *c);

typedef void shmVmPreloadProc(shmClient *c, struct cmdShm *cmd, int argc,
                              robj **argv);

struct cmdShm {
  char *name;
  cmdShmProc *proc;
  int arity;
  int flags;
  shmVmPreloadProc *vm_preload_proc;
  int vm_firstkey;
  int vm_lastkey;
  int vm_keystep;
};
struct shmFunctionSym {
  char *name;
  unsigned long pointer;
};
typedef struct _shmSortObject {
  robj *obj;
  union {
    double score;
    robj *cmpobj;
  } u;
} shmSortObject;
typedef struct _shmSortOperation {
  int type;
  robj *pattern;
} shmSortOperation;
typedef struct sortskiplistNode {
  robj *obj;
  double score;
  struct sortskiplistNode *backward;
  struct sortskiplistLevel {
    struct sortskiplistNode *forward;
    unsigned int span;
  } level[];
} sortskiplistNode;
typedef struct sortskiplist {
  struct sortskiplistNode *header, *tail;
  unsigned long length;
  int level;
} sortskiplist;
typedef struct sortset {
  hash *hash;
  sortskiplist *zsl;
} sortset;
#define SHM_IOJOB_LOAD 0
#define SHM_IOJOB_PREPARE_SWAP 1
#define SHM_IOJOB_DO_SWAP 2
typedef struct iojob {
  int type;
  shmDb *db;
  robj *key;
  robj *id;
  robj *val;
  off_t page;
  off_t pages;
  int canceled;
  pthread_t thread;
} iojob;
typedef struct {
  robj *subject;
  unsigned char encoding;
  unsigned char direction;
  unsigned char *zi;
  lNode *ln;
} listTypeIterator;
typedef struct {
  listTypeIterator *li;
  unsigned char *zi;
  lNode *ln;
} listTypeEntry;
typedef struct {
  robj *subject;
  int encoding;
  int ii;
  hashIterator *di;
} setTypeIterator;
typedef struct {
  int encoding;
  unsigned char *zi;
  unsigned char *zk, *zv;
  unsigned int zklen, zvlen;
  hashIterator *di;
  hashEntry *de;
} hashTypeIterator;
#define SHM_HASH_KEY 1
#define SHM_HASH_VALUE 2
extern struct shmServer server;
extern struct soStruct shared;
extern hashType setHashType;
extern hashType sortsetHashType;
extern double R_Zero, R_PosInf, R_NegInf, R_Nan;
hashType hashHashType;

shmClient *netCreateClient(int fd);

void netCloseTimedoutClients(void);

void netFreeClient(shmClient *c);

void netResetClient(shmClient *c);

void netSendReply(elLoop *el, int fd, void *privdata, int mask);

void netAdd(shmClient *c, robj *obj);

void *addDeferredBeginBulkLength(shmClient *c);

void setDeferredBeginBulkLength(shmClient *c, void *node, long length);

void netAddDstR(shmClient *c, dstr s);

void netProcessInputBuffer(shmClient *c);

void netAcceptTcpHandler(elLoop *el, int fd, void *privdata, int mask);

void netAcceptUnixHandler(elLoop *el, int fd, void *privdata, int mask);

void netReadQueryFromClient(elLoop *el, int fd, void *privdata, int mask);

void netAddBulk(shmClient *c, robj *obj);

void netAddBulkCString(shmClient *c, char *s);

void netAddBulkCBuffer(shmClient *c, void *p, size_t len);

void netAddBulkLongLong(shmClient *c, long long ll);

void acceptHandler(elLoop *el, int fd, void *privdata, int mask);

void netAdd(shmClient *c, robj *obj);

void netAddDstR(shmClient *c, dstr s);

void netAddError(shmClient *c, char *err);

void netAddStatus(shmClient *c, char *status);

void netAddDouble(shmClient *c, double d);

void netAddLongLong(shmClient *c, long long ll);

void netAddBeginBulkLen(shmClient *c, long length);

void *netDupReply(void *o);

void netGetClientsMaxBuffers(unsigned long *longest_outpulisttype,
                          unsigned long *biggest_input_buffer);

void netRewriteClientCommandVector(shmClient *c, int argc, ...);

#ifdef __GNUC__

void netAddErrorFormat(shmClient *c, const char *fmt, ...)
__attribute__((format(printf, 2, 3)));

void netAddStatusFormat(shmClient *c, const char *fmt, ...)
__attribute__((format(printf, 2, 3)));

#else

void netAddErrorFormat(shmClient *c, const char *fmt, ...);

void netAddStatusFormat(shmClient *c, const char *fmt, ...);

#endif

void listTypeTryConversion(robj *subject, robj *value);

void listTypePush(robj *subject, robj *value, int where);

robj *listTypePop(robj *subject, int where);

unsigned long listTypeLength(robj *subject);

listTypeIterator *listTypeInitIterator(robj *subject, int index,
                                       unsigned char direction);

void listTypeReleaseIterator(listTypeIterator *li);

int listTypeNext(listTypeIterator *li, listTypeEntry *entry);

robj *listTypeGet(listTypeEntry *entry);

void listTypeInsert(listTypeEntry *entry, robj *value, int where);

int listTypeEqual(listTypeEntry *entry, robj *o);

void listTypeDelete(listTypeEntry *entry);

void listTypeConvert(robj *subject, int enc);

void unblockClientWaitingData(shmClient *c);

int handleClientsWaitingListPush(shmClient *c, robj *key, robj *ele);

void popGenericCmd(shmClient *c, int where);

void unwatchAllKeys(shmClient *c);

void initClientBeginState(shmClient *c);

void netFreeClientBeginState(shmClient *c);

void queueBeginCmd(shmClient *c);

void touchWatchedKey(shmDb *db, robj *key);

void touchWatchedKeysOnFlush(int dbid);

void objDecRefCount(void *o);

void objIncRefCount(robj *o);

void objFreeString(robj *o);

void objFreeList(robj *o);

void objFreeSet(robj *o);

void objFreeSortSet(robj *o);

void objFreeHash(robj *o);

robj *objCreate(int type, void *ptr);

robj *objCreateString(char *ptr, size_t len);

robj *objStringDup(robj *o);

robj *objTryEncoding(robj *o);

robj *objGetDecoded(robj *o);

size_t objStringLen(robj *o);

robj *objCreateStringFromLongLong(long long value);

robj *objCreateList(void);

robj *objCreateSortlIst(void);

robj *objCreateSet(void);

robj *objCreateIs(void);

robj *objCreateHash(void);

robj *objCreateSortSet(void);

int objGetLongFromOrReply(shmClient *c, robj *o, long *target,
                             const char *msg);

int objCheckType(shmClient *c, robj *o, int type);

int objGetLongLongOrReply(shmClient *c, robj *o, long long *target,
                                 const char *msg);

int objGetDoubleOrReply(shmClient *c, robj *o, double *target,
                               const char *msg);

int objGetLongLong(robj *o, long long *target);

char *objStrEncoding(int encoding);

int objCompareString(robj *a, robj *b);

int objEqualString(robj *a, robj *b);

unsigned long objEstimateIdleTime(robj *o);

int syncWrite(int fd, char *ptr, ssize_t size, int timeout);

int syncRead(int fd, char *ptr, ssize_t size, int timeout);

int syncReadLine(int fd, char *ptr, ssize_t size, int timeout);

int fwriteBulkString(FILE *fp, char *s, unsigned long len);

int fwriteBulkDouble(FILE *fp, double d);

int fwriteBulkLongLong(FILE *fp, long long l);

int fwriteBulkObject(FILE *fp, robj *obj);

void replFeedSlaves(list *slaves, int hashid, robj **argv, int argc);

void replFeedMonitors(list *monitors, int hashid, robj **argv, int argc);

int syncWithMaster(void);

void updateSlavesWaitingAsave(int asaveerr);

void replCron(void);

void startLoading(FILE *fp);

void loadingProgress(off_t pos);

void stopLoading(void);

int sdbLoad(char *filename);

int sdbSaveBackground(char *filename);

void sdbRemoveTempFile(pid_t childpid);

int sdbSave(char *filename);

int sdbSaveObject(FILE *fp, robj *o);

off_t sdbSavedObjectLen(robj *o);

off_t sdbSavedObjectPages(robj *o);

robj *sdbLoadObject(int type, FILE *fp);

void backgroundSaveDoneHandler(int statloc);

void appendOnlyFlush(void);

void appendOnlyFeed(struct cmdShm *cmd, int hashid, robj **argv,
                    int argc);

void apendOnlyRemoveTempFile(pid_t childpid);

int appendOnlyRewriteAsync(void);

int appendOnlyLoad(char *filename);

void apendOnlyStop(void);

int appendOnlyStart(void);

void asyncRewriteDoneHandler(int statloc);

sortskiplist *zslCreate(void);

void zslFree(sortskiplist *zsl);

sortskiplistNode *zslInsert(sortskiplist *zsl, double score, robj *obj);

void freeMemoryIfNeeded(void);

int processCmd(shmClient *c);

void setupSignalHandlers(void);

struct cmdShm *lookupCmd(dstr name);

struct cmdShm *lookupCommandByCString(char *s);

void call(shmClient *c);

int prepareForShutdown();

void shmLog(int level, const char *fmt, ...);

void usage();

void updateHashResizePolicy(void);

int htNeedsResize(hash *hash);

void oom(const char *msg);

void populateCommandTable(void);

void vmInitialize(void);

void vmSetPageUsedPagesFree(off_t page, off_t count);

robj *vmReadObj(robj *o);

robj *vmPreviewObj(robj *o);

int vmSwapOutOneObjBlocking(void);

int vmSwapOutOneObjThreaded(void);

int vmCanSwapOutObj(void);

void vmThreadedIODoneJob(elLoop *el, int fd, void *privdata,
                              int mask);

void vmStopThreadedIOJob(robj *o);

void vmLockThreadedIO(void);

void unvmLockThreadedIO(void);

int vmSwapOutObjThreaded(robj *key, robj *val, shmDb *db);

void vmFreeIOJob(iojob *j);

void vmQueueIOJob(iojob *j);

int vmSwapOutObj(robj *o, off_t page);

robj *vmSwapInObj(off_t page, int type);

void vmWaitIOJobQueueEmpty(void);

void vmReopenSwap(void);

int vmFree(off_t page);

void vmSortunionPreloadKeys(shmClient *c, struct cmdShm *cmd,
                                         int argc, robj **argv);

void vmBeginPreloadKeys(shmClient *c, struct cmdShm *cmd,
                                  int argc, robj **argv);

int vmBlockClientOnSwappedKeys(shmClient *c);

int vmDontWaitForSwappedKey(shmClient *c, robj *key);

void vmHandleClientsBlockedOnSwappedKey(shmDb *db, robj *key);

vmptr *vmSwapOutObjBlocking(robj *val);

robj *setTypeCreate(robj *value);

int setTypeAdd(robj *subject, robj *value);

int setTypeRemove(robj *subject, robj *value);

int setTypeIsMember(robj *subject, robj *value);

setTypeIterator *setTypeInitIterator(robj *subject);

void setTypeReleaseIterator(setTypeIterator *si);

int setTypeNext(setTypeIterator *si, robj **objele, int64_t *llele);

robj *setTypeNextObject(setTypeIterator *si);

int setTypeRandomElement(robj *setobj, robj **objele, int64_t *llele);

unsigned long setTypeSize(robj *subject);

void setTypeConvert(robj *subject, int enc);

void convertToRealHash(robj *o);

void hashTypeTryConversion(robj *subject, robj **argv, int start, int end);

void hashTypeTryObjectEncoding(robj *subject, robj **o1, robj **o2);

int hashTypeGet(robj *o, robj *key, robj **objval, unsigned char **v,
                unsigned int *vlen);

robj *hashTypeGetObject(robj *o, robj *key);

int hashTypeExists(robj *o, robj *key);

int hashTypeSet(robj *o, robj *key, robj *value);

int hashTypeDelete(robj *o, robj *key);

unsigned long hashTypeLength(robj *o);

hashTypeIterator *hashTypeInitIterator(robj *subject);

void hashTypeReleaseIterator(hashTypeIterator *hi);

int hashTypeNext(hashTypeIterator *hi);

int hashTypeCurrent(hashTypeIterator *hi, int what, robj **objval,
                    unsigned char **v, unsigned int *vlen);

robj *hashTypeCurrentObject(hashTypeIterator *hi, int what);

robj *hashTypeLookupWriteOrCreate(shmClient *c, robj *key);

int msgUnsubscribeAllChannels(shmClient *c, int notify);

int msgUnsubscribeAllPatterns(shmClient *c, int notify);

void freeMsgPattern(void *p);

int listMatchMsgPattern(void *a, void *b);

int strCmpLen(const char *pattern, int patternLen, const char *string,
                   int stringLen, int nocase);

int strCmp(const char *pattern, const char *string, int nocase);

long long memToLongLong(const char *p, int *err);

int longLongToStr(char *s, size_t len, long long value);

int dstrToLong(dstr s, long *longval);

int dstrToLongLong(dstr s, long long *longval);

int objToLongLong(robj *o, long long *llongval);

long long timeUs(void);

void loadServerConfig(char *filename);

void appendServerSaveParams(time_t seconds, int changes);

void resetServerSaveParams();

int removeExpire(shmDb *db, robj *key);

void propagateExpire(shmDb *db, robj *key);

int expireIfNeeded(shmDb *db, robj *key);

time_t getExpire(shmDb *db, robj *key);

void setExpire(shmDb *db, robj *key, time_t when);

robj *lookupKey(shmDb *db, robj *key);

robj *lookupKeyRead(shmDb *db, robj *key);

robj *lookupKeyWrite(shmDb *db, robj *key);

robj *lookupKeyReadOrReply(shmClient *c, robj *key, robj *reply);

robj *lookupKeyWriteOrReply(shmClient *c, robj *key, robj *reply);

int dbAdd(shmDb *db, robj *key, robj *val);

int dbReplace(shmDb *db, robj *key, robj *val);

int dbExists(shmDb *db, robj *key);

robj *dbRandomKey(shmDb *db);

int dbDelete(shmDb *db, robj *key);

long long emptyDb();

int selectDb(shmClient *c, int id);

char *shmGitSHA1(void);

char *shmGitDirty(void);

void authCmd(shmClient *c);

void helloCmd(shmClient *c);

void reflectCmd(shmClient *c);

void setCmd(shmClient *c);

void setneCmd(shmClient *c);

void setxCmd(shmClient *c);

void getCmd(shmClient *c);

void delCmd(shmClient *c);

void existsCmd(shmClient *c);

void setbCmd(shmClient *c);

void getbCmd(shmClient *c);

void owrangeCmd(shmClient *c);

void getrangeCmd(shmClient *c);

void raiseCmd(shmClient *c);

void dcrCmd(shmClient *c);

void raisebyCmd(shmClient *c);

void dcrbyCmd(shmClient *c);

void changedbCmd(shmClient *c);

void randkeyCmd(shmClient *c);

void allkeysCmd(shmClient *c);

void dbcountCmd(shmClient *c);

void lsavetimeCmd(shmClient *c);

void saveCmd(shmClient *c);

void asaveCmd(shmClient *c);

void asyncrwCmd(shmClient *c);

void shutoffCmd(shmClient *c);

void mvkeyCmd(shmClient *c);

void renameCmd(shmClient *c);

void renameneCmd(shmClient *c);

void listpushCmd(shmClient *c);

void multipushCmd(shmClient *c);

void listpusheCmd(shmClient *c);

void multipusheCmd(shmClient *c);

void insleindCmd(shmClient *c);

void listpopCmd(shmClient *c);

void rempopCmd(shmClient *c);

void listlenCmd(shmClient *c);

void getleindCmd(shmClient *c);

void listrangeCmd(shmClient *c);

void listtrimCmd(shmClient *c);

void tpCmd(shmClient *c);

void listsetCmd(shmClient *c);

void setaddCmd(shmClient *c);

void setremCmd(shmClient *c);

void smvkeyCmd(shmClient *c);

void setmemberofCmd(shmClient *c);

void setcardCmd(shmClient *c);

void setpopCmd(shmClient *c);

void setranditemCmd(shmClient *c);

void setinterCmd(shmClient *c);

void setintertokeyCmd(shmClient *c);

void setunionCmd(shmClient *c);

void setuniontokeyCmd(shmClient *c);

void setdiffCmd(shmClient *c);

void setdifftokeyCmd(shmClient *c);

void syncCmd(shmClient *c);

void dropdbCmd(shmClient *c);

void dropallCmd(shmClient *c);

void sortCmd(shmClient *c);

void listremCmd(shmClient *c);

void rpoplistpushCmd(shmClient *c);

void infoCmd(shmClient *c);

void multigetCmd(shmClient *c);

void listenallCmd(shmClient *c);

void expireCmd(shmClient *c);

void exatCmd(shmClient *c);

void keygsCmd(shmClient *c);

void timetoliveCmd(shmClient *c);

void remexpirationCmd(shmClient *c);

void setslaveCmd(shmClient *c);

void debugCmd(shmClient *c);

void multisetCmd(shmClient *c);

void multiseteCmd(shmClient *c);

void sortaddCmd(shmClient *c);

void sortraisebyCmd(shmClient *c);

void sortrangeCmd(shmClient *c);

void sortrangebyscoreCmd(shmClient *c);

void sortrevrangebyscoreCmd(shmClient *c);

void sortcountCmd(shmClient *c);

void sortrevrangeCmd(shmClient *c);

void sortcardCmd(shmClient *c);

void sortremCmd(shmClient *c);

void sortscoreCmd(shmClient *c);

void sortremrangebyscoreCmd(shmClient *c);

void beginCmd(shmClient *c);

void commitCmd(shmClient *c);

void rollbackCmd(shmClient *c);

void bpopCmd(shmClient *c);

void brempopCmd(shmClient *c);

void brempoplistpushCmd(shmClient *c);

void addCmd(shmClient *c);

void getlenCmd(shmClient *c);

void sortrankCmd(shmClient *c);

void sortrevrankCmd(shmClient *c);

void hashsetCmd(shmClient *c);

void hashsetneCmd(shmClient *c);

void hashgetCmd(shmClient *c);

void hmultisetCmd(shmClient *c);

void hmultigetCmd(shmClient *c);

void hashdelCmd(shmClient *c);

void hashlenCmd(shmClient *c);

void sortremrangebyrankCmd(shmClient *c);

void sortuniontokeyCmd(shmClient *c);

void sortintertokeyCmd(shmClient *c);

void hashallkeysCmd(shmClient *c);

void hashvalsCmd(shmClient *c);

void hashgetallCmd(shmClient *c);

void hashexistsCmd(shmClient *c);

void configCmd(shmClient *c);

void hashraisebyCmd(shmClient *c);

void listenCmd(shmClient *c);

void unlistenCmd(shmClient *c);

void plistenCmd(shmClient *c);

void punlistenCmd(shmClient *c);

void postmesCmd(shmClient *c);

void lookCmd(shmClient *c);

void unlookCmd(shmClient *c);

void objCmd(shmClient *c);

#if defined(__GNUC__)

void *calloc(size_t count, size_t size) __attribute__((deprecated));

void free(void *ptr) __attribute__((deprecated));

void *malloc(size_t size) __attribute__((deprecated));

void *realloc(void *ptr, size_t size) __attribute__((deprecated));

#endif
