#ifndef __HASH_H
#define __HASH_H
#define HASH_OK 0
#define HASH_ERR 1
#define HASH_NOTUSED(V) ((void)V)
typedef struct hashEntry {
				void *key;
				void *val;
				struct hashEntry *next;
} hashEntry;
typedef struct hashType {
				unsigned int (*hashFunction)(const void *key);
				void *(*keyDup)(void *privdata, const void *key);
				void *(*valDup)(void *privdata, const void *obj);
				int (*keyCompare)(void *privdata, const void *key1, const void *key2);
				void (*keyDestructor)(void *privdata, void *key);
				void (*valDestructor)(void *privdata, void *obj);
} hashType;
typedef struct hashht {
				hashEntry **table;
				unsigned long size;
				unsigned long sizemask;
				unsigned long used;
} hashht;
typedef struct hash {
				hashType *type;
				void *privdata;
				hashht ht[2];
				int rehashidx;
				int iterators;
} hash;
typedef struct hashIterator {
				hash *d;
				int table, index, safe;
				hashEntry *entry, *nextEntry;
} hashIterator;
#define HASH_HT_INITIAL_SIZE 4
#define hashFreeEntryVal(d, entry) \
				if ((d)->type->valDestructor)    \
								(d)->type->valDestructor((d)->privdata, (entry)->val)
#define hashSetHashVal(d, entry, _val_)                     \
				do {                                                      \
								if ((d)->type->valDup)                                  \
												entry->val = (d)->type->valDup((d)->privdata, _val_); \
								else                                                    \
												entry->val = (_val_);                                 \
				} while (0)
#define hashFreeEntryKey(d, entry) \
				if ((d)->type->keyDestructor)    \
								(d)->type->keyDestructor((d)->privdata, (entry)->key)
#define hashSetHashKey(d, entry, _key_)                     \
				do {                                                      \
								if ((d)->type->keyDup)                                  \
												entry->key = (d)->type->keyDup((d)->privdata, _key_); \
								else                                                    \
												entry->key = (_key_);                                 \
				} while (0)
#define hashCompareHashKeys(d, key1, key2)                                    \
				(((d)->type->keyCompare) ? (d)->type->keyCompare((d)->privdata, key1, key2) \
				 : (key1) == (key2))
#define hashHashKey(d, key) (d)->type->hashFunction(key)
#define hashGetEntryKey(he) ((he)->key)
#define hashGetEntryVal(he) ((he)->val)
#define hashSlots(d) ((d)->ht[0].size + (d)->ht[1].size)
#define hashSize(d) ((d)->ht[0].used + (d)->ht[1].used)
#define hashIsRehashing(ht) ((ht)->rehashidx != -1)

hash *hashCreate(hashType *type, void *privDataPtr);

int hashExpand(hash *d, unsigned long size);

int hashAdd(hash *d, void *key, void *val);

int hashReplace(hash *d, void *key, void *val);

int hashDelete(hash *d, const void *key);

int hashDeleteNoFree(hash *d, const void *key);

void hashRelease(hash *d);

hashEntry *hashFind(hash *d, const void *key);

void *hashFetchValue(hash *d, const void *key);

int hashResize(hash *d);

hashIterator *hashGetIterator(hash *d);

hashIterator *hashGetSafeIterator(hash *d);

hashEntry *hashNext(hashIterator *iter);

void hashReleaseIterator(hashIterator *iter);

hashEntry *hashGetRandomKey(hash *d);

void hashPrintStats(hash *d);

unsigned int hashGenHashFunction(const unsigned char *buf, int len);

unsigned int hashGenCaseHashFunction(const unsigned char *buf, int len);

void hashEmpty(hash *d);

void hashEnableResize(void);

void hashDisableResize(void);

int hashRehash(hash *d, int n);

int hashRehashMilliseconds(hash *d, int ms);

extern hashType hashTypeHeapStringCopyKey;
extern hashType hashTypeHeapStrings;
extern hashType hashTypeHeapStringCopyKeyValue;
#endif
