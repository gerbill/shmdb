#include <sys/epoll.h>
typedef struct elApiState {
				int epfd;
				struct epoll_event events[EL_SETSIZE];
} elApiState;

static int elApiCreate(elLoop *l) {
				elApiState *state = dbmalloc(sizeof(elApiState));
				if (!state) return -1;
				state->epfd = epoll_create(1024);
				if (state->epfd == -1) return -1;
				l->apidata = state;
				return 0;
}

static void elApiFree(elLoop *l) {
				elApiState *state = l->apidata;
				close(state->epfd);
				dbfree(state);
}

static int elApiAddEvent(elLoop *l, int fd, int mask) {
				elApiState *state = l->apidata;
				struct epoll_event ee;
				int op =
								l->events[fd].mask == EL_NONE ? EPOLL_CTL_ADD : EPOLL_CTL_MOD;
				ee.events = 0;
				mask |= l->events[fd].mask;
				if (mask & EL_READABLE) ee.events |= EPOLLIN;
				if (mask & EL_WRITABLE) ee.events |= EPOLLOUT;
				ee.data.u64 = 0;
				ee.data.fd = fd;
				if (epoll_ctl(state->epfd, op, fd, &ee) == -1) return -1;
				return 0;
}

static void elApiDelEvent(elLoop *l, int fd, int delmask) {
				elApiState *state = l->apidata;
				struct epoll_event ee;
				int mask = l->events[fd].mask & (~delmask);
				ee.events = 0;
				if (mask & EL_READABLE) ee.events |= EPOLLIN;
				if (mask & EL_WRITABLE) ee.events |= EPOLLOUT;
				ee.data.u64 = 0;
				ee.data.fd = fd;
				if (mask != EL_NONE) {
								epoll_ctl(state->epfd, EPOLL_CTL_MOD, fd, &ee);
				} else {
								epoll_ctl(state->epfd, EPOLL_CTL_DEL, fd, &ee);
				}
}

static int elApiPoll(elLoop *l, struct timeval *tvp) {
				elApiState *state = l->apidata;
				int retval, numevents = 0;
				retval = epoll_wait(state->epfd, state->events, EL_SETSIZE,
				                    tvp ? (tvp->tv_sec * 1000 + tvp->tv_usec / 1000) : -1);
				if (retval > 0) {
								int j;
								numevents = retval;
								for (j = 0; j < numevents; j++) {
												int mask = 0;
												struct epoll_event *e = state->events + j;
												if (e->events & EPOLLIN) mask |= EL_READABLE;
												if (e->events & EPOLLOUT) mask |= EL_WRITABLE;
												l->fired[j].fd = e->data.fd;
												l->fired[j].mask = mask;
								}
				}
				return numevents;
}

static char *elApiName(void) {
				return "epoll";
}
