#include "shm.h"
#include "slog.h"
#ifdef HAVE_BACKTRACE
#include <execinfo.h>
#include <ucontext.h>
#endif
#include <time.h>
#include <signal.h>
#include <sys/wait.h>
#include <errno.h>
#include <assert.h>
#include <ctype.h>
#include <stdarg.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/uio.h>
#include <limits.h>
#include <float.h>
#include <math.h>
#include <pthread.h>
#include <sys/resource.h>
struct soStruct shared;
double R_Zero, R_PosInf, R_NegInf, R_Nan;
struct shmServer server;
struct cmdShm *commandTable;
struct cmdShm roCommandTable[] = {
		{"add", addCmd, 3, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"allkeys", allkeysCmd, 2, 0, NULL, 0, 0, 0},
		{"asave", asaveCmd, 1, 0, NULL, 0, 0, 0},
		{"asyncrw", asyncrwCmd, 1, 0, NULL, 0, 0, 0},
		{"auth", authCmd, 2, 0, NULL, 0, 0, 0},
		{"begin", beginCmd, 1, 0, NULL, 0, 0, 0},
		{"bpop", bpopCmd, -3, 0, NULL, 1, 1, 1},
		{"brempop", brempopCmd, -3, 0, NULL, 1, 1, 1},
		{"brempoplpush", brempoplistpushCmd, 4, SHM_CMD_DENYNOMEM, NULL, 1, 2, 1},
		{"changedb", changedbCmd, 2, 0, NULL, 0, 0, 0},
		{"commit", commitCmd, 1, SHM_CMD_DENYNOMEM, vmBeginPreloadKeys, 0, 0, 0},
		{"config", configCmd, -2, 0, NULL, 0, 0, 0},
		{"dbcount", dbcountCmd, 1, 0, NULL, 0, 0, 0},
		{"dcr", dcrCmd, 2, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"dcrby", dcrbyCmd, 3, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"debug", debugCmd, -2, 0, NULL, 0, 0, 0},
		{"del", delCmd, -2, 0, NULL, 0, 0, 0},
		{"dropall", dropallCmd, 1, 0, NULL, 0, 0, 0},
		{"dropdb", dropdbCmd, 1, 0, NULL, 0, 0, 0},
		{"exat", exatCmd, 3, 0, NULL, 0, 0, 0},
		{"exists", existsCmd, 2, 0, NULL, 1, 1, 1},
		{"expire", expireCmd, 3, 0, NULL, 0, 0, 0},
		{"get", getCmd, 2, 0, NULL, 1, 1, 1},
		{"getb", getbCmd, 3, 0, NULL, 1, 1, 1},
		{"getleind", getleindCmd, 3, 0, NULL, 1, 1, 1},
		{"getlen", getlenCmd, 2, 0, NULL, 1, 1, 1},
		{"getrange", getrangeCmd, 4, 0, NULL, 1, 1, 1},
		{"hashdel", hashdelCmd, 3, 0, NULL, 1, 1, 1},
		{"hashexists", hashexistsCmd, 3, 0, NULL, 1, 1, 1},
		{"hashget", hashgetCmd, 3, 0, NULL, 1, 1, 1},
		{"hashgetall", hashgetallCmd, 2, 0, NULL, 1, 1, 1},
		{"hashincrby", hashraisebyCmd, 4, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"hashkeys", hashallkeysCmd, 2, 0, NULL, 1, 1, 1},
		{"hashlen", hashlenCmd, 2, 0, NULL, 1, 1, 1},
		{"hashmget", hmultigetCmd, -3, 0, NULL, 1, 1, 1},
		{"hashmset", hmultisetCmd, -4, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"hashset", hashsetCmd, 4, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"hashsetne", hashsetneCmd, 4, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"hashvals", hashvalsCmd, 2, 0, NULL, 1, 1, 1},
		{"hello", helloCmd, 1, 0, NULL, 0, 0, 0},
		{"info", infoCmd, 1, 0, NULL, 0, 0, 0},
		{"insleind", insleindCmd, 5, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"keygs", keygsCmd, 3, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"listen", listenCmd, -2, 0, NULL, 0, 0, 0},
		{"listenall", listenallCmd, 1, 0, NULL, 0, 0, 0},
		{"listlen", listlenCmd, 2, 0, NULL, 1, 1, 1},
		{"listpop", listpopCmd, 2, 0, NULL, 1, 1, 1},
		{"listpush", listpushCmd, 3, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"listpushe", listpusheCmd, 3, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"listrange", listrangeCmd, 4, 0, NULL, 1, 1, 1},
		{"listrem", listremCmd, 4, 0, NULL, 1, 1, 1},
		{"listset", listsetCmd, 4, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"listtrim", listtrimCmd, 4, 0, NULL, 1, 1, 1},
		{"look", lookCmd, -2, 0, NULL, 0, 0, 0},
		{"lsavetime", lsavetimeCmd, 1, 0, NULL, 0, 0, 0},
		{"multiget", multigetCmd, -2, 0, NULL, 1, -1, 1},
		{"multipush", multipushCmd, 3, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"multipushe", multipusheCmd, 3, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"multiset", multisetCmd, -3, SHM_CMD_DENYNOMEM, NULL, 1, -1, 2},
		{"multisete", multiseteCmd, -3, SHM_CMD_DENYNOMEM, NULL, 1, -1, 2},
		{"mvkey", mvkeyCmd, 3, 0, NULL, 1, 1, 1},
		{"object", objCmd, -2, 0, NULL, 0, 0, 0},
		{"owrange", owrangeCmd, 4, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"plisten", plistenCmd, -2, 0, NULL, 0, 0, 0},
		{"postmes", postmesCmd, 3, SHM_CMD_FORCE_REPL, NULL, 0, 0, 0},
		{"punlisten", punlistenCmd, -1, 0, NULL, 0, 0, 0},
		{"raise", raiseCmd, 2, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"raiseby", raisebyCmd, 3, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"randkey", randkeyCmd, 1, 0, NULL, 0, 0, 0},
		{"reflect", reflectCmd, 2, 0, NULL, 0, 0, 0},
		{"remexpiration", remexpirationCmd, 2, 0, NULL, 1, 1, 1},
		{"rempop", rempopCmd, 2, 0, NULL, 1, 1, 1},
		{"rename", renameCmd, 3, 0, NULL, 1, 1, 1},
		{"renamene", renameneCmd, 3, 0, NULL, 1, 1, 1},
		{"rollback", rollbackCmd, 1, 0, NULL, 0, 0, 0},
		{"save", saveCmd, 1, 0, NULL, 0, 0, 0},
		{"set", setCmd, 3, SHM_CMD_DENYNOMEM, NULL, 0, 0, 0},
		{"setadd", setaddCmd, 3, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"setb", setbCmd, 4, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"setcard", setcardCmd, 2, 0, NULL, 1, 1, 1},
		{"setdiff", setdiffCmd, -2, SHM_CMD_DENYNOMEM, NULL, 1, -1, 1},
		{"setdifftokey", setdifftokeyCmd, -3, SHM_CMD_DENYNOMEM, NULL, 2, -1, 1},
		{"setinter", setinterCmd, -2, SHM_CMD_DENYNOMEM, NULL, 1, -1, 1},
		{"setintertokey", setintertokeyCmd, -3, SHM_CMD_DENYNOMEM, NULL, 2, -1, 1},
		{"setmemberof", setmemberofCmd, 3, 0, NULL, 1, 1, 1},
		{"setmembers", setinterCmd, 2, 0, NULL, 1, 1, 1},
		{"setmove", smvkeyCmd, 4, 0, NULL, 1, 2, 1},
		{"setne", setneCmd, 3, SHM_CMD_DENYNOMEM, NULL, 0, 0, 0},
		{"setpop", setpopCmd, 2, 0, NULL, 1, 1, 1},
		{"setranditem", setranditemCmd, 2, 0, NULL, 1, 1, 1},
		{"setrem", setremCmd, 3, 0, NULL, 1, 1, 1},
		{"setslave", setslaveCmd, 3, 0, NULL, 0, 0, 0},
		{"setunion", setunionCmd, -2, SHM_CMD_DENYNOMEM, NULL, 1, -1, 1},
		{"setuniontokey", setuniontokeyCmd, -3, SHM_CMD_DENYNOMEM, NULL, 2, -1, 1},
		{"setx", setxCmd, 4, SHM_CMD_DENYNOMEM, NULL, 0, 0, 0},
		{"shutoff", shutoffCmd, 1, 0, NULL, 0, 0, 0},
		{"slog", slogCmd, -2, 0, NULL, 0, 0, 0},
		{"sort", sortCmd, -2, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"sortadd", sortaddCmd, 4, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"sortcard", sortcardCmd, 2, 0, NULL, 1, 1, 1},
		{"sortcount", sortcountCmd, 4, 0, NULL, 1, 1, 1},
		{"sortincrby", sortraisebyCmd, 4, SHM_CMD_DENYNOMEM, NULL, 1, 1, 1},
		{"sortintertokey", sortintertokeyCmd, -4, SHM_CMD_DENYNOMEM, vmSortunionPreloadKeys, 0, 0, 0},
		{"sortrange", sortrangeCmd, -4, 0, NULL, 1, 1, 1},
		{"sortrangebyscore", sortrangebyscoreCmd, -4, 0, NULL, 1, 1, 1},
		{"sortrank", sortrankCmd, 3, 0, NULL, 1, 1, 1},
		{"sortrem", sortremCmd, 3, 0, NULL, 1, 1, 1},
		{"sortremrangebyrank", sortremrangebyrankCmd, 4, 0, NULL, 1, 1, 1},
		{"sortremrangebyscore", sortremrangebyscoreCmd, 4, 0, NULL, 1, 1, 1},
		{"sortrevrange", sortrevrangeCmd, -4, 0, NULL, 1, 1, 1},
		{"sortrevrangebyscore", sortrevrangebyscoreCmd, -4, 0, NULL, 1, 1, 1},
		{"sortrevrank", sortrevrankCmd, 3, 0, NULL, 1, 1, 1},
		{"sortscore", sortscoreCmd, 3, 0, NULL, 1, 1, 1},
		{"sortuniontokey", sortuniontokeyCmd, -4, SHM_CMD_DENYNOMEM, vmSortunionPreloadKeys, 0, 0, 0},
		{"substring", getrangeCmd, 4, 0, NULL, 1, 1, 1},
		{"swapl", rpoplistpushCmd, 3, SHM_CMD_DENYNOMEM, NULL, 1, 2, 1},
		{"sync", syncCmd, 1, 0, NULL, 0, 0, 0},
		{"timetolive", timetoliveCmd, 2, 0, NULL, 1, 1, 1},
		{"tp", tpCmd, 2, 0, NULL, 1, 1, 1},
		{"unlisten", unlistenCmd, -1, 0, NULL, 0, 0, 0},
		{"unlook", unlookCmd, 1, 0, NULL, 0, 0, 0}
};

void shmLog(int level, const char *fmt, ...) {
				const int syslogLevelMap[] = {LOG_DEBUG, LOG_INFO, LOG_NOTICE, LOG_WARNING};
				const char *c = ".-*#";
				time_t now = time(NULL);
				va_list ap;
				FILE *fp;
				char buf[64];
				char msg[SHM_MAX_LOGMSG_LEN];
				if (level < server.verbosity) return;
				fp = (server.logfile == NULL) ? stdout : fopen(server.logfile, "a");
				if (!fp) return;
				va_start(ap, fmt);
				vsnprintf(msg, sizeof(msg), fmt, ap);
				va_end(ap);
				strftime(buf, sizeof(buf), "%d %b %H:%M:%S", localtime(&now));
				fprintf(fp, "[%d] %s %c %s\n", (int)getpid(), buf, c[level], msg);
				fflush(fp);
				if (server.logfile) fclose(fp);
				if (server.syslog_enabled) syslog(syslogLevelMap[level], "%s", msg);
}

void oom(const char *msg) {
				shmLog(SHM_WARNING, "%s: Out of memory\n", msg);
				sleep(1);
				abort();
}

void hashVanillaFree(void *privdata, void *val) {
				HASH_NOTUSED(privdata);
				dbfree(val);
}

void hashListDestructor(void *privdata, void *val) {
				HASH_NOTUSED(privdata);
	lRelease((list *) val);
}

int hashDstRKeyCompare(void *privdata, const void *key1, const void *key2) {
				int l1, l2;
				HASH_NOTUSED(privdata);
				l1 = dstrlen((dstr)key1);
				l2 = dstrlen((dstr)key2);
				if (l1 != l2) return 0;
				return memcmp(key1, key2, l1) == 0;
}

int hashDstRKeyCaseCompare(void *privdata, const void *key1, const void *key2) {
				HASH_NOTUSED(privdata);
				return strcasecmp(key1, key2) == 0;
}

void hashShmObjectDestructor(void *privdata, void *val) {
				HASH_NOTUSED(privdata);
				if (val == NULL) return;
				objDecRefCount(val);
}

void hashDstRDestructor(void *privdata, void *val) {
				HASH_NOTUSED(privdata);
				dstrfree(val);
}

int hashObjKeyCompare(void *privdata, const void *key1, const void *key2) {
				const robj *o1 = key1, *o2 = key2;
				return hashDstRKeyCompare(privdata, o1->ptr, o2->ptr);
}

unsigned int hashObjHash(const void *key) {
				const robj *o = key;
				return hashGenHashFunction(o->ptr, dstrlen((dstr)o->ptr));
}

unsigned int hashDstRHash(const void *key) {
				return hashGenHashFunction((unsigned char *)key, dstrlen((char *)key));
}

unsigned int hashDstRCaseHash(const void *key) {
				return hashGenCaseHashFunction((unsigned char *)key, dstrlen((char *)key));
}

int hashEncObjKeyCompare(void *privdata, const void *key1, const void *key2) {
				robj *o1 = (robj *)key1, *o2 = (robj *)key2;
				int cmp;
				if (o1->encoding == SHM_ENCODING_INT && o2->encoding == SHM_ENCODING_INT)
								return o1->ptr == o2->ptr;
				o1 = objGetDecoded(o1);
				o2 = objGetDecoded(o2);
				cmp = hashDstRKeyCompare(privdata, o1->ptr, o2->ptr);
				objDecRefCount(o1);
				objDecRefCount(o2);
				return cmp;
}

unsigned int hashEncObjHash(const void *key) {
				robj *o = (robj *)key;
				if (o->encoding == SHM_ENCODING_RAW) {
								return hashGenHashFunction(o->ptr, dstrlen((dstr)o->ptr));
				} else {
								if (o->encoding == SHM_ENCODING_INT) {
												char buf[32];
												int len;
												len = longLongToStr(buf, 32, (long)o->ptr);
												return hashGenHashFunction((unsigned char *)buf, len);
								} else {
												unsigned int hash;
												o = objGetDecoded(o);
												hash = hashGenHashFunction(o->ptr, dstrlen((dstr)o->ptr));
												objDecRefCount(o);
												return hash;
								}
				}
}
hashType setHashType = {
				hashEncObjHash,          NULL, NULL, hashEncObjKeyCompare,
				hashShmObjectDestructor, NULL
};
hashType sortsetHashType = {
				hashEncObjHash,          NULL, NULL, hashEncObjKeyCompare,
				hashShmObjectDestructor, NULL
};
hashType dbHashType = {hashDstRHash,
				               NULL,
				               NULL,
				               hashDstRKeyCompare,
				               hashDstRDestructor,
				               hashShmObjectDestructor};
hashType keyptrHashType = {hashDstRHash,       NULL, NULL,
				                   hashDstRKeyCompare, NULL, NULL};
hashType commandTableHashType = {
				hashDstRCaseHash,   NULL, NULL, hashDstRKeyCaseCompare,
				hashDstRDestructor, NULL
};
hashType hashHashType = {hashEncObjHash,
				                 NULL,
				                 NULL,
				                 hashEncObjKeyCompare,
				                 hashShmObjectDestructor,
				                 hashShmObjectDestructor};
hashType keylistHashType = {
				hashObjHash,       NULL, NULL, hashObjKeyCompare, hashShmObjectDestructor,
				hashListDestructor
};

int htNeedsResize(hash *hash) {
				long long size, used;
				size = hashSlots(hash);
				used = hashSize(hash);
				return (size && used && size > HASH_HT_INITIAL_SIZE &&
				        (used * 100 / size < SHM_HT_MINFILL));
}

void tryResizeHashTables(void) {
				int j;
				for (j = 0; j < server.dbnum; j++) {
								if (htNeedsResize(server.db[j].hash)) hashResize(server.db[j].hash);
								if (htNeedsResize(server.db[j].expires)) hashResize(server.db[j].expires);
				}
}

void incrementallyRehash(void) {
				int j;
				for (j = 0; j < server.dbnum; j++) {
								if (hashIsRehashing(server.db[j].hash)) {
												hashRehashMilliseconds(server.db[j].hash, 1);
												break;
								}
				}
}

void updateHashResizePolicy(void) {
				if (server.asavechildpid == -1 && server.bgrewritechildpid == -1)
								hashEnableResize();
				else
								hashDisableResize();
}

void activeExpireCycle(void) {
				int j;
				for (j = 0; j < server.dbnum; j++) {
								int expired;
								shmDb *db = server.db + j;
								do {
												long num = hashSize(db->expires);
												time_t now = time(NULL);
												expired = 0;
												if (num > SHM_EXPIRELOOKUPS_PER_CRON) num = SHM_EXPIRELOOKUPS_PER_CRON;
												while (num--) {
																hashEntry *de;
																time_t t;
																if ((de = hashGetRandomKey(db->expires)) == NULL) break;
																t = (time_t)hashGetEntryVal(de);
																if (now > t) {
																				dstr key = hashGetEntryKey(de);
																				robj *keyobj = objCreateString(key, dstrlen(key));
																				propagateExpire(db, keyobj);
																				dbDelete(db, keyobj);
																				objDecRefCount(keyobj);
																				expired++;
																				server.stat_expiredkeys++;
																}
												}
								} while (expired > SHM_EXPIRELOOKUPS_PER_CRON / 4);
				}
}

void updateLRUClock(void) {
				server.lruclock = (time(NULL) / SHM_LRU_CLOCK_RESOLUTION) & SHM_LRU_CLOCK_MAX;
}

int serverCron(struct elLoop *eventLoop, long long id, void *clientData) {
				int j, loops = server.cronloops;
				SHM_NOTUSED(eventLoop);
				SHM_NOTUSED(id);
				SHM_NOTUSED(clientData);
				server.unixtime = time(NULL);
				updateLRUClock();
				if (server.shutdown_asap) {
								if (prepareForShutdown() == SHM_OK) exit(0);
								shmLog(SHM_WARNING,
								       "SIGTERM received but errors trying to shut down the server, "
								       "check the logs for more information");
				}
				for (j = 0; j < server.dbnum; j++) {
								long long size, used, vkeys;
								size = hashSlots(server.db[j].hash);
								used = hashSize(server.db[j].hash);
								vkeys = hashSize(server.db[j].expires);
								if (!(loops % 50) && (used || vkeys)) {
												shmLog(SHM_VERBOSE, "DB %d: %lld keys (%lld volatile) in %lld slots HT.",
												       j, used, vkeys, size);
								}
				}
				if (server.asavechildpid == -1 && server.bgrewritechildpid == -1) {
								if (!(loops % 10)) tryResizeHashTables();
								if (server.activerehashing) incrementallyRehash();
				}
				if (!(loops % 50)) {
								shmLog(SHM_VERBOSE, "%d clients connected (%d slaves), %zu bytes in use",
								       lLength(server.clients) - lLength(server.slaves),
								       lLength(server.slaves), dbmalloc_used_memory());
				}
				if ((server.maxidletime && !(loops % 100)) || server.bpop_blocked_clients)
								netCloseTimedoutClients();
				if (server.asavechildpid != -1 || server.bgrewritechildpid != -1) {
								int statloc;
								pid_t pid;
								if ((pid = wait3(&statloc, WNOHANG, NULL)) != 0) {
												if (pid == server.asavechildpid) {
																backgroundSaveDoneHandler(statloc);
												} else {
													asyncRewriteDoneHandler(statloc);
												}
												updateHashResizePolicy();
								}
				} else {
								time_t now = time(NULL);
								for (j = 0; j < server.saveparamslen; j++) {
												struct saveparam *sp = server.saveparams + j;
												if (server.dirty >= sp->changes && now - server.lastsave > sp->seconds) {
																shmLog(SHM_NOTICE, "%d changes in %d seconds. Saving...", sp->changes,
																       sp->seconds);
																sdbSaveBackground(server.dbfilename);
																break;
												}
								}
				}
				if (server.masterhost == NULL) activeExpireCycle();
				if (vmCanSwapOutObj()) {
								while (server.vm_enabled && dbmalloc_used_memory() > server.vm_max_memory) {
												int retval = (server.vm_max_threads == 0) ? vmSwapOutOneObjBlocking()
												             : vmSwapOutOneObjThreaded();
												if (retval == SHM_ERR && !(loops % 300) &&
												    dbmalloc_used_memory() >
												    (server.vm_max_memory + server.vm_max_memory / 10)) {
																shmLog(SHM_WARNING,
																       "WARNING: vm-max-memory limit exceeded by more than 10%% but "
																       "unable to swap more objects out!");
												}
												if (retval == SHM_ERR || server.vm_max_threads > 0) break;
								}
				}
				if (!(loops % 10)) replCron();
				server.cronloops++;
				return 100;
}

void beforeSleep(struct elLoop *eventLoop) {
				SHM_NOTUSED(eventLoop);
				lNode *ln;
				shmClient *c;
				if (server.vm_enabled && lLength(server.io_ready_clients)) {
								lIter li;
					lRewind(server.io_ready_clients, &li);
								while ((ln = lNext(&li))) {
												c = ln->value;
												struct cmdShm *cmd;
									lDelNode(server.io_ready_clients, ln);
												c->flags &= (~SHM_IO_WAIT);
												server.vm_blocked_clients--;
												elCreateFileEvent(server.el, c->fd, EL_READABLE, netReadQueryFromClient, c);
												cmd = lookupCmd(c->argv[0]->ptr);
												shmAssert(cmd != NULL);
												call(c);
												netResetClient(c);
												if (c->querybuf && dstrlen(c->querybuf) > 0) netProcessInputBuffer(c);
								}
				}
				while (lLength(server.unblocked_clients)) {
								ln = lFirst(server.unblocked_clients);
								shmAssert(ln != NULL);
								c = ln->value;
					lDelNode(server.unblocked_clients, ln);
								c->flags &= ~SHM_UNBLOCKED;
								if (c->querybuf && dstrlen(c->querybuf) > 0) netProcessInputBuffer(c);
				}
  appendOnlyFlush();
}

void createSharedObjects(void) {
				int j;
				shared.crlf = objCreate(SHM_STRING, dstrnew("\r\n"));
				shared.ok = objCreate(SHM_STRING, dstrnew("+OK\r\n"));
				shared.err = objCreate(SHM_STRING, dstrnew("-ERR\r\n"));
				shared.emptybulk = objCreate(SHM_STRING, dstrnew("$0\r\n\r\n"));
				shared.czero = objCreate(SHM_STRING, dstrnew(":0\r\n"));
				shared.cone = objCreate(SHM_STRING, dstrnew(":1\r\n"));
				shared.cnegone = objCreate(SHM_STRING, dstrnew(":-1\r\n"));
				shared.nullbulk = objCreate(SHM_STRING, dstrnew("$-1\r\n"));
				shared.nullbeginbulk = objCreate(SHM_STRING, dstrnew("*-1\r\n"));
				shared.emptybeginbulk = objCreate(SHM_STRING, dstrnew("*0\r\n"));
				shared.world = objCreate(SHM_STRING, dstrnew("+WORLD\r\n"));
				shared.queued = objCreate(SHM_STRING, dstrnew("+QUEUED\r\n"));
				shared.wrongtypeerr = objCreate(
								SHM_STRING,
								dstrnew(
												"-ERR Operation against a key holding the wrong kind of value\r\n"));
				shared.nokeyerr = objCreate(SHM_STRING, dstrnew("-ERR no such key\r\n"));
				shared.syntaxerr = objCreate(SHM_STRING, dstrnew("-ERR syntax error\r\n"));
				shared.sameobjecterr = objCreate(
								SHM_STRING,
								dstrnew("-ERR source and destination objects are the same\r\n"));
				shared.outofrangeerr =
								objCreate(SHM_STRING, dstrnew("-ERR index out of range\r\n"));
				shared.loadingerr = objCreate(
								SHM_STRING, dstrnew("-LOADING Shm is loading the dataset in memory\r\n"));
				shared.space = objCreate(SHM_STRING, dstrnew(" "));
				shared.colon = objCreate(SHM_STRING, dstrnew(":"));
				shared.plus = objCreate(SHM_STRING, dstrnew("+"));
				shared.select0 = objCreateString("changedb 0\r\n", 10);
				shared.select1 = objCreateString("changedb 1\r\n", 10);
				shared.select2 = objCreateString("changedb 2\r\n", 10);
				shared.select3 = objCreateString("changedb 3\r\n", 10);
				shared.select4 = objCreateString("changedb 4\r\n", 10);
				shared.select5 = objCreateString("changedb 5\r\n", 10);
				shared.select6 = objCreateString("changedb 6\r\n", 10);
				shared.select7 = objCreateString("changedb 7\r\n", 10);
				shared.select8 = objCreateString("changedb 8\r\n", 10);
				shared.select9 = objCreateString("changedb 9\r\n", 10);
				shared.messagebulk = objCreateString("$7\r\nmessage\r\n", 13);
				shared.pmessagebulk = objCreateString("$8\r\npmessage\r\n", 14);
				shared.subscribebulk = objCreateString("$9\r\nsubscribe\r\n", 15);
				shared.unsubscribebulk = objCreateString("$11\r\nunsubscribe\r\n", 18);
				shared.psubscribebulk = objCreateString("$10\r\npsubscribe\r\n", 17);
				shared.punsubscribebulk = objCreateString("$12\r\npunsubscribe\r\n", 19);
				shared.mbulk3 = objCreateString("*3\r\n", 4);
				shared.mbulk4 = objCreateString("*4\r\n", 4);
				for (j = 0; j < SHM_SHARED_INTEGERS; j++) {
								shared.integers[j] = objCreate(SHM_STRING, (void *)(long)j);
								shared.integers[j]->encoding = SHM_ENCODING_INT;
				}
}

void initServerConfig() {
				server.port = SHM_RPORT;
				server.bindaddr = NULL;
				server.socketpath = NULL;
				server.ipfd = -1;
				server.sofd = -1;
				server.dbnum = SHM_DEFAULT_DBNUM;
				server.verbosity = SHM_VERBOSE;
				server.maxidletime = SHM_MAXIDLETIME;
				server.saveparams = NULL;
				server.loading = 0;
				server.logfile = NULL;
				server.syslog_enabled = 0;
				server.syslog_ident = dbstrdup("shm");
				server.syslog_facility = LOG_LOCAL0;
				server.daemonize = 0;
				server.appendonly = 0;
				server.appendfsync = APPENDFSYNC_EVERYSEC;
				server.no_appendfsync_on_rewrite = 0;
				server.lastfsync = time(NULL);
				server.appendfd = -1;
				server.appendseldb = -1;
				server.pidfile = dbstrdup("/var/run/shm.pid");
				server.dbfilename = dbstrdup("dump.sdb");
				server.appendfilename = dbstrdup("appendonly.dat");
				server.requirepass = NULL;
				server.sdbcompression = 1;
				server.activerehashing = 1;
				server.maxclients = 0;
				server.bpop_blocked_clients = 0;
				server.maxmemory = 0;
				server.maxmemory_policy = SHM_MAXMEMORY_VOLATILE_LRU;
				server.maxmemory_samples = 3;
				server.vm_enabled = 0;
				server.vm_swap_file = dbstrdup("/tmp/shm-%p.swap");
				server.vm_page_size = 256;
				server.vm_pages = 1024 * 1024 * 100;
				server.vm_max_memory = 1024LL * 1024 * 1024 * 1;
				server.vm_max_threads = 4;
				server.vm_blocked_clients = 0;
				server.hash_max_zipmap_entries = SHM_HASH_MAX_ZIPMAP_ENTRIES;
				server.hash_max_zipmap_value = SHM_HASH_MAX_ZIPMAP_VALUE;
				server.list_max_sortlist_entries = SHM_LIST_MAX_SORTLIST_ENTRIES;
				server.list_max_sortlist_value = SHM_LIST_MAX_SORTLIST_VALUE;
				server.set_max_is_entries = SHM_SET_MAX_IS_ENTRIES;
				server.shutdown_asap = 0;
				updateLRUClock();
				resetServerSaveParams();
				appendServerSaveParams(60 * 60, 1);
				appendServerSaveParams(300, 100);
				appendServerSaveParams(60, 10000);
				server.masterauth = NULL;
				server.masterhost = NULL;
				server.masterport = 20971;
				server.master = NULL;
				server.replstate = SHM_REPL_NONE;
				server.repl_serve_stale_data = 1;
				R_Zero = 0.0;
				R_PosInf = 1.0 / R_Zero;
				R_NegInf = -1.0 / R_Zero;
				R_Nan = R_Zero / R_Zero;
				server.commands = hashCreate(&commandTableHashType, NULL);
				populateCommandTable();
				server.delCmd = lookupCommandByCString("del");
				server.beginCmd = lookupCommandByCString("begin");
				server.slog_log_slower_than = SHM_SLOG_LOG_SLOWER_THAN;
				server.slog_max_len = SHM_SLOG_MAX_LEN;
}

void initServer() {
				int j;
				signal(SIGHUP, SIG_IGN);
				signal(SIGPIPE, SIG_IGN);
				setupSignalHandlers();
				if (server.syslog_enabled) {
								openlog(server.syslog_ident, LOG_PID | LOG_NDELAY | LOG_NOWAIT,
								        server.syslog_facility);
				}
				server.mainthread = pthread_self();
				server.clients = lCreate();
				server.slaves = lCreate();
				server.monitors = lCreate();
				server.unblocked_clients = lCreate();
				createSharedObjects();
				server.el = elCreateLoop();
				server.db = dbmalloc(sizeof(shmDb) * server.dbnum);
				if (server.port != 0) {
								server.ipfd = snetTcpServer(server.neterr, server.port, server.bindaddr);
								if (server.ipfd == SNET_ERR) {
												shmLog(SHM_WARNING, "Opening port: %s", server.neterr);
												exit(1);
								}
				}
				if (server.socketpath != NULL) {
								unlink(server.socketpath);
								server.sofd = snetUnixServer(server.neterr, server.socketpath);
								if (server.sofd == SNET_ERR) {
												shmLog(SHM_WARNING, "Opening socket: %s", server.neterr);
												exit(1);
								}
				}
				if (server.ipfd < 0 && server.sofd < 0) {
								shmLog(SHM_WARNING, "Configured to not listen anywhere, exiting.");
								exit(1);
				}
				for (j = 0; j < server.dbnum; j++) {
								server.db[j].hash = hashCreate(&dbHashType, NULL);
								server.db[j].expires = hashCreate(&keyptrHashType, NULL);
								server.db[j].blocking_keys = hashCreate(&keylistHashType, NULL);
								server.db[j].watched_keys = hashCreate(&keylistHashType, NULL);
								if (server.vm_enabled)
												server.db[j].io_keys = hashCreate(&keylistHashType, NULL);
								server.db[j].id = j;
				}
				server.msg_channels = hashCreate(&keylistHashType, NULL);
				server.msg_patterns = lCreate();
				lSetFreeMethod(server.msg_patterns, freeMsgPattern);
				lSetMatchMethod(server.msg_patterns, listMatchMsgPattern);
				server.cronloops = 0;
				server.asavechildpid = -1;
				server.bgrewritechildpid = -1;
				server.bgrewritebuf = dstrempty();
				server.aofbuf = dstrempty();
				server.lastsave = time(NULL);
				server.dirty = 0;
				server.stat_numcommands = 0;
				server.stat_numconnections = 0;
				server.stat_expiredkeys = 0;
				server.stat_evictedkeys = 0;
				server.stat_starttime = time(NULL);
				server.stat_keyspace_misses = 0;
				server.stat_keyspace_hits = 0;
				server.unixtime = time(NULL);
				elCreateTimeEvent(server.el, 1, serverCron, NULL, NULL);
				if (server.ipfd > 0 && elCreateFileEvent(server.el, server.ipfd, EL_READABLE,
				                                         netAcceptTcpHandler, NULL) == EL_ERR)
								oom("creating file event");
				if (server.sofd > 0 && elCreateFileEvent(server.el, server.sofd, EL_READABLE,
				                                         netAcceptUnixHandler, NULL) == EL_ERR)
								oom("creating file event");
				if (server.appendonly) {
								server.appendfd =
												open(server.appendfilename, O_WRONLY | O_APPEND | O_CREAT, 0644);
								if (server.appendfd == -1) {
												shmLog(SHM_WARNING, "Can't open the append-only file: %s",
												       strerror(errno));
												exit(1);
								}
				}
				if (server.vm_enabled) vmInitialize();
				slogInit();
				srand(time(NULL) ^ getpid());
}

void populateCommandTable(void) {
				int j;
				int numcommands = sizeof(roCommandTable) / sizeof(struct cmdShm);
				for (j = 0; j < numcommands; j++) {
								struct cmdShm *c = roCommandTable + j;
								int retval;
								retval = hashAdd(server.commands, dstrnew(c->name), c);
								assert(retval == HASH_OK);
				}
}
struct cmdShm *lookupCmd(dstr name) {
				return hashFetchValue(server.commands, name);
}
struct cmdShm *lookupCommandByCString(char *s) {
				struct cmdShm *cmd;
				dstr name = dstrnew(s);
				cmd = hashFetchValue(server.commands, name);
				dstrfree(name);
				return cmd;
}

void call(shmClient *c) {
				long long dirty, start = timeUs(), duration;
				dirty = server.dirty;
				c->cmd->proc(c);
				dirty = server.dirty - dirty;
				duration = timeUs() - start;
				slogPushEntryIfNeeded(c->argv, c->argc, duration);
				if (server.appendonly && dirty > 0)
					appendOnlyFeed(c->cmd, c->db->id, c->argv, c->argc);
				if ((dirty > 0 || c->cmd->flags & SHM_CMD_FORCE_REPL) &&
				    lLength(server.slaves))
								replFeedSlaves(server.slaves, c->db->id, c->argv, c->argc);
				if (lLength(server.monitors))
								replFeedMonitors(server.monitors, c->db->id, c->argv, c->argc);
				server.stat_numcommands++;
}

int processCmd(shmClient *c) {
				if (!strcasecmp(c->argv[0]->ptr, "exit")) {
								netAdd(c, shared.ok);
								c->flags |= SHM_CLOSE_AFTER_REPLY;
								return SHM_ERR;
				}
				c->cmd = lookupCmd(c->argv[0]->ptr);
				if (!c->cmd) {
								netAddErrorFormat(c, "unknown command '%s'", (char *)c->argv[0]->ptr);
								return SHM_OK;
				} else if ((c->cmd->arity > 0 && c->cmd->arity != c->argc) ||
				           (c->argc < -c->cmd->arity)) {
								netAddErrorFormat(c, "wrong number of arguments for '%s' command",
								                    c->cmd->name);
								return SHM_OK;
				}
				if (server.requirepass && !c->authenticated && c->cmd->proc != authCmd) {
								netAddError(c, "operation not permitted");
								return SHM_OK;
				}
				if (server.maxmemory) freeMemoryIfNeeded();
				if (server.maxmemory && (c->cmd->flags & SHM_CMD_DENYNOMEM) &&
				    dbmalloc_used_memory() > server.maxmemory) {
								netAddError(c, "command not allowed when used memory > 'maxmemory'");
								return SHM_OK;
				}
				if ((hashSize(c->msg_channels) > 0 ||
				     lLength(c->msg_patterns) > 0) &&
				    c->cmd->proc != listenCmd && c->cmd->proc != unlistenCmd &&
				    c->cmd->proc != plistenCmd &&
				    c->cmd->proc != punlistenCmd) {
								netAddError(
												c, "only (P)SUBSCRIBE / (P)UNSUBSCRIBE / QUIT allowed in this context");
								return SHM_OK;
				}
				if (server.masterhost && server.replstate != SHM_REPL_CONNECTED &&
				    server.repl_serve_stale_data == 0 && c->cmd->proc != infoCmd &&
				    c->cmd->proc != setslaveCmd) {
								netAddError(
												c, "link with MASTER is down and slave-serve-stale-data is set to no");
								return SHM_OK;
				}
				if (server.loading && c->cmd->proc != infoCmd) {
								netAdd(c, shared.loadingerr);
								return SHM_OK;
				}
				if (c->flags & SHM_BEGIN && c->cmd->proc != commitCmd &&
				    c->cmd->proc != rollbackCmd && c->cmd->proc != beginCmd &&
				    c->cmd->proc != lookCmd) {
								queueBeginCmd(c);
								netAdd(c, shared.queued);
				} else {
								if (server.vm_enabled && server.vm_max_threads > 0 &&
								    vmBlockClientOnSwappedKeys(c))
												return SHM_ERR;
								call(c);
				}
				return SHM_OK;
}

int prepareForShutdown() {
				shmLog(SHM_WARNING, "User requested shutdown...");
				if (server.asavechildpid != -1) {
								shmLog(SHM_WARNING, "There is a child saving an .sdb. Killing it!");
								kill(server.asavechildpid, SIGKILL);
								sdbRemoveTempFile(server.asavechildpid);
				}
				if (server.appendonly) {
								if (server.bgrewritechildpid != -1) {
												shmLog(SHM_WARNING, "There is a child rewriting the AOF. Killing it!");
												kill(server.bgrewritechildpid, SIGKILL);
								}
								shmLog(SHM_NOTICE, "Calling fsync() on the AOF file.");
								appendonly_fsync(server.appendfd);
				}
				if (server.saveparamslen > 0) {
								shmLog(SHM_NOTICE, "Saving the final SDB snapshot before exiting.");
								if (sdbSave(server.dbfilename) != SHM_OK) {
												shmLog(SHM_WARNING, "Error trying to save the DB, can't exit.");
												return SHM_ERR;
								}
				}
				if (server.vm_enabled) {
								shmLog(SHM_NOTICE, "Removing the swap file.");
								unlink(server.vm_swap_file);
				}
				if (server.daemonize) {
								shmLog(SHM_NOTICE, "Removing the pid file.");
								unlink(server.pidfile);
				}
				if (server.ipfd != -1) close(server.ipfd);
				if (server.sofd != -1) close(server.sofd);
				shmLog(SHM_WARNING, "Shm is now ready to exit, bye bye...");
				return SHM_OK;
}

void authCmd(shmClient *c) {
				if (!server.requirepass || !strcmp(c->argv[1]->ptr, server.requirepass)) {
								c->authenticated = 1;
								netAdd(c, shared.ok);
				} else {
								c->authenticated = 0;
								netAddError(c, "invalid password");
				}
}

void helloCmd(shmClient *c) {
				netAdd(c, shared.world);
}

void reflectCmd(shmClient *c) {
				netAddBulk(c, c->argv[1]);
}

void bytesToHuman(char *s, unsigned long long n) {
				double d;
				if (n < 1024) {
								sprintf(s, "%lluB", n);
								return;
				} else if (n < (1024 * 1024)) {
								d = (double)n / (1024);
								sprintf(s, "%.2fK", d);
				} else if (n < (1024LL * 1024 * 1024)) {
								d = (double)n / (1024 * 1024);
								sprintf(s, "%.2fM", d);
				} else if (n < (1024LL * 1024 * 1024 * 1024)) {
								d = (double)n / (1024LL * 1024 * 1024);
								sprintf(s, "%.2fG", d);
				}
}

dstr genShmInfoString(void) {
				dstr info;
				time_t uptime = time(NULL) - server.stat_starttime;
				int j;
				char hmem[64];
				struct rusage self_ru, c_ru;
				unsigned long lol, bib;
				getrusage(RUSAGE_SELF, &self_ru);
				getrusage(RUSAGE_CHILDREN, &c_ru);
				netGetClientsMaxBuffers(&lol, &bib);
				bytesToHuman(hmem, dbmalloc_used_memory());
				info = dstrcatprintf(
								dstrempty(),
								"shm_version:%s\r\n"
								"shm_git_sha1:%s\r\n"
								"shm_git_dirty:%d\r\n"
								"arch_bits:%s\r\n"
								"beginplexing_api:%s\r\n"
								"process_id:%ld\r\n"
								"uptime_in_seconds:%ld\r\n"
								"uptime_in_days:%ld\r\n"
								"lru_clock:%ld\r\n"
								"used_cpu_sys:%.2f\r\n"
								"used_cpu_user:%.2f\r\n"
								"used_cpu_sys_children:%.2f\r\n"
								"used_cpu_user_children:%.2f\r\n"
								"connected_clients:%d\r\n"
								"connected_slaves:%d\r\n"
								"client_longest_outpulisttype:%lu\r\n"
								"client_biggest_input_buf:%lu\r\n"
								"blocked_clients:%d\r\n"
								"used_memory:%zu\r\n"
								"used_memory_human:%s\r\n"
								"used_memory_rss:%zu\r\n"
								"mem_fragmentation_ratio:%.2f\r\n"
								"use_tcmalloc:%d\r\n"
								"loading:%d\r\n"
								"aof_enabled:%d\r\n"
								"changes_since_last_save:%lld\r\n"
								"asave_in_progress:%d\r\n"
								"last_save_time:%ld\r\n"
								"asyncrw_in_progress:%d\r\n"
								"total_connections_received:%lld\r\n"
								"total_commands_processed:%lld\r\n"
								"expired_keys:%lld\r\n"
								"evicted_keys:%lld\r\n"
								"allkeyspace_hits:%lld\r\n"
								"allkeyspace_misses:%lld\r\n"
								"hash_max_zipmap_entries:%zu\r\n"
								"hash_max_zipmap_value:%zu\r\n"
								"msg_channels:%ld\r\n"
								"msg_patterns:%u\r\n"
								"vm_enabled:%d\r\n"
								"role:%s\r\n",
								SHM_VERSION, shmGitSHA1(), strtol(shmGitDirty(), NULL, 10) > 0,
								(sizeof(long) == 8) ? "64" : "32", elGetApiName(), (long)getpid(), uptime,
								uptime / (3600 * 24), (unsigned long)server.lruclock,
								(float)self_ru.ru_utime.tv_sec +
								(float)self_ru.ru_utime.tv_usec / 1000000,
								(float)self_ru.ru_stime.tv_sec +
								(float)self_ru.ru_stime.tv_usec / 1000000,
								(float)c_ru.ru_utime.tv_sec + (float)c_ru.ru_utime.tv_usec / 1000000,
								(float)c_ru.ru_stime.tv_sec + (float)c_ru.ru_stime.tv_usec / 1000000,
								lLength(server.clients) - lLength(server.slaves),
								lLength(server.slaves), lol, bib, server.bpop_blocked_clients,
								dbmalloc_used_memory(), hmem, dbmalloc_get_rss(),
								dbmalloc_get_fragmentation_ratio(),
#ifdef USE_TCMALLOC
								1,
#else
								0,
#endif
								server.loading, server.appendonly, server.dirty,
								server.asavechildpid != -1, server.lastsave,
								server.bgrewritechildpid != -1, server.stat_numconnections,
								server.stat_numcommands, server.stat_expiredkeys, server.stat_evictedkeys,
								server.stat_keyspace_hits, server.stat_keyspace_misses,
								server.hash_max_zipmap_entries, server.hash_max_zipmap_value,
								hashSize(server.msg_channels), lLength(server.msg_patterns),
								server.vm_enabled != 0, server.masterhost == NULL ? "master" : "slave");
				if (server.masterhost) {
								info = dstrcatprintf(
												info,
												"master_host:%s\r\n"
												"master_port:%d\r\n"
												"master_link_status:%s\r\n"
												"master_last_io_seconds_ago:%d\r\n"
												"master_sync_in_progress:%d\r\n",
												server.masterhost, server.masterport,
												(server.replstate == SHM_REPL_CONNECTED) ? "up" : "down",
												server.master ? ((int)(time(NULL) - server.master->lastinteraction))
												: -1,
												server.replstate == SHM_REPL_TRANSFER);
								if (server.replstate == SHM_REPL_TRANSFER) {
												info = dstrcatprintf(info,
												                    "master_sync_left_bytes:%ld\r\n"
												                    "master_sync_last_io_seconds_ago:%d\r\n",
												                    (long)server.repl_transfer_left,
												                    (int)(time(NULL) - server.repl_transfer_lastio));
								}
				}
				if (server.vm_enabled) {
								vmLockThreadedIO();
								info = dstrcatprintf(info,
								                    "vm_conf_max_memory:%llu\r\n"
								                    "vm_conf_page_size:%llu\r\n"
								                    "vm_conf_pages:%llu\r\n"
								                    "vm_stats_used_pages:%llu\r\n"
								                    "vm_stats_swapped_objects:%llu\r\n"
								                    "vm_stats_swappin_count:%llu\r\n"
								                    "vm_stats_swappout_count:%llu\r\n"
								                    "vm_stats_io_newjobs_len:%lu\r\n"
								                    "vm_stats_io_processing_len:%lu\r\n"
								                    "vm_stats_io_processed_len:%lu\r\n"
								                    "vm_stats_io_active_threads:%lu\r\n"
								                    "vm_stats_blocked_clients:%lu\r\n",
								                    (unsigned long long)server.vm_max_memory,
								                    (unsigned long long)server.vm_page_size,
								                    (unsigned long long)server.vm_pages,
								                    (unsigned long long)server.vm_stats_used_pages,
								                    (unsigned long long)server.vm_stats_swapped_objects,
								                    (unsigned long long)server.vm_stats_swapins,
								                    (unsigned long long)server.vm_stats_swapouts,
								                    (unsigned long)lLength(server.io_newjobs),
								                    (unsigned long)lLength(server.io_processing),
								                    (unsigned long)lLength(server.io_processed),
								                    (unsigned long)server.io_active_threads,
								                    (unsigned long)server.vm_blocked_clients);
								unvmLockThreadedIO();
				}
				if (server.loading) {
								double perc;
								time_t eta, elapsed;
								off_t remaining_bytes =
												server.loading_total_bytes - server.loading_loaded_bytes;
								perc = ((double)server.loading_loaded_bytes / server.loading_total_bytes) *
								       100;
								elapsed = time(NULL) - server.loading_start_time;
								if (elapsed == 0) {
												eta = 1;
								} else {
												eta = (elapsed * remaining_bytes) / server.loading_loaded_bytes;
								}
								info = dstrcatprintf(info,
								                    "loading_start_time:%ld\r\n"
								                    "loading_total_bytes:%llu\r\n"
								                    "loading_loaded_bytes:%llu\r\n"
								                    "loading_loaded_perc:%.2f\r\n"
								                    "loading_eta_seconds:%ld\r\n",
								                    (unsigned long)server.loading_start_time,
								                    (unsigned long long)server.loading_total_bytes,
								                    (unsigned long long)server.loading_loaded_bytes, perc,
								                    eta);
				}
				for (j = 0; j < server.dbnum; j++) {
								long long keys, vkeys;
								keys = hashSize(server.db[j].hash);
								vkeys = hashSize(server.db[j].expires);
								if (keys || vkeys) {
												info =
																dstrcatprintf(info, "db%d:keys=%lld,expires=%lld\r\n", j, keys, vkeys);
								}
				}
				return info;
}

void infoCmd(shmClient *c) {
				dstr info = genShmInfoString();
				netAddDstR(
								c, dstrcatprintf(dstrempty(), "$%lu\r\n", (unsigned long)dstrlen(info)));
				netAddDstR(c, info);
				netAdd(c, shared.crlf);
}

void listenallCmd(shmClient *c) {
				if (c->flags & SHM_SLAVE) return;
				c->flags |= (SHM_SLAVE | SHM_MONITOR);
				c->slaveseldb = 0;
	lAddNodeTail(server.monitors, c);
				netAdd(c, shared.ok);
}

void freeMemoryIfNeeded(void) {
				if (server.maxmemory_policy == SHM_MAXMEMORY_NO_EVICTION) return;
				while (server.maxmemory && dbmalloc_used_memory() > server.maxmemory) {
								int j, k, freed = 0;
								for (j = 0; j < server.dbnum; j++) {
												long bestval = 0;
												dstr bestkey = NULL;
												struct hashEntry *de;
												shmDb *db = server.db + j;
												hash *hash;
												if (server.maxmemory_policy == SHM_MAXMEMORY_ALLKEYS_LRU ||
												    server.maxmemory_policy == SHM_MAXMEMORY_ALLKEYS_RANDOM) {
																hash = server.db[j].hash;
												} else {
																hash = server.db[j].expires;
												}
												if (hashSize(hash) == 0) continue;
												if (server.maxmemory_policy == SHM_MAXMEMORY_ALLKEYS_RANDOM ||
												    server.maxmemory_policy == SHM_MAXMEMORY_VOLATILE_RANDOM) {
																de = hashGetRandomKey(hash);
																bestkey = hashGetEntryKey(de);
												} else if (server.maxmemory_policy == SHM_MAXMEMORY_ALLKEYS_LRU ||
												           server.maxmemory_policy == SHM_MAXMEMORY_VOLATILE_LRU) {
																for (k = 0; k < server.maxmemory_samples; k++) {
																				dstr thiskey;
																				long thisval;
																				robj *o;
																				de = hashGetRandomKey(hash);
																				thiskey = hashGetEntryKey(de);
																				if (server.maxmemory_policy == SHM_MAXMEMORY_VOLATILE_LRU)
																								de = hashFind(db->hash, thiskey);
																				o = hashGetEntryVal(de);
																				thisval = objEstimateIdleTime(o);
																				if (bestkey == NULL || thisval > bestval) {
																								bestkey = thiskey;
																								bestval = thisval;
																				}
																}
												} else if (server.maxmemory_policy == SHM_MAXMEMORY_VOLATILE_TTL) {
																for (k = 0; k < server.maxmemory_samples; k++) {
																				dstr thiskey;
																				long thisval;
																				de = hashGetRandomKey(hash);
																				thiskey = hashGetEntryKey(de);
																				thisval = (long)hashGetEntryVal(de);
																				if (bestkey == NULL || thisval < bestval) {
																								bestkey = thiskey;
																								bestval = thisval;
																				}
																}
												}
												if (bestkey) {
																robj *keyobj = objCreateString(bestkey, dstrlen(bestkey));
																propagateExpire(db, keyobj);
																dbDelete(db, keyobj);
																server.stat_evictedkeys++;
																objDecRefCount(keyobj);
																freed++;
												}
								}
								if (!freed) return;
				}
}
#ifdef __linux__

int linuxOvercommitMemoryValue(void) {
				FILE *fp = fopen("/proc/sys/vm/overcommit_memory", "r");
				char buf[64];
				if (!fp) return -1;
				if (fgets(buf, 64, fp) == NULL) {
								fclose(fp);
								return -1;
				}
				fclose(fp);
				return atoi(buf);
}

void linuxOvercommitMemoryWarning(void) {
				if (linuxOvercommitMemoryValue() == 0) {
								shmLog(
												SHM_WARNING,
												"WARNING overcommit_memory is set to 0! Background save may fail under "
												"low memory condition. To fix this issue add 'vm.overcommit_memory = "
												"1' to /etc/sysctl.conf and then reboot or run the command 'sysctl "
												"vm.overcommit_memory=1' for this to take effect.");
				}
}
#endif

void createPidFile(void) {
				FILE *fp = fopen(server.pidfile, "w");
				if (fp) {
								fprintf(fp, "%d\n", (int)getpid());
								fclose(fp);
				}
}

void daemonize(void) {
				int fd;
				if (fork() != 0) exit(0);
				setsid();
				if ((fd = open("/dev/null", O_RDWR, 0)) != -1) {
								dup2(fd, STDIN_FILENO);
								dup2(fd, STDOUT_FILENO);
								dup2(fd, STDERR_FILENO);
								if (fd > STDERR_FILENO) close(fd);
				}
}

void version() {
				printf("Shm server version %s (%s:%d)\n", SHM_VERSION, shmGitSHA1(),
				       atoi(shmGitDirty()) > 0);
				exit(0);
}

void usage() {
				fprintf(stderr, "Usage: ./shm-server [/path/to/shm.conf]\n");
				fprintf(stderr, "       ./shm-server - (read config from stdin)\n");
				exit(1);
}

int main(int argc, char **argv) {
				time_t start;
				initServerConfig();
				if (argc == 2) {
								if (strcmp(argv[1], "-v") == 0 || strcmp(argv[1], "--version") == 0)
												version();
								if (strcmp(argv[1], "--help") == 0) usage();
								resetServerSaveParams();
								loadServerConfig(argv[1]);
				} else if ((argc > 2)) {
								usage();
				} else {
								shmLog(
												SHM_WARNING,
												"Warning: no config file specified, using the default config. In order "
												"to specify a config file use 'shm-server /path/to/shm.conf'");
				}
				if (server.daemonize) daemonize();
				initServer();
				if (server.daemonize) createPidFile();
				shmLog(SHM_NOTICE, "Server started, Shm version " SHM_VERSION);
#ifdef __linux__
				linuxOvercommitMemoryWarning();
#endif
				start = time(NULL);
				if (server.appendonly) {
								if (appendOnlyLoad(server.appendfilename) == SHM_OK)
												shmLog(SHM_NOTICE, "DB loaded from append only file: %ld seconds",
												       time(NULL) - start);
				} else {
								if (sdbLoad(server.dbfilename) == SHM_OK)
												shmLog(SHM_NOTICE, "DB loaded from disk: %ld seconds",
												       time(NULL) - start);
				}
				if (server.ipfd > 0)
								shmLog(SHM_NOTICE,
								       "The server is now ready to accept connections on port %d",
								       server.port);
				if (server.sofd > 0)
								shmLog(SHM_NOTICE, "The server is now ready to accept connections at %s",
								       server.socketpath);
				elSetBeforeSleepProc(server.el, beforeSleep);
				elMain(server.el);
				elDeleteLoop(server.el);
				return 0;
}
#ifdef HAVE_BACKTRACE

static void *getMcontextEip(ucontext_t *uc) {
#if defined(__FreeBSD__)
				return (void *)uc->uc_mcontext.mc_eip;
#elif defined(__dietlibc__)
				return (void *)uc->uc_mcontext.eip;
#elif defined(__APPLE__) && !defined(MAC_OS_X_VERSION_10_6)
#if __x86_64__
				return (void *)uc->uc_mcontext->__ss.__rip;
#else
				return (void *)uc->uc_mcontext->__ss.__eip;
#endif
#elif defined(__APPLE__) && defined(MAC_OS_X_VERSION_10_6)
#if defined(_STRUCT_X86_THREAD_STATE64) && !defined(__i386__)
				return (void *)uc->uc_mcontext->__ss.__rip;
#else
				return (void *)uc->uc_mcontext->__ss.__eip;
#endif
#elif defined(__i386__)
				return (void *)uc->uc_mcontext.gregs[14];
#elif defined(__X86_64__) || defined(__x86_64__)
				return (void *)uc->uc_mcontext.gregs[16];
#elif defined(__ia64__)
				return (void *)uc->uc_mcontext.sc_ip;
#else
				return NULL;
#endif
}

static void sigsegvHandler(int sig, siginfo_t *info, void *secret) {
				void *trace[100];
				char **messages = NULL;
				int i, trace_size = 0;
				ucontext_t *uc = (ucontext_t *)secret;
				dstr infostring;
				struct sigaction act;
				SHM_NOTUSED(info);
				shmLog(SHM_WARNING,
				       "======= Ooops! Shm %s got signal: -%d- =======", SHM_VERSION, sig);
				infostring = genShmInfoString();
				shmLog(SHM_WARNING, "%s", infostring);
				trace_size = backtrace(trace, 100);
				if (getMcontextEip(uc) != NULL) {
								trace[1] = getMcontextEip(uc);
				}
				messages = backtrace_symbols(trace, trace_size);
				for (i = 1; i < trace_size; ++i) shmLog(SHM_WARNING, "%s", messages[i]);
				if (server.daemonize) unlink(server.pidfile);
				sigemptyset(&act.sa_mask);
				act.sa_flags = SA_NODEFER | SA_ONSTACK | SA_RESETHAND;
				act.sa_handler = SIG_DFL;
				sigaction(sig, &act, NULL);
				kill(getpid(), sig);
}
#endif

static void sigtermHandler(int sig) {
				SHM_NOTUSED(sig);
				shmLog(SHM_WARNING, "Received SIGTERM, scheduling shutdown...");
				server.shutdown_asap = 1;
}

void setupSignalHandlers(void) {
				struct sigaction act;
				sigemptyset(&act.sa_mask);
				act.sa_flags = SA_NODEFER | SA_ONSTACK | SA_RESETHAND;
				act.sa_handler = sigtermHandler;
				sigaction(SIGTERM, &act, NULL);
#ifdef HAVE_BACKTRACE
				sigemptyset(&act.sa_mask);
				act.sa_flags = SA_NODEFER | SA_ONSTACK | SA_RESETHAND | SA_SIGINFO;
				act.sa_sigaction = sigsegvHandler;
				sigaction(SIGSEGV, &act, NULL);
				sigaction(SIGBUS, &act, NULL);
				sigaction(SIGFPE, &act, NULL);
				sigaction(SIGILL, &act, NULL);
#endif
				return;
}
