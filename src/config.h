#ifndef __CONFIG_H
#define __CONFIG_H
#ifdef __APPLE__
#include <AvailabilityMacros.h>
#endif
#if defined(USE_TCMALLOC)
#include <google/tcmalloc.h>
#if TC_VERSION_MAJOR >= 1 && TC_VERSION_MINOR >= 6
#define HAVE_MALLOC_SIZE 1
#define shm_malloc_size(p) tc_malloc_size(p)
#endif
#elif defined(__APPLE__)
#include <malloc/malloc.h>
#define HAVE_MALLOC_SIZE 1
#define shm_malloc_size(p) malloc_size(p)
#endif
#if defined(__APPLE__) && !defined(MAC_OS_X_VERSION_10_6)
#define shm_fstat fstat64
#define shm_stat stat64
#else
#define shm_fstat fstat
#define shm_stat stat
#endif
#ifdef __linux__
#define HAVE_PROCFS 1
#endif
#if defined(__APPLE__)
#define HAVE_TASKINFO 1
#endif
#if defined(__APPLE__) || defined(__linux__)
#define HAVE_BACKTRACE 1
#endif
#ifdef __linux__
#define HAVE_EPOLL 1
#endif
#if (defined(__APPLE__) && defined(MAC_OS_X_VERSION_10_6)) || \
        defined(__FreeBSD__) || defined(__OpenBSD__) || defined(__NetBSD__)
#define HAVE_KQUEUE 1
#endif
#ifdef __linux__
#define appendonly_fsync fdatasync
#else
#define appendonly_fsync fsync
#endif
#endif
