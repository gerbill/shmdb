#define SORTLIST_HEAD 0
#define SORTLIST_TAIL 1

unsigned char *sortlistNew(void);

unsigned char *sortlistPush(unsigned char *zl, unsigned char *s,
                           unsigned int slen, int where);

unsigned char *sortlistIndex(unsigned char *zl, int index);

unsigned char *sortlistNext(unsigned char *zl, unsigned char *p);

unsigned char *sortlistPrev(unsigned char *zl, unsigned char *p);

unsigned int sortlistGet(unsigned char *p, unsigned char **sval,
                        unsigned int *slen, long long *lval);

unsigned char *sortlistInsert(unsigned char *zl, unsigned char *p,
                             unsigned char *s, unsigned int slen);

unsigned char *sortlistDelete(unsigned char *zl, unsigned char **p);

unsigned char *sortlistDeleteRange(unsigned char *zl, unsigned int index,
                                  unsigned int num);

unsigned int sortlistCompare(unsigned char *p, unsigned char *s,
                            unsigned int slen);

unsigned int sortlistLen(unsigned char *zl);

unsigned int sortlistSize(unsigned char *zl);
