#include "shm.h"

void listTypeTryConversion(robj *subject, robj *value) {
				if (subject->encoding != SHM_ENCODING_SORTLIST) return;
				if (value->encoding == SHM_ENCODING_RAW &&
				    dstrlen(value->ptr) > server.list_max_sortlist_value)
								listTypeConvert(subject, SHM_ENCODING_LINKEDLIST);
}

void listTypePush(robj *subject, robj *value, int where) {
				listTypeTryConversion(subject, value);
				if (subject->encoding == SHM_ENCODING_SORTLIST &&
				    sortlistLen(subject->ptr) >= server.list_max_sortlist_entries)
								listTypeConvert(subject, SHM_ENCODING_LINKEDLIST);
				if (subject->encoding == SHM_ENCODING_SORTLIST) {
								int pos = (where == SHM_HEAD) ? SORTLIST_HEAD : SORTLIST_TAIL;
								value = objGetDecoded(value);
								subject->ptr =
												sortlistPush(subject->ptr, value->ptr, dstrlen(value->ptr), pos);
								objDecRefCount(value);
				} else if (subject->encoding == SHM_ENCODING_LINKEDLIST) {
								if (where == SHM_HEAD) {
									lAddNodeHead(subject->ptr, value);
								} else {
									lAddNodeTail(subject->ptr, value);
								}
								objIncRefCount(value);
				} else {
								shmPanic("Unknown list encoding");
				}
}

robj *listTypePop(robj *subject, int where) {
				robj *value = NULL;
				if (subject->encoding == SHM_ENCODING_SORTLIST) {
								unsigned char *p;
								unsigned char *vstr;
								unsigned int vlen;
								long long vlong;
								int pos = (where == SHM_HEAD) ? 0 : -1;
								p = sortlistIndex(subject->ptr, pos);
								if (sortlistGet(p, &vstr, &vlen, &vlong)) {
												if (vstr) {
																value = objCreateString((char *)vstr, vlen);
												} else {
																value = objCreateStringFromLongLong(vlong);
												}
												subject->ptr = sortlistDelete(subject->ptr, &p);
								}
				} else if (subject->encoding == SHM_ENCODING_LINKEDLIST) {
								list *list = subject->ptr;
								lNode *ln;
								if (where == SHM_HEAD) {
												ln = lFirst(list);
								} else {
												ln = lLast(list);
								}
								if (ln != NULL) {
												value = lNodeValue(ln);
												objIncRefCount(value);
									lDelNode(list, ln);
								}
				} else {
								shmPanic("Unknown list encoding");
				}
				return value;
}

unsigned long listTypeLength(robj *subject) {
				if (subject->encoding == SHM_ENCODING_SORTLIST) {
								return sortlistLen(subject->ptr);
				} else if (subject->encoding == SHM_ENCODING_LINKEDLIST) {
								return lLength((list *)subject->ptr);
				} else {
								shmPanic("Unknown list encoding");
				}
}

listTypeIterator *listTypeInitIterator(robj *subject, int index,
                                       unsigned char direction) {
				listTypeIterator *li = dbmalloc(sizeof(listTypeIterator));
				li->subject = subject;
				li->encoding = subject->encoding;
				li->direction = direction;
				if (li->encoding == SHM_ENCODING_SORTLIST) {
								li->zi = sortlistIndex(subject->ptr, index);
				} else if (li->encoding == SHM_ENCODING_LINKEDLIST) {
								li->ln = lIndex(subject->ptr, index);
				} else {
								shmPanic("Unknown list encoding");
				}
				return li;
}

void listTypeReleaseIterator(listTypeIterator *li) {
				dbfree(li);
}

int listTypeNext(listTypeIterator *li, listTypeEntry *entry) {
				shmAssert(li->subject->encoding == li->encoding);
				entry->li = li;
				if (li->encoding == SHM_ENCODING_SORTLIST) {
								entry->zi = li->zi;
								if (entry->zi != NULL) {
												if (li->direction == SHM_TAIL)
																li->zi = sortlistNext(li->subject->ptr, li->zi);
												else
																li->zi = sortlistPrev(li->subject->ptr, li->zi);
												return 1;
								}
				} else if (li->encoding == SHM_ENCODING_LINKEDLIST) {
								entry->ln = li->ln;
								if (entry->ln != NULL) {
												if (li->direction == SHM_TAIL)
																li->ln = li->ln->next;
												else
																li->ln = li->ln->prev;
												return 1;
								}
				} else {
								shmPanic("Unknown list encoding");
				}
				return 0;
}

robj *listTypeGet(listTypeEntry *entry) {
				listTypeIterator *li = entry->li;
				robj *value = NULL;
				if (li->encoding == SHM_ENCODING_SORTLIST) {
								unsigned char *vstr;
								unsigned int vlen;
								long long vlong;
								shmAssert(entry->zi != NULL);
								if (sortlistGet(entry->zi, &vstr, &vlen, &vlong)) {
												if (vstr) {
																value = objCreateString((char *)vstr, vlen);
												} else {
																value = objCreateStringFromLongLong(vlong);
												}
								}
				} else if (li->encoding == SHM_ENCODING_LINKEDLIST) {
								shmAssert(entry->ln != NULL);
								value = lNodeValue(entry->ln);
								objIncRefCount(value);
				} else {
								shmPanic("Unknown list encoding");
				}
				return value;
}

void listTypeInsert(listTypeEntry *entry, robj *value, int where) {
				robj *subject = entry->li->subject;
				if (entry->li->encoding == SHM_ENCODING_SORTLIST) {
								value = objGetDecoded(value);
								if (where == SHM_TAIL) {
												unsigned char *next = sortlistNext(subject->ptr, entry->zi);
												if (next == NULL) {
																subject->ptr =
																				sortlistPush(subject->ptr, value->ptr, dstrlen(value->ptr), SHM_TAIL);
												} else {
																subject->ptr =
																				sortlistInsert(subject->ptr, next, value->ptr, dstrlen(value->ptr));
												}
								} else {
												subject->ptr = sortlistInsert(subject->ptr, entry->zi, value->ptr,
												                             dstrlen(value->ptr));
								}
								objDecRefCount(value);
				} else if (entry->li->encoding == SHM_ENCODING_LINKEDLIST) {
								if (where == SHM_TAIL) {
									lInsertNode(subject->ptr, entry->ln, value, L_START_TAIL);
								} else {
									lInsertNode(subject->ptr, entry->ln, value, L_START_HEAD);
								}
								objIncRefCount(value);
				} else {
								shmPanic("Unknown list encoding");
				}
}

int listTypeEqual(listTypeEntry *entry, robj *o) {
				listTypeIterator *li = entry->li;
				if (li->encoding == SHM_ENCODING_SORTLIST) {
								shmAssert(o->encoding == SHM_ENCODING_RAW);
								return sortlistCompare(entry->zi, o->ptr, dstrlen(o->ptr));
				} else if (li->encoding == SHM_ENCODING_LINKEDLIST) {
								return objEqualString(o, lNodeValue(entry->ln));
				} else {
								shmPanic("Unknown list encoding");
				}
}

void listTypeDelete(listTypeEntry *entry) {
				listTypeIterator *li = entry->li;
				if (li->encoding == SHM_ENCODING_SORTLIST) {
								unsigned char *p = entry->zi;
								li->subject->ptr = sortlistDelete(li->subject->ptr, &p);
								if (li->direction == SHM_TAIL)
												li->zi = p;
								else
												li->zi = sortlistPrev(li->subject->ptr, p);
				} else if (entry->li->encoding == SHM_ENCODING_LINKEDLIST) {
								lNode *next;
								if (li->direction == SHM_TAIL)
												next = entry->ln->next;
								else
												next = entry->ln->prev;
					lDelNode(li->subject->ptr, entry->ln);
								li->ln = next;
				} else {
								shmPanic("Unknown list encoding");
				}
}

void listTypeConvert(robj *subject, int enc) {
				listTypeIterator *li;
				listTypeEntry entry;
				shmAssert(subject->type == SHM_LIST);
				if (enc == SHM_ENCODING_LINKEDLIST) {
								list *l = lCreate();
								lSetFreeMethod(l, objDecRefCount);
								li = listTypeInitIterator(subject, 0, SHM_TAIL);
								while (listTypeNext(li, &entry)) lAddNodeTail(l, listTypeGet(&entry));
								listTypeReleaseIterator(li);
								subject->encoding = SHM_ENCODING_LINKEDLIST;
								dbfree(subject->ptr);
								subject->ptr = l;
				} else {
								shmPanic("Unsupported list conversion");
				}
}

void pushGenericCmd(shmClient *c, int where) {
				robj *lobj = lookupKeyWrite(c->db, c->argv[1]);
				c->argv[2] = objTryEncoding(c->argv[2]);
				if (lobj == NULL) {
								if (handleClientsWaitingListPush(c, c->argv[1], c->argv[2])) {
												netAdd(c, shared.cone);
												return;
								}
								lobj = objCreateSortlIst();
								dbAdd(c->db, c->argv[1], lobj);
				} else {
								if (lobj->type != SHM_LIST) {
												netAdd(c, shared.wrongtypeerr);
												return;
								}
								if (handleClientsWaitingListPush(c, c->argv[1], c->argv[2])) {
												touchWatchedKey(c->db, c->argv[1]);
												netAdd(c, shared.cone);
												return;
								}
				}
				listTypePush(lobj, c->argv[2], where);
				netAddLongLong(c, listTypeLength(lobj));
				touchWatchedKey(c->db, c->argv[1]);
				server.dirty++;
}

void listpushCmd(shmClient *c) {
				pushGenericCmd(c, SHM_HEAD);
}

void multipushCmd(shmClient *c) {
				pushGenericCmd(c, SHM_TAIL);
}

void pushxGenericCmd(shmClient *c, robj *refval, robj *val, int where) {
				robj *subject;
				listTypeIterator *iter;
				listTypeEntry entry;
				int inserted = 0;
				if ((subject = lookupKeyReadOrReply(c, c->argv[1], shared.czero)) == NULL ||
				    objCheckType(c, subject, SHM_LIST))
								return;
				if (refval != NULL) {
								shmAssert(refval->encoding == SHM_ENCODING_RAW);
								listTypeTryConversion(subject, val);
								iter = listTypeInitIterator(subject, 0, SHM_TAIL);
								while (listTypeNext(iter, &entry)) {
												if (listTypeEqual(&entry, refval)) {
																listTypeInsert(&entry, val, where);
																inserted = 1;
																break;
												}
								}
								listTypeReleaseIterator(iter);
								if (inserted) {
												if (subject->encoding == SHM_ENCODING_SORTLIST &&
												    sortlistLen(subject->ptr) > server.list_max_sortlist_entries)
																listTypeConvert(subject, SHM_ENCODING_LINKEDLIST);
												touchWatchedKey(c->db, c->argv[1]);
												server.dirty++;
								} else {
												netAdd(c, shared.cnegone);
												return;
								}
				} else {
								listTypePush(subject, val, where);
								touchWatchedKey(c->db, c->argv[1]);
								server.dirty++;
				}
				netAddLongLong(c, listTypeLength(subject));
}

void listpusheCmd(shmClient *c) {
				c->argv[2] = objTryEncoding(c->argv[2]);
				pushxGenericCmd(c, NULL, c->argv[2], SHM_HEAD);
}

void multipusheCmd(shmClient *c) {
				c->argv[2] = objTryEncoding(c->argv[2]);
				pushxGenericCmd(c, NULL, c->argv[2], SHM_TAIL);
}

void insleindCmd(shmClient *c) {
				c->argv[4] = objTryEncoding(c->argv[4]);
				if (strcasecmp(c->argv[2]->ptr, "after") == 0) {
								pushxGenericCmd(c, c->argv[3], c->argv[4], SHM_TAIL);
				} else if (strcasecmp(c->argv[2]->ptr, "before") == 0) {
								pushxGenericCmd(c, c->argv[3], c->argv[4], SHM_HEAD);
				} else {
								netAdd(c, shared.syntaxerr);
				}
}

void listlenCmd(shmClient *c) {
				robj *o = lookupKeyReadOrReply(c, c->argv[1], shared.czero);
				if (o == NULL || objCheckType(c, o, SHM_LIST)) return;
				netAddLongLong(c, listTypeLength(o));
}

void getleindCmd(shmClient *c) {
				robj *o = lookupKeyReadOrReply(c, c->argv[1], shared.nullbulk);
				if (o == NULL || objCheckType(c, o, SHM_LIST)) return;
				int index = atoi(c->argv[2]->ptr);
				robj *value = NULL;
				if (o->encoding == SHM_ENCODING_SORTLIST) {
								unsigned char *p;
								unsigned char *vstr;
								unsigned int vlen;
								long long vlong;
								p = sortlistIndex(o->ptr, index);
								if (sortlistGet(p, &vstr, &vlen, &vlong)) {
												if (vstr) {
																value = objCreateString((char *)vstr, vlen);
												} else {
																value = objCreateStringFromLongLong(vlong);
												}
												netAddBulk(c, value);
												objDecRefCount(value);
								} else {
												netAdd(c, shared.nullbulk);
								}
				} else if (o->encoding == SHM_ENCODING_LINKEDLIST) {
								lNode *ln = lIndex(o->ptr, index);
								if (ln != NULL) {
												value = lNodeValue(ln);
												netAddBulk(c, value);
								} else {
												netAdd(c, shared.nullbulk);
								}
				} else {
								shmPanic("Unknown list encoding");
				}
}

void listsetCmd(shmClient *c) {
				robj *o = lookupKeyWriteOrReply(c, c->argv[1], shared.nokeyerr);
				if (o == NULL || objCheckType(c, o, SHM_LIST)) return;
				int index = atoi(c->argv[2]->ptr);
				robj *value = (c->argv[3] = objTryEncoding(c->argv[3]));
				listTypeTryConversion(o, value);
				if (o->encoding == SHM_ENCODING_SORTLIST) {
								unsigned char *p, *zl = o->ptr;
								p = sortlistIndex(zl, index);
								if (p == NULL) {
												netAdd(c, shared.outofrangeerr);
								} else {
												o->ptr = sortlistDelete(o->ptr, &p);
												value = objGetDecoded(value);
												o->ptr = sortlistInsert(o->ptr, p, value->ptr, dstrlen(value->ptr));
												objDecRefCount(value);
												netAdd(c, shared.ok);
												touchWatchedKey(c->db, c->argv[1]);
												server.dirty++;
								}
				} else if (o->encoding == SHM_ENCODING_LINKEDLIST) {
								lNode *ln = lIndex(o->ptr, index);
								if (ln == NULL) {
												netAdd(c, shared.outofrangeerr);
								} else {
												objDecRefCount((robj *)lNodeValue(ln));
												lNodeValue(ln) = value;
												objIncRefCount(value);
												netAdd(c, shared.ok);
												touchWatchedKey(c->db, c->argv[1]);
												server.dirty++;
								}
				} else {
								shmPanic("Unknown list encoding");
				}
}

void popGenericCmd(shmClient *c, int where) {
				robj *o = lookupKeyWriteOrReply(c, c->argv[1], shared.nullbulk);
				if (o == NULL || objCheckType(c, o, SHM_LIST)) return;
				robj *value = listTypePop(o, where);
				if (value == NULL) {
								netAdd(c, shared.nullbulk);
				} else {
								netAddBulk(c, value);
								objDecRefCount(value);
								if (listTypeLength(o) == 0) dbDelete(c->db, c->argv[1]);
								touchWatchedKey(c->db, c->argv[1]);
								server.dirty++;
				}
}

void listpopCmd(shmClient *c) {
				popGenericCmd(c, SHM_HEAD);
}

void rempopCmd(shmClient *c) {
				popGenericCmd(c, SHM_TAIL);
}

void listrangeCmd(shmClient *c) {
				robj *o;
				int start = atoi(c->argv[2]->ptr);
				int end = atoi(c->argv[3]->ptr);
				int llen;
				int rangelen;
				if ((o = lookupKeyReadOrReply(c, c->argv[1], shared.emptybeginbulk)) ==
				    NULL ||
				    objCheckType(c, o, SHM_LIST))
								return;
				llen = listTypeLength(o);
				if (start < 0) start = llen + start;
				if (end < 0) end = llen + end;
				if (start < 0) start = 0;
				if (start > end || start >= llen) {
								netAdd(c, shared.emptybeginbulk);
								return;
				}
				if (end >= llen) end = llen - 1;
				rangelen = (end - start) + 1;
				netAddBeginBulkLen(c, rangelen);
				if (o->encoding == SHM_ENCODING_SORTLIST) {
								unsigned char *p = sortlistIndex(o->ptr, start);
								unsigned char *vstr;
								unsigned int vlen;
								long long vlong;
								while (rangelen--) {
												sortlistGet(p, &vstr, &vlen, &vlong);
												if (vstr) {
																netAddBulkCBuffer(c, vstr, vlen);
												} else {
																netAddBulkLongLong(c, vlong);
												}
												p = sortlistNext(o->ptr, p);
								}
				} else if (o->encoding == SHM_ENCODING_LINKEDLIST) {
								lNode *ln;
								if (start > llen / 2) start -= llen;
								ln = lIndex(o->ptr, start);
								while (rangelen--) {
												netAddBulk(c, ln->value);
												ln = ln->next;
								}
				} else {
								shmPanic("List encoding is not LINKEDLIST nor SORTLIST!");
				}
}

void listtrimCmd(shmClient *c) {
				robj *o;
				int start = atoi(c->argv[2]->ptr);
				int end = atoi(c->argv[3]->ptr);
				int llen;
				int j, ltrim, rtrim;
				list *list;
				lNode *ln;
				if ((o = lookupKeyWriteOrReply(c, c->argv[1], shared.ok)) == NULL ||
				    objCheckType(c, o, SHM_LIST))
								return;
				llen = listTypeLength(o);
				if (start < 0) start = llen + start;
				if (end < 0) end = llen + end;
				if (start < 0) start = 0;
				if (start > end || start >= llen) {
								ltrim = llen;
								rtrim = 0;
				} else {
								if (end >= llen) end = llen - 1;
								ltrim = start;
								rtrim = llen - end - 1;
				}
				if (o->encoding == SHM_ENCODING_SORTLIST) {
								o->ptr = sortlistDeleteRange(o->ptr, 0, ltrim);
								o->ptr = sortlistDeleteRange(o->ptr, -rtrim, rtrim);
				} else if (o->encoding == SHM_ENCODING_LINKEDLIST) {
								list = o->ptr;
								for (j = 0; j < ltrim; j++) {
												ln = lFirst(list);
									lDelNode(list, ln);
								}
								for (j = 0; j < rtrim; j++) {
												ln = lLast(list);
									lDelNode(list, ln);
								}
				} else {
								shmPanic("Unknown list encoding");
				}
				if (listTypeLength(o) == 0) dbDelete(c->db, c->argv[1]);
				touchWatchedKey(c->db, c->argv[1]);
				server.dirty++;
				netAdd(c, shared.ok);
}

void listremCmd(shmClient *c) {
				robj *subject, *obj;
				obj = c->argv[3] = objTryEncoding(c->argv[3]);
				int toremove = atoi(c->argv[2]->ptr);
				int removed = 0;
				listTypeEntry entry;
				subject = lookupKeyWriteOrReply(c, c->argv[1], shared.czero);
				if (subject == NULL || objCheckType(c, subject, SHM_LIST)) return;
				if (subject->encoding == SHM_ENCODING_SORTLIST) obj = objGetDecoded(obj);
				listTypeIterator *li;
				if (toremove < 0) {
								toremove = -toremove;
								li = listTypeInitIterator(subject, -1, SHM_HEAD);
				} else {
								li = listTypeInitIterator(subject, 0, SHM_TAIL);
				}
				while (listTypeNext(li, &entry)) {
								if (listTypeEqual(&entry, obj)) {
												listTypeDelete(&entry);
												server.dirty++;
												removed++;
												if (toremove && removed == toremove) break;
								}
				}
				listTypeReleaseIterator(li);
				if (subject->encoding == SHM_ENCODING_SORTLIST) objDecRefCount(obj);
				if (listTypeLength(subject) == 0) dbDelete(c->db, c->argv[1]);
				netAddLongLong(c, removed);
				if (removed) touchWatchedKey(c->db, c->argv[1]);
}

void rpoplpushHandlePush(shmClient *origclient, shmClient *c, robj *dstkey,
                         robj *dstobj, robj *value) {
				robj *aux;
				if (!handleClientsWaitingListPush(origclient, dstkey, value)) {
								if (!dstobj) {
												dstobj = objCreateSortlIst();
												dbAdd(c->db, dstkey, dstobj);
								} else {
												touchWatchedKey(c->db, dstkey);
								}
								listTypePush(dstobj, value, SHM_HEAD);
								if (c != origclient) {
												aux = objCreateString("LPUSH", 5);
												netRewriteClientCommandVector(origclient, 3, aux, dstkey, value);
												objDecRefCount(aux);
								} else {
												aux = objCreateString("RPOPLPUSH", 9);
												netRewriteClientCommandVector(origclient, 3, aux, c->argv[1], c->argv[2]);
												objDecRefCount(aux);
								}
								server.dirty++;
				}
				netAddBulk(c, value);
}

void rpoplistpushCmd(shmClient *c) {
				robj *sobj, *value;
				if ((sobj = lookupKeyWriteOrReply(c, c->argv[1], shared.nullbulk)) == NULL ||
				    objCheckType(c, sobj, SHM_LIST))
								return;
				if (listTypeLength(sobj) == 0) {
								netAdd(c, shared.nullbulk);
				} else {
								robj *dobj = lookupKeyWrite(c->db, c->argv[2]);
								robj *touchedkey = c->argv[1];
								if (dobj && objCheckType(c, dobj, SHM_LIST)) return;
								value = listTypePop(sobj, SHM_TAIL);
								objIncRefCount(touchedkey);
								rpoplpushHandlePush(c, c, c->argv[2], dobj, value);
								objDecRefCount(value);
								if (listTypeLength(sobj) == 0) dbDelete(c->db, touchedkey);
								touchWatchedKey(c->db, touchedkey);
								objDecRefCount(touchedkey);
								server.dirty++;
				}
}

void blockForKeys(shmClient *c, robj **keys, int numkeys, time_t timeout,
                  robj *target) {
				hashEntry *de;
				list *l;
				int j;
				c->bpop.keys = dbmalloc(sizeof(robj *) * numkeys);
				c->bpop.count = numkeys;
				c->bpop.timeout = timeout;
				c->bpop.target = target;
				if (target != NULL) {
								objIncRefCount(target);
				}
				for (j = 0; j < numkeys; j++) {
								c->bpop.keys[j] = keys[j];
								objIncRefCount(keys[j]);
								de = hashFind(c->db->blocking_keys, keys[j]);
								if (de == NULL) {
												int retval;
												l = lCreate();
												retval = hashAdd(c->db->blocking_keys, keys[j], l);
												objIncRefCount(keys[j]);
												shmAssert(retval == HASH_OK);
								} else {
												l = hashGetEntryVal(de);
								}
					lAddNodeTail(l, c);
				}
				c->flags |= SHM_BLOCKED;
				server.bpop_blocked_clients++;
}

void unblockClientWaitingData(shmClient *c) {
				hashEntry *de;
				list *l;
				int j;
				shmAssert(c->bpop.keys != NULL);
				for (j = 0; j < c->bpop.count; j++) {
								de = hashFind(c->db->blocking_keys, c->bpop.keys[j]);
								shmAssert(de != NULL);
								l = hashGetEntryVal(de);
					lDelNode(l, lSearchKey(l, c));
								if (lLength(l) == 0) hashDelete(c->db->blocking_keys, c->bpop.keys[j]);
								objDecRefCount(c->bpop.keys[j]);
				}
				dbfree(c->bpop.keys);
				c->bpop.keys = NULL;
				if (c->bpop.target) objDecRefCount(c->bpop.target);
				c->bpop.target = NULL;
				c->flags &= ~SHM_BLOCKED;
				c->flags |= SHM_UNBLOCKED;
				server.bpop_blocked_clients--;
	lAddNodeTail(server.unblocked_clients, c);
}

int handleClientsWaitingListPush(shmClient *c, robj *key, robj *ele) {
				struct hashEntry *de;
				shmClient *receiver;
				int numclients;
				list *clients;
				lNode *ln;
				robj *dstkey, *dstobj;
				de = hashFind(c->db->blocking_keys, key);
				if (de == NULL) return 0;
				clients = hashGetEntryVal(de);
				numclients = lLength(clients);
				while (numclients--) {
								ln = lFirst(clients);
								shmAssert(ln != NULL);
								receiver = ln->value;
								dstkey = receiver->bpop.target;
								if (dstkey) objIncRefCount(dstkey);
								unblockClientWaitingData(receiver);
								if (dstkey == NULL) {
												netAddBeginBulkLen(receiver, 2);
												netAddBulk(receiver, key);
												netAddBulk(receiver, ele);
												return 1;
								} else {
												dstobj = lookupKeyWrite(receiver->db, dstkey);
												if (!(dstobj && objCheckType(receiver, dstobj, SHM_LIST))) {
																rpoplpushHandlePush(c, receiver, dstkey, dstobj, ele);
																objDecRefCount(dstkey);
																return 1;
												}
												objDecRefCount(dstkey);
								}
				}
				return 0;
}

int getTimeoutFromObjectOrReply(shmClient *c, robj *object, time_t *timeout) {
				long tval;
				if (objGetLongFromOrReply(c, object, &tval,
				                             "timeout is not an integer or out of range") !=
				    SHM_OK)
								return SHM_ERR;
				if (tval < 0) {
								netAddError(c, "timeout is negative");
								return SHM_ERR;
				}
				if (tval > 0) tval += time(NULL);
				*timeout = tval;
				return SHM_OK;
}

void blockingPopGenericCmd(shmClient *c, int where) {
				robj *o;
				time_t timeout;
				int j;
				if (getTimeoutFromObjectOrReply(c, c->argv[c->argc - 1], &timeout) != SHM_OK)
								return;
				for (j = 1; j < c->argc - 1; j++) {
								o = lookupKeyWrite(c->db, c->argv[j]);
								if (o != NULL) {
												if (o->type != SHM_LIST) {
																netAdd(c, shared.wrongtypeerr);
																return;
												} else {
																if (listTypeLength(o) != 0) {
																				struct cmdShm *orig_cmd;
																				robj *argv[2], **orig_argv;
																				int orig_argc;
																				orig_argv = c->argv;
																				orig_argc = c->argc;
																				orig_cmd = c->cmd;
																				argv[1] = c->argv[j];
																				c->argv = argv;
																				c->argc = 2;
																				netAddBeginBulkLen(c, 2);
																				netAddBulk(c, argv[1]);
																				popGenericCmd(c, where);
																				c->argv = orig_argv;
																				c->argc = orig_argc;
																				c->cmd = orig_cmd;
																				return;
																}
												}
								}
				}
				if (c->flags & SHM_BEGIN) {
								netAdd(c, shared.nullbeginbulk);
								return;
				}
				blockForKeys(c, c->argv + 1, c->argc - 2, timeout, NULL);
}

void bpopCmd(shmClient *c) {
				blockingPopGenericCmd(c, SHM_HEAD);
}

void brempopCmd(shmClient *c) {
				blockingPopGenericCmd(c, SHM_TAIL);
}

void brempoplistpushCmd(shmClient *c) {
				time_t timeout;
				if (getTimeoutFromObjectOrReply(c, c->argv[3], &timeout) != SHM_OK) return;
				robj *key = lookupKeyWrite(c->db, c->argv[1]);
				if (key == NULL) {
								if (c->flags & SHM_BEGIN) {
												netAdd(c, shared.nullbulk);
								} else {
												blockForKeys(c, c->argv + 1, 1, timeout, c->argv[2]);
								}
				} else {
								if (key->type != SHM_LIST) {
												netAdd(c, shared.wrongtypeerr);
								} else {
												shmAssert(listTypeLength(key) > 0);
												rpoplistpushCmd(c);
								}
				}
}
