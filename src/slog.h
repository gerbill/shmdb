typedef struct slogEntry {
				robj **argv;
				int argc;
				long long id;
				long long duration;
				time_t time;
} slogEntry;

void slogInit(void);

void slogPushEntryIfNeeded(robj **argv, int argc, long long duration);

void slogCmd(shmClient *c);
